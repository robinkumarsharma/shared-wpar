static __inline unsigned int __bswap_32(unsigned int __bsx)
{
  return __builtin_bswap32(__bsx);
}
typedef unsigned long int __uint64_t;
static __inline __uint64_t __bswap_64(__uint64_t __bsx)
{
  return __builtin_bswap64(__bsx);
}
extern long int strtol(const char *__restrict __nptr, char **__restrict __endptr, int __base) __attribute__((__nothrow__)) __attribute__((__leaf__)) __attribute__((__nonnull__(1)));
extern __inline __attribute__((__nothrow__)) __attribute__((__leaf__)) __attribute__((__pure__)) __attribute__((__nonnull__(1))) __attribute__((__gnu_inline__)) int atoi(const char *__nptr)
{
  return (int)strtol(__nptr, (char **)(void *)0, 10);
}
extern __inline __attribute__((__nothrow__)) __attribute__((__leaf__)) __attribute__((__pure__)) __attribute__((__nonnull__(1))) __attribute__((__gnu_inline__)) long int atol(const char *__nptr)
{
  return strtol(__nptr, (char **)(void *)0, 10);
}
extern long long int strtoll(const char *__restrict __nptr, char **__restrict __endptr, int __base) __attribute__((__nothrow__)) __attribute__((__leaf__)) __attribute__((__nonnull__(1)));
__extension__ extern __inline __attribute__((__nothrow__)) __attribute__((__leaf__)) __attribute__((__pure__)) __attribute__((__nonnull__(1))) __attribute__((__gnu_inline__)) long long int atoll(const char *__nptr)
{
  return strtoll(__nptr, (char **)(void *)0, 10);
}
__extension__ extern __inline __attribute__((__nothrow__)) __attribute__((__leaf__)) __attribute__((__const__)) __attribute__((__gnu_inline__)) unsigned int gnu_dev_major(unsigned long long int __dev)
{
  return (__dev >> 8 & 4095) | ((unsigned int)(__dev >> 32) & ~4095);
}
__extension__ extern __inline __attribute__((__nothrow__)) __attribute__((__leaf__)) __attribute__((__const__)) __attribute__((__gnu_inline__)) unsigned int gnu_dev_minor(unsigned long long int __dev)
{
  return (__dev & 255) | ((unsigned int)(__dev >> 12) & ~255);
}
__extension__ extern __inline __attribute__((__nothrow__)) __attribute__((__leaf__)) __attribute__((__const__)) __attribute__((__gnu_inline__)) unsigned long long int gnu_dev_makedev(unsigned int __major, unsigned int __minor)
{
  return (((__minor & 255) | (__major & 4095) << 8) | (unsigned long long int)(__minor & ~255) << 12) | (unsigned long long int)(__major & ~4095) << 32;
}
typedef unsigned long int size_t;
typedef int (*__compar_fn_t)(const void *, const void *);
extern __inline __attribute__((__nonnull__(1, 2, 5))) __attribute__((__gnu_inline__)) void *bsearch(const void *__key, const void *__base, size_t __nmemb, size_t __size, __compar_fn_t __compar)
{
  size_t __l;
  size_t __u;
  size_t __idx;
  const void *__p;
  int __comparison;
  __l = 0;
  __u = __nmemb;
  while (__l < __u)
    {
      __idx = (__l + __u) / 2;
      __p = (void *)((const char *)__base + __idx * __size);
      __comparison = (*__compar)(__key, __p);
      if (__comparison < 0)
        {
          __u = __idx;
        }
      else
        {
          if (__comparison > 0)
            {
              __l = __idx + 1;
            }
          else
            {
              return (void *)__p;
            }
        }
    }
  return (void *)0;
}
extern double strtod(const char *__restrict __nptr, char **__restrict __endptr) __attribute__((__nothrow__)) __attribute__((__leaf__)) __attribute__((__nonnull__(1)));
extern __inline __attribute__((__nothrow__)) __attribute__((__leaf__)) __attribute__((__pure__)) __attribute__((__nonnull__(1))) __attribute__((__gnu_inline__)) double atof(const char *__nptr)
{
  return strtod(__nptr, (char **)(void *)0);
}
extern __inline __attribute__((__gnu_inline__)) size_t __strcspn_c1(const char *__s, int __reject)
{
  size_t __result = 0;
  while (__s[__result] != '\000' && __s[__result] != __reject)
    {
       ++__result;
    }
  return __result;
}
extern __inline __attribute__((__gnu_inline__)) size_t __strcspn_c2(const char *__s, int __reject1, int __reject2)
{
  size_t __result = 0;
  while ((__s[__result] != '\000' && __s[__result] != __reject1) && __s[__result] != __reject2)
    {
       ++__result;
    }
  return __result;
}
extern __inline __attribute__((__gnu_inline__)) size_t __strcspn_c3(const char *__s, int __reject1, int __reject2, int __reject3)
{
  size_t __result = 0;
  while (((__s[__result] != '\000' && __s[__result] != __reject1) && __s[__result] != __reject2) && __s[__result] != __reject3)
    {
       ++__result;
    }
  return __result;
}
extern __inline __attribute__((__gnu_inline__)) size_t __strspn_c1(const char *__s, int __accept)
{
  size_t __result = 0;
  while (__s[__result] == __accept)
    {
       ++__result;
    }
  return __result;
}
extern __inline __attribute__((__gnu_inline__)) size_t __strspn_c2(const char *__s, int __accept1, int __accept2)
{
  size_t __result = 0;
  while (__s[__result] == __accept1 || __s[__result] == __accept2)
    {
       ++__result;
    }
  return __result;
}
extern __inline __attribute__((__gnu_inline__)) size_t __strspn_c3(const char *__s, int __accept1, int __accept2, int __accept3)
{
  size_t __result = 0;
  while ((__s[__result] == __accept1 || __s[__result] == __accept2) || __s[__result] == __accept3)
    {
       ++__result;
    }
  return __result;
}
extern __inline __attribute__((__gnu_inline__)) char *__strpbrk_c2(const char *__s, int __accept1, int __accept2)
{
  while ((*__s != '\000' && *__s != __accept1) && *__s != __accept2)
    {
       ++__s;
    }
  return *__s == '\000' ? (void *)0 : (char *)(size_t)__s;
}
extern __inline __attribute__((__gnu_inline__)) char *__strpbrk_c3(const char *__s, int __accept1, int __accept2, int __accept3)
{
  while (((*__s != '\000' && *__s != __accept1) && *__s != __accept2) && *__s != __accept3)
    {
       ++__s;
    }
  return *__s == '\000' ? (void *)0 : (char *)(size_t)__s;
}
extern __inline __attribute__((__gnu_inline__)) char *__strtok_r_1c(char *__s, char __sep, char **__nextp)
{
  char *__result;
  if (__s == (void *)0)
    {
      __s = *__nextp;
    }
  while (*__s == __sep)
    {
       ++__s;
    }
  __result = (void *)0;
  if (*__s != '\000')
    {
      __result = __s++;
      while (*__s != '\000')
        {
          if (*__s++ == __sep)
            {
              __s[ -1] = '\000';
              break;
            }
        }
    }
  *__nextp = __s;
  return __result;
}
extern void *__rawmemchr(const void *__s, int __c);
extern __inline __attribute__((__gnu_inline__)) char *__strsep_1c(char **__s, char __reject)
{
  char *__retval = *__s;
  if (__retval != (void *)0 && (*__s = (__builtin_constant_p(__reject) && !__builtin_constant_p(__retval)) && __reject == '\000' ? (char *)__rawmemchr(__retval, __reject) : __builtin_strchr(__retval, __reject)) != (void *)0)
    {
      *(*__s)++ = '\000';
    }
  return __retval;
}
extern __inline __attribute__((__gnu_inline__)) char *__strsep_2c(char **__s, char __reject1, char __reject2)
{
  char *__retval = *__s;
  if (__retval != (void *)0)
    {
      char *__cp = __retval;
      while (1)
        {
          if (*__cp == '\000')
            {
              __cp = (void *)0;
              break;
            }
          if (*__cp == __reject1 || *__cp == __reject2)
            {
              *__cp++ = '\000';
              break;
            }
           ++__cp;
        }
      *__s = __cp;
    }
  return __retval;
}
extern __inline __attribute__((__gnu_inline__)) char *__strsep_3c(char **__s, char __reject1, char __reject2, char __reject3)
{
  char *__retval = *__s;
  if (__retval != (void *)0)
    {
      char *__cp = __retval;
      while (1)
        {
          if (*__cp == '\000')
            {
              __cp = (void *)0;
              break;
            }
          if ((*__cp == __reject1 || *__cp == __reject2) || *__cp == __reject3)
            {
              *__cp++ = '\000';
              break;
            }
           ++__cp;
        }
      *__s = __cp;
    }
  return __retval;
}
extern int signgam;
enum mcc_enum_anon_7
{
  _IEEE_ =  -1,
  _SVID_ = 0,
  _XOPEN_ = 1,
  _POSIX_ = 2,
  _ISOC_ = 3
};
typedef enum mcc_enum_anon_7 _LIB_VERSION_TYPE;
extern _LIB_VERSION_TYPE _LIB_VERSION;
extern __inline __attribute__((__nothrow__)) __attribute__((__leaf__)) __attribute__((__const__)) __attribute__((__always_inline__)) __attribute__((__gnu_inline__)) int __signbitf(float __x)
{
  int __m;
  __asm__ ("pmovmskb %1, %0" : "=r"(__m) : "x"(__x));
  return (__m & 8) != 0;
}
extern __inline __attribute__((__nothrow__)) __attribute__((__leaf__)) __attribute__((__const__)) __attribute__((__always_inline__)) __attribute__((__gnu_inline__)) int __signbit(double __x)
{
  int __m;
  __asm__ ("pmovmskb %1, %0" : "=r"(__m) : "x"(__x));
  return (__m & 128) != 0;
}
extern __inline __attribute__((__nothrow__)) __attribute__((__leaf__)) __attribute__((__const__)) __attribute__((__always_inline__)) __attribute__((__gnu_inline__)) int __signbitl(long double __x)
{
  __extension__ union  mcc_union_anon_38
  {
    __extension__ long double __l;
    __extension__ int __i[3L];
  };
  __extension__ union mcc_union_anon_38 __u = {.__l = __x};
  return (__u.__i[2] & 32768) != 0;
}
struct _IO_FILE_plus;
extern struct _IO_FILE_plus _IO_2_1_stdin_;
extern struct _IO_FILE_plus _IO_2_1_stdout_;
extern struct _IO_FILE_plus _IO_2_1_stderr_;
struct _IO_FILE;
extern struct _IO_FILE *stdin;
extern struct _IO_FILE *stdout;
extern struct _IO_FILE *stderr;
extern int sys_nerr;
extern const char *const sys_errlist[];
typedef __builtin_va_list __gnuc_va_list;
typedef struct _IO_FILE FILE;
extern int vfprintf(FILE *__restrict __s, const char *__restrict __format, __gnuc_va_list __arg);
extern struct _IO_FILE *stdout;
extern __inline __attribute__((__gnu_inline__)) int vprintf(const char *__restrict __fmt, __gnuc_va_list __arg)
{
  return vfprintf(stdout, __fmt, __arg);
}
typedef struct _IO_FILE _IO_FILE;
extern int _IO_getc(_IO_FILE *__fp);
extern struct _IO_FILE *stdin;
extern __inline __attribute__((__gnu_inline__)) int getchar(void)
{
  return _IO_getc(stdin);
}
struct _IO_marker;
typedef long int __off_t;
typedef void _IO_lock_t;
typedef long int __off64_t;
struct  _IO_FILE
{
  int _flags;
  char *_IO_read_ptr;
  char *_IO_read_end;
  char *_IO_read_base;
  char *_IO_write_base;
  char *_IO_write_ptr;
  char *_IO_write_end;
  char *_IO_buf_base;
  char *_IO_buf_end;
  char *_IO_save_base;
  char *_IO_backup_base;
  char *_IO_save_end;
  struct _IO_marker *_markers;
  struct _IO_FILE *_chain;
  int _fileno;
  int _flags2;
  __off_t _old_offset;
  unsigned short int _cur_column;
  signed char _vtable_offset;
  char _shortbuf[1L];
  _IO_lock_t *_lock;
  __off64_t _offset;
  void *__pad1;
  void *__pad2;
  void *__pad3;
  void *__pad4;
  size_t __pad5;
  int _mode;
  char _unused2[20L];
};
extern int __uflow(_IO_FILE *);
extern __inline __attribute__((__gnu_inline__)) int fgetc_unlocked(FILE *__fp)
{
  return __builtin_expect((*__fp)._IO_read_ptr >= (*__fp)._IO_read_end, 0) ? __uflow(__fp) : *((unsigned char *)(*__fp)._IO_read_ptr++);
}
extern __inline __attribute__((__gnu_inline__)) int getc_unlocked(FILE *__fp)
{
  return __builtin_expect((*__fp)._IO_read_ptr >= (*__fp)._IO_read_end, 0) ? __uflow(__fp) : *((unsigned char *)(*__fp)._IO_read_ptr++);
}
extern __inline __attribute__((__gnu_inline__)) int getchar_unlocked(void)
{
  return __builtin_expect((*stdin)._IO_read_ptr >= (*stdin)._IO_read_end, 0) ? __uflow(stdin) : *((unsigned char *)(*stdin)._IO_read_ptr++);
}
extern int _IO_putc(int __c, _IO_FILE *__fp);
extern __inline __attribute__((__gnu_inline__)) int putchar(int __c)
{
  return _IO_putc(__c, stdout);
}
extern int __overflow(_IO_FILE *, int);
extern __inline __attribute__((__gnu_inline__)) int fputc_unlocked(int __c, FILE *__stream)
{
  return __builtin_expect((*__stream)._IO_write_ptr >= (*__stream)._IO_write_end, 0) ? __overflow(__stream, (unsigned char)__c) : (unsigned char)(*(*__stream)._IO_write_ptr++ = __c);
}
extern __inline __attribute__((__gnu_inline__)) int putc_unlocked(int __c, FILE *__stream)
{
  return __builtin_expect((*__stream)._IO_write_ptr >= (*__stream)._IO_write_end, 0) ? __overflow(__stream, (unsigned char)__c) : (unsigned char)(*(*__stream)._IO_write_ptr++ = __c);
}
extern __inline __attribute__((__gnu_inline__)) int putchar_unlocked(int __c)
{
  return __builtin_expect((*stdout)._IO_write_ptr >= (*stdout)._IO_write_end, 0) ? __overflow(stdout, (unsigned char)__c) : (unsigned char)(*(*stdout)._IO_write_ptr++ = __c);
}
extern __inline __attribute__((__nothrow__)) __attribute__((__leaf__)) __attribute__((__gnu_inline__)) int feof_unlocked(FILE *__stream)
{
  return ((*__stream)._flags & 16) != 0;
}
extern __inline __attribute__((__nothrow__)) __attribute__((__leaf__)) __attribute__((__gnu_inline__)) int ferror_unlocked(FILE *__stream)
{
  return ((*__stream)._flags & 32) != 0;
}
struct kad_node_t;
typedef struct kad_node_t kad_node_t;
typedef int (*kad_op_f)(kad_node_t *, int);
int kad_op_add(kad_node_t *p, int action);
int kad_op_mul(kad_node_t *p, int action);
int kad_op_cmul(kad_node_t *p, int action);
int kad_op_ce_bin_neg(kad_node_t *p, int action);
int kad_op_square(kad_node_t *p, int action);
int kad_op_sigm(kad_node_t *p, int action);
int kad_op_tanh(kad_node_t *p, int action);
int kad_op_relu(kad_node_t *p, int action);
int kad_op_matmul(kad_node_t *p, int action);
int kad_op_avg(kad_node_t *p, int action);
int kad_op_1minus(kad_node_t *p, int action);
int kad_op_select(kad_node_t *p, int action);
int kad_op_ce_multi(kad_node_t *p, int action);
int kad_op_softmax(kad_node_t *p, int action);
int kad_op_dropout(kad_node_t *p, int action);
int kad_op_conv2d(kad_node_t *p, int action);
int kad_op_max2d(kad_node_t *p, int action);
int kad_op_conv1d(kad_node_t *p, int action);
int kad_op_max1d(kad_node_t *p, int action);
int kad_op_slice(kad_node_t *p, int action);
int kad_op_max(kad_node_t *p, int action);
int kad_op_ce_bin(kad_node_t *p, int action);
int kad_op_sub(kad_node_t *p, int action);
int kad_op_sample_normal(kad_node_t *p, int action);
int kad_op_reduce_sum(kad_node_t *p, int action);
int kad_op_reduce_mean(kad_node_t *p, int action);
int kad_op_log(kad_node_t *p, int action);
int kad_op_avg1d(kad_node_t *p, int action);
int kad_op_mse(kad_node_t *p, int action);
int kad_op_reshape(kad_node_t *p, int action);
int kad_op_concat(kad_node_t *p, int action);
int kad_op_stdnorm(kad_node_t *p, int action);
int kad_op_exp(kad_node_t *p, int action);
int kad_op_sin(kad_node_t *p, int action);
int kad_op_stack(kad_node_t *p, int action);
int kad_op_reverse(kad_node_t *p, int action);
extern kad_op_f kad_op_list[64L];
extern char *kad_op_name[64L];
typedef unsigned char uint8_t;
typedef unsigned short int uint16_t;
typedef int int32_t;
typedef unsigned int uint32_t;
struct  kad_node_t
{
  uint8_t n_d;
  uint8_t flag;
  uint16_t op;
  int32_t n_child;
  int32_t tmp;
  int32_t ptr_size;
  int32_t d[4L];
  int32_t ext_label;
  uint32_t ext_flag;
  float *x;
  float *g;
  void *ptr;
  void *gtmp;
  struct kad_node_t **child;
  struct kad_node_t *pre;
};
static __inline int kad_len(const kad_node_t *p)
{
  int i;
  int n = 1;
  for (i = 0; i < (*p).n_d;  ++i)
    {
      n *= (*p).d[i];
    }
  return n;
}
extern void *calloc(size_t __nmemb, size_t __size) __attribute__((__nothrow__)) __attribute__((__leaf__)) __attribute__((__malloc__));
static __inline kad_node_t *kad_new_core(int n_d, int op, int n_child)
{
  kad_node_t *s;
  if (n_d >= 4)
    {
      return 0;
    }
  s = (kad_node_t *)calloc(1, sizeof(kad_node_t));
  ((((*s).n_d = n_d, (*s).op = op)), (*s).n_child = n_child);
  if ((*s).n_child)
    {
      (*s).child = (kad_node_t **)calloc((*s).n_child, sizeof(kad_node_t *));
    }
  return s;
}
typedef __gnuc_va_list va_list;
static __inline kad_node_t *kad_vleaf(uint8_t flag, float *x, float *g, int n_d, va_list ap)
{
  kad_node_t *p;
  int i;
  if (n_d > 4)
    {
      return 0;
    }
  p = (kad_node_t *)calloc(1, sizeof(kad_node_t));
  (*p).n_d = n_d;
  for (i = 0; i < n_d;  ++i)
    {
      (*p).d[i] = (__builtin_va_arg(ap, int32_t));
    }
  ((((*p).x = x, (*p).g = g)), (*p).flag = flag);
  return p;
}
kad_node_t *kad_const(float *x, int n_d, ...)
{
  va_list ap;
  kad_node_t *p;
  __builtin_va_start(ap, n_d);
  p = kad_vleaf(2, x, 0, n_d, ap);
  __builtin_va_end(ap);
  return p;
}
kad_node_t *kad_feed(int n_d, ...)
{
  va_list ap;
  kad_node_t *p;
  __builtin_va_start(ap, n_d);
  p = kad_vleaf(0, 0, 0, n_d, ap);
  __builtin_va_end(ap);
  return p;
}
kad_node_t *kad_var(float *x, float *g, int n_d, ...)
{
  va_list ap;
  kad_node_t *p;
  __builtin_va_start(ap, n_d);
  p = kad_vleaf(1, x, g, n_d, ap);
  __builtin_va_end(ap);
  return p;
}
kad_op_f kad_op_list[64L] = {[0] = 0, [1] = kad_op_add, [2] = kad_op_mul, [3] = kad_op_cmul, [4] = kad_op_ce_bin_neg, [5] = kad_op_square, [6] = kad_op_sigm, [7] = kad_op_tanh, [8] = kad_op_relu, [9] = kad_op_matmul, [10] = kad_op_avg, [11] = kad_op_1minus, [12] = kad_op_select, [13] = kad_op_ce_multi, [14] = kad_op_softmax, [15] = kad_op_dropout, [16] = kad_op_conv2d, [17] = kad_op_max2d, [18] = kad_op_conv1d, [19] = kad_op_max1d, [20] = kad_op_slice, [21] = kad_op_max, [22] = kad_op_ce_bin, [23] = kad_op_sub, [24] = kad_op_sample_normal, [25] = kad_op_reduce_sum, [26] = kad_op_reduce_mean, [27] = kad_op_log, [28] = kad_op_avg1d, [29] = kad_op_mse, [30] = kad_op_reshape, [31] = kad_op_concat, [32] = kad_op_stdnorm, [33] = kad_op_exp, [34] = kad_op_sin, [35] = kad_op_stack, [36] = kad_op_reverse};
extern void free(void *__ptr) __attribute__((__nothrow__)) __attribute__((__leaf__));
static __inline kad_node_t *kad_finalize_node(kad_node_t *s)
{
  int i;
  if (kad_op_list[(*s).op](s, 4) < 0)
    {
      if ((*s).ptr)
        {
          free((*s).ptr);
        }
      free((*s).child);
      free(s);
      return 0;
    }
  for (i = 0; i < (*s).n_child;  ++i)
    {
      if ((*(*s).child[i]).flag & 1)
        {
          break;
        }
    }
  if (i < (*s).n_child)
    {
      (*s).flag |= 1;
    }
  return s;
}
static __inline kad_node_t *kad_op2_core(int op, kad_node_t *x, kad_node_t *y)
{
  kad_node_t *s;
  s = kad_new_core(0, op, 2);
  ((*s).child[0] = x, (*s).child[1] = y);
  return kad_finalize_node(s);
}
static __inline kad_node_t *kad_op1_core(int op, kad_node_t *x)
{
  kad_node_t *s;
  s = kad_new_core(0, op, 1);
  (*s).child[0] = x;
  return kad_finalize_node(s);
}
kad_node_t *kad_add(kad_node_t *x, kad_node_t *y)
{
  return kad_op2_core(1, x, y);
}
kad_node_t *kad_sub(kad_node_t *x, kad_node_t *y)
{
  return kad_op2_core(23, x, y);
}
kad_node_t *kad_mul(kad_node_t *x, kad_node_t *y)
{
  return kad_op2_core(2, x, y);
}
kad_node_t *kad_cmul(kad_node_t *x, kad_node_t *y)
{
  return kad_op2_core(3, x, y);
}
kad_node_t *kad_matmul(kad_node_t *x, kad_node_t *y)
{
  return kad_op2_core(9, x, y);
}
kad_node_t *kad_ce_multi(kad_node_t *x, kad_node_t *y)
{
  return kad_op2_core(13, x, y);
}
kad_node_t *kad_ce_bin(kad_node_t *x, kad_node_t *y)
{
  return kad_op2_core(22, x, y);
}
kad_node_t *kad_ce_bin_neg(kad_node_t *x, kad_node_t *y)
{
  return kad_op2_core(4, x, y);
}
kad_node_t *kad_mse(kad_node_t *x, kad_node_t *y)
{
  return kad_op2_core(29, x, y);
}
kad_node_t *kad_log(kad_node_t *x)
{
  return kad_op1_core(27, x);
}
kad_node_t *kad_exp(kad_node_t *x)
{
  return kad_op1_core(33, x);
}
kad_node_t *kad_sin(kad_node_t *x)
{
  return kad_op1_core(34, x);
}
kad_node_t *kad_square(kad_node_t *x)
{
  return kad_op1_core(5, x);
}
kad_node_t *kad_sigm(kad_node_t *x)
{
  return kad_op1_core(6, x);
}
kad_node_t *kad_tanh(kad_node_t *x)
{
  return kad_op1_core(7, x);
}
kad_node_t *kad_relu(kad_node_t *x)
{
  return kad_op1_core(8, x);
}
kad_node_t *kad_1minus(kad_node_t *x)
{
  return kad_op1_core(11, x);
}
kad_node_t *kad_softmax(kad_node_t *x)
{
  return kad_op1_core(14, x);
}
kad_node_t *kad_stdnorm(kad_node_t *x)
{
  return kad_op1_core(32, x);
}
kad_node_t *kad_ce_multi_weighted(kad_node_t *pred, kad_node_t *truth, kad_node_t *weight)
{
  kad_node_t *s;
  s = kad_new_core(0, 13, 3);
  ((((*s).child[0] = pred, (*s).child[1] = truth)), (*s).child[2] = weight);
  return kad_finalize_node(s);
}
static __inline int conv_find_par(int in_size, int kernel_size, int stride, int pad0, int *new_pad0, int *new_pad1)
{
  int out_size;
  int pad_both;
  if (pad0 ==  -2 && stride == 1)
    {
      out_size = in_size;
    }
  else
    {
      out_size = (in_size - kernel_size + (pad0 > 0 ? pad0 : 0) + stride - 1) / stride + 1;
    }
  pad_both = (out_size - 1) * stride + kernel_size - in_size;
  *new_pad0 = pad_both / 2;
  *new_pad1 = pad_both - *new_pad0;
  return out_size;
}
struct mcc_struct_anon_44;
typedef struct mcc_struct_anon_44 conv_conf_t;
struct  mcc_struct_anon_44
{
  int kernel_size;
  int stride;
  int pad[2L];
};
static __inline conv_conf_t *conv2d_gen_aux(int in_row, int in_col, int kernel_r, int kernel_c, int stride_r, int stride_c, int top_pad, int left_pad)
{
  conv_conf_t *cnn;
  cnn = (conv_conf_t *)calloc(2, sizeof(conv_conf_t));
  (cnn[0].kernel_size = kernel_r, cnn[0].stride = stride_r);
  (cnn[1].kernel_size = kernel_c, cnn[1].stride = stride_c);
  conv_find_par(in_row, kernel_r, stride_r, top_pad, &cnn[0].pad[0], &cnn[0].pad[1]);
  conv_find_par(in_col, kernel_c, stride_c, left_pad, &cnn[1].pad[0], &cnn[1].pad[1]);
  return cnn;
}
kad_node_t *kad_conv2d(kad_node_t *x, kad_node_t *w, int stride_r, int stride_c, int top_pad, int left_pad)
{
  kad_node_t *s;
  if ((*x).n_d != 4 || (*w).n_d != 4)
    {
      return 0;
    }
  s = kad_new_core(0, 16, 2);
  ((*s).child[0] = x, (*s).child[1] = w);
  (*s).ptr = conv2d_gen_aux((*x).d[2], (*x).d[3], (*w).d[2], (*w).d[3], stride_r, stride_c, top_pad, left_pad);
  (*s).ptr_size = sizeof(conv_conf_t) * 2;
  return kad_finalize_node(s);
}
kad_node_t *kad_max2d(kad_node_t *x, int kernel_r, int kernel_c, int stride_r, int stride_c, int top_pad, int left_pad)
{
  kad_node_t *s;
  if ((*x).n_d != 4)
    {
      return 0;
    }
  s = kad_new_core(0, 17, 1);
  (*s).child[0] = x;
  (*s).ptr = conv2d_gen_aux((*x).d[2], (*x).d[3], kernel_r, kernel_c, stride_r, stride_c, top_pad, left_pad);
  (*s).ptr_size = sizeof(conv_conf_t) * 2;
  return kad_finalize_node(s);
}
static __inline conv_conf_t *conv1d_gen_aux(int in_col, int kernel_c, int stride_c, int left_pad)
{
  conv_conf_t *cnn;
  cnn = (conv_conf_t *)calloc(1, sizeof(conv_conf_t));
  ((*cnn).kernel_size = kernel_c, (*cnn).stride = stride_c);
  conv_find_par(in_col, kernel_c, stride_c, left_pad, &(*cnn).pad[0], &(*cnn).pad[1]);
  return cnn;
}
kad_node_t *kad_conv1d(kad_node_t *x, kad_node_t *w, int stride, int left_pad)
{
  kad_node_t *s;
  if ((*x).n_d != 3 || (*w).n_d != 3)
    {
      return 0;
    }
  s = kad_new_core(0, 18, 2);
  ((*s).child[0] = x, (*s).child[1] = w);
  (*s).ptr = conv1d_gen_aux((*x).d[2], (*w).d[2], stride, left_pad);
  (*s).ptr_size = sizeof(conv_conf_t);
  return kad_finalize_node(s);
}
kad_node_t *kad_max1d(kad_node_t *x, int kernel_size, int stride, int left_pad)
{
  kad_node_t *s;
  if ((*x).n_d != 3)
    {
      return 0;
    }
  s = kad_new_core(0, 19, 1);
  (*s).child[0] = x;
  (*s).ptr = conv1d_gen_aux((*x).d[2], kernel_size, stride, left_pad);
  (*s).ptr_size = sizeof(conv_conf_t);
  return kad_finalize_node(s);
}
kad_node_t *kad_avg1d(kad_node_t *x, int kernel_size, int stride, int left_pad)
{
  kad_node_t *s;
  if ((*x).n_d != 3)
    {
      return 0;
    }
  s = kad_new_core(0, 28, 1);
  (*s).child[0] = x;
  (*s).ptr = conv1d_gen_aux((*x).d[2], kernel_size, stride, left_pad);
  (*s).ptr_size = sizeof(conv_conf_t);
  return kad_finalize_node(s);
}
static kad_node_t *kad_pooling_general(int op, int n, kad_node_t **x)
{
  kad_node_t *s;
  int i;
  s = kad_new_core(0, op, n);
  (*s).flag |= 4;
  for (i = 0; i < n;  ++i)
    {
      (*s).child[i] = x[i];
    }
  return kad_finalize_node(s);
}
kad_node_t *kad_avg(int n, kad_node_t **x)
{
  return kad_pooling_general(10, n, x);
}
kad_node_t *kad_max(int n, kad_node_t **x)
{
  return kad_pooling_general(21, n, x);
}
kad_node_t *kad_stack(int n, kad_node_t **x)
{
  return kad_pooling_general(35, n, x);
}
kad_node_t *kad_select(int n, kad_node_t **x, int which)
{
  int32_t *aux;
  kad_node_t *s;
  int32_t i;
  aux = (int32_t *)calloc(1, 4);
  *aux = which;
  s = kad_new_core(0, 12, n);
  for (i = 0; i < n;  ++i)
    {
      (*s).child[i] = x[i];
    }
  ((((*s).flag |= 4, (*s).ptr = aux)), (*s).ptr_size = 4);
  return kad_finalize_node(s);
}
extern void *malloc(size_t __size) __attribute__((__nothrow__)) __attribute__((__leaf__)) __attribute__((__malloc__));
static kad_node_t *kad_reduce_general(int op, kad_node_t *x, int axis)
{
  int32_t *aux;
  kad_node_t *s;
  aux = (int32_t *)malloc(4);
  aux[0] = axis;
  s = kad_new_core(0, op, 1);
  (*s).child[0] = x;
  ((*s).ptr = aux, (*s).ptr_size = 4);
  return kad_finalize_node(s);
}
kad_node_t *kad_reduce_sum(kad_node_t *x, int axis)
{
  return kad_reduce_general(25, x, axis);
}
kad_node_t *kad_reduce_mean(kad_node_t *x, int axis)
{
  return kad_reduce_general(26, x, axis);
}
void *kad_rng(void);
typedef unsigned long int uint64_t;
struct  mcc_struct_anon_43
{
  uint64_t s[2L];
  double n_gset;
  int n_iset;
  volatile int lock;
};
typedef struct mcc_struct_anon_43 kad_rng_t;
kad_node_t *kad_dropout(kad_node_t *x, kad_node_t *y)
{
  kad_node_t *z;
  z = kad_op2_core(15, x, y);
  ((*z).ptr = kad_rng(), (*z).ptr_size = sizeof(kad_rng_t));
  return z;
}
kad_node_t *kad_sample_normal(kad_node_t *x)
{
  kad_node_t *z;
  z = kad_op1_core(24, x);
  ((*z).ptr = kad_rng(), (*z).ptr_size = sizeof(kad_rng_t));
  return z;
}
kad_node_t *kad_slice(kad_node_t *x, int axis, int start, int end)
{
  int32_t *aux;
  kad_node_t *s;
  if (end < start || start < 0)
    {
      return 0;
    }
  aux = (int32_t *)malloc(3 * 4);
  (((aux[0] = axis, aux[1] = start)), aux[2] = end);
  s = kad_new_core(0, 20, 1);
  (*s).child[0] = x;
  ((*s).ptr = aux, (*s).ptr_size = 3 * 4);
  return kad_finalize_node(s);
}
kad_node_t *kad_concat_array(int axis, int n, kad_node_t **p)
{
  int32_t *aux;
  kad_node_t *s;
  int32_t i;
  aux = (int32_t *)malloc(4);
  aux[0] = axis;
  s = kad_new_core(0, 31, n);
  for (i = 0; i < n;  ++i)
    {
      (*s).child[i] = p[i];
    }
  ((*s).ptr = aux, (*s).ptr_size = 4);
  return kad_finalize_node(s);
}
typedef struct kad_node_t *kad_node_p;
kad_node_t *kad_concat(int axis, int n, ...)
{
  kad_node_t **p;
  va_list ap;
  int i;
  kad_node_t *s;
  p = (kad_node_t **)malloc(n * sizeof(kad_node_t *));
  __builtin_va_start(ap, n);
  for (i = 0; i < n;  ++i)
    {
      p[i] = (__builtin_va_arg(ap, kad_node_p));
    }
  __builtin_va_end(ap);
  s = kad_concat_array(axis, n, p);
  free(p);
  return s;
}
kad_node_t *kad_reshape(kad_node_t *x, int n_d, int *d)
{
  int32_t i;
  kad_node_t *s;
  int32_t *aux = 0;
  if (n_d > 0)
    {
      aux = (int32_t *)malloc(n_d * 4);
      for (i = 0; i < n_d;  ++i)
        {
          aux[i] = d ? d[i] :  -1;
        }
    }
  s = kad_new_core(0, 30, 1);
  ((((*s).child[0] = x, (*s).ptr = aux)), (*s).ptr_size = n_d * 4);
  return kad_finalize_node(s);
}
kad_node_t *kad_reverse(kad_node_t *x, int axis)
{
  int32_t *aux;
  kad_node_t *s;
  aux = (int32_t *)malloc(4);
  *aux = axis;
  s = kad_new_core(0, 36, 1);
  ((((*s).child[0] = x, (*s).ptr = aux)), (*s).ptr_size = 4);
  return kad_finalize_node(s);
}
kad_node_t *kad_switch(int n, kad_node_t **p)
{
  int32_t *aux;
  kad_node_t *s;
  int32_t i;
  aux = (int32_t *)calloc(1, 4);
  s = kad_new_core(0, 12, n);
  for (i = 0; i < n;  ++i)
    {
      (*s).child[i] = p[i];
    }
  ((*s).ptr = aux, (*s).ptr_size = 4);
  return kad_finalize_node(s);
}
static void kad_mark_back(int n, kad_node_t **v)
{
  int i;
  int j;
  for (i = 0; i < n;  ++i)
    {
      if ((*v[i]).n_child == 0)
        {
          continue;
        }
      for (j = 0; j < (*v[i]).n_child;  ++j)
        {
          if ((*(*v[i]).child[j]).flag & 1)
            {
              break;
            }
        }
      if (j < (*v[i]).n_child)
        {
          (*v[i]).flag |= 1;
        }
      else
        {
          (*v[i]).flag &= ~1;
        }
    }
}
extern void *realloc(void *__ptr, size_t __size) __attribute__((__nothrow__)) __attribute__((__leaf__)) __attribute__((__warn_unused_result__));
static void kad_allocate_internal(int n, kad_node_t **v)
{
  int i;
  kad_mark_back(n, v);
  for (i = 0; i < n;  ++i)
    {
      kad_node_t *p = v[i];
      if ((*p).n_child == 0)
        {
          continue;
        }
      (*p).x = (float *)realloc((*p).x, kad_len(p) * sizeof(float));
      if ((*p).flag & 1)
        {
          (*p).g = (float *)realloc((*p).g, kad_len(p) * sizeof(float));
          kad_op_list[(*p).op](p, 1);
        }
    }
}
int kad_sync_dim(int n, kad_node_t **v, int batch_size)
{
  int i;
  int req_alloc = 0;
  int req_sync = 0;
  int old_size = 0;
  for (i = 0; i < n;  ++i)
    {
      if (((*v[i]).n_child == 0 && !((*v[i]).flag & 1)) && !((*v[i]).flag & 2))
        {
          old_size = (*v[i]).d[0];
          if (batch_size > 0 && (*v[i]).d[0] != batch_size)
            {
              ((*v[i]).d[0] = batch_size, req_sync = 1);
            }
        }
      else
        {
          if ((*v[i]).n_child > 0 && req_sync)
            {
              kad_op_list[(*v[i]).op](v[i], 4);
            }
        }
    }
  if (old_size < batch_size)
    {
      req_alloc = 1;
    }
  for (i = 0; i < n;  ++i)
    {
      if ((*v[i]).n_child > 0 && (*v[i]).x == 0)
        {
          req_alloc = 1;
        }
    }
  if (req_alloc)
    {
      kad_allocate_internal(n, v);
    }
  return batch_size > 0 ? batch_size : old_size;
}
extern void __assert_fail(const char *__assertion, const char *__file, unsigned int __line, const char *__function) __attribute__((__nothrow__)) __attribute__((__leaf__)) __attribute__((__noreturn__));
kad_node_t **kad_compile_array(int *n_node, int n_roots, kad_node_t **roots)
{
  struct  mcc_struct_anon_45
  {
    size_t n;
    size_t m;
    kad_node_p *a;
  };
  int i;
  static const char __MERCURIUM_PRETTY_FUNCTION__[58L] = "kad_node_t **kad_compile_array(int *, int, kad_node_t **)";
  struct mcc_struct_anon_45 stack = {.n = 0, .m = 0, .a = 0};
  struct mcc_struct_anon_45 a = {.n = 0, .m = 0, .a = 0};
  for (i = 0; i < n_roots;  ++i)
    {
      (*roots[i]).tmp = 1;
      do
        {
          if (stack.n == stack.m)
            {
              stack.m = stack.m ? stack.m << 1 : 2;
              stack.a = (kad_node_p *)realloc(stack.a, sizeof(kad_node_p) * stack.m);
            }
          stack.a[stack.n++] = roots[i];
        }
      while (0);
    }
  while (stack.n)
    {
      kad_node_t *p = stack.a[ --stack.n];
      for (i = 0; i < (*p).n_child;  ++i)
        {
          kad_node_t *q = (*p).child[i];
          if ((*q).tmp == 0)
            {
              do
                {
                  if (stack.n == stack.m)
                    {
                      stack.m = stack.m ? stack.m << 1 : 2;
                      stack.a = (kad_node_p *)realloc(stack.a, sizeof(kad_node_p) * stack.m);
                    }
                  stack.a[stack.n++] = q;
                }
              while (0);
            }
          (*q).tmp += 1 << 1;
        }
    }
  for (i = 0; i < n_roots;  ++i)
    {
      if ((*roots[i]).tmp >> 1 == 0)
        {
          do
            {
              if (stack.n == stack.m)
                {
                  stack.m = stack.m ? stack.m << 1 : 2;
                  stack.a = (kad_node_p *)realloc(stack.a, sizeof(kad_node_p) * stack.m);
                }
              stack.a[stack.n++] = roots[i];
            }
          while (0);
        }
    }
  while (stack.n)
    {
      kad_node_t *p = stack.a[ --stack.n];
      do
        {
          if (a.n == a.m)
            {
              a.m = a.m ? a.m << 1 : 2;
              a.a = (kad_node_p *)realloc(a.a, sizeof(kad_node_p) * a.m);
            }
          a.a[a.n++] = p;
        }
      while (0);
      for (i = 0; i < (*p).n_child;  ++i)
        {
          (*(*p).child[i]).tmp -= 1 << 1;
          if ((*(*p).child[i]).tmp >> 1 == 0)
            {
              do
                {
                  if (stack.n == stack.m)
                    {
                      stack.m = stack.m ? stack.m << 1 : 2;
                      stack.a = (kad_node_p *)realloc(stack.a, sizeof(kad_node_p) * stack.m);
                    }
                  stack.a[stack.n++] = (*p).child[i];
                }
              while (0);
            }
        }
    }
  free(stack.a);
  for (i = 0; i < (int)a.n;  ++i)
    {
      (*a.a[i]).tmp >> 1 == 0 ? (void)0 : __assert_fail("a.a[i]->tmp>>1 == 0", "kautodiff_parallel.c", 467, __MERCURIUM_PRETTY_FUNCTION__);
      (*a.a[i]).tmp = 0;
    }
  for (i = 0; i < (int)a.n >> 1;  ++i)
    {
      kad_node_p t;
      (((t = a.a[i], a.a[i] = a.a[a.n - 1 - i])), a.a[a.n - 1 - i] = t);
    }
  kad_allocate_internal(a.n, a.a);
  *n_node = a.n;
  return a.a;
}
kad_node_t **kad_compile(int *n_node, int n_roots, ...)
{
  kad_node_t **roots;
  va_list ap;
  int i;
  kad_node_t **ret;
  roots = (kad_node_t **)malloc(n_roots * sizeof(kad_node_t *));
  __builtin_va_start(ap, n_roots);
  for (i = 0; i < n_roots;  ++i)
    {
      roots[i] = (__builtin_va_arg(ap, kad_node_p));
    }
  __builtin_va_end(ap);
  ret = kad_compile_array(n_node, n_roots, roots);
  free(roots);
  return ret;
}
void kad_delete(int n, kad_node_t **a)
{
  int i;
  for (i = 0; i < n;  ++i)
    {
      kad_node_t *p = a[i];
      if ((*p).n_child)
        {
          free((*p).x);
          free((*p).g);
        }
      free((*p).child);
      free((*p).ptr);
      free((*p).gtmp);
      free(p);
    }
  free(a);
}
int kad_size_var(int n, kad_node_t *const *v)
{
  int i;
  int c;
  for (i = c = 0; i < n;  ++i)
    {
      if ((*v[i]).n_child == 0 && (*v[i]).flag & 1)
        {
          c += kad_len(v[i]);
        }
    }
  return c;
}
int kad_size_const(int n, kad_node_t *const *v)
{
  int i;
  int c;
  for (i = c = 0; i < n;  ++i)
    {
      if ((*v[i]).n_child == 0 && (*v[i]).flag & 2)
        {
          c += kad_len(v[i]);
        }
    }
  return c;
}
static void kad_propagate_marks(int n, kad_node_t **a)
{
  int i;
  int j;
  for (i = n - 1; i >= 0;  --i)
    {
      kad_node_t *p = a[i];
      if ((*p).tmp > 0)
        {
          if ((*p).op == 12 && !((*p).flag & 4))
            {
              int32_t *aux = (int32_t *)(*p).ptr;
              if ((*(*p).child[*aux]).tmp == 0)
                {
                  (*(*p).child[*aux]).tmp = 1;
                }
            }
          else
            {
              for (j = 0; j < (*p).n_child;  ++j)
                {
                  if ((*(*p).child[j]).tmp == 0)
                    {
                      (*(*p).child[j]).tmp = 1;
                    }
                }
            }
        }
    }
}
void ompss_forward_prop(int start, int end, kad_node_t **a, int action)
{
  int i;
  for (i = start; i <= end;  ++i)
    {
      if ((*a[i]).n_child && (*a[i]).tmp > 0)
        {
          kad_op_list[(*a[i]).op](a[i], action);
        }
    }
}
void ompss_backward_prop(int end, int start, kad_node_t **a, int action)
{
  int i;
  for (i = end; i >= start;  --i)
    {
      if ((*a[i]).n_child && (*a[i]).tmp > 0)
        {
          kad_op_list[(*a[i]).op](a[i], action);
        }
    }
}
struct  nanos_args_0_t
{
  kad_node_t **a;
  int s;
  int e;
  int *end;
  int node;
  int layers;
};
typedef void *nanos_wd_t;
enum mcc_enum_anon_5
{
  NANOS_OK = 0,
  NANOS_UNKNOWN_ERR = 1,
  NANOS_UNIMPLEMENTED = 2,
  NANOS_ENOMEM = 3,
  NANOS_INVALID_PARAM = 4,
  NANOS_INVALID_REQUEST = 5
};
typedef enum mcc_enum_anon_5 nanos_err_t;
typedef unsigned int nanos_copy_id_t;
extern nanos_err_t nanos_get_addr(nanos_copy_id_t copy_id, void **addr, nanos_wd_t cwd);
extern void nanos_handle_error(nanos_err_t err);
static void nanos_xlate_fun_kautodiffparallelc_0(struct nanos_args_0_t *const arg, void *wd)
{
  {
    void *device_base_address;
    nanos_err_t nanos_err;
    device_base_address = 0;
    nanos_err = nanos_get_addr(0, &device_base_address, wd);
    if (nanos_err != NANOS_OK)
      {
        nanos_handle_error(nanos_err);
      }
    (*arg).a = (kad_node_t **)device_base_address;
  }
}
struct  nanos_args_1_t
{
  kad_node_t **a;
  int s;
  int e;
};
static void nanos_xlate_fun_kautodiffparallelc_1(struct nanos_args_1_t *const arg, void *wd)
{
  {
    void *device_base_address;
    nanos_err_t nanos_err;
    device_base_address = 0;
    nanos_err = nanos_get_addr(0, &device_base_address, wd);
    if (nanos_err != NANOS_OK)
      {
        nanos_handle_error(nanos_err);
      }
    (*arg).a = (kad_node_t **)device_base_address;
  }
}
struct  nanos_args_2_t
{
  kad_node_t **a;
  int s;
  int e;
  int *end;
  int node;
  int layers;
};
static void nanos_xlate_fun_kautodiffparallelc_2(struct nanos_args_2_t *const arg, void *wd)
{
  {
    void *device_base_address;
    nanos_err_t nanos_err;
    device_base_address = 0;
    nanos_err = nanos_get_addr(0, &device_base_address, wd);
    if (nanos_err != NANOS_OK)
      {
        nanos_handle_error(nanos_err);
      }
    (*arg).a = (kad_node_t **)device_base_address;
  }
}
struct  nanos_args_3_t
{
  kad_node_t **a;
  int s;
  int e;
};
static void nanos_xlate_fun_kautodiffparallelc_3(struct nanos_args_3_t *const arg, void *wd)
{
  {
    void *device_base_address;
    nanos_err_t nanos_err;
    device_base_address = 0;
    nanos_err = nanos_get_addr(0, &device_base_address, wd);
    if (nanos_err != NANOS_OK)
      {
        nanos_handle_error(nanos_err);
      }
    (*arg).a = (kad_node_t **)device_base_address;
  }
}
extern nanos_err_t nanos_in_final(_Bool *result);
struct  mcc_struct_anon_15
{
  void (*outline)(void *);
};
typedef struct mcc_struct_anon_15 nanos_smp_args_t;
static void smp_ol_kad_eval_marked_0(struct nanos_args_0_t *const args);
struct  mcc_struct_anon_11
{
  _Bool mandatory_creation:1;
  _Bool tied:1;
  _Bool clear_chunk:1;
  _Bool reserved0:1;
  _Bool reserved1:1;
  _Bool reserved2:1;
  _Bool reserved3:1;
  _Bool reserved4:1;
};
typedef struct mcc_struct_anon_11 nanos_wd_props_t;
struct  nanos_const_wd_definition_tag
{
  nanos_wd_props_t props;
  size_t data_alignment;
  size_t num_copies;
  size_t num_devices;
  size_t num_dimensions;
  const char *description;
};
typedef struct nanos_const_wd_definition_tag nanos_const_wd_definition_t;
struct  mcc_struct_anon_14
{
  void *(*factory)(void *);
  void *arg;
};
typedef struct mcc_struct_anon_14 nanos_device_t;
struct  nanos_const_wd_definition_1
{
  nanos_const_wd_definition_t base;
  nanos_device_t devices[1L];
};
extern void *nanos_smp_factory(void *args);
struct  mcc_struct_anon_12
{
  _Bool is_final:1;
  _Bool is_recover:1;
  _Bool is_implicit:1;
  _Bool reserved3:1;
  _Bool reserved4:1;
  _Bool reserved5:1;
  _Bool reserved6:1;
  _Bool reserved7:1;
};
typedef struct mcc_struct_anon_12 nanos_wd_dyn_flags_t;
typedef void *nanos_thread_t;
struct  mcc_struct_anon_13
{
  nanos_wd_dyn_flags_t flags;
  nanos_thread_t tie_to;
  int priority;
  void *callback;
  void *arguments;
};
typedef struct mcc_struct_anon_13 nanos_wd_dyn_props_t;
struct mcc_struct_anon_4;
typedef struct mcc_struct_anon_4 nanos_copy_data_internal_t;
typedef nanos_copy_data_internal_t nanos_copy_data_t;
struct mcc_struct_anon_0;
typedef struct mcc_struct_anon_0 nanos_region_dimension_internal_t;
typedef void *nanos_wg_t;
extern nanos_err_t nanos_create_wd_compact(nanos_wd_t *wd, nanos_const_wd_definition_t *const_data, nanos_wd_dyn_props_t *dyn_props, size_t data_size, void **data, nanos_wg_t wg, nanos_copy_data_t **copies, nanos_region_dimension_internal_t **dimensions);
extern nanos_wd_t nanos_current_wd(void);
struct  mcc_struct_anon_0
{
  size_t size;
  size_t lower_bound;
  size_t accessed_length;
};
typedef nanos_region_dimension_internal_t nanos_region_dimension_t;
struct  mcc_struct_anon_1
{
  _Bool input:1;
  _Bool output:1;
  _Bool can_rename:1;
  _Bool concurrent:1;
  _Bool commutative:1;
};
typedef struct mcc_struct_anon_1 nanos_access_type_internal_t;
typedef long int ptrdiff_t;
struct  mcc_struct_anon_2
{
  void *address;
  nanos_access_type_internal_t flags;
  short int dimension_count;
  const nanos_region_dimension_internal_t *dimensions;
  ptrdiff_t offset;
};
typedef struct mcc_struct_anon_2 nanos_data_access_internal_t;
typedef nanos_data_access_internal_t nanos_data_access_t;
enum mcc_enum_anon_0
{
  NANOS_PRIVATE = 0,
  NANOS_SHARED = 1
};
typedef enum mcc_enum_anon_0 nanos_sharing_t;
struct  mcc_struct_anon_5
{
  _Bool input:1;
  _Bool output:1;
};
typedef unsigned int reg_t;
struct  mcc_struct_anon_4
{
  void *address;
  nanos_sharing_t sharing;
  struct mcc_struct_anon_5 flags;
  short int dimension_count;
  const nanos_region_dimension_internal_t *dimensions;
  ptrdiff_t offset;
  uint64_t host_base_address;
  reg_t host_region_id;
  _Bool remote_host;
  void *deducted_cd;
};
typedef void (*nanos_translate_args_t)(void *, nanos_wd_t);
extern nanos_err_t nanos_set_translate_function(nanos_wd_t wd, nanos_translate_args_t translate_args);
typedef void *nanos_team_t;
extern nanos_err_t nanos_submit(nanos_wd_t wd, size_t num_data_accesses, nanos_data_access_t *data_accesses, nanos_team_t team);
extern nanos_err_t nanos_create_wd_and_run_compact(nanos_const_wd_definition_t *const_data, nanos_wd_dyn_props_t *dyn_props, size_t data_size, void *data, size_t num_data_accesses, nanos_data_access_t *data_accesses, nanos_copy_data_t *copies, nanos_region_dimension_internal_t *dimensions, nanos_translate_args_t translate_args);
static void smp_ol_kad_eval_marked_1(struct nanos_args_1_t *const args);
static void smp_ol_kad_eval_marked_2(struct nanos_args_2_t *const args);
static void smp_ol_kad_eval_marked_3(struct nanos_args_3_t *const args);
extern nanos_err_t nanos_wg_wait_completion(nanos_wg_t wg, _Bool avoid_flush);
void kad_eval_marked(int n, kad_node_t **a, int layers, int ulen, int *start, int *end, int parallel)
{
  int i;
  int u;
  int l;
  kad_propagate_marks(n, a);
  if (layers == 1 || !parallel)
    {
      for (i = 0; i < n;  ++i)
        {
          if ((*a[i]).n_child && (*a[i]).tmp > 0)
            {
              kad_op_list[(*a[i]).op](a[i], 2);
            }
        }
      for (i = 0; i < n;  ++i)
        {
          (*a[i]).tmp = 0;
        }
    }
  else
    {
      int node =  -1;
      for (u = 0; u < ulen;  ++u)
        {
          for (l = 0; l < layers;  ++l)
            {
              int s = start[u * layers + l];
              int e = end[u * layers + l];
              node++;
              if (l > 0 && u > 0)
                {
                  {
                    _Bool mcc_is_in_final;
                    nanos_err_t mcc_err_in_final = nanos_in_final(&mcc_is_in_final);
                    if (mcc_err_in_final != NANOS_OK)
                      {
                        nanos_handle_error(mcc_err_in_final);
                      }
                    if (mcc_is_in_final)
                      {
                        ompss_forward_prop(s, e, a, 2);
                      }
                    else
                      {
                        {
                          nanos_wd_dyn_props_t nanos_wd_dyn_props;
                          struct nanos_args_0_t *ol_args;
                          nanos_err_t nanos_err;
                          struct nanos_args_0_t imm_args;
                          nanos_region_dimension_t dimensions_0[1L];
                          nanos_data_access_t dependences[3L];
                          nanos_region_dimension_t dimensions_1[1L];
                          nanos_region_dimension_t dimensions_2[1L];
                          static nanos_smp_args_t smp_ol_kad_eval_marked_0_args = {.outline = (void (*)(void *))(void (*)(struct nanos_args_0_t *))&smp_ol_kad_eval_marked_0};
                          static struct nanos_const_wd_definition_1 nanos_wd_const_data = {.base = {.props = {.mandatory_creation = 0, .tied = 0, .clear_chunk = 0, .reserved0 = 0, .reserved1 = 0, .reserved2 = 0, .reserved3 = 0, .reserved4 = 0}, .data_alignment = __alignof__(struct nanos_args_0_t), .num_copies = 3, .num_devices = 1, .num_dimensions = 3, .description = 0}, .devices = {[0] = {.factory = &nanos_smp_factory, .arg = &smp_ol_kad_eval_marked_0_args}}};
                          nanos_wd_dyn_props.tie_to = 0;
                          nanos_wd_dyn_props.priority = 0;
                          nanos_wd_dyn_props.flags.is_final = 0;
                          nanos_wd_dyn_props.flags.is_implicit = 0;
                          nanos_wd_dyn_props.flags.is_recover = 0;
                          ol_args = (struct nanos_args_0_t *)0;
                          nanos_wd_t nanos_wd_ = (nanos_wd_t)0;
                          nanos_copy_data_t *ol_copy_data = (nanos_copy_data_t *)0;
                          nanos_region_dimension_internal_t *ol_copy_dimensions = (nanos_region_dimension_internal_t *)0;
                          nanos_err = nanos_create_wd_compact(&nanos_wd_, &nanos_wd_const_data.base, &nanos_wd_dyn_props, sizeof(struct nanos_args_0_t), (void **)&ol_args, nanos_current_wd(), &ol_copy_data, &ol_copy_dimensions);
                          if (nanos_err != NANOS_OK)
                            {
                              nanos_handle_error(nanos_err);
                            }
                          dimensions_0[0].size = 1L * sizeof(kad_node_t *);
                          dimensions_0[0].lower_bound = (s - 1 - 0L) * sizeof(kad_node_t *);
                          dimensions_0[0].accessed_length = (s - 1 - 0L - (s - 1 - 0L) + 1) * sizeof(kad_node_t *);
                          dependences[0].address = (void *)a;
                          dependences[0].offset = 8L * (((s - 1) - (0L)));
                          dependences[0].dimensions = dimensions_0;
                          dependences[0].flags.input = 1;
                          dependences[0].flags.output = 0;
                          dependences[0].flags.can_rename = 0;
                          dependences[0].flags.concurrent = 0;
                          dependences[0].flags.commutative = 0;
                          dependences[0].dimension_count = 1;
                          dimensions_1[0].size = 1L * sizeof(kad_node_t *);
                          dimensions_1[0].lower_bound = (end[node - layers] - 0L) * sizeof(kad_node_t *);
                          dimensions_1[0].accessed_length = (end[node - layers] - 0L - (end[node - layers] - 0L) + 1) * sizeof(kad_node_t *);
                          dependences[1].address = (void *)a;
                          dependences[1].offset = 8L * (((end[node - layers]) - (0L)));
                          dependences[1].dimensions = dimensions_1;
                          dependences[1].flags.input = 1;
                          dependences[1].flags.output = 0;
                          dependences[1].flags.can_rename = 0;
                          dependences[1].flags.concurrent = 0;
                          dependences[1].flags.commutative = 0;
                          dependences[1].dimension_count = 1;
                          dimensions_2[0].size = 1L * sizeof(kad_node_t *);
                          dimensions_2[0].lower_bound = (e - 0L) * sizeof(kad_node_t *);
                          dimensions_2[0].accessed_length = (e - 0L - (e - 0L) + 1) * sizeof(kad_node_t *);
                          dependences[2].address = (void *)a;
                          dependences[2].offset = 8L * (((e) - (0L)));
                          dependences[2].dimensions = dimensions_2;
                          dependences[2].flags.input = 0;
                          dependences[2].flags.output = 1;
                          dependences[2].flags.can_rename = 0;
                          dependences[2].flags.concurrent = 0;
                          dependences[2].flags.commutative = 0;
                          dependences[2].dimension_count = 1;
                          if (nanos_wd_ != (nanos_wd_t)0)
                            {
                              (*ol_args).a = a;
                              (*ol_args).s = s;
                              (*ol_args).e = e;
                              (*ol_args).end = end;
                              (*ol_args).node = node;
                              (*ol_args).layers = layers;
                              ol_copy_dimensions[0 + 0].size = 1L * sizeof(kad_node_t *);
                              ol_copy_dimensions[0 + 0].lower_bound = (s - 1 - 0L) * sizeof(kad_node_t *);
                              ol_copy_dimensions[0 + 0].accessed_length = (s - 1 - 0L - (s - 1 - 0L) + 1) * sizeof(kad_node_t *);
                              ol_copy_data[0].sharing = NANOS_SHARED;
                              ol_copy_data[0].address = (void *)a;
                              ol_copy_data[0].flags.input = 1;
                              ol_copy_data[0].flags.output = 0;
                              ol_copy_data[0].dimension_count = (short int)1;
                              ol_copy_data[0].dimensions = &ol_copy_dimensions[0];
                              ol_copy_data[0].offset = 8L * (((s - 1) - (0L)));
                              ol_copy_dimensions[1 + 0].size = 1L * sizeof(kad_node_t *);
                              ol_copy_dimensions[1 + 0].lower_bound = (end[node - layers] - 0L) * sizeof(kad_node_t *);
                              ol_copy_dimensions[1 + 0].accessed_length = (end[node - layers] - 0L - (end[node - layers] - 0L) + 1) * sizeof(kad_node_t *);
                              ol_copy_data[1].sharing = NANOS_SHARED;
                              ol_copy_data[1].address = (void *)a;
                              ol_copy_data[1].flags.input = 1;
                              ol_copy_data[1].flags.output = 0;
                              ol_copy_data[1].dimension_count = (short int)1;
                              ol_copy_data[1].dimensions = &ol_copy_dimensions[1];
                              ol_copy_data[1].offset = 8L * (((end[node - layers]) - (0L)));
                              ol_copy_dimensions[2 + 0].size = 1L * sizeof(kad_node_t *);
                              ol_copy_dimensions[2 + 0].lower_bound = (e - 0L) * sizeof(kad_node_t *);
                              ol_copy_dimensions[2 + 0].accessed_length = (e - 0L - (e - 0L) + 1) * sizeof(kad_node_t *);
                              ol_copy_data[2].sharing = NANOS_SHARED;
                              ol_copy_data[2].address = (void *)a;
                              ol_copy_data[2].flags.input = 0;
                              ol_copy_data[2].flags.output = 1;
                              ol_copy_data[2].dimension_count = (short int)1;
                              ol_copy_data[2].dimensions = &ol_copy_dimensions[2];
                              ol_copy_data[2].offset = 8L * (((e) - (0L)));
                              nanos_err = nanos_set_translate_function(nanos_wd_, (nanos_translate_args_t)nanos_xlate_fun_kautodiffparallelc_0);
                              if (nanos_err != NANOS_OK)
                                {
                                  nanos_handle_error(nanos_err);
                                }
                              nanos_err = nanos_submit(nanos_wd_, 3, &dependences[0], (nanos_team_t)0);
                              if (nanos_err != NANOS_OK)
                                {
                                  nanos_handle_error(nanos_err);
                                }
                            }
                          else
                            {
                              nanos_region_dimension_internal_t imm_copy_dimensions[3L];
                              nanos_copy_data_t imm_copy_data[3L];
                              imm_args.a = a;
                              imm_args.s = s;
                              imm_args.e = e;
                              imm_args.end = end;
                              imm_args.node = node;
                              imm_args.layers = layers;
                              imm_copy_dimensions[0 + 0].size = 1L * sizeof(kad_node_t *);
                              imm_copy_dimensions[0 + 0].lower_bound = (s - 1 - 0L) * sizeof(kad_node_t *);
                              imm_copy_dimensions[0 + 0].accessed_length = (s - 1 - 0L - (s - 1 - 0L) + 1) * sizeof(kad_node_t *);
                              imm_copy_data[0].sharing = NANOS_SHARED;
                              imm_copy_data[0].address = (void *)a;
                              imm_copy_data[0].flags.input = 1;
                              imm_copy_data[0].flags.output = 0;
                              imm_copy_data[0].dimension_count = (short int)1;
                              imm_copy_data[0].dimensions = &imm_copy_dimensions[0];
                              imm_copy_data[0].offset = 8L * (((s - 1) - (0L)));
                              imm_copy_dimensions[1 + 0].size = 1L * sizeof(kad_node_t *);
                              imm_copy_dimensions[1 + 0].lower_bound = (end[node - layers] - 0L) * sizeof(kad_node_t *);
                              imm_copy_dimensions[1 + 0].accessed_length = (end[node - layers] - 0L - (end[node - layers] - 0L) + 1) * sizeof(kad_node_t *);
                              imm_copy_data[1].sharing = NANOS_SHARED;
                              imm_copy_data[1].address = (void *)a;
                              imm_copy_data[1].flags.input = 1;
                              imm_copy_data[1].flags.output = 0;
                              imm_copy_data[1].dimension_count = (short int)1;
                              imm_copy_data[1].dimensions = &imm_copy_dimensions[1];
                              imm_copy_data[1].offset = 8L * (((end[node - layers]) - (0L)));
                              imm_copy_dimensions[2 + 0].size = 1L * sizeof(kad_node_t *);
                              imm_copy_dimensions[2 + 0].lower_bound = (e - 0L) * sizeof(kad_node_t *);
                              imm_copy_dimensions[2 + 0].accessed_length = (e - 0L - (e - 0L) + 1) * sizeof(kad_node_t *);
                              imm_copy_data[2].sharing = NANOS_SHARED;
                              imm_copy_data[2].address = (void *)a;
                              imm_copy_data[2].flags.input = 0;
                              imm_copy_data[2].flags.output = 1;
                              imm_copy_data[2].dimension_count = (short int)1;
                              imm_copy_data[2].dimensions = &imm_copy_dimensions[2];
                              imm_copy_data[2].offset = 8L * (((e) - (0L)));
                              nanos_err = nanos_create_wd_and_run_compact(&nanos_wd_const_data.base, &nanos_wd_dyn_props, sizeof(struct nanos_args_0_t), &imm_args, 3, &dependences[0], imm_copy_data, imm_copy_dimensions, (nanos_translate_args_t)nanos_xlate_fun_kautodiffparallelc_0);
                              if (nanos_err != NANOS_OK)
                                {
                                  nanos_handle_error(nanos_err);
                                }
                            }
                        }
                      }
                  }
                }
              else
                {
                  if (u == 0 && l > 0)
                    {
                      {
                        _Bool mcc_is_in_final;
                        nanos_err_t mcc_err_in_final = nanos_in_final(&mcc_is_in_final);
                        if (mcc_err_in_final != NANOS_OK)
                          {
                            nanos_handle_error(mcc_err_in_final);
                          }
                        if (mcc_is_in_final)
                          {
                            ompss_forward_prop(s, e, a, 2);
                          }
                        else
                          {
                            {
                              nanos_wd_dyn_props_t nanos_wd_dyn_props;
                              struct nanos_args_1_t *ol_args;
                              nanos_err_t nanos_err;
                              struct nanos_args_1_t imm_args;
                              nanos_region_dimension_t dimensions_3[1L];
                              nanos_data_access_t dependences[2L];
                              nanos_region_dimension_t dimensions_4[1L];
                              static nanos_smp_args_t smp_ol_kad_eval_marked_1_args = {.outline = (void (*)(void *))(void (*)(struct nanos_args_1_t *))&smp_ol_kad_eval_marked_1};
                              static struct nanos_const_wd_definition_1 nanos_wd_const_data = {.base = {.props = {.mandatory_creation = 0, .tied = 0, .clear_chunk = 0, .reserved0 = 0, .reserved1 = 0, .reserved2 = 0, .reserved3 = 0, .reserved4 = 0}, .data_alignment = __alignof__(struct nanos_args_1_t), .num_copies = 2, .num_devices = 1, .num_dimensions = 2, .description = 0}, .devices = {[0] = {.factory = &nanos_smp_factory, .arg = &smp_ol_kad_eval_marked_1_args}}};
                              nanos_wd_dyn_props.tie_to = 0;
                              nanos_wd_dyn_props.priority = 0;
                              nanos_wd_dyn_props.flags.is_final = 0;
                              nanos_wd_dyn_props.flags.is_implicit = 0;
                              nanos_wd_dyn_props.flags.is_recover = 0;
                              ol_args = (struct nanos_args_1_t *)0;
                              nanos_wd_t nanos_wd_ = (nanos_wd_t)0;
                              nanos_copy_data_t *ol_copy_data = (nanos_copy_data_t *)0;
                              nanos_region_dimension_internal_t *ol_copy_dimensions = (nanos_region_dimension_internal_t *)0;
                              nanos_err = nanos_create_wd_compact(&nanos_wd_, &nanos_wd_const_data.base, &nanos_wd_dyn_props, sizeof(struct nanos_args_1_t), (void **)&ol_args, nanos_current_wd(), &ol_copy_data, &ol_copy_dimensions);
                              if (nanos_err != NANOS_OK)
                                {
                                  nanos_handle_error(nanos_err);
                                }
                              dimensions_3[0].size = 1L * sizeof(kad_node_t *);
                              dimensions_3[0].lower_bound = (s - 1 - 0L) * sizeof(kad_node_t *);
                              dimensions_3[0].accessed_length = (s - 1 - 0L - (s - 1 - 0L) + 1) * sizeof(kad_node_t *);
                              dependences[0].address = (void *)a;
                              dependences[0].offset = 8L * (((s - 1) - (0L)));
                              dependences[0].dimensions = dimensions_3;
                              dependences[0].flags.input = 1;
                              dependences[0].flags.output = 0;
                              dependences[0].flags.can_rename = 0;
                              dependences[0].flags.concurrent = 0;
                              dependences[0].flags.commutative = 0;
                              dependences[0].dimension_count = 1;
                              dimensions_4[0].size = 1L * sizeof(kad_node_t *);
                              dimensions_4[0].lower_bound = (e - 0L) * sizeof(kad_node_t *);
                              dimensions_4[0].accessed_length = (e - 0L - (e - 0L) + 1) * sizeof(kad_node_t *);
                              dependences[1].address = (void *)a;
                              dependences[1].offset = 8L * (((e) - (0L)));
                              dependences[1].dimensions = dimensions_4;
                              dependences[1].flags.input = 0;
                              dependences[1].flags.output = 1;
                              dependences[1].flags.can_rename = 0;
                              dependences[1].flags.concurrent = 0;
                              dependences[1].flags.commutative = 0;
                              dependences[1].dimension_count = 1;
                              if (nanos_wd_ != (nanos_wd_t)0)
                                {
                                  (*ol_args).a = a;
                                  (*ol_args).s = s;
                                  (*ol_args).e = e;
                                  ol_copy_dimensions[0 + 0].size = 1L * sizeof(kad_node_t *);
                                  ol_copy_dimensions[0 + 0].lower_bound = (s - 1 - 0L) * sizeof(kad_node_t *);
                                  ol_copy_dimensions[0 + 0].accessed_length = (s - 1 - 0L - (s - 1 - 0L) + 1) * sizeof(kad_node_t *);
                                  ol_copy_data[0].sharing = NANOS_SHARED;
                                  ol_copy_data[0].address = (void *)a;
                                  ol_copy_data[0].flags.input = 1;
                                  ol_copy_data[0].flags.output = 0;
                                  ol_copy_data[0].dimension_count = (short int)1;
                                  ol_copy_data[0].dimensions = &ol_copy_dimensions[0];
                                  ol_copy_data[0].offset = 8L * (((s - 1) - (0L)));
                                  ol_copy_dimensions[1 + 0].size = 1L * sizeof(kad_node_t *);
                                  ol_copy_dimensions[1 + 0].lower_bound = (e - 0L) * sizeof(kad_node_t *);
                                  ol_copy_dimensions[1 + 0].accessed_length = (e - 0L - (e - 0L) + 1) * sizeof(kad_node_t *);
                                  ol_copy_data[1].sharing = NANOS_SHARED;
                                  ol_copy_data[1].address = (void *)a;
                                  ol_copy_data[1].flags.input = 0;
                                  ol_copy_data[1].flags.output = 1;
                                  ol_copy_data[1].dimension_count = (short int)1;
                                  ol_copy_data[1].dimensions = &ol_copy_dimensions[1];
                                  ol_copy_data[1].offset = 8L * (((e) - (0L)));
                                  nanos_err = nanos_set_translate_function(nanos_wd_, (nanos_translate_args_t)nanos_xlate_fun_kautodiffparallelc_1);
                                  if (nanos_err != NANOS_OK)
                                    {
                                      nanos_handle_error(nanos_err);
                                    }
                                  nanos_err = nanos_submit(nanos_wd_, 2, &dependences[0], (nanos_team_t)0);
                                  if (nanos_err != NANOS_OK)
                                    {
                                      nanos_handle_error(nanos_err);
                                    }
                                }
                              else
                                {
                                  nanos_region_dimension_internal_t imm_copy_dimensions[2L];
                                  nanos_copy_data_t imm_copy_data[2L];
                                  imm_args.a = a;
                                  imm_args.s = s;
                                  imm_args.e = e;
                                  imm_copy_dimensions[0 + 0].size = 1L * sizeof(kad_node_t *);
                                  imm_copy_dimensions[0 + 0].lower_bound = (s - 1 - 0L) * sizeof(kad_node_t *);
                                  imm_copy_dimensions[0 + 0].accessed_length = (s - 1 - 0L - (s - 1 - 0L) + 1) * sizeof(kad_node_t *);
                                  imm_copy_data[0].sharing = NANOS_SHARED;
                                  imm_copy_data[0].address = (void *)a;
                                  imm_copy_data[0].flags.input = 1;
                                  imm_copy_data[0].flags.output = 0;
                                  imm_copy_data[0].dimension_count = (short int)1;
                                  imm_copy_data[0].dimensions = &imm_copy_dimensions[0];
                                  imm_copy_data[0].offset = 8L * (((s - 1) - (0L)));
                                  imm_copy_dimensions[1 + 0].size = 1L * sizeof(kad_node_t *);
                                  imm_copy_dimensions[1 + 0].lower_bound = (e - 0L) * sizeof(kad_node_t *);
                                  imm_copy_dimensions[1 + 0].accessed_length = (e - 0L - (e - 0L) + 1) * sizeof(kad_node_t *);
                                  imm_copy_data[1].sharing = NANOS_SHARED;
                                  imm_copy_data[1].address = (void *)a;
                                  imm_copy_data[1].flags.input = 0;
                                  imm_copy_data[1].flags.output = 1;
                                  imm_copy_data[1].dimension_count = (short int)1;
                                  imm_copy_data[1].dimensions = &imm_copy_dimensions[1];
                                  imm_copy_data[1].offset = 8L * (((e) - (0L)));
                                  nanos_err = nanos_create_wd_and_run_compact(&nanos_wd_const_data.base, &nanos_wd_dyn_props, sizeof(struct nanos_args_1_t), &imm_args, 2, &dependences[0], imm_copy_data, imm_copy_dimensions, (nanos_translate_args_t)nanos_xlate_fun_kautodiffparallelc_1);
                                  if (nanos_err != NANOS_OK)
                                    {
                                      nanos_handle_error(nanos_err);
                                    }
                                }
                            }
                          }
                      }
                    }
                  else
                    {
                      if (l == 0 && u > 0)
                        {
                          {
                            _Bool mcc_is_in_final;
                            nanos_err_t mcc_err_in_final = nanos_in_final(&mcc_is_in_final);
                            if (mcc_err_in_final != NANOS_OK)
                              {
                                nanos_handle_error(mcc_err_in_final);
                              }
                            if (mcc_is_in_final)
                              {
                                ompss_forward_prop(s, e, a, 2);
                              }
                            else
                              {
                                {
                                  nanos_wd_dyn_props_t nanos_wd_dyn_props;
                                  struct nanos_args_2_t *ol_args;
                                  nanos_err_t nanos_err;
                                  struct nanos_args_2_t imm_args;
                                  nanos_region_dimension_t dimensions_5[1L];
                                  nanos_data_access_t dependences[2L];
                                  nanos_region_dimension_t dimensions_6[1L];
                                  static nanos_smp_args_t smp_ol_kad_eval_marked_2_args = {.outline = (void (*)(void *))(void (*)(struct nanos_args_2_t *))&smp_ol_kad_eval_marked_2};
                                  static struct nanos_const_wd_definition_1 nanos_wd_const_data = {.base = {.props = {.mandatory_creation = 0, .tied = 0, .clear_chunk = 0, .reserved0 = 0, .reserved1 = 0, .reserved2 = 0, .reserved3 = 0, .reserved4 = 0}, .data_alignment = __alignof__(struct nanos_args_2_t), .num_copies = 2, .num_devices = 1, .num_dimensions = 2, .description = 0}, .devices = {[0] = {.factory = &nanos_smp_factory, .arg = &smp_ol_kad_eval_marked_2_args}}};
                                  nanos_wd_dyn_props.tie_to = 0;
                                  nanos_wd_dyn_props.priority = 0;
                                  nanos_wd_dyn_props.flags.is_final = 0;
                                  nanos_wd_dyn_props.flags.is_implicit = 0;
                                  nanos_wd_dyn_props.flags.is_recover = 0;
                                  ol_args = (struct nanos_args_2_t *)0;
                                  nanos_wd_t nanos_wd_ = (nanos_wd_t)0;
                                  nanos_copy_data_t *ol_copy_data = (nanos_copy_data_t *)0;
                                  nanos_region_dimension_internal_t *ol_copy_dimensions = (nanos_region_dimension_internal_t *)0;
                                  nanos_err = nanos_create_wd_compact(&nanos_wd_, &nanos_wd_const_data.base, &nanos_wd_dyn_props, sizeof(struct nanos_args_2_t), (void **)&ol_args, nanos_current_wd(), &ol_copy_data, &ol_copy_dimensions);
                                  if (nanos_err != NANOS_OK)
                                    {
                                      nanos_handle_error(nanos_err);
                                    }
                                  dimensions_5[0].size = 1L * sizeof(kad_node_t *);
                                  dimensions_5[0].lower_bound = (end[node - layers] - 0L) * sizeof(kad_node_t *);
                                  dimensions_5[0].accessed_length = (end[node - layers] - 0L - (end[node - layers] - 0L) + 1) * sizeof(kad_node_t *);
                                  dependences[0].address = (void *)a;
                                  dependences[0].offset = 8L * (((end[node - layers]) - (0L)));
                                  dependences[0].dimensions = dimensions_5;
                                  dependences[0].flags.input = 1;
                                  dependences[0].flags.output = 0;
                                  dependences[0].flags.can_rename = 0;
                                  dependences[0].flags.concurrent = 0;
                                  dependences[0].flags.commutative = 0;
                                  dependences[0].dimension_count = 1;
                                  dimensions_6[0].size = 1L * sizeof(kad_node_t *);
                                  dimensions_6[0].lower_bound = (e - 0L) * sizeof(kad_node_t *);
                                  dimensions_6[0].accessed_length = (e - 0L - (e - 0L) + 1) * sizeof(kad_node_t *);
                                  dependences[1].address = (void *)a;
                                  dependences[1].offset = 8L * (((e) - (0L)));
                                  dependences[1].dimensions = dimensions_6;
                                  dependences[1].flags.input = 0;
                                  dependences[1].flags.output = 1;
                                  dependences[1].flags.can_rename = 0;
                                  dependences[1].flags.concurrent = 0;
                                  dependences[1].flags.commutative = 0;
                                  dependences[1].dimension_count = 1;
                                  if (nanos_wd_ != (nanos_wd_t)0)
                                    {
                                      (*ol_args).a = a;
                                      (*ol_args).s = s;
                                      (*ol_args).e = e;
                                      (*ol_args).end = end;
                                      (*ol_args).node = node;
                                      (*ol_args).layers = layers;
                                      ol_copy_dimensions[0 + 0].size = 1L * sizeof(kad_node_t *);
                                      ol_copy_dimensions[0 + 0].lower_bound = (end[node - layers] - 0L) * sizeof(kad_node_t *);
                                      ol_copy_dimensions[0 + 0].accessed_length = (end[node - layers] - 0L - (end[node - layers] - 0L) + 1) * sizeof(kad_node_t *);
                                      ol_copy_data[0].sharing = NANOS_SHARED;
                                      ol_copy_data[0].address = (void *)a;
                                      ol_copy_data[0].flags.input = 1;
                                      ol_copy_data[0].flags.output = 0;
                                      ol_copy_data[0].dimension_count = (short int)1;
                                      ol_copy_data[0].dimensions = &ol_copy_dimensions[0];
                                      ol_copy_data[0].offset = 8L * (((end[node - layers]) - (0L)));
                                      ol_copy_dimensions[1 + 0].size = 1L * sizeof(kad_node_t *);
                                      ol_copy_dimensions[1 + 0].lower_bound = (e - 0L) * sizeof(kad_node_t *);
                                      ol_copy_dimensions[1 + 0].accessed_length = (e - 0L - (e - 0L) + 1) * sizeof(kad_node_t *);
                                      ol_copy_data[1].sharing = NANOS_SHARED;
                                      ol_copy_data[1].address = (void *)a;
                                      ol_copy_data[1].flags.input = 0;
                                      ol_copy_data[1].flags.output = 1;
                                      ol_copy_data[1].dimension_count = (short int)1;
                                      ol_copy_data[1].dimensions = &ol_copy_dimensions[1];
                                      ol_copy_data[1].offset = 8L * (((e) - (0L)));
                                      nanos_err = nanos_set_translate_function(nanos_wd_, (nanos_translate_args_t)nanos_xlate_fun_kautodiffparallelc_2);
                                      if (nanos_err != NANOS_OK)
                                        {
                                          nanos_handle_error(nanos_err);
                                        }
                                      nanos_err = nanos_submit(nanos_wd_, 2, &dependences[0], (nanos_team_t)0);
                                      if (nanos_err != NANOS_OK)
                                        {
                                          nanos_handle_error(nanos_err);
                                        }
                                    }
                                  else
                                    {
                                      nanos_region_dimension_internal_t imm_copy_dimensions[2L];
                                      nanos_copy_data_t imm_copy_data[2L];
                                      imm_args.a = a;
                                      imm_args.s = s;
                                      imm_args.e = e;
                                      imm_args.end = end;
                                      imm_args.node = node;
                                      imm_args.layers = layers;
                                      imm_copy_dimensions[0 + 0].size = 1L * sizeof(kad_node_t *);
                                      imm_copy_dimensions[0 + 0].lower_bound = (end[node - layers] - 0L) * sizeof(kad_node_t *);
                                      imm_copy_dimensions[0 + 0].accessed_length = (end[node - layers] - 0L - (end[node - layers] - 0L) + 1) * sizeof(kad_node_t *);
                                      imm_copy_data[0].sharing = NANOS_SHARED;
                                      imm_copy_data[0].address = (void *)a;
                                      imm_copy_data[0].flags.input = 1;
                                      imm_copy_data[0].flags.output = 0;
                                      imm_copy_data[0].dimension_count = (short int)1;
                                      imm_copy_data[0].dimensions = &imm_copy_dimensions[0];
                                      imm_copy_data[0].offset = 8L * (((end[node - layers]) - (0L)));
                                      imm_copy_dimensions[1 + 0].size = 1L * sizeof(kad_node_t *);
                                      imm_copy_dimensions[1 + 0].lower_bound = (e - 0L) * sizeof(kad_node_t *);
                                      imm_copy_dimensions[1 + 0].accessed_length = (e - 0L - (e - 0L) + 1) * sizeof(kad_node_t *);
                                      imm_copy_data[1].sharing = NANOS_SHARED;
                                      imm_copy_data[1].address = (void *)a;
                                      imm_copy_data[1].flags.input = 0;
                                      imm_copy_data[1].flags.output = 1;
                                      imm_copy_data[1].dimension_count = (short int)1;
                                      imm_copy_data[1].dimensions = &imm_copy_dimensions[1];
                                      imm_copy_data[1].offset = 8L * (((e) - (0L)));
                                      nanos_err = nanos_create_wd_and_run_compact(&nanos_wd_const_data.base, &nanos_wd_dyn_props, sizeof(struct nanos_args_2_t), &imm_args, 2, &dependences[0], imm_copy_data, imm_copy_dimensions, (nanos_translate_args_t)nanos_xlate_fun_kautodiffparallelc_2);
                                      if (nanos_err != NANOS_OK)
                                        {
                                          nanos_handle_error(nanos_err);
                                        }
                                    }
                                }
                              }
                          }
                        }
                      else
                        {
                          if (l == 0 && u == 0)
                            {
                              {
                                _Bool mcc_is_in_final;
                                nanos_err_t mcc_err_in_final = nanos_in_final(&mcc_is_in_final);
                                if (mcc_err_in_final != NANOS_OK)
                                  {
                                    nanos_handle_error(mcc_err_in_final);
                                  }
                                if (mcc_is_in_final)
                                  {
                                    ompss_forward_prop(s, e, a, 2);
                                  }
                                else
                                  {
                                    {
                                      nanos_wd_dyn_props_t nanos_wd_dyn_props;
                                      struct nanos_args_3_t *ol_args;
                                      nanos_err_t nanos_err;
                                      struct nanos_args_3_t imm_args;
                                      nanos_region_dimension_t dimensions_7[1L];
                                      nanos_data_access_t dependences[2L];
                                      nanos_region_dimension_t dimensions_8[1L];
                                      static nanos_smp_args_t smp_ol_kad_eval_marked_3_args = {.outline = (void (*)(void *))(void (*)(struct nanos_args_3_t *))&smp_ol_kad_eval_marked_3};
                                      static struct nanos_const_wd_definition_1 nanos_wd_const_data = {.base = {.props = {.mandatory_creation = 0, .tied = 0, .clear_chunk = 0, .reserved0 = 0, .reserved1 = 0, .reserved2 = 0, .reserved3 = 0, .reserved4 = 0}, .data_alignment = __alignof__(struct nanos_args_3_t), .num_copies = 2, .num_devices = 1, .num_dimensions = 2, .description = 0}, .devices = {[0] = {.factory = &nanos_smp_factory, .arg = &smp_ol_kad_eval_marked_3_args}}};
                                      nanos_wd_dyn_props.tie_to = 0;
                                      nanos_wd_dyn_props.priority = 0;
                                      nanos_wd_dyn_props.flags.is_final = 0;
                                      nanos_wd_dyn_props.flags.is_implicit = 0;
                                      nanos_wd_dyn_props.flags.is_recover = 0;
                                      ol_args = (struct nanos_args_3_t *)0;
                                      nanos_wd_t nanos_wd_ = (nanos_wd_t)0;
                                      nanos_copy_data_t *ol_copy_data = (nanos_copy_data_t *)0;
                                      nanos_region_dimension_internal_t *ol_copy_dimensions = (nanos_region_dimension_internal_t *)0;
                                      nanos_err = nanos_create_wd_compact(&nanos_wd_, &nanos_wd_const_data.base, &nanos_wd_dyn_props, sizeof(struct nanos_args_3_t), (void **)&ol_args, nanos_current_wd(), &ol_copy_data, &ol_copy_dimensions);
                                      if (nanos_err != NANOS_OK)
                                        {
                                          nanos_handle_error(nanos_err);
                                        }
                                      dimensions_7[0].size = 1L * sizeof(kad_node_t *);
                                      dimensions_7[0].lower_bound = (s - 0L) * sizeof(kad_node_t *);
                                      dimensions_7[0].accessed_length = (s - 0L - (s - 0L) + 1) * sizeof(kad_node_t *);
                                      dependences[0].address = (void *)a;
                                      dependences[0].offset = 8L * (((s) - (0L)));
                                      dependences[0].dimensions = dimensions_7;
                                      dependences[0].flags.input = 1;
                                      dependences[0].flags.output = 0;
                                      dependences[0].flags.can_rename = 0;
                                      dependences[0].flags.concurrent = 0;
                                      dependences[0].flags.commutative = 0;
                                      dependences[0].dimension_count = 1;
                                      dimensions_8[0].size = 1L * sizeof(kad_node_t *);
                                      dimensions_8[0].lower_bound = (e - 0L) * sizeof(kad_node_t *);
                                      dimensions_8[0].accessed_length = (e - 0L - (e - 0L) + 1) * sizeof(kad_node_t *);
                                      dependences[1].address = (void *)a;
                                      dependences[1].offset = 8L * (((e) - (0L)));
                                      dependences[1].dimensions = dimensions_8;
                                      dependences[1].flags.input = 0;
                                      dependences[1].flags.output = 1;
                                      dependences[1].flags.can_rename = 0;
                                      dependences[1].flags.concurrent = 0;
                                      dependences[1].flags.commutative = 0;
                                      dependences[1].dimension_count = 1;
                                      if (nanos_wd_ != (nanos_wd_t)0)
                                        {
                                          (*ol_args).a = a;
                                          (*ol_args).s = s;
                                          (*ol_args).e = e;
                                          ol_copy_dimensions[0 + 0].size = 1L * sizeof(kad_node_t *);
                                          ol_copy_dimensions[0 + 0].lower_bound = (s - 0L) * sizeof(kad_node_t *);
                                          ol_copy_dimensions[0 + 0].accessed_length = (s - 0L - (s - 0L) + 1) * sizeof(kad_node_t *);
                                          ol_copy_data[0].sharing = NANOS_SHARED;
                                          ol_copy_data[0].address = (void *)a;
                                          ol_copy_data[0].flags.input = 1;
                                          ol_copy_data[0].flags.output = 0;
                                          ol_copy_data[0].dimension_count = (short int)1;
                                          ol_copy_data[0].dimensions = &ol_copy_dimensions[0];
                                          ol_copy_data[0].offset = 8L * (((s) - (0L)));
                                          ol_copy_dimensions[1 + 0].size = 1L * sizeof(kad_node_t *);
                                          ol_copy_dimensions[1 + 0].lower_bound = (e - 0L) * sizeof(kad_node_t *);
                                          ol_copy_dimensions[1 + 0].accessed_length = (e - 0L - (e - 0L) + 1) * sizeof(kad_node_t *);
                                          ol_copy_data[1].sharing = NANOS_SHARED;
                                          ol_copy_data[1].address = (void *)a;
                                          ol_copy_data[1].flags.input = 0;
                                          ol_copy_data[1].flags.output = 1;
                                          ol_copy_data[1].dimension_count = (short int)1;
                                          ol_copy_data[1].dimensions = &ol_copy_dimensions[1];
                                          ol_copy_data[1].offset = 8L * (((e) - (0L)));
                                          nanos_err = nanos_set_translate_function(nanos_wd_, (nanos_translate_args_t)nanos_xlate_fun_kautodiffparallelc_3);
                                          if (nanos_err != NANOS_OK)
                                            {
                                              nanos_handle_error(nanos_err);
                                            }
                                          nanos_err = nanos_submit(nanos_wd_, 2, &dependences[0], (nanos_team_t)0);
                                          if (nanos_err != NANOS_OK)
                                            {
                                              nanos_handle_error(nanos_err);
                                            }
                                        }
                                      else
                                        {
                                          nanos_region_dimension_internal_t imm_copy_dimensions[2L];
                                          nanos_copy_data_t imm_copy_data[2L];
                                          imm_args.a = a;
                                          imm_args.s = s;
                                          imm_args.e = e;
                                          imm_copy_dimensions[0 + 0].size = 1L * sizeof(kad_node_t *);
                                          imm_copy_dimensions[0 + 0].lower_bound = (s - 0L) * sizeof(kad_node_t *);
                                          imm_copy_dimensions[0 + 0].accessed_length = (s - 0L - (s - 0L) + 1) * sizeof(kad_node_t *);
                                          imm_copy_data[0].sharing = NANOS_SHARED;
                                          imm_copy_data[0].address = (void *)a;
                                          imm_copy_data[0].flags.input = 1;
                                          imm_copy_data[0].flags.output = 0;
                                          imm_copy_data[0].dimension_count = (short int)1;
                                          imm_copy_data[0].dimensions = &imm_copy_dimensions[0];
                                          imm_copy_data[0].offset = 8L * (((s) - (0L)));
                                          imm_copy_dimensions[1 + 0].size = 1L * sizeof(kad_node_t *);
                                          imm_copy_dimensions[1 + 0].lower_bound = (e - 0L) * sizeof(kad_node_t *);
                                          imm_copy_dimensions[1 + 0].accessed_length = (e - 0L - (e - 0L) + 1) * sizeof(kad_node_t *);
                                          imm_copy_data[1].sharing = NANOS_SHARED;
                                          imm_copy_data[1].address = (void *)a;
                                          imm_copy_data[1].flags.input = 0;
                                          imm_copy_data[1].flags.output = 1;
                                          imm_copy_data[1].dimension_count = (short int)1;
                                          imm_copy_data[1].dimensions = &imm_copy_dimensions[1];
                                          imm_copy_data[1].offset = 8L * (((e) - (0L)));
                                          nanos_err = nanos_create_wd_and_run_compact(&nanos_wd_const_data.base, &nanos_wd_dyn_props, sizeof(struct nanos_args_3_t), &imm_args, 2, &dependences[0], imm_copy_data, imm_copy_dimensions, (nanos_translate_args_t)nanos_xlate_fun_kautodiffparallelc_3);
                                          if (nanos_err != NANOS_OK)
                                            {
                                              nanos_handle_error(nanos_err);
                                            }
                                        }
                                    }
                                  }
                              }
                            }
                        }
                    }
                }
            }
        }
      {
        nanos_err_t nanos_err;
        nanos_wd_t nanos_wd_ = nanos_current_wd();
        nanos_err = nanos_wg_wait_completion(nanos_wd_, 0);
        if (nanos_err != NANOS_OK)
          {
            nanos_handle_error(nanos_err);
          }
      }
      kad_op_list[(*a[n - 1]).op](a[n - 1], 2);
      for (i = 0; i < n;  ++i)
        {
          (*a[i]).tmp = 0;
        }
      {
        nanos_err_t nanos_err;
        nanos_wd_t nanos_wd_ = nanos_current_wd();
        nanos_err = nanos_wg_wait_completion(nanos_wd_, 0);
        if (nanos_err != NANOS_OK)
          {
            nanos_handle_error(nanos_err);
          }
      }
    }
}
const float *kad_eval_at(int n, kad_node_t **a, int from, int layers, int ulen, int *start, int *end, int parallel)
{
  int i;
  if (from < 0 || from >= n)
    {
      from = n - 1;
    }
  for (i = 0; i < n;  ++i)
    {
      (*a[i]).tmp = i == from;
    }
  kad_eval_marked(n, a, layers, ulen, start, end, parallel);
  return (*a[from]).x;
}
struct  nanos_args_4_t
{
  kad_node_t **a;
  int from;
  kad_op_f (*kad_op_list)[64L];
};
static void nanos_xlate_fun_kautodiffparallelc_4(struct nanos_args_4_t *const arg, void *wd)
{
  {
    void *device_base_address;
    nanos_err_t nanos_err;
    device_base_address = 0;
    nanos_err = nanos_get_addr(0, &device_base_address, wd);
    if (nanos_err != NANOS_OK)
      {
        nanos_handle_error(nanos_err);
      }
    (*arg).a = (kad_node_t **)device_base_address;
  }
}
struct  nanos_args_5_t
{
  kad_node_t **a;
  int e;
  int s;
  int i_d;
};
static void nanos_xlate_fun_kautodiffparallelc_5(struct nanos_args_5_t *const arg, void *wd)
{
  {
    void *device_base_address;
    nanos_err_t nanos_err;
    device_base_address = 0;
    nanos_err = nanos_get_addr(0, &device_base_address, wd);
    if (nanos_err != NANOS_OK)
      {
        nanos_handle_error(nanos_err);
      }
    (*arg).a = (kad_node_t **)device_base_address;
  }
}
struct  nanos_args_6_t
{
  kad_node_t **a;
  int e;
  int s;
  int from;
  int i_d;
};
static void nanos_xlate_fun_kautodiffparallelc_6(struct nanos_args_6_t *const arg, void *wd)
{
  {
    void *device_base_address;
    nanos_err_t nanos_err;
    device_base_address = 0;
    nanos_err = nanos_get_addr(0, &device_base_address, wd);
    if (nanos_err != NANOS_OK)
      {
        nanos_handle_error(nanos_err);
      }
    (*arg).a = (kad_node_t **)device_base_address;
  }
}
struct  nanos_args_7_t
{
  kad_node_t **a;
  int e;
  int s;
  int i_d;
};
static void nanos_xlate_fun_kautodiffparallelc_7(struct nanos_args_7_t *const arg, void *wd)
{
  {
    void *device_base_address;
    nanos_err_t nanos_err;
    device_base_address = 0;
    nanos_err = nanos_get_addr(0, &device_base_address, wd);
    if (nanos_err != NANOS_OK)
      {
        nanos_handle_error(nanos_err);
      }
    (*arg).a = (kad_node_t **)device_base_address;
  }
}
struct  nanos_args_8_t
{
  kad_node_t **a;
  int e;
  int s;
};
static void nanos_xlate_fun_kautodiffparallelc_8(struct nanos_args_8_t *const arg, void *wd)
{
  {
    void *device_base_address;
    nanos_err_t nanos_err;
    device_base_address = 0;
    nanos_err = nanos_get_addr(0, &device_base_address, wd);
    if (nanos_err != NANOS_OK)
      {
        nanos_handle_error(nanos_err);
      }
    (*arg).a = (kad_node_t **)device_base_address;
  }
}
struct  nanos_args_9_t
{
  kad_node_t **a;
  int e;
  int s;
  int from;
};
static void nanos_xlate_fun_kautodiffparallelc_9(struct nanos_args_9_t *const arg, void *wd)
{
  {
    void *device_base_address;
    nanos_err_t nanos_err;
    device_base_address = 0;
    nanos_err = nanos_get_addr(0, &device_base_address, wd);
    if (nanos_err != NANOS_OK)
      {
        nanos_handle_error(nanos_err);
      }
    (*arg).a = (kad_node_t **)device_base_address;
  }
}
struct  nanos_args_10_t
{
  kad_node_t **a;
  int e;
  int s;
};
static void nanos_xlate_fun_kautodiffparallelc_10(struct nanos_args_10_t *const arg, void *wd)
{
  {
    void *device_base_address;
    nanos_err_t nanos_err;
    device_base_address = 0;
    nanos_err = nanos_get_addr(0, &device_base_address, wd);
    if (nanos_err != NANOS_OK)
      {
        nanos_handle_error(nanos_err);
      }
    (*arg).a = (kad_node_t **)device_base_address;
  }
}
struct  nanos_args_11_t
{
  kad_node_t **a;
  int e;
  int s;
  int i_d;
};
static void nanos_xlate_fun_kautodiffparallelc_11(struct nanos_args_11_t *const arg, void *wd)
{
  {
    void *device_base_address;
    nanos_err_t nanos_err;
    device_base_address = 0;
    nanos_err = nanos_get_addr(0, &device_base_address, wd);
    if (nanos_err != NANOS_OK)
      {
        nanos_handle_error(nanos_err);
      }
    (*arg).a = (kad_node_t **)device_base_address;
  }
}
struct  nanos_args_12_t
{
  kad_node_t **a;
  int e;
  int s;
  int from;
  int i_d;
};
static void nanos_xlate_fun_kautodiffparallelc_12(struct nanos_args_12_t *const arg, void *wd)
{
  {
    void *device_base_address;
    nanos_err_t nanos_err;
    device_base_address = 0;
    nanos_err = nanos_get_addr(0, &device_base_address, wd);
    if (nanos_err != NANOS_OK)
      {
        nanos_handle_error(nanos_err);
      }
    (*arg).a = (kad_node_t **)device_base_address;
  }
}
struct  nanos_args_13_t
{
  kad_node_t **a;
  int e;
  int s;
  int i_d;
};
static void nanos_xlate_fun_kautodiffparallelc_13(struct nanos_args_13_t *const arg, void *wd)
{
  {
    void *device_base_address;
    nanos_err_t nanos_err;
    device_base_address = 0;
    nanos_err = nanos_get_addr(0, &device_base_address, wd);
    if (nanos_err != NANOS_OK)
      {
        nanos_handle_error(nanos_err);
      }
    (*arg).a = (kad_node_t **)device_base_address;
  }
}
extern void *memset(void *__s, int __c, size_t __n) __attribute__((__nothrow__)) __attribute__((__leaf__)) __attribute__((__nonnull__(1)));
static void smp_ol_kad_grad_4(struct nanos_args_4_t *const args);
static void smp_ol_kad_grad_5(struct nanos_args_5_t *const args);
static void smp_ol_kad_grad_6(struct nanos_args_6_t *const args);
static void smp_ol_kad_grad_7(struct nanos_args_7_t *const args);
static void smp_ol_kad_grad_8(struct nanos_args_8_t *const args);
static void smp_ol_kad_grad_9(struct nanos_args_9_t *const args);
static void smp_ol_kad_grad_10(struct nanos_args_10_t *const args);
static void smp_ol_kad_grad_11(struct nanos_args_11_t *const args);
static void smp_ol_kad_grad_12(struct nanos_args_12_t *const args);
static void smp_ol_kad_grad_13(struct nanos_args_13_t *const args);
void kad_grad(int n, kad_node_t **a, int from, int layers, int ulen, int *start, int *end, int parallel)
{
  int i;
  int u;
  int l;
  static const char __MERCURIUM_PRETTY_FUNCTION__[68L] = "void kad_grad(int, kad_node_t **, int, int, int, int *, int *, int)";
  if (from < 0 || from >= n)
    {
      from = n - 1;
    }
  (*a[from]).n_d == 0 ? (void)0 : __assert_fail("a[from]->n_d == 0", "kautodiff_parallel.c", 640, __MERCURIUM_PRETTY_FUNCTION__);
  for (i = 0; i < n;  ++i)
    {
      (*a[i]).tmp = i == from;
    }
  kad_propagate_marks(n, a);
  for (i = 0; i <= from;  ++i)
    {
      if ((*a[i]).g && (*a[i]).tmp > 0)
        {
          memset((*a[i]).g, 0, kad_len(a[i]) * sizeof(float));
        }
    }
  if (layers == 1 || !parallel)
    {
      for ((i = from, (*a[i]).g[0] = 1.000000000000000000000000e+00f); i >= 0;  --i)
        {
          if ((*a[i]).n_child && (*a[i]).tmp > 0)
            {
              kad_op_list[(*a[i]).op](a[i], 3);
            }
        }
      for (i = 0; i <= from;  ++i)
        {
          (*a[i]).tmp = 0;
        }
    }
  else
    {
      {
        _Bool mcc_is_in_final;
        nanos_err_t mcc_err_in_final = nanos_in_final(&mcc_is_in_final);
        if (mcc_err_in_final != NANOS_OK)
          {
            nanos_handle_error(mcc_err_in_final);
          }
        if (mcc_is_in_final)
          {
            {
              (*a[from]).g[0] = 1.000000000000000000000000e+00f;
              kad_op_list[(*a[from]).op](a[from], 3);
            }
          }
        else
          {
            {
              nanos_wd_dyn_props_t nanos_wd_dyn_props;
              struct nanos_args_4_t *ol_args;
              nanos_err_t nanos_err;
              struct nanos_args_4_t imm_args;
              nanos_region_dimension_t dimensions_9[1L];
              nanos_data_access_t dependences[1L];
              static nanos_smp_args_t smp_ol_kad_grad_4_args = {.outline = (void (*)(void *))(void (*)(struct nanos_args_4_t *))&smp_ol_kad_grad_4};
              static struct nanos_const_wd_definition_1 nanos_wd_const_data = {.base = {.props = {.mandatory_creation = 0, .tied = 0, .clear_chunk = 0, .reserved0 = 0, .reserved1 = 0, .reserved2 = 0, .reserved3 = 0, .reserved4 = 0}, .data_alignment = __alignof__(struct nanos_args_4_t), .num_copies = 1, .num_devices = 1, .num_dimensions = 1, .description = 0}, .devices = {[0] = {.factory = &nanos_smp_factory, .arg = &smp_ol_kad_grad_4_args}}};
              nanos_wd_dyn_props.tie_to = 0;
              nanos_wd_dyn_props.priority = 0;
              nanos_wd_dyn_props.flags.is_final = 0;
              nanos_wd_dyn_props.flags.is_implicit = 0;
              nanos_wd_dyn_props.flags.is_recover = 0;
              ol_args = (struct nanos_args_4_t *)0;
              nanos_wd_t nanos_wd_ = (nanos_wd_t)0;
              nanos_copy_data_t *ol_copy_data = (nanos_copy_data_t *)0;
              nanos_region_dimension_internal_t *ol_copy_dimensions = (nanos_region_dimension_internal_t *)0;
              nanos_err = nanos_create_wd_compact(&nanos_wd_, &nanos_wd_const_data.base, &nanos_wd_dyn_props, sizeof(struct nanos_args_4_t), (void **)&ol_args, nanos_current_wd(), &ol_copy_data, &ol_copy_dimensions);
              if (nanos_err != NANOS_OK)
                {
                  nanos_handle_error(nanos_err);
                }
              dimensions_9[0].size = 1L * sizeof(kad_node_t *);
              dimensions_9[0].lower_bound = (from - 0L) * sizeof(kad_node_t *);
              dimensions_9[0].accessed_length = (from - 0L - (from - 0L) + 1) * sizeof(kad_node_t *);
              dependences[0].address = (void *)a;
              dependences[0].offset = 8L * (((from) - (0L)));
              dependences[0].dimensions = dimensions_9;
              dependences[0].flags.input = 0;
              dependences[0].flags.output = 1;
              dependences[0].flags.can_rename = 0;
              dependences[0].flags.concurrent = 0;
              dependences[0].flags.commutative = 0;
              dependences[0].dimension_count = 1;
              if (nanos_wd_ != (nanos_wd_t)0)
                {
                  (*ol_args).a = a;
                  (*ol_args).from = from;
                  (*ol_args).kad_op_list = &kad_op_list;
                  ol_copy_dimensions[0 + 0].size = 1L * sizeof(kad_node_t *);
                  ol_copy_dimensions[0 + 0].lower_bound = (from - 0L) * sizeof(kad_node_t *);
                  ol_copy_dimensions[0 + 0].accessed_length = (from - 0L - (from - 0L) + 1) * sizeof(kad_node_t *);
                  ol_copy_data[0].sharing = NANOS_SHARED;
                  ol_copy_data[0].address = (void *)a;
                  ol_copy_data[0].flags.input = 0;
                  ol_copy_data[0].flags.output = 1;
                  ol_copy_data[0].dimension_count = (short int)1;
                  ol_copy_data[0].dimensions = &ol_copy_dimensions[0];
                  ol_copy_data[0].offset = 8L * (((from) - (0L)));
                  nanos_err = nanos_set_translate_function(nanos_wd_, (nanos_translate_args_t)nanos_xlate_fun_kautodiffparallelc_4);
                  if (nanos_err != NANOS_OK)
                    {
                      nanos_handle_error(nanos_err);
                    }
                  nanos_err = nanos_submit(nanos_wd_, 1, &dependences[0], (nanos_team_t)0);
                  if (nanos_err != NANOS_OK)
                    {
                      nanos_handle_error(nanos_err);
                    }
                }
              else
                {
                  nanos_region_dimension_internal_t imm_copy_dimensions[1L];
                  nanos_copy_data_t imm_copy_data[1L];
                  imm_args.a = a;
                  imm_args.from = from;
                  imm_args.kad_op_list = &kad_op_list;
                  imm_copy_dimensions[0 + 0].size = 1L * sizeof(kad_node_t *);
                  imm_copy_dimensions[0 + 0].lower_bound = (from - 0L) * sizeof(kad_node_t *);
                  imm_copy_dimensions[0 + 0].accessed_length = (from - 0L - (from - 0L) + 1) * sizeof(kad_node_t *);
                  imm_copy_data[0].sharing = NANOS_SHARED;
                  imm_copy_data[0].address = (void *)a;
                  imm_copy_data[0].flags.input = 0;
                  imm_copy_data[0].flags.output = 1;
                  imm_copy_data[0].dimension_count = (short int)1;
                  imm_copy_data[0].dimensions = &imm_copy_dimensions[0];
                  imm_copy_data[0].offset = 8L * (((from) - (0L)));
                  nanos_err = nanos_create_wd_and_run_compact(&nanos_wd_const_data.base, &nanos_wd_dyn_props, sizeof(struct nanos_args_4_t), &imm_args, 1, &dependences[0], imm_copy_data, imm_copy_dimensions, (nanos_translate_args_t)nanos_xlate_fun_kautodiffparallelc_4);
                  if (nanos_err != NANOS_OK)
                    {
                      nanos_handle_error(nanos_err);
                    }
                }
            }
          }
      }
      int node = ulen * layers - 1;
      for (u = ulen - 1; u >= 0;  --u)
        {
          for (l = layers - 1; l >= 0;  --l)
            {
              int s = start[u * layers + l];
              int e = end[u * layers + l];
              if (0 < u && u < ulen - 1)
                {
                  if (0 < l && l < layers - 1)
                    {
                      int i_d = start[node + layers];
                      {
                        _Bool mcc_is_in_final;
                        nanos_err_t mcc_err_in_final = nanos_in_final(&mcc_is_in_final);
                        if (mcc_err_in_final != NANOS_OK)
                          {
                            nanos_handle_error(mcc_err_in_final);
                          }
                        if (mcc_is_in_final)
                          {
                            ompss_backward_prop(e, s, a, 3);
                          }
                        else
                          {
                            {
                              nanos_wd_dyn_props_t nanos_wd_dyn_props;
                              struct nanos_args_5_t *ol_args;
                              nanos_err_t nanos_err;
                              struct nanos_args_5_t imm_args;
                              nanos_region_dimension_t dimensions_10[1L];
                              nanos_data_access_t dependences[3L];
                              nanos_region_dimension_t dimensions_11[1L];
                              nanos_region_dimension_t dimensions_12[1L];
                              static nanos_smp_args_t smp_ol_kad_grad_5_args = {.outline = (void (*)(void *))(void (*)(struct nanos_args_5_t *))&smp_ol_kad_grad_5};
                              static struct nanos_const_wd_definition_1 nanos_wd_const_data = {.base = {.props = {.mandatory_creation = 0, .tied = 0, .clear_chunk = 0, .reserved0 = 0, .reserved1 = 0, .reserved2 = 0, .reserved3 = 0, .reserved4 = 0}, .data_alignment = __alignof__(struct nanos_args_5_t), .num_copies = 3, .num_devices = 1, .num_dimensions = 3, .description = 0}, .devices = {[0] = {.factory = &nanos_smp_factory, .arg = &smp_ol_kad_grad_5_args}}};
                              nanos_wd_dyn_props.tie_to = 0;
                              nanos_wd_dyn_props.priority = 0;
                              nanos_wd_dyn_props.flags.is_final = 0;
                              nanos_wd_dyn_props.flags.is_implicit = 0;
                              nanos_wd_dyn_props.flags.is_recover = 0;
                              ol_args = (struct nanos_args_5_t *)0;
                              nanos_wd_t nanos_wd_ = (nanos_wd_t)0;
                              nanos_copy_data_t *ol_copy_data = (nanos_copy_data_t *)0;
                              nanos_region_dimension_internal_t *ol_copy_dimensions = (nanos_region_dimension_internal_t *)0;
                              nanos_err = nanos_create_wd_compact(&nanos_wd_, &nanos_wd_const_data.base, &nanos_wd_dyn_props, sizeof(struct nanos_args_5_t), (void **)&ol_args, nanos_current_wd(), &ol_copy_data, &ol_copy_dimensions);
                              if (nanos_err != NANOS_OK)
                                {
                                  nanos_handle_error(nanos_err);
                                }
                              dimensions_10[0].size = 1L * sizeof(kad_node_t *);
                              dimensions_10[0].lower_bound = (e + 1 - 0L) * sizeof(kad_node_t *);
                              dimensions_10[0].accessed_length = (e + 1 - 0L - (e + 1 - 0L) + 1) * sizeof(kad_node_t *);
                              dependences[0].address = (void *)a;
                              dependences[0].offset = 8L * (((e + 1) - (0L)));
                              dependences[0].dimensions = dimensions_10;
                              dependences[0].flags.input = 1;
                              dependences[0].flags.output = 0;
                              dependences[0].flags.can_rename = 0;
                              dependences[0].flags.concurrent = 0;
                              dependences[0].flags.commutative = 0;
                              dependences[0].dimension_count = 1;
                              dimensions_11[0].size = 1L * sizeof(kad_node_t *);
                              dimensions_11[0].lower_bound = (i_d - 0L) * sizeof(kad_node_t *);
                              dimensions_11[0].accessed_length = (i_d - 0L - (i_d - 0L) + 1) * sizeof(kad_node_t *);
                              dependences[1].address = (void *)a;
                              dependences[1].offset = 8L * (((i_d) - (0L)));
                              dependences[1].dimensions = dimensions_11;
                              dependences[1].flags.input = 1;
                              dependences[1].flags.output = 0;
                              dependences[1].flags.can_rename = 0;
                              dependences[1].flags.concurrent = 0;
                              dependences[1].flags.commutative = 0;
                              dependences[1].dimension_count = 1;
                              dimensions_12[0].size = 1L * sizeof(kad_node_t *);
                              dimensions_12[0].lower_bound = (s - 0L) * sizeof(kad_node_t *);
                              dimensions_12[0].accessed_length = (s - 0L - (s - 0L) + 1) * sizeof(kad_node_t *);
                              dependences[2].address = (void *)a;
                              dependences[2].offset = 8L * (((s) - (0L)));
                              dependences[2].dimensions = dimensions_12;
                              dependences[2].flags.input = 0;
                              dependences[2].flags.output = 1;
                              dependences[2].flags.can_rename = 0;
                              dependences[2].flags.concurrent = 0;
                              dependences[2].flags.commutative = 0;
                              dependences[2].dimension_count = 1;
                              if (nanos_wd_ != (nanos_wd_t)0)
                                {
                                  (*ol_args).a = a;
                                  (*ol_args).e = e;
                                  (*ol_args).s = s;
                                  (*ol_args).i_d = i_d;
                                  ol_copy_dimensions[0 + 0].size = 1L * sizeof(kad_node_t *);
                                  ol_copy_dimensions[0 + 0].lower_bound = (e + 1 - 0L) * sizeof(kad_node_t *);
                                  ol_copy_dimensions[0 + 0].accessed_length = (e + 1 - 0L - (e + 1 - 0L) + 1) * sizeof(kad_node_t *);
                                  ol_copy_data[0].sharing = NANOS_SHARED;
                                  ol_copy_data[0].address = (void *)a;
                                  ol_copy_data[0].flags.input = 1;
                                  ol_copy_data[0].flags.output = 0;
                                  ol_copy_data[0].dimension_count = (short int)1;
                                  ol_copy_data[0].dimensions = &ol_copy_dimensions[0];
                                  ol_copy_data[0].offset = 8L * (((e + 1) - (0L)));
                                  ol_copy_dimensions[1 + 0].size = 1L * sizeof(kad_node_t *);
                                  ol_copy_dimensions[1 + 0].lower_bound = (i_d - 0L) * sizeof(kad_node_t *);
                                  ol_copy_dimensions[1 + 0].accessed_length = (i_d - 0L - (i_d - 0L) + 1) * sizeof(kad_node_t *);
                                  ol_copy_data[1].sharing = NANOS_SHARED;
                                  ol_copy_data[1].address = (void *)a;
                                  ol_copy_data[1].flags.input = 1;
                                  ol_copy_data[1].flags.output = 0;
                                  ol_copy_data[1].dimension_count = (short int)1;
                                  ol_copy_data[1].dimensions = &ol_copy_dimensions[1];
                                  ol_copy_data[1].offset = 8L * (((i_d) - (0L)));
                                  ol_copy_dimensions[2 + 0].size = 1L * sizeof(kad_node_t *);
                                  ol_copy_dimensions[2 + 0].lower_bound = (s - 0L) * sizeof(kad_node_t *);
                                  ol_copy_dimensions[2 + 0].accessed_length = (s - 0L - (s - 0L) + 1) * sizeof(kad_node_t *);
                                  ol_copy_data[2].sharing = NANOS_SHARED;
                                  ol_copy_data[2].address = (void *)a;
                                  ol_copy_data[2].flags.input = 0;
                                  ol_copy_data[2].flags.output = 1;
                                  ol_copy_data[2].dimension_count = (short int)1;
                                  ol_copy_data[2].dimensions = &ol_copy_dimensions[2];
                                  ol_copy_data[2].offset = 8L * (((s) - (0L)));
                                  nanos_err = nanos_set_translate_function(nanos_wd_, (nanos_translate_args_t)nanos_xlate_fun_kautodiffparallelc_5);
                                  if (nanos_err != NANOS_OK)
                                    {
                                      nanos_handle_error(nanos_err);
                                    }
                                  nanos_err = nanos_submit(nanos_wd_, 3, &dependences[0], (nanos_team_t)0);
                                  if (nanos_err != NANOS_OK)
                                    {
                                      nanos_handle_error(nanos_err);
                                    }
                                }
                              else
                                {
                                  nanos_region_dimension_internal_t imm_copy_dimensions[3L];
                                  nanos_copy_data_t imm_copy_data[3L];
                                  imm_args.a = a;
                                  imm_args.e = e;
                                  imm_args.s = s;
                                  imm_args.i_d = i_d;
                                  imm_copy_dimensions[0 + 0].size = 1L * sizeof(kad_node_t *);
                                  imm_copy_dimensions[0 + 0].lower_bound = (e + 1 - 0L) * sizeof(kad_node_t *);
                                  imm_copy_dimensions[0 + 0].accessed_length = (e + 1 - 0L - (e + 1 - 0L) + 1) * sizeof(kad_node_t *);
                                  imm_copy_data[0].sharing = NANOS_SHARED;
                                  imm_copy_data[0].address = (void *)a;
                                  imm_copy_data[0].flags.input = 1;
                                  imm_copy_data[0].flags.output = 0;
                                  imm_copy_data[0].dimension_count = (short int)1;
                                  imm_copy_data[0].dimensions = &imm_copy_dimensions[0];
                                  imm_copy_data[0].offset = 8L * (((e + 1) - (0L)));
                                  imm_copy_dimensions[1 + 0].size = 1L * sizeof(kad_node_t *);
                                  imm_copy_dimensions[1 + 0].lower_bound = (i_d - 0L) * sizeof(kad_node_t *);
                                  imm_copy_dimensions[1 + 0].accessed_length = (i_d - 0L - (i_d - 0L) + 1) * sizeof(kad_node_t *);
                                  imm_copy_data[1].sharing = NANOS_SHARED;
                                  imm_copy_data[1].address = (void *)a;
                                  imm_copy_data[1].flags.input = 1;
                                  imm_copy_data[1].flags.output = 0;
                                  imm_copy_data[1].dimension_count = (short int)1;
                                  imm_copy_data[1].dimensions = &imm_copy_dimensions[1];
                                  imm_copy_data[1].offset = 8L * (((i_d) - (0L)));
                                  imm_copy_dimensions[2 + 0].size = 1L * sizeof(kad_node_t *);
                                  imm_copy_dimensions[2 + 0].lower_bound = (s - 0L) * sizeof(kad_node_t *);
                                  imm_copy_dimensions[2 + 0].accessed_length = (s - 0L - (s - 0L) + 1) * sizeof(kad_node_t *);
                                  imm_copy_data[2].sharing = NANOS_SHARED;
                                  imm_copy_data[2].address = (void *)a;
                                  imm_copy_data[2].flags.input = 0;
                                  imm_copy_data[2].flags.output = 1;
                                  imm_copy_data[2].dimension_count = (short int)1;
                                  imm_copy_data[2].dimensions = &imm_copy_dimensions[2];
                                  imm_copy_data[2].offset = 8L * (((s) - (0L)));
                                  nanos_err = nanos_create_wd_and_run_compact(&nanos_wd_const_data.base, &nanos_wd_dyn_props, sizeof(struct nanos_args_5_t), &imm_args, 3, &dependences[0], imm_copy_data, imm_copy_dimensions, (nanos_translate_args_t)nanos_xlate_fun_kautodiffparallelc_5);
                                  if (nanos_err != NANOS_OK)
                                    {
                                      nanos_handle_error(nanos_err);
                                    }
                                }
                            }
                          }
                      }
                    }
                  else
                    {
                      if (l == layers - 1)
                        {
                          int i_d = start[node + layers];
                          {
                            _Bool mcc_is_in_final;
                            nanos_err_t mcc_err_in_final = nanos_in_final(&mcc_is_in_final);
                            if (mcc_err_in_final != NANOS_OK)
                              {
                                nanos_handle_error(mcc_err_in_final);
                              }
                            if (mcc_is_in_final)
                              {
                                ompss_backward_prop(e, s, a, 3);
                              }
                            else
                              {
                                {
                                  nanos_wd_dyn_props_t nanos_wd_dyn_props;
                                  struct nanos_args_6_t *ol_args;
                                  nanos_err_t nanos_err;
                                  struct nanos_args_6_t imm_args;
                                  nanos_region_dimension_t dimensions_13[1L];
                                  nanos_data_access_t dependences[3L];
                                  nanos_region_dimension_t dimensions_14[1L];
                                  nanos_region_dimension_t dimensions_15[1L];
                                  static nanos_smp_args_t smp_ol_kad_grad_6_args = {.outline = (void (*)(void *))(void (*)(struct nanos_args_6_t *))&smp_ol_kad_grad_6};
                                  static struct nanos_const_wd_definition_1 nanos_wd_const_data = {.base = {.props = {.mandatory_creation = 0, .tied = 0, .clear_chunk = 0, .reserved0 = 0, .reserved1 = 0, .reserved2 = 0, .reserved3 = 0, .reserved4 = 0}, .data_alignment = __alignof__(struct nanos_args_6_t), .num_copies = 3, .num_devices = 1, .num_dimensions = 3, .description = 0}, .devices = {[0] = {.factory = &nanos_smp_factory, .arg = &smp_ol_kad_grad_6_args}}};
                                  nanos_wd_dyn_props.tie_to = 0;
                                  nanos_wd_dyn_props.priority = 0;
                                  nanos_wd_dyn_props.flags.is_final = 0;
                                  nanos_wd_dyn_props.flags.is_implicit = 0;
                                  nanos_wd_dyn_props.flags.is_recover = 0;
                                  ol_args = (struct nanos_args_6_t *)0;
                                  nanos_wd_t nanos_wd_ = (nanos_wd_t)0;
                                  nanos_copy_data_t *ol_copy_data = (nanos_copy_data_t *)0;
                                  nanos_region_dimension_internal_t *ol_copy_dimensions = (nanos_region_dimension_internal_t *)0;
                                  nanos_err = nanos_create_wd_compact(&nanos_wd_, &nanos_wd_const_data.base, &nanos_wd_dyn_props, sizeof(struct nanos_args_6_t), (void **)&ol_args, nanos_current_wd(), &ol_copy_data, &ol_copy_dimensions);
                                  if (nanos_err != NANOS_OK)
                                    {
                                      nanos_handle_error(nanos_err);
                                    }
                                  dimensions_13[0].size = 1L * sizeof(kad_node_t *);
                                  dimensions_13[0].lower_bound = (from - 0L) * sizeof(kad_node_t *);
                                  dimensions_13[0].accessed_length = (from - 0L - (from - 0L) + 1) * sizeof(kad_node_t *);
                                  dependences[0].address = (void *)a;
                                  dependences[0].offset = 8L * (((from) - (0L)));
                                  dependences[0].dimensions = dimensions_13;
                                  dependences[0].flags.input = 1;
                                  dependences[0].flags.output = 0;
                                  dependences[0].flags.can_rename = 0;
                                  dependences[0].flags.concurrent = 0;
                                  dependences[0].flags.commutative = 0;
                                  dependences[0].dimension_count = 1;
                                  dimensions_14[0].size = 1L * sizeof(kad_node_t *);
                                  dimensions_14[0].lower_bound = (i_d - 0L) * sizeof(kad_node_t *);
                                  dimensions_14[0].accessed_length = (i_d - 0L - (i_d - 0L) + 1) * sizeof(kad_node_t *);
                                  dependences[1].address = (void *)a;
                                  dependences[1].offset = 8L * (((i_d) - (0L)));
                                  dependences[1].dimensions = dimensions_14;
                                  dependences[1].flags.input = 1;
                                  dependences[1].flags.output = 0;
                                  dependences[1].flags.can_rename = 0;
                                  dependences[1].flags.concurrent = 0;
                                  dependences[1].flags.commutative = 0;
                                  dependences[1].dimension_count = 1;
                                  dimensions_15[0].size = 1L * sizeof(kad_node_t *);
                                  dimensions_15[0].lower_bound = (s - 0L) * sizeof(kad_node_t *);
                                  dimensions_15[0].accessed_length = (s - 0L - (s - 0L) + 1) * sizeof(kad_node_t *);
                                  dependences[2].address = (void *)a;
                                  dependences[2].offset = 8L * (((s) - (0L)));
                                  dependences[2].dimensions = dimensions_15;
                                  dependences[2].flags.input = 0;
                                  dependences[2].flags.output = 1;
                                  dependences[2].flags.can_rename = 0;
                                  dependences[2].flags.concurrent = 0;
                                  dependences[2].flags.commutative = 0;
                                  dependences[2].dimension_count = 1;
                                  if (nanos_wd_ != (nanos_wd_t)0)
                                    {
                                      (*ol_args).a = a;
                                      (*ol_args).e = e;
                                      (*ol_args).s = s;
                                      (*ol_args).from = from;
                                      (*ol_args).i_d = i_d;
                                      ol_copy_dimensions[0 + 0].size = 1L * sizeof(kad_node_t *);
                                      ol_copy_dimensions[0 + 0].lower_bound = (from - 0L) * sizeof(kad_node_t *);
                                      ol_copy_dimensions[0 + 0].accessed_length = (from - 0L - (from - 0L) + 1) * sizeof(kad_node_t *);
                                      ol_copy_data[0].sharing = NANOS_SHARED;
                                      ol_copy_data[0].address = (void *)a;
                                      ol_copy_data[0].flags.input = 1;
                                      ol_copy_data[0].flags.output = 0;
                                      ol_copy_data[0].dimension_count = (short int)1;
                                      ol_copy_data[0].dimensions = &ol_copy_dimensions[0];
                                      ol_copy_data[0].offset = 8L * (((from) - (0L)));
                                      ol_copy_dimensions[1 + 0].size = 1L * sizeof(kad_node_t *);
                                      ol_copy_dimensions[1 + 0].lower_bound = (i_d - 0L) * sizeof(kad_node_t *);
                                      ol_copy_dimensions[1 + 0].accessed_length = (i_d - 0L - (i_d - 0L) + 1) * sizeof(kad_node_t *);
                                      ol_copy_data[1].sharing = NANOS_SHARED;
                                      ol_copy_data[1].address = (void *)a;
                                      ol_copy_data[1].flags.input = 1;
                                      ol_copy_data[1].flags.output = 0;
                                      ol_copy_data[1].dimension_count = (short int)1;
                                      ol_copy_data[1].dimensions = &ol_copy_dimensions[1];
                                      ol_copy_data[1].offset = 8L * (((i_d) - (0L)));
                                      ol_copy_dimensions[2 + 0].size = 1L * sizeof(kad_node_t *);
                                      ol_copy_dimensions[2 + 0].lower_bound = (s - 0L) * sizeof(kad_node_t *);
                                      ol_copy_dimensions[2 + 0].accessed_length = (s - 0L - (s - 0L) + 1) * sizeof(kad_node_t *);
                                      ol_copy_data[2].sharing = NANOS_SHARED;
                                      ol_copy_data[2].address = (void *)a;
                                      ol_copy_data[2].flags.input = 0;
                                      ol_copy_data[2].flags.output = 1;
                                      ol_copy_data[2].dimension_count = (short int)1;
                                      ol_copy_data[2].dimensions = &ol_copy_dimensions[2];
                                      ol_copy_data[2].offset = 8L * (((s) - (0L)));
                                      nanos_err = nanos_set_translate_function(nanos_wd_, (nanos_translate_args_t)nanos_xlate_fun_kautodiffparallelc_6);
                                      if (nanos_err != NANOS_OK)
                                        {
                                          nanos_handle_error(nanos_err);
                                        }
                                      nanos_err = nanos_submit(nanos_wd_, 3, &dependences[0], (nanos_team_t)0);
                                      if (nanos_err != NANOS_OK)
                                        {
                                          nanos_handle_error(nanos_err);
                                        }
                                    }
                                  else
                                    {
                                      nanos_region_dimension_internal_t imm_copy_dimensions[3L];
                                      nanos_copy_data_t imm_copy_data[3L];
                                      imm_args.a = a;
                                      imm_args.e = e;
                                      imm_args.s = s;
                                      imm_args.from = from;
                                      imm_args.i_d = i_d;
                                      imm_copy_dimensions[0 + 0].size = 1L * sizeof(kad_node_t *);
                                      imm_copy_dimensions[0 + 0].lower_bound = (from - 0L) * sizeof(kad_node_t *);
                                      imm_copy_dimensions[0 + 0].accessed_length = (from - 0L - (from - 0L) + 1) * sizeof(kad_node_t *);
                                      imm_copy_data[0].sharing = NANOS_SHARED;
                                      imm_copy_data[0].address = (void *)a;
                                      imm_copy_data[0].flags.input = 1;
                                      imm_copy_data[0].flags.output = 0;
                                      imm_copy_data[0].dimension_count = (short int)1;
                                      imm_copy_data[0].dimensions = &imm_copy_dimensions[0];
                                      imm_copy_data[0].offset = 8L * (((from) - (0L)));
                                      imm_copy_dimensions[1 + 0].size = 1L * sizeof(kad_node_t *);
                                      imm_copy_dimensions[1 + 0].lower_bound = (i_d - 0L) * sizeof(kad_node_t *);
                                      imm_copy_dimensions[1 + 0].accessed_length = (i_d - 0L - (i_d - 0L) + 1) * sizeof(kad_node_t *);
                                      imm_copy_data[1].sharing = NANOS_SHARED;
                                      imm_copy_data[1].address = (void *)a;
                                      imm_copy_data[1].flags.input = 1;
                                      imm_copy_data[1].flags.output = 0;
                                      imm_copy_data[1].dimension_count = (short int)1;
                                      imm_copy_data[1].dimensions = &imm_copy_dimensions[1];
                                      imm_copy_data[1].offset = 8L * (((i_d) - (0L)));
                                      imm_copy_dimensions[2 + 0].size = 1L * sizeof(kad_node_t *);
                                      imm_copy_dimensions[2 + 0].lower_bound = (s - 0L) * sizeof(kad_node_t *);
                                      imm_copy_dimensions[2 + 0].accessed_length = (s - 0L - (s - 0L) + 1) * sizeof(kad_node_t *);
                                      imm_copy_data[2].sharing = NANOS_SHARED;
                                      imm_copy_data[2].address = (void *)a;
                                      imm_copy_data[2].flags.input = 0;
                                      imm_copy_data[2].flags.output = 1;
                                      imm_copy_data[2].dimension_count = (short int)1;
                                      imm_copy_data[2].dimensions = &imm_copy_dimensions[2];
                                      imm_copy_data[2].offset = 8L * (((s) - (0L)));
                                      nanos_err = nanos_create_wd_and_run_compact(&nanos_wd_const_data.base, &nanos_wd_dyn_props, sizeof(struct nanos_args_6_t), &imm_args, 3, &dependences[0], imm_copy_data, imm_copy_dimensions, (nanos_translate_args_t)nanos_xlate_fun_kautodiffparallelc_6);
                                      if (nanos_err != NANOS_OK)
                                        {
                                          nanos_handle_error(nanos_err);
                                        }
                                    }
                                }
                              }
                          }
                        }
                      else
                        {
                          if (l == 0)
                            {
                              int i_d = start[node + layers];
                              {
                                _Bool mcc_is_in_final;
                                nanos_err_t mcc_err_in_final = nanos_in_final(&mcc_is_in_final);
                                if (mcc_err_in_final != NANOS_OK)
                                  {
                                    nanos_handle_error(mcc_err_in_final);
                                  }
                                if (mcc_is_in_final)
                                  {
                                    ompss_backward_prop(e, s, a, 3);
                                  }
                                else
                                  {
                                    {
                                      nanos_wd_dyn_props_t nanos_wd_dyn_props;
                                      struct nanos_args_7_t *ol_args;
                                      nanos_err_t nanos_err;
                                      struct nanos_args_7_t imm_args;
                                      nanos_region_dimension_t dimensions_16[1L];
                                      nanos_data_access_t dependences[3L];
                                      nanos_region_dimension_t dimensions_17[1L];
                                      nanos_region_dimension_t dimensions_18[1L];
                                      static nanos_smp_args_t smp_ol_kad_grad_7_args = {.outline = (void (*)(void *))(void (*)(struct nanos_args_7_t *))&smp_ol_kad_grad_7};
                                      static struct nanos_const_wd_definition_1 nanos_wd_const_data = {.base = {.props = {.mandatory_creation = 0, .tied = 0, .clear_chunk = 0, .reserved0 = 0, .reserved1 = 0, .reserved2 = 0, .reserved3 = 0, .reserved4 = 0}, .data_alignment = __alignof__(struct nanos_args_7_t), .num_copies = 3, .num_devices = 1, .num_dimensions = 3, .description = 0}, .devices = {[0] = {.factory = &nanos_smp_factory, .arg = &smp_ol_kad_grad_7_args}}};
                                      nanos_wd_dyn_props.tie_to = 0;
                                      nanos_wd_dyn_props.priority = 0;
                                      nanos_wd_dyn_props.flags.is_final = 0;
                                      nanos_wd_dyn_props.flags.is_implicit = 0;
                                      nanos_wd_dyn_props.flags.is_recover = 0;
                                      ol_args = (struct nanos_args_7_t *)0;
                                      nanos_wd_t nanos_wd_ = (nanos_wd_t)0;
                                      nanos_copy_data_t *ol_copy_data = (nanos_copy_data_t *)0;
                                      nanos_region_dimension_internal_t *ol_copy_dimensions = (nanos_region_dimension_internal_t *)0;
                                      nanos_err = nanos_create_wd_compact(&nanos_wd_, &nanos_wd_const_data.base, &nanos_wd_dyn_props, sizeof(struct nanos_args_7_t), (void **)&ol_args, nanos_current_wd(), &ol_copy_data, &ol_copy_dimensions);
                                      if (nanos_err != NANOS_OK)
                                        {
                                          nanos_handle_error(nanos_err);
                                        }
                                      dimensions_16[0].size = 1L * sizeof(kad_node_t *);
                                      dimensions_16[0].lower_bound = (e + 1 - 0L) * sizeof(kad_node_t *);
                                      dimensions_16[0].accessed_length = (e + 1 - 0L - (e + 1 - 0L) + 1) * sizeof(kad_node_t *);
                                      dependences[0].address = (void *)a;
                                      dependences[0].offset = 8L * (((e + 1) - (0L)));
                                      dependences[0].dimensions = dimensions_16;
                                      dependences[0].flags.input = 1;
                                      dependences[0].flags.output = 0;
                                      dependences[0].flags.can_rename = 0;
                                      dependences[0].flags.concurrent = 0;
                                      dependences[0].flags.commutative = 0;
                                      dependences[0].dimension_count = 1;
                                      dimensions_17[0].size = 1L * sizeof(kad_node_t *);
                                      dimensions_17[0].lower_bound = (i_d - 0L) * sizeof(kad_node_t *);
                                      dimensions_17[0].accessed_length = (i_d - 0L - (i_d - 0L) + 1) * sizeof(kad_node_t *);
                                      dependences[1].address = (void *)a;
                                      dependences[1].offset = 8L * (((i_d) - (0L)));
                                      dependences[1].dimensions = dimensions_17;
                                      dependences[1].flags.input = 1;
                                      dependences[1].flags.output = 0;
                                      dependences[1].flags.can_rename = 0;
                                      dependences[1].flags.concurrent = 0;
                                      dependences[1].flags.commutative = 0;
                                      dependences[1].dimension_count = 1;
                                      dimensions_18[0].size = 1L * sizeof(kad_node_t *);
                                      dimensions_18[0].lower_bound = (s - 0L) * sizeof(kad_node_t *);
                                      dimensions_18[0].accessed_length = (s - 0L - (s - 0L) + 1) * sizeof(kad_node_t *);
                                      dependences[2].address = (void *)a;
                                      dependences[2].offset = 8L * (((s) - (0L)));
                                      dependences[2].dimensions = dimensions_18;
                                      dependences[2].flags.input = 0;
                                      dependences[2].flags.output = 1;
                                      dependences[2].flags.can_rename = 0;
                                      dependences[2].flags.concurrent = 0;
                                      dependences[2].flags.commutative = 0;
                                      dependences[2].dimension_count = 1;
                                      if (nanos_wd_ != (nanos_wd_t)0)
                                        {
                                          (*ol_args).a = a;
                                          (*ol_args).e = e;
                                          (*ol_args).s = s;
                                          (*ol_args).i_d = i_d;
                                          ol_copy_dimensions[0 + 0].size = 1L * sizeof(kad_node_t *);
                                          ol_copy_dimensions[0 + 0].lower_bound = (e + 1 - 0L) * sizeof(kad_node_t *);
                                          ol_copy_dimensions[0 + 0].accessed_length = (e + 1 - 0L - (e + 1 - 0L) + 1) * sizeof(kad_node_t *);
                                          ol_copy_data[0].sharing = NANOS_SHARED;
                                          ol_copy_data[0].address = (void *)a;
                                          ol_copy_data[0].flags.input = 1;
                                          ol_copy_data[0].flags.output = 0;
                                          ol_copy_data[0].dimension_count = (short int)1;
                                          ol_copy_data[0].dimensions = &ol_copy_dimensions[0];
                                          ol_copy_data[0].offset = 8L * (((e + 1) - (0L)));
                                          ol_copy_dimensions[1 + 0].size = 1L * sizeof(kad_node_t *);
                                          ol_copy_dimensions[1 + 0].lower_bound = (i_d - 0L) * sizeof(kad_node_t *);
                                          ol_copy_dimensions[1 + 0].accessed_length = (i_d - 0L - (i_d - 0L) + 1) * sizeof(kad_node_t *);
                                          ol_copy_data[1].sharing = NANOS_SHARED;
                                          ol_copy_data[1].address = (void *)a;
                                          ol_copy_data[1].flags.input = 1;
                                          ol_copy_data[1].flags.output = 0;
                                          ol_copy_data[1].dimension_count = (short int)1;
                                          ol_copy_data[1].dimensions = &ol_copy_dimensions[1];
                                          ol_copy_data[1].offset = 8L * (((i_d) - (0L)));
                                          ol_copy_dimensions[2 + 0].size = 1L * sizeof(kad_node_t *);
                                          ol_copy_dimensions[2 + 0].lower_bound = (s - 0L) * sizeof(kad_node_t *);
                                          ol_copy_dimensions[2 + 0].accessed_length = (s - 0L - (s - 0L) + 1) * sizeof(kad_node_t *);
                                          ol_copy_data[2].sharing = NANOS_SHARED;
                                          ol_copy_data[2].address = (void *)a;
                                          ol_copy_data[2].flags.input = 0;
                                          ol_copy_data[2].flags.output = 1;
                                          ol_copy_data[2].dimension_count = (short int)1;
                                          ol_copy_data[2].dimensions = &ol_copy_dimensions[2];
                                          ol_copy_data[2].offset = 8L * (((s) - (0L)));
                                          nanos_err = nanos_set_translate_function(nanos_wd_, (nanos_translate_args_t)nanos_xlate_fun_kautodiffparallelc_7);
                                          if (nanos_err != NANOS_OK)
                                            {
                                              nanos_handle_error(nanos_err);
                                            }
                                          nanos_err = nanos_submit(nanos_wd_, 3, &dependences[0], (nanos_team_t)0);
                                          if (nanos_err != NANOS_OK)
                                            {
                                              nanos_handle_error(nanos_err);
                                            }
                                        }
                                      else
                                        {
                                          nanos_region_dimension_internal_t imm_copy_dimensions[3L];
                                          nanos_copy_data_t imm_copy_data[3L];
                                          imm_args.a = a;
                                          imm_args.e = e;
                                          imm_args.s = s;
                                          imm_args.i_d = i_d;
                                          imm_copy_dimensions[0 + 0].size = 1L * sizeof(kad_node_t *);
                                          imm_copy_dimensions[0 + 0].lower_bound = (e + 1 - 0L) * sizeof(kad_node_t *);
                                          imm_copy_dimensions[0 + 0].accessed_length = (e + 1 - 0L - (e + 1 - 0L) + 1) * sizeof(kad_node_t *);
                                          imm_copy_data[0].sharing = NANOS_SHARED;
                                          imm_copy_data[0].address = (void *)a;
                                          imm_copy_data[0].flags.input = 1;
                                          imm_copy_data[0].flags.output = 0;
                                          imm_copy_data[0].dimension_count = (short int)1;
                                          imm_copy_data[0].dimensions = &imm_copy_dimensions[0];
                                          imm_copy_data[0].offset = 8L * (((e + 1) - (0L)));
                                          imm_copy_dimensions[1 + 0].size = 1L * sizeof(kad_node_t *);
                                          imm_copy_dimensions[1 + 0].lower_bound = (i_d - 0L) * sizeof(kad_node_t *);
                                          imm_copy_dimensions[1 + 0].accessed_length = (i_d - 0L - (i_d - 0L) + 1) * sizeof(kad_node_t *);
                                          imm_copy_data[1].sharing = NANOS_SHARED;
                                          imm_copy_data[1].address = (void *)a;
                                          imm_copy_data[1].flags.input = 1;
                                          imm_copy_data[1].flags.output = 0;
                                          imm_copy_data[1].dimension_count = (short int)1;
                                          imm_copy_data[1].dimensions = &imm_copy_dimensions[1];
                                          imm_copy_data[1].offset = 8L * (((i_d) - (0L)));
                                          imm_copy_dimensions[2 + 0].size = 1L * sizeof(kad_node_t *);
                                          imm_copy_dimensions[2 + 0].lower_bound = (s - 0L) * sizeof(kad_node_t *);
                                          imm_copy_dimensions[2 + 0].accessed_length = (s - 0L - (s - 0L) + 1) * sizeof(kad_node_t *);
                                          imm_copy_data[2].sharing = NANOS_SHARED;
                                          imm_copy_data[2].address = (void *)a;
                                          imm_copy_data[2].flags.input = 0;
                                          imm_copy_data[2].flags.output = 1;
                                          imm_copy_data[2].dimension_count = (short int)1;
                                          imm_copy_data[2].dimensions = &imm_copy_dimensions[2];
                                          imm_copy_data[2].offset = 8L * (((s) - (0L)));
                                          nanos_err = nanos_create_wd_and_run_compact(&nanos_wd_const_data.base, &nanos_wd_dyn_props, sizeof(struct nanos_args_7_t), &imm_args, 3, &dependences[0], imm_copy_data, imm_copy_dimensions, (nanos_translate_args_t)nanos_xlate_fun_kautodiffparallelc_7);
                                          if (nanos_err != NANOS_OK)
                                            {
                                              nanos_handle_error(nanos_err);
                                            }
                                        }
                                    }
                                  }
                              }
                            }
                        }
                    }
                }
              else
                {
                  if (u == ulen - 1)
                    {
                      if (0 < l && l < layers - 1)
                        {
                          {
                            _Bool mcc_is_in_final;
                            nanos_err_t mcc_err_in_final = nanos_in_final(&mcc_is_in_final);
                            if (mcc_err_in_final != NANOS_OK)
                              {
                                nanos_handle_error(mcc_err_in_final);
                              }
                            if (mcc_is_in_final)
                              {
                                ompss_backward_prop(e, s, a, 3);
                              }
                            else
                              {
                                {
                                  nanos_wd_dyn_props_t nanos_wd_dyn_props;
                                  struct nanos_args_8_t *ol_args;
                                  nanos_err_t nanos_err;
                                  struct nanos_args_8_t imm_args;
                                  nanos_region_dimension_t dimensions_19[1L];
                                  nanos_data_access_t dependences[2L];
                                  nanos_region_dimension_t dimensions_20[1L];
                                  static nanos_smp_args_t smp_ol_kad_grad_8_args = {.outline = (void (*)(void *))(void (*)(struct nanos_args_8_t *))&smp_ol_kad_grad_8};
                                  static struct nanos_const_wd_definition_1 nanos_wd_const_data = {.base = {.props = {.mandatory_creation = 0, .tied = 0, .clear_chunk = 0, .reserved0 = 0, .reserved1 = 0, .reserved2 = 0, .reserved3 = 0, .reserved4 = 0}, .data_alignment = __alignof__(struct nanos_args_8_t), .num_copies = 2, .num_devices = 1, .num_dimensions = 2, .description = 0}, .devices = {[0] = {.factory = &nanos_smp_factory, .arg = &smp_ol_kad_grad_8_args}}};
                                  nanos_wd_dyn_props.tie_to = 0;
                                  nanos_wd_dyn_props.priority = 0;
                                  nanos_wd_dyn_props.flags.is_final = 0;
                                  nanos_wd_dyn_props.flags.is_implicit = 0;
                                  nanos_wd_dyn_props.flags.is_recover = 0;
                                  ol_args = (struct nanos_args_8_t *)0;
                                  nanos_wd_t nanos_wd_ = (nanos_wd_t)0;
                                  nanos_copy_data_t *ol_copy_data = (nanos_copy_data_t *)0;
                                  nanos_region_dimension_internal_t *ol_copy_dimensions = (nanos_region_dimension_internal_t *)0;
                                  nanos_err = nanos_create_wd_compact(&nanos_wd_, &nanos_wd_const_data.base, &nanos_wd_dyn_props, sizeof(struct nanos_args_8_t), (void **)&ol_args, nanos_current_wd(), &ol_copy_data, &ol_copy_dimensions);
                                  if (nanos_err != NANOS_OK)
                                    {
                                      nanos_handle_error(nanos_err);
                                    }
                                  dimensions_19[0].size = 1L * sizeof(kad_node_t *);
                                  dimensions_19[0].lower_bound = (e + 1 - 0L) * sizeof(kad_node_t *);
                                  dimensions_19[0].accessed_length = (e + 1 - 0L - (e + 1 - 0L) + 1) * sizeof(kad_node_t *);
                                  dependences[0].address = (void *)a;
                                  dependences[0].offset = 8L * (((e + 1) - (0L)));
                                  dependences[0].dimensions = dimensions_19;
                                  dependences[0].flags.input = 1;
                                  dependences[0].flags.output = 0;
                                  dependences[0].flags.can_rename = 0;
                                  dependences[0].flags.concurrent = 0;
                                  dependences[0].flags.commutative = 0;
                                  dependences[0].dimension_count = 1;
                                  dimensions_20[0].size = 1L * sizeof(kad_node_t *);
                                  dimensions_20[0].lower_bound = (s - 0L) * sizeof(kad_node_t *);
                                  dimensions_20[0].accessed_length = (s - 0L - (s - 0L) + 1) * sizeof(kad_node_t *);
                                  dependences[1].address = (void *)a;
                                  dependences[1].offset = 8L * (((s) - (0L)));
                                  dependences[1].dimensions = dimensions_20;
                                  dependences[1].flags.input = 0;
                                  dependences[1].flags.output = 1;
                                  dependences[1].flags.can_rename = 0;
                                  dependences[1].flags.concurrent = 0;
                                  dependences[1].flags.commutative = 0;
                                  dependences[1].dimension_count = 1;
                                  if (nanos_wd_ != (nanos_wd_t)0)
                                    {
                                      (*ol_args).a = a;
                                      (*ol_args).e = e;
                                      (*ol_args).s = s;
                                      ol_copy_dimensions[0 + 0].size = 1L * sizeof(kad_node_t *);
                                      ol_copy_dimensions[0 + 0].lower_bound = (e + 1 - 0L) * sizeof(kad_node_t *);
                                      ol_copy_dimensions[0 + 0].accessed_length = (e + 1 - 0L - (e + 1 - 0L) + 1) * sizeof(kad_node_t *);
                                      ol_copy_data[0].sharing = NANOS_SHARED;
                                      ol_copy_data[0].address = (void *)a;
                                      ol_copy_data[0].flags.input = 1;
                                      ol_copy_data[0].flags.output = 0;
                                      ol_copy_data[0].dimension_count = (short int)1;
                                      ol_copy_data[0].dimensions = &ol_copy_dimensions[0];
                                      ol_copy_data[0].offset = 8L * (((e + 1) - (0L)));
                                      ol_copy_dimensions[1 + 0].size = 1L * sizeof(kad_node_t *);
                                      ol_copy_dimensions[1 + 0].lower_bound = (s - 0L) * sizeof(kad_node_t *);
                                      ol_copy_dimensions[1 + 0].accessed_length = (s - 0L - (s - 0L) + 1) * sizeof(kad_node_t *);
                                      ol_copy_data[1].sharing = NANOS_SHARED;
                                      ol_copy_data[1].address = (void *)a;
                                      ol_copy_data[1].flags.input = 0;
                                      ol_copy_data[1].flags.output = 1;
                                      ol_copy_data[1].dimension_count = (short int)1;
                                      ol_copy_data[1].dimensions = &ol_copy_dimensions[1];
                                      ol_copy_data[1].offset = 8L * (((s) - (0L)));
                                      nanos_err = nanos_set_translate_function(nanos_wd_, (nanos_translate_args_t)nanos_xlate_fun_kautodiffparallelc_8);
                                      if (nanos_err != NANOS_OK)
                                        {
                                          nanos_handle_error(nanos_err);
                                        }
                                      nanos_err = nanos_submit(nanos_wd_, 2, &dependences[0], (nanos_team_t)0);
                                      if (nanos_err != NANOS_OK)
                                        {
                                          nanos_handle_error(nanos_err);
                                        }
                                    }
                                  else
                                    {
                                      nanos_region_dimension_internal_t imm_copy_dimensions[2L];
                                      nanos_copy_data_t imm_copy_data[2L];
                                      imm_args.a = a;
                                      imm_args.e = e;
                                      imm_args.s = s;
                                      imm_copy_dimensions[0 + 0].size = 1L * sizeof(kad_node_t *);
                                      imm_copy_dimensions[0 + 0].lower_bound = (e + 1 - 0L) * sizeof(kad_node_t *);
                                      imm_copy_dimensions[0 + 0].accessed_length = (e + 1 - 0L - (e + 1 - 0L) + 1) * sizeof(kad_node_t *);
                                      imm_copy_data[0].sharing = NANOS_SHARED;
                                      imm_copy_data[0].address = (void *)a;
                                      imm_copy_data[0].flags.input = 1;
                                      imm_copy_data[0].flags.output = 0;
                                      imm_copy_data[0].dimension_count = (short int)1;
                                      imm_copy_data[0].dimensions = &imm_copy_dimensions[0];
                                      imm_copy_data[0].offset = 8L * (((e + 1) - (0L)));
                                      imm_copy_dimensions[1 + 0].size = 1L * sizeof(kad_node_t *);
                                      imm_copy_dimensions[1 + 0].lower_bound = (s - 0L) * sizeof(kad_node_t *);
                                      imm_copy_dimensions[1 + 0].accessed_length = (s - 0L - (s - 0L) + 1) * sizeof(kad_node_t *);
                                      imm_copy_data[1].sharing = NANOS_SHARED;
                                      imm_copy_data[1].address = (void *)a;
                                      imm_copy_data[1].flags.input = 0;
                                      imm_copy_data[1].flags.output = 1;
                                      imm_copy_data[1].dimension_count = (short int)1;
                                      imm_copy_data[1].dimensions = &imm_copy_dimensions[1];
                                      imm_copy_data[1].offset = 8L * (((s) - (0L)));
                                      nanos_err = nanos_create_wd_and_run_compact(&nanos_wd_const_data.base, &nanos_wd_dyn_props, sizeof(struct nanos_args_8_t), &imm_args, 2, &dependences[0], imm_copy_data, imm_copy_dimensions, (nanos_translate_args_t)nanos_xlate_fun_kautodiffparallelc_8);
                                      if (nanos_err != NANOS_OK)
                                        {
                                          nanos_handle_error(nanos_err);
                                        }
                                    }
                                }
                              }
                          }
                        }
                      else
                        {
                          if (l == layers - 1)
                            {
                              {
                                _Bool mcc_is_in_final;
                                nanos_err_t mcc_err_in_final = nanos_in_final(&mcc_is_in_final);
                                if (mcc_err_in_final != NANOS_OK)
                                  {
                                    nanos_handle_error(mcc_err_in_final);
                                  }
                                if (mcc_is_in_final)
                                  {
                                    ompss_backward_prop(e, s, a, 3);
                                  }
                                else
                                  {
                                    {
                                      nanos_wd_dyn_props_t nanos_wd_dyn_props;
                                      struct nanos_args_9_t *ol_args;
                                      nanos_err_t nanos_err;
                                      struct nanos_args_9_t imm_args;
                                      nanos_region_dimension_t dimensions_21[1L];
                                      nanos_data_access_t dependences[2L];
                                      nanos_region_dimension_t dimensions_22[1L];
                                      static nanos_smp_args_t smp_ol_kad_grad_9_args = {.outline = (void (*)(void *))(void (*)(struct nanos_args_9_t *))&smp_ol_kad_grad_9};
                                      static struct nanos_const_wd_definition_1 nanos_wd_const_data = {.base = {.props = {.mandatory_creation = 0, .tied = 0, .clear_chunk = 0, .reserved0 = 0, .reserved1 = 0, .reserved2 = 0, .reserved3 = 0, .reserved4 = 0}, .data_alignment = __alignof__(struct nanos_args_9_t), .num_copies = 2, .num_devices = 1, .num_dimensions = 2, .description = 0}, .devices = {[0] = {.factory = &nanos_smp_factory, .arg = &smp_ol_kad_grad_9_args}}};
                                      nanos_wd_dyn_props.tie_to = 0;
                                      nanos_wd_dyn_props.priority = 0;
                                      nanos_wd_dyn_props.flags.is_final = 0;
                                      nanos_wd_dyn_props.flags.is_implicit = 0;
                                      nanos_wd_dyn_props.flags.is_recover = 0;
                                      ol_args = (struct nanos_args_9_t *)0;
                                      nanos_wd_t nanos_wd_ = (nanos_wd_t)0;
                                      nanos_copy_data_t *ol_copy_data = (nanos_copy_data_t *)0;
                                      nanos_region_dimension_internal_t *ol_copy_dimensions = (nanos_region_dimension_internal_t *)0;
                                      nanos_err = nanos_create_wd_compact(&nanos_wd_, &nanos_wd_const_data.base, &nanos_wd_dyn_props, sizeof(struct nanos_args_9_t), (void **)&ol_args, nanos_current_wd(), &ol_copy_data, &ol_copy_dimensions);
                                      if (nanos_err != NANOS_OK)
                                        {
                                          nanos_handle_error(nanos_err);
                                        }
                                      dimensions_21[0].size = 1L * sizeof(kad_node_t *);
                                      dimensions_21[0].lower_bound = (from - 0L) * sizeof(kad_node_t *);
                                      dimensions_21[0].accessed_length = (from - 0L - (from - 0L) + 1) * sizeof(kad_node_t *);
                                      dependences[0].address = (void *)a;
                                      dependences[0].offset = 8L * (((from) - (0L)));
                                      dependences[0].dimensions = dimensions_21;
                                      dependences[0].flags.input = 1;
                                      dependences[0].flags.output = 0;
                                      dependences[0].flags.can_rename = 0;
                                      dependences[0].flags.concurrent = 0;
                                      dependences[0].flags.commutative = 0;
                                      dependences[0].dimension_count = 1;
                                      dimensions_22[0].size = 1L * sizeof(kad_node_t *);
                                      dimensions_22[0].lower_bound = (s - 0L) * sizeof(kad_node_t *);
                                      dimensions_22[0].accessed_length = (s - 0L - (s - 0L) + 1) * sizeof(kad_node_t *);
                                      dependences[1].address = (void *)a;
                                      dependences[1].offset = 8L * (((s) - (0L)));
                                      dependences[1].dimensions = dimensions_22;
                                      dependences[1].flags.input = 0;
                                      dependences[1].flags.output = 1;
                                      dependences[1].flags.can_rename = 0;
                                      dependences[1].flags.concurrent = 0;
                                      dependences[1].flags.commutative = 0;
                                      dependences[1].dimension_count = 1;
                                      if (nanos_wd_ != (nanos_wd_t)0)
                                        {
                                          (*ol_args).a = a;
                                          (*ol_args).e = e;
                                          (*ol_args).s = s;
                                          (*ol_args).from = from;
                                          ol_copy_dimensions[0 + 0].size = 1L * sizeof(kad_node_t *);
                                          ol_copy_dimensions[0 + 0].lower_bound = (from - 0L) * sizeof(kad_node_t *);
                                          ol_copy_dimensions[0 + 0].accessed_length = (from - 0L - (from - 0L) + 1) * sizeof(kad_node_t *);
                                          ol_copy_data[0].sharing = NANOS_SHARED;
                                          ol_copy_data[0].address = (void *)a;
                                          ol_copy_data[0].flags.input = 1;
                                          ol_copy_data[0].flags.output = 0;
                                          ol_copy_data[0].dimension_count = (short int)1;
                                          ol_copy_data[0].dimensions = &ol_copy_dimensions[0];
                                          ol_copy_data[0].offset = 8L * (((from) - (0L)));
                                          ol_copy_dimensions[1 + 0].size = 1L * sizeof(kad_node_t *);
                                          ol_copy_dimensions[1 + 0].lower_bound = (s - 0L) * sizeof(kad_node_t *);
                                          ol_copy_dimensions[1 + 0].accessed_length = (s - 0L - (s - 0L) + 1) * sizeof(kad_node_t *);
                                          ol_copy_data[1].sharing = NANOS_SHARED;
                                          ol_copy_data[1].address = (void *)a;
                                          ol_copy_data[1].flags.input = 0;
                                          ol_copy_data[1].flags.output = 1;
                                          ol_copy_data[1].dimension_count = (short int)1;
                                          ol_copy_data[1].dimensions = &ol_copy_dimensions[1];
                                          ol_copy_data[1].offset = 8L * (((s) - (0L)));
                                          nanos_err = nanos_set_translate_function(nanos_wd_, (nanos_translate_args_t)nanos_xlate_fun_kautodiffparallelc_9);
                                          if (nanos_err != NANOS_OK)
                                            {
                                              nanos_handle_error(nanos_err);
                                            }
                                          nanos_err = nanos_submit(nanos_wd_, 2, &dependences[0], (nanos_team_t)0);
                                          if (nanos_err != NANOS_OK)
                                            {
                                              nanos_handle_error(nanos_err);
                                            }
                                        }
                                      else
                                        {
                                          nanos_region_dimension_internal_t imm_copy_dimensions[2L];
                                          nanos_copy_data_t imm_copy_data[2L];
                                          imm_args.a = a;
                                          imm_args.e = e;
                                          imm_args.s = s;
                                          imm_args.from = from;
                                          imm_copy_dimensions[0 + 0].size = 1L * sizeof(kad_node_t *);
                                          imm_copy_dimensions[0 + 0].lower_bound = (from - 0L) * sizeof(kad_node_t *);
                                          imm_copy_dimensions[0 + 0].accessed_length = (from - 0L - (from - 0L) + 1) * sizeof(kad_node_t *);
                                          imm_copy_data[0].sharing = NANOS_SHARED;
                                          imm_copy_data[0].address = (void *)a;
                                          imm_copy_data[0].flags.input = 1;
                                          imm_copy_data[0].flags.output = 0;
                                          imm_copy_data[0].dimension_count = (short int)1;
                                          imm_copy_data[0].dimensions = &imm_copy_dimensions[0];
                                          imm_copy_data[0].offset = 8L * (((from) - (0L)));
                                          imm_copy_dimensions[1 + 0].size = 1L * sizeof(kad_node_t *);
                                          imm_copy_dimensions[1 + 0].lower_bound = (s - 0L) * sizeof(kad_node_t *);
                                          imm_copy_dimensions[1 + 0].accessed_length = (s - 0L - (s - 0L) + 1) * sizeof(kad_node_t *);
                                          imm_copy_data[1].sharing = NANOS_SHARED;
                                          imm_copy_data[1].address = (void *)a;
                                          imm_copy_data[1].flags.input = 0;
                                          imm_copy_data[1].flags.output = 1;
                                          imm_copy_data[1].dimension_count = (short int)1;
                                          imm_copy_data[1].dimensions = &imm_copy_dimensions[1];
                                          imm_copy_data[1].offset = 8L * (((s) - (0L)));
                                          nanos_err = nanos_create_wd_and_run_compact(&nanos_wd_const_data.base, &nanos_wd_dyn_props, sizeof(struct nanos_args_9_t), &imm_args, 2, &dependences[0], imm_copy_data, imm_copy_dimensions, (nanos_translate_args_t)nanos_xlate_fun_kautodiffparallelc_9);
                                          if (nanos_err != NANOS_OK)
                                            {
                                              nanos_handle_error(nanos_err);
                                            }
                                        }
                                    }
                                  }
                              }
                            }
                          else
                            {
                              if (l == 0)
                                {
                                  {
                                    _Bool mcc_is_in_final;
                                    nanos_err_t mcc_err_in_final = nanos_in_final(&mcc_is_in_final);
                                    if (mcc_err_in_final != NANOS_OK)
                                      {
                                        nanos_handle_error(mcc_err_in_final);
                                      }
                                    if (mcc_is_in_final)
                                      {
                                        ompss_backward_prop(e, s, a, 3);
                                      }
                                    else
                                      {
                                        {
                                          nanos_wd_dyn_props_t nanos_wd_dyn_props;
                                          struct nanos_args_10_t *ol_args;
                                          nanos_err_t nanos_err;
                                          struct nanos_args_10_t imm_args;
                                          nanos_region_dimension_t dimensions_23[1L];
                                          nanos_data_access_t dependences[2L];
                                          nanos_region_dimension_t dimensions_24[1L];
                                          static nanos_smp_args_t smp_ol_kad_grad_10_args = {.outline = (void (*)(void *))(void (*)(struct nanos_args_10_t *))&smp_ol_kad_grad_10};
                                          static struct nanos_const_wd_definition_1 nanos_wd_const_data = {.base = {.props = {.mandatory_creation = 0, .tied = 0, .clear_chunk = 0, .reserved0 = 0, .reserved1 = 0, .reserved2 = 0, .reserved3 = 0, .reserved4 = 0}, .data_alignment = __alignof__(struct nanos_args_10_t), .num_copies = 2, .num_devices = 1, .num_dimensions = 2, .description = 0}, .devices = {[0] = {.factory = &nanos_smp_factory, .arg = &smp_ol_kad_grad_10_args}}};
                                          nanos_wd_dyn_props.tie_to = 0;
                                          nanos_wd_dyn_props.priority = 0;
                                          nanos_wd_dyn_props.flags.is_final = 0;
                                          nanos_wd_dyn_props.flags.is_implicit = 0;
                                          nanos_wd_dyn_props.flags.is_recover = 0;
                                          ol_args = (struct nanos_args_10_t *)0;
                                          nanos_wd_t nanos_wd_ = (nanos_wd_t)0;
                                          nanos_copy_data_t *ol_copy_data = (nanos_copy_data_t *)0;
                                          nanos_region_dimension_internal_t *ol_copy_dimensions = (nanos_region_dimension_internal_t *)0;
                                          nanos_err = nanos_create_wd_compact(&nanos_wd_, &nanos_wd_const_data.base, &nanos_wd_dyn_props, sizeof(struct nanos_args_10_t), (void **)&ol_args, nanos_current_wd(), &ol_copy_data, &ol_copy_dimensions);
                                          if (nanos_err != NANOS_OK)
                                            {
                                              nanos_handle_error(nanos_err);
                                            }
                                          dimensions_23[0].size = 1L * sizeof(kad_node_t *);
                                          dimensions_23[0].lower_bound = (e + 1 - 0L) * sizeof(kad_node_t *);
                                          dimensions_23[0].accessed_length = (e + 1 - 0L - (e + 1 - 0L) + 1) * sizeof(kad_node_t *);
                                          dependences[0].address = (void *)a;
                                          dependences[0].offset = 8L * (((e + 1) - (0L)));
                                          dependences[0].dimensions = dimensions_23;
                                          dependences[0].flags.input = 1;
                                          dependences[0].flags.output = 0;
                                          dependences[0].flags.can_rename = 0;
                                          dependences[0].flags.concurrent = 0;
                                          dependences[0].flags.commutative = 0;
                                          dependences[0].dimension_count = 1;
                                          dimensions_24[0].size = 1L * sizeof(kad_node_t *);
                                          dimensions_24[0].lower_bound = (s - 0L) * sizeof(kad_node_t *);
                                          dimensions_24[0].accessed_length = (s - 0L - (s - 0L) + 1) * sizeof(kad_node_t *);
                                          dependences[1].address = (void *)a;
                                          dependences[1].offset = 8L * (((s) - (0L)));
                                          dependences[1].dimensions = dimensions_24;
                                          dependences[1].flags.input = 0;
                                          dependences[1].flags.output = 1;
                                          dependences[1].flags.can_rename = 0;
                                          dependences[1].flags.concurrent = 0;
                                          dependences[1].flags.commutative = 0;
                                          dependences[1].dimension_count = 1;
                                          if (nanos_wd_ != (nanos_wd_t)0)
                                            {
                                              (*ol_args).a = a;
                                              (*ol_args).e = e;
                                              (*ol_args).s = s;
                                              ol_copy_dimensions[0 + 0].size = 1L * sizeof(kad_node_t *);
                                              ol_copy_dimensions[0 + 0].lower_bound = (e + 1 - 0L) * sizeof(kad_node_t *);
                                              ol_copy_dimensions[0 + 0].accessed_length = (e + 1 - 0L - (e + 1 - 0L) + 1) * sizeof(kad_node_t *);
                                              ol_copy_data[0].sharing = NANOS_SHARED;
                                              ol_copy_data[0].address = (void *)a;
                                              ol_copy_data[0].flags.input = 1;
                                              ol_copy_data[0].flags.output = 0;
                                              ol_copy_data[0].dimension_count = (short int)1;
                                              ol_copy_data[0].dimensions = &ol_copy_dimensions[0];
                                              ol_copy_data[0].offset = 8L * (((e + 1) - (0L)));
                                              ol_copy_dimensions[1 + 0].size = 1L * sizeof(kad_node_t *);
                                              ol_copy_dimensions[1 + 0].lower_bound = (s - 0L) * sizeof(kad_node_t *);
                                              ol_copy_dimensions[1 + 0].accessed_length = (s - 0L - (s - 0L) + 1) * sizeof(kad_node_t *);
                                              ol_copy_data[1].sharing = NANOS_SHARED;
                                              ol_copy_data[1].address = (void *)a;
                                              ol_copy_data[1].flags.input = 0;
                                              ol_copy_data[1].flags.output = 1;
                                              ol_copy_data[1].dimension_count = (short int)1;
                                              ol_copy_data[1].dimensions = &ol_copy_dimensions[1];
                                              ol_copy_data[1].offset = 8L * (((s) - (0L)));
                                              nanos_err = nanos_set_translate_function(nanos_wd_, (nanos_translate_args_t)nanos_xlate_fun_kautodiffparallelc_10);
                                              if (nanos_err != NANOS_OK)
                                                {
                                                  nanos_handle_error(nanos_err);
                                                }
                                              nanos_err = nanos_submit(nanos_wd_, 2, &dependences[0], (nanos_team_t)0);
                                              if (nanos_err != NANOS_OK)
                                                {
                                                  nanos_handle_error(nanos_err);
                                                }
                                            }
                                          else
                                            {
                                              nanos_region_dimension_internal_t imm_copy_dimensions[2L];
                                              nanos_copy_data_t imm_copy_data[2L];
                                              imm_args.a = a;
                                              imm_args.e = e;
                                              imm_args.s = s;
                                              imm_copy_dimensions[0 + 0].size = 1L * sizeof(kad_node_t *);
                                              imm_copy_dimensions[0 + 0].lower_bound = (e + 1 - 0L) * sizeof(kad_node_t *);
                                              imm_copy_dimensions[0 + 0].accessed_length = (e + 1 - 0L - (e + 1 - 0L) + 1) * sizeof(kad_node_t *);
                                              imm_copy_data[0].sharing = NANOS_SHARED;
                                              imm_copy_data[0].address = (void *)a;
                                              imm_copy_data[0].flags.input = 1;
                                              imm_copy_data[0].flags.output = 0;
                                              imm_copy_data[0].dimension_count = (short int)1;
                                              imm_copy_data[0].dimensions = &imm_copy_dimensions[0];
                                              imm_copy_data[0].offset = 8L * (((e + 1) - (0L)));
                                              imm_copy_dimensions[1 + 0].size = 1L * sizeof(kad_node_t *);
                                              imm_copy_dimensions[1 + 0].lower_bound = (s - 0L) * sizeof(kad_node_t *);
                                              imm_copy_dimensions[1 + 0].accessed_length = (s - 0L - (s - 0L) + 1) * sizeof(kad_node_t *);
                                              imm_copy_data[1].sharing = NANOS_SHARED;
                                              imm_copy_data[1].address = (void *)a;
                                              imm_copy_data[1].flags.input = 0;
                                              imm_copy_data[1].flags.output = 1;
                                              imm_copy_data[1].dimension_count = (short int)1;
                                              imm_copy_data[1].dimensions = &imm_copy_dimensions[1];
                                              imm_copy_data[1].offset = 8L * (((s) - (0L)));
                                              nanos_err = nanos_create_wd_and_run_compact(&nanos_wd_const_data.base, &nanos_wd_dyn_props, sizeof(struct nanos_args_10_t), &imm_args, 2, &dependences[0], imm_copy_data, imm_copy_dimensions, (nanos_translate_args_t)nanos_xlate_fun_kautodiffparallelc_10);
                                              if (nanos_err != NANOS_OK)
                                                {
                                                  nanos_handle_error(nanos_err);
                                                }
                                            }
                                        }
                                      }
                                  }
                                }
                            }
                        }
                    }
                  else
                    {
                      if (u == 0)
                        {
                          if (0 < l && l < layers - 1)
                            {
                              int i_d = start[node + layers];
                              {
                                _Bool mcc_is_in_final;
                                nanos_err_t mcc_err_in_final = nanos_in_final(&mcc_is_in_final);
                                if (mcc_err_in_final != NANOS_OK)
                                  {
                                    nanos_handle_error(mcc_err_in_final);
                                  }
                                if (mcc_is_in_final)
                                  {
                                    ompss_backward_prop(e, s, a, 3);
                                  }
                                else
                                  {
                                    {
                                      nanos_wd_dyn_props_t nanos_wd_dyn_props;
                                      struct nanos_args_11_t *ol_args;
                                      nanos_err_t nanos_err;
                                      struct nanos_args_11_t imm_args;
                                      nanos_region_dimension_t dimensions_25[1L];
                                      nanos_data_access_t dependences[3L];
                                      nanos_region_dimension_t dimensions_26[1L];
                                      nanos_region_dimension_t dimensions_27[1L];
                                      static nanos_smp_args_t smp_ol_kad_grad_11_args = {.outline = (void (*)(void *))(void (*)(struct nanos_args_11_t *))&smp_ol_kad_grad_11};
                                      static struct nanos_const_wd_definition_1 nanos_wd_const_data = {.base = {.props = {.mandatory_creation = 0, .tied = 0, .clear_chunk = 0, .reserved0 = 0, .reserved1 = 0, .reserved2 = 0, .reserved3 = 0, .reserved4 = 0}, .data_alignment = __alignof__(struct nanos_args_11_t), .num_copies = 3, .num_devices = 1, .num_dimensions = 3, .description = 0}, .devices = {[0] = {.factory = &nanos_smp_factory, .arg = &smp_ol_kad_grad_11_args}}};
                                      nanos_wd_dyn_props.tie_to = 0;
                                      nanos_wd_dyn_props.priority = 0;
                                      nanos_wd_dyn_props.flags.is_final = 0;
                                      nanos_wd_dyn_props.flags.is_implicit = 0;
                                      nanos_wd_dyn_props.flags.is_recover = 0;
                                      ol_args = (struct nanos_args_11_t *)0;
                                      nanos_wd_t nanos_wd_ = (nanos_wd_t)0;
                                      nanos_copy_data_t *ol_copy_data = (nanos_copy_data_t *)0;
                                      nanos_region_dimension_internal_t *ol_copy_dimensions = (nanos_region_dimension_internal_t *)0;
                                      nanos_err = nanos_create_wd_compact(&nanos_wd_, &nanos_wd_const_data.base, &nanos_wd_dyn_props, sizeof(struct nanos_args_11_t), (void **)&ol_args, nanos_current_wd(), &ol_copy_data, &ol_copy_dimensions);
                                      if (nanos_err != NANOS_OK)
                                        {
                                          nanos_handle_error(nanos_err);
                                        }
                                      dimensions_25[0].size = 1L * sizeof(kad_node_t *);
                                      dimensions_25[0].lower_bound = (i_d - 0L) * sizeof(kad_node_t *);
                                      dimensions_25[0].accessed_length = (i_d - 0L - (i_d - 0L) + 1) * sizeof(kad_node_t *);
                                      dependences[0].address = (void *)a;
                                      dependences[0].offset = 8L * (((i_d) - (0L)));
                                      dependences[0].dimensions = dimensions_25;
                                      dependences[0].flags.input = 1;
                                      dependences[0].flags.output = 0;
                                      dependences[0].flags.can_rename = 0;
                                      dependences[0].flags.concurrent = 0;
                                      dependences[0].flags.commutative = 0;
                                      dependences[0].dimension_count = 1;
                                      dimensions_26[0].size = 1L * sizeof(kad_node_t *);
                                      dimensions_26[0].lower_bound = (e + 1 - 0L) * sizeof(kad_node_t *);
                                      dimensions_26[0].accessed_length = (e + 1 - 0L - (e + 1 - 0L) + 1) * sizeof(kad_node_t *);
                                      dependences[1].address = (void *)a;
                                      dependences[1].offset = 8L * (((e + 1) - (0L)));
                                      dependences[1].dimensions = dimensions_26;
                                      dependences[1].flags.input = 1;
                                      dependences[1].flags.output = 0;
                                      dependences[1].flags.can_rename = 0;
                                      dependences[1].flags.concurrent = 0;
                                      dependences[1].flags.commutative = 0;
                                      dependences[1].dimension_count = 1;
                                      dimensions_27[0].size = 1L * sizeof(kad_node_t *);
                                      dimensions_27[0].lower_bound = (s - 0L) * sizeof(kad_node_t *);
                                      dimensions_27[0].accessed_length = (s - 0L - (s - 0L) + 1) * sizeof(kad_node_t *);
                                      dependences[2].address = (void *)a;
                                      dependences[2].offset = 8L * (((s) - (0L)));
                                      dependences[2].dimensions = dimensions_27;
                                      dependences[2].flags.input = 0;
                                      dependences[2].flags.output = 1;
                                      dependences[2].flags.can_rename = 0;
                                      dependences[2].flags.concurrent = 0;
                                      dependences[2].flags.commutative = 0;
                                      dependences[2].dimension_count = 1;
                                      if (nanos_wd_ != (nanos_wd_t)0)
                                        {
                                          (*ol_args).a = a;
                                          (*ol_args).e = e;
                                          (*ol_args).s = s;
                                          (*ol_args).i_d = i_d;
                                          ol_copy_dimensions[0 + 0].size = 1L * sizeof(kad_node_t *);
                                          ol_copy_dimensions[0 + 0].lower_bound = (i_d - 0L) * sizeof(kad_node_t *);
                                          ol_copy_dimensions[0 + 0].accessed_length = (i_d - 0L - (i_d - 0L) + 1) * sizeof(kad_node_t *);
                                          ol_copy_data[0].sharing = NANOS_SHARED;
                                          ol_copy_data[0].address = (void *)a;
                                          ol_copy_data[0].flags.input = 1;
                                          ol_copy_data[0].flags.output = 0;
                                          ol_copy_data[0].dimension_count = (short int)1;
                                          ol_copy_data[0].dimensions = &ol_copy_dimensions[0];
                                          ol_copy_data[0].offset = 8L * (((i_d) - (0L)));
                                          ol_copy_dimensions[1 + 0].size = 1L * sizeof(kad_node_t *);
                                          ol_copy_dimensions[1 + 0].lower_bound = (e + 1 - 0L) * sizeof(kad_node_t *);
                                          ol_copy_dimensions[1 + 0].accessed_length = (e + 1 - 0L - (e + 1 - 0L) + 1) * sizeof(kad_node_t *);
                                          ol_copy_data[1].sharing = NANOS_SHARED;
                                          ol_copy_data[1].address = (void *)a;
                                          ol_copy_data[1].flags.input = 1;
                                          ol_copy_data[1].flags.output = 0;
                                          ol_copy_data[1].dimension_count = (short int)1;
                                          ol_copy_data[1].dimensions = &ol_copy_dimensions[1];
                                          ol_copy_data[1].offset = 8L * (((e + 1) - (0L)));
                                          ol_copy_dimensions[2 + 0].size = 1L * sizeof(kad_node_t *);
                                          ol_copy_dimensions[2 + 0].lower_bound = (s - 0L) * sizeof(kad_node_t *);
                                          ol_copy_dimensions[2 + 0].accessed_length = (s - 0L - (s - 0L) + 1) * sizeof(kad_node_t *);
                                          ol_copy_data[2].sharing = NANOS_SHARED;
                                          ol_copy_data[2].address = (void *)a;
                                          ol_copy_data[2].flags.input = 0;
                                          ol_copy_data[2].flags.output = 1;
                                          ol_copy_data[2].dimension_count = (short int)1;
                                          ol_copy_data[2].dimensions = &ol_copy_dimensions[2];
                                          ol_copy_data[2].offset = 8L * (((s) - (0L)));
                                          nanos_err = nanos_set_translate_function(nanos_wd_, (nanos_translate_args_t)nanos_xlate_fun_kautodiffparallelc_11);
                                          if (nanos_err != NANOS_OK)
                                            {
                                              nanos_handle_error(nanos_err);
                                            }
                                          nanos_err = nanos_submit(nanos_wd_, 3, &dependences[0], (nanos_team_t)0);
                                          if (nanos_err != NANOS_OK)
                                            {
                                              nanos_handle_error(nanos_err);
                                            }
                                        }
                                      else
                                        {
                                          nanos_region_dimension_internal_t imm_copy_dimensions[3L];
                                          nanos_copy_data_t imm_copy_data[3L];
                                          imm_args.a = a;
                                          imm_args.e = e;
                                          imm_args.s = s;
                                          imm_args.i_d = i_d;
                                          imm_copy_dimensions[0 + 0].size = 1L * sizeof(kad_node_t *);
                                          imm_copy_dimensions[0 + 0].lower_bound = (i_d - 0L) * sizeof(kad_node_t *);
                                          imm_copy_dimensions[0 + 0].accessed_length = (i_d - 0L - (i_d - 0L) + 1) * sizeof(kad_node_t *);
                                          imm_copy_data[0].sharing = NANOS_SHARED;
                                          imm_copy_data[0].address = (void *)a;
                                          imm_copy_data[0].flags.input = 1;
                                          imm_copy_data[0].flags.output = 0;
                                          imm_copy_data[0].dimension_count = (short int)1;
                                          imm_copy_data[0].dimensions = &imm_copy_dimensions[0];
                                          imm_copy_data[0].offset = 8L * (((i_d) - (0L)));
                                          imm_copy_dimensions[1 + 0].size = 1L * sizeof(kad_node_t *);
                                          imm_copy_dimensions[1 + 0].lower_bound = (e + 1 - 0L) * sizeof(kad_node_t *);
                                          imm_copy_dimensions[1 + 0].accessed_length = (e + 1 - 0L - (e + 1 - 0L) + 1) * sizeof(kad_node_t *);
                                          imm_copy_data[1].sharing = NANOS_SHARED;
                                          imm_copy_data[1].address = (void *)a;
                                          imm_copy_data[1].flags.input = 1;
                                          imm_copy_data[1].flags.output = 0;
                                          imm_copy_data[1].dimension_count = (short int)1;
                                          imm_copy_data[1].dimensions = &imm_copy_dimensions[1];
                                          imm_copy_data[1].offset = 8L * (((e + 1) - (0L)));
                                          imm_copy_dimensions[2 + 0].size = 1L * sizeof(kad_node_t *);
                                          imm_copy_dimensions[2 + 0].lower_bound = (s - 0L) * sizeof(kad_node_t *);
                                          imm_copy_dimensions[2 + 0].accessed_length = (s - 0L - (s - 0L) + 1) * sizeof(kad_node_t *);
                                          imm_copy_data[2].sharing = NANOS_SHARED;
                                          imm_copy_data[2].address = (void *)a;
                                          imm_copy_data[2].flags.input = 0;
                                          imm_copy_data[2].flags.output = 1;
                                          imm_copy_data[2].dimension_count = (short int)1;
                                          imm_copy_data[2].dimensions = &imm_copy_dimensions[2];
                                          imm_copy_data[2].offset = 8L * (((s) - (0L)));
                                          nanos_err = nanos_create_wd_and_run_compact(&nanos_wd_const_data.base, &nanos_wd_dyn_props, sizeof(struct nanos_args_11_t), &imm_args, 3, &dependences[0], imm_copy_data, imm_copy_dimensions, (nanos_translate_args_t)nanos_xlate_fun_kautodiffparallelc_11);
                                          if (nanos_err != NANOS_OK)
                                            {
                                              nanos_handle_error(nanos_err);
                                            }
                                        }
                                    }
                                  }
                              }
                            }
                          else
                            {
                              if (l == layers - 1)
                                {
                                  int i_d = start[node + layers];
                                  {
                                    _Bool mcc_is_in_final;
                                    nanos_err_t mcc_err_in_final = nanos_in_final(&mcc_is_in_final);
                                    if (mcc_err_in_final != NANOS_OK)
                                      {
                                        nanos_handle_error(mcc_err_in_final);
                                      }
                                    if (mcc_is_in_final)
                                      {
                                        ompss_backward_prop(e, s, a, 3);
                                      }
                                    else
                                      {
                                        {
                                          nanos_wd_dyn_props_t nanos_wd_dyn_props;
                                          struct nanos_args_12_t *ol_args;
                                          nanos_err_t nanos_err;
                                          struct nanos_args_12_t imm_args;
                                          nanos_region_dimension_t dimensions_28[1L];
                                          nanos_data_access_t dependences[3L];
                                          nanos_region_dimension_t dimensions_29[1L];
                                          nanos_region_dimension_t dimensions_30[1L];
                                          static nanos_smp_args_t smp_ol_kad_grad_12_args = {.outline = (void (*)(void *))(void (*)(struct nanos_args_12_t *))&smp_ol_kad_grad_12};
                                          static struct nanos_const_wd_definition_1 nanos_wd_const_data = {.base = {.props = {.mandatory_creation = 0, .tied = 0, .clear_chunk = 0, .reserved0 = 0, .reserved1 = 0, .reserved2 = 0, .reserved3 = 0, .reserved4 = 0}, .data_alignment = __alignof__(struct nanos_args_12_t), .num_copies = 3, .num_devices = 1, .num_dimensions = 3, .description = 0}, .devices = {[0] = {.factory = &nanos_smp_factory, .arg = &smp_ol_kad_grad_12_args}}};
                                          nanos_wd_dyn_props.tie_to = 0;
                                          nanos_wd_dyn_props.priority = 0;
                                          nanos_wd_dyn_props.flags.is_final = 0;
                                          nanos_wd_dyn_props.flags.is_implicit = 0;
                                          nanos_wd_dyn_props.flags.is_recover = 0;
                                          ol_args = (struct nanos_args_12_t *)0;
                                          nanos_wd_t nanos_wd_ = (nanos_wd_t)0;
                                          nanos_copy_data_t *ol_copy_data = (nanos_copy_data_t *)0;
                                          nanos_region_dimension_internal_t *ol_copy_dimensions = (nanos_region_dimension_internal_t *)0;
                                          nanos_err = nanos_create_wd_compact(&nanos_wd_, &nanos_wd_const_data.base, &nanos_wd_dyn_props, sizeof(struct nanos_args_12_t), (void **)&ol_args, nanos_current_wd(), &ol_copy_data, &ol_copy_dimensions);
                                          if (nanos_err != NANOS_OK)
                                            {
                                              nanos_handle_error(nanos_err);
                                            }
                                          dimensions_28[0].size = 1L * sizeof(kad_node_t *);
                                          dimensions_28[0].lower_bound = (from - 0L) * sizeof(kad_node_t *);
                                          dimensions_28[0].accessed_length = (from - 0L - (from - 0L) + 1) * sizeof(kad_node_t *);
                                          dependences[0].address = (void *)a;
                                          dependences[0].offset = 8L * (((from) - (0L)));
                                          dependences[0].dimensions = dimensions_28;
                                          dependences[0].flags.input = 1;
                                          dependences[0].flags.output = 0;
                                          dependences[0].flags.can_rename = 0;
                                          dependences[0].flags.concurrent = 0;
                                          dependences[0].flags.commutative = 0;
                                          dependences[0].dimension_count = 1;
                                          dimensions_29[0].size = 1L * sizeof(kad_node_t *);
                                          dimensions_29[0].lower_bound = (i_d - 0L) * sizeof(kad_node_t *);
                                          dimensions_29[0].accessed_length = (i_d - 0L - (i_d - 0L) + 1) * sizeof(kad_node_t *);
                                          dependences[1].address = (void *)a;
                                          dependences[1].offset = 8L * (((i_d) - (0L)));
                                          dependences[1].dimensions = dimensions_29;
                                          dependences[1].flags.input = 1;
                                          dependences[1].flags.output = 0;
                                          dependences[1].flags.can_rename = 0;
                                          dependences[1].flags.concurrent = 0;
                                          dependences[1].flags.commutative = 0;
                                          dependences[1].dimension_count = 1;
                                          dimensions_30[0].size = 1L * sizeof(kad_node_t *);
                                          dimensions_30[0].lower_bound = (s - 0L) * sizeof(kad_node_t *);
                                          dimensions_30[0].accessed_length = (s - 0L - (s - 0L) + 1) * sizeof(kad_node_t *);
                                          dependences[2].address = (void *)a;
                                          dependences[2].offset = 8L * (((s) - (0L)));
                                          dependences[2].dimensions = dimensions_30;
                                          dependences[2].flags.input = 0;
                                          dependences[2].flags.output = 1;
                                          dependences[2].flags.can_rename = 0;
                                          dependences[2].flags.concurrent = 0;
                                          dependences[2].flags.commutative = 0;
                                          dependences[2].dimension_count = 1;
                                          if (nanos_wd_ != (nanos_wd_t)0)
                                            {
                                              (*ol_args).a = a;
                                              (*ol_args).e = e;
                                              (*ol_args).s = s;
                                              (*ol_args).from = from;
                                              (*ol_args).i_d = i_d;
                                              ol_copy_dimensions[0 + 0].size = 1L * sizeof(kad_node_t *);
                                              ol_copy_dimensions[0 + 0].lower_bound = (from - 0L) * sizeof(kad_node_t *);
                                              ol_copy_dimensions[0 + 0].accessed_length = (from - 0L - (from - 0L) + 1) * sizeof(kad_node_t *);
                                              ol_copy_data[0].sharing = NANOS_SHARED;
                                              ol_copy_data[0].address = (void *)a;
                                              ol_copy_data[0].flags.input = 1;
                                              ol_copy_data[0].flags.output = 0;
                                              ol_copy_data[0].dimension_count = (short int)1;
                                              ol_copy_data[0].dimensions = &ol_copy_dimensions[0];
                                              ol_copy_data[0].offset = 8L * (((from) - (0L)));
                                              ol_copy_dimensions[1 + 0].size = 1L * sizeof(kad_node_t *);
                                              ol_copy_dimensions[1 + 0].lower_bound = (i_d - 0L) * sizeof(kad_node_t *);
                                              ol_copy_dimensions[1 + 0].accessed_length = (i_d - 0L - (i_d - 0L) + 1) * sizeof(kad_node_t *);
                                              ol_copy_data[1].sharing = NANOS_SHARED;
                                              ol_copy_data[1].address = (void *)a;
                                              ol_copy_data[1].flags.input = 1;
                                              ol_copy_data[1].flags.output = 0;
                                              ol_copy_data[1].dimension_count = (short int)1;
                                              ol_copy_data[1].dimensions = &ol_copy_dimensions[1];
                                              ol_copy_data[1].offset = 8L * (((i_d) - (0L)));
                                              ol_copy_dimensions[2 + 0].size = 1L * sizeof(kad_node_t *);
                                              ol_copy_dimensions[2 + 0].lower_bound = (s - 0L) * sizeof(kad_node_t *);
                                              ol_copy_dimensions[2 + 0].accessed_length = (s - 0L - (s - 0L) + 1) * sizeof(kad_node_t *);
                                              ol_copy_data[2].sharing = NANOS_SHARED;
                                              ol_copy_data[2].address = (void *)a;
                                              ol_copy_data[2].flags.input = 0;
                                              ol_copy_data[2].flags.output = 1;
                                              ol_copy_data[2].dimension_count = (short int)1;
                                              ol_copy_data[2].dimensions = &ol_copy_dimensions[2];
                                              ol_copy_data[2].offset = 8L * (((s) - (0L)));
                                              nanos_err = nanos_set_translate_function(nanos_wd_, (nanos_translate_args_t)nanos_xlate_fun_kautodiffparallelc_12);
                                              if (nanos_err != NANOS_OK)
                                                {
                                                  nanos_handle_error(nanos_err);
                                                }
                                              nanos_err = nanos_submit(nanos_wd_, 3, &dependences[0], (nanos_team_t)0);
                                              if (nanos_err != NANOS_OK)
                                                {
                                                  nanos_handle_error(nanos_err);
                                                }
                                            }
                                          else
                                            {
                                              nanos_region_dimension_internal_t imm_copy_dimensions[3L];
                                              nanos_copy_data_t imm_copy_data[3L];
                                              imm_args.a = a;
                                              imm_args.e = e;
                                              imm_args.s = s;
                                              imm_args.from = from;
                                              imm_args.i_d = i_d;
                                              imm_copy_dimensions[0 + 0].size = 1L * sizeof(kad_node_t *);
                                              imm_copy_dimensions[0 + 0].lower_bound = (from - 0L) * sizeof(kad_node_t *);
                                              imm_copy_dimensions[0 + 0].accessed_length = (from - 0L - (from - 0L) + 1) * sizeof(kad_node_t *);
                                              imm_copy_data[0].sharing = NANOS_SHARED;
                                              imm_copy_data[0].address = (void *)a;
                                              imm_copy_data[0].flags.input = 1;
                                              imm_copy_data[0].flags.output = 0;
                                              imm_copy_data[0].dimension_count = (short int)1;
                                              imm_copy_data[0].dimensions = &imm_copy_dimensions[0];
                                              imm_copy_data[0].offset = 8L * (((from) - (0L)));
                                              imm_copy_dimensions[1 + 0].size = 1L * sizeof(kad_node_t *);
                                              imm_copy_dimensions[1 + 0].lower_bound = (i_d - 0L) * sizeof(kad_node_t *);
                                              imm_copy_dimensions[1 + 0].accessed_length = (i_d - 0L - (i_d - 0L) + 1) * sizeof(kad_node_t *);
                                              imm_copy_data[1].sharing = NANOS_SHARED;
                                              imm_copy_data[1].address = (void *)a;
                                              imm_copy_data[1].flags.input = 1;
                                              imm_copy_data[1].flags.output = 0;
                                              imm_copy_data[1].dimension_count = (short int)1;
                                              imm_copy_data[1].dimensions = &imm_copy_dimensions[1];
                                              imm_copy_data[1].offset = 8L * (((i_d) - (0L)));
                                              imm_copy_dimensions[2 + 0].size = 1L * sizeof(kad_node_t *);
                                              imm_copy_dimensions[2 + 0].lower_bound = (s - 0L) * sizeof(kad_node_t *);
                                              imm_copy_dimensions[2 + 0].accessed_length = (s - 0L - (s - 0L) + 1) * sizeof(kad_node_t *);
                                              imm_copy_data[2].sharing = NANOS_SHARED;
                                              imm_copy_data[2].address = (void *)a;
                                              imm_copy_data[2].flags.input = 0;
                                              imm_copy_data[2].flags.output = 1;
                                              imm_copy_data[2].dimension_count = (short int)1;
                                              imm_copy_data[2].dimensions = &imm_copy_dimensions[2];
                                              imm_copy_data[2].offset = 8L * (((s) - (0L)));
                                              nanos_err = nanos_create_wd_and_run_compact(&nanos_wd_const_data.base, &nanos_wd_dyn_props, sizeof(struct nanos_args_12_t), &imm_args, 3, &dependences[0], imm_copy_data, imm_copy_dimensions, (nanos_translate_args_t)nanos_xlate_fun_kautodiffparallelc_12);
                                              if (nanos_err != NANOS_OK)
                                                {
                                                  nanos_handle_error(nanos_err);
                                                }
                                            }
                                        }
                                      }
                                  }
                                }
                              else
                                {
                                  if (l == 0)
                                    {
                                      int i_d = start[node + layers];
                                      {
                                        _Bool mcc_is_in_final;
                                        nanos_err_t mcc_err_in_final = nanos_in_final(&mcc_is_in_final);
                                        if (mcc_err_in_final != NANOS_OK)
                                          {
                                            nanos_handle_error(mcc_err_in_final);
                                          }
                                        if (mcc_is_in_final)
                                          {
                                            ompss_backward_prop(e, s, a, 3);
                                          }
                                        else
                                          {
                                            {
                                              nanos_wd_dyn_props_t nanos_wd_dyn_props;
                                              struct nanos_args_13_t *ol_args;
                                              nanos_err_t nanos_err;
                                              struct nanos_args_13_t imm_args;
                                              nanos_region_dimension_t dimensions_31[1L];
                                              nanos_data_access_t dependences[3L];
                                              nanos_region_dimension_t dimensions_32[1L];
                                              nanos_region_dimension_t dimensions_33[1L];
                                              static nanos_smp_args_t smp_ol_kad_grad_13_args = {.outline = (void (*)(void *))(void (*)(struct nanos_args_13_t *))&smp_ol_kad_grad_13};
                                              static struct nanos_const_wd_definition_1 nanos_wd_const_data = {.base = {.props = {.mandatory_creation = 0, .tied = 0, .clear_chunk = 0, .reserved0 = 0, .reserved1 = 0, .reserved2 = 0, .reserved3 = 0, .reserved4 = 0}, .data_alignment = __alignof__(struct nanos_args_13_t), .num_copies = 3, .num_devices = 1, .num_dimensions = 3, .description = 0}, .devices = {[0] = {.factory = &nanos_smp_factory, .arg = &smp_ol_kad_grad_13_args}}};
                                              nanos_wd_dyn_props.tie_to = 0;
                                              nanos_wd_dyn_props.priority = 0;
                                              nanos_wd_dyn_props.flags.is_final = 0;
                                              nanos_wd_dyn_props.flags.is_implicit = 0;
                                              nanos_wd_dyn_props.flags.is_recover = 0;
                                              ol_args = (struct nanos_args_13_t *)0;
                                              nanos_wd_t nanos_wd_ = (nanos_wd_t)0;
                                              nanos_copy_data_t *ol_copy_data = (nanos_copy_data_t *)0;
                                              nanos_region_dimension_internal_t *ol_copy_dimensions = (nanos_region_dimension_internal_t *)0;
                                              nanos_err = nanos_create_wd_compact(&nanos_wd_, &nanos_wd_const_data.base, &nanos_wd_dyn_props, sizeof(struct nanos_args_13_t), (void **)&ol_args, nanos_current_wd(), &ol_copy_data, &ol_copy_dimensions);
                                              if (nanos_err != NANOS_OK)
                                                {
                                                  nanos_handle_error(nanos_err);
                                                }
                                              dimensions_31[0].size = 1L * sizeof(kad_node_t *);
                                              dimensions_31[0].lower_bound = (e + 1 - 0L) * sizeof(kad_node_t *);
                                              dimensions_31[0].accessed_length = (e + 1 - 0L - (e + 1 - 0L) + 1) * sizeof(kad_node_t *);
                                              dependences[0].address = (void *)a;
                                              dependences[0].offset = 8L * (((e + 1) - (0L)));
                                              dependences[0].dimensions = dimensions_31;
                                              dependences[0].flags.input = 1;
                                              dependences[0].flags.output = 0;
                                              dependences[0].flags.can_rename = 0;
                                              dependences[0].flags.concurrent = 0;
                                              dependences[0].flags.commutative = 0;
                                              dependences[0].dimension_count = 1;
                                              dimensions_32[0].size = 1L * sizeof(kad_node_t *);
                                              dimensions_32[0].lower_bound = (i_d - 0L) * sizeof(kad_node_t *);
                                              dimensions_32[0].accessed_length = (i_d - 0L - (i_d - 0L) + 1) * sizeof(kad_node_t *);
                                              dependences[1].address = (void *)a;
                                              dependences[1].offset = 8L * (((i_d) - (0L)));
                                              dependences[1].dimensions = dimensions_32;
                                              dependences[1].flags.input = 1;
                                              dependences[1].flags.output = 0;
                                              dependences[1].flags.can_rename = 0;
                                              dependences[1].flags.concurrent = 0;
                                              dependences[1].flags.commutative = 0;
                                              dependences[1].dimension_count = 1;
                                              dimensions_33[0].size = 1L * sizeof(kad_node_t *);
                                              dimensions_33[0].lower_bound = (s - 0L) * sizeof(kad_node_t *);
                                              dimensions_33[0].accessed_length = (s - 0L - (s - 0L) + 1) * sizeof(kad_node_t *);
                                              dependences[2].address = (void *)a;
                                              dependences[2].offset = 8L * (((s) - (0L)));
                                              dependences[2].dimensions = dimensions_33;
                                              dependences[2].flags.input = 0;
                                              dependences[2].flags.output = 1;
                                              dependences[2].flags.can_rename = 0;
                                              dependences[2].flags.concurrent = 0;
                                              dependences[2].flags.commutative = 0;
                                              dependences[2].dimension_count = 1;
                                              if (nanos_wd_ != (nanos_wd_t)0)
                                                {
                                                  (*ol_args).a = a;
                                                  (*ol_args).e = e;
                                                  (*ol_args).s = s;
                                                  (*ol_args).i_d = i_d;
                                                  ol_copy_dimensions[0 + 0].size = 1L * sizeof(kad_node_t *);
                                                  ol_copy_dimensions[0 + 0].lower_bound = (e + 1 - 0L) * sizeof(kad_node_t *);
                                                  ol_copy_dimensions[0 + 0].accessed_length = (e + 1 - 0L - (e + 1 - 0L) + 1) * sizeof(kad_node_t *);
                                                  ol_copy_data[0].sharing = NANOS_SHARED;
                                                  ol_copy_data[0].address = (void *)a;
                                                  ol_copy_data[0].flags.input = 1;
                                                  ol_copy_data[0].flags.output = 0;
                                                  ol_copy_data[0].dimension_count = (short int)1;
                                                  ol_copy_data[0].dimensions = &ol_copy_dimensions[0];
                                                  ol_copy_data[0].offset = 8L * (((e + 1) - (0L)));
                                                  ol_copy_dimensions[1 + 0].size = 1L * sizeof(kad_node_t *);
                                                  ol_copy_dimensions[1 + 0].lower_bound = (i_d - 0L) * sizeof(kad_node_t *);
                                                  ol_copy_dimensions[1 + 0].accessed_length = (i_d - 0L - (i_d - 0L) + 1) * sizeof(kad_node_t *);
                                                  ol_copy_data[1].sharing = NANOS_SHARED;
                                                  ol_copy_data[1].address = (void *)a;
                                                  ol_copy_data[1].flags.input = 1;
                                                  ol_copy_data[1].flags.output = 0;
                                                  ol_copy_data[1].dimension_count = (short int)1;
                                                  ol_copy_data[1].dimensions = &ol_copy_dimensions[1];
                                                  ol_copy_data[1].offset = 8L * (((i_d) - (0L)));
                                                  ol_copy_dimensions[2 + 0].size = 1L * sizeof(kad_node_t *);
                                                  ol_copy_dimensions[2 + 0].lower_bound = (s - 0L) * sizeof(kad_node_t *);
                                                  ol_copy_dimensions[2 + 0].accessed_length = (s - 0L - (s - 0L) + 1) * sizeof(kad_node_t *);
                                                  ol_copy_data[2].sharing = NANOS_SHARED;
                                                  ol_copy_data[2].address = (void *)a;
                                                  ol_copy_data[2].flags.input = 0;
                                                  ol_copy_data[2].flags.output = 1;
                                                  ol_copy_data[2].dimension_count = (short int)1;
                                                  ol_copy_data[2].dimensions = &ol_copy_dimensions[2];
                                                  ol_copy_data[2].offset = 8L * (((s) - (0L)));
                                                  nanos_err = nanos_set_translate_function(nanos_wd_, (nanos_translate_args_t)nanos_xlate_fun_kautodiffparallelc_13);
                                                  if (nanos_err != NANOS_OK)
                                                    {
                                                      nanos_handle_error(nanos_err);
                                                    }
                                                  nanos_err = nanos_submit(nanos_wd_, 3, &dependences[0], (nanos_team_t)0);
                                                  if (nanos_err != NANOS_OK)
                                                    {
                                                      nanos_handle_error(nanos_err);
                                                    }
                                                }
                                              else
                                                {
                                                  nanos_region_dimension_internal_t imm_copy_dimensions[3L];
                                                  nanos_copy_data_t imm_copy_data[3L];
                                                  imm_args.a = a;
                                                  imm_args.e = e;
                                                  imm_args.s = s;
                                                  imm_args.i_d = i_d;
                                                  imm_copy_dimensions[0 + 0].size = 1L * sizeof(kad_node_t *);
                                                  imm_copy_dimensions[0 + 0].lower_bound = (e + 1 - 0L) * sizeof(kad_node_t *);
                                                  imm_copy_dimensions[0 + 0].accessed_length = (e + 1 - 0L - (e + 1 - 0L) + 1) * sizeof(kad_node_t *);
                                                  imm_copy_data[0].sharing = NANOS_SHARED;
                                                  imm_copy_data[0].address = (void *)a;
                                                  imm_copy_data[0].flags.input = 1;
                                                  imm_copy_data[0].flags.output = 0;
                                                  imm_copy_data[0].dimension_count = (short int)1;
                                                  imm_copy_data[0].dimensions = &imm_copy_dimensions[0];
                                                  imm_copy_data[0].offset = 8L * (((e + 1) - (0L)));
                                                  imm_copy_dimensions[1 + 0].size = 1L * sizeof(kad_node_t *);
                                                  imm_copy_dimensions[1 + 0].lower_bound = (i_d - 0L) * sizeof(kad_node_t *);
                                                  imm_copy_dimensions[1 + 0].accessed_length = (i_d - 0L - (i_d - 0L) + 1) * sizeof(kad_node_t *);
                                                  imm_copy_data[1].sharing = NANOS_SHARED;
                                                  imm_copy_data[1].address = (void *)a;
                                                  imm_copy_data[1].flags.input = 1;
                                                  imm_copy_data[1].flags.output = 0;
                                                  imm_copy_data[1].dimension_count = (short int)1;
                                                  imm_copy_data[1].dimensions = &imm_copy_dimensions[1];
                                                  imm_copy_data[1].offset = 8L * (((i_d) - (0L)));
                                                  imm_copy_dimensions[2 + 0].size = 1L * sizeof(kad_node_t *);
                                                  imm_copy_dimensions[2 + 0].lower_bound = (s - 0L) * sizeof(kad_node_t *);
                                                  imm_copy_dimensions[2 + 0].accessed_length = (s - 0L - (s - 0L) + 1) * sizeof(kad_node_t *);
                                                  imm_copy_data[2].sharing = NANOS_SHARED;
                                                  imm_copy_data[2].address = (void *)a;
                                                  imm_copy_data[2].flags.input = 0;
                                                  imm_copy_data[2].flags.output = 1;
                                                  imm_copy_data[2].dimension_count = (short int)1;
                                                  imm_copy_data[2].dimensions = &imm_copy_dimensions[2];
                                                  imm_copy_data[2].offset = 8L * (((s) - (0L)));
                                                  nanos_err = nanos_create_wd_and_run_compact(&nanos_wd_const_data.base, &nanos_wd_dyn_props, sizeof(struct nanos_args_13_t), &imm_args, 3, &dependences[0], imm_copy_data, imm_copy_dimensions, (nanos_translate_args_t)nanos_xlate_fun_kautodiffparallelc_13);
                                                  if (nanos_err != NANOS_OK)
                                                    {
                                                      nanos_handle_error(nanos_err);
                                                    }
                                                }
                                            }
                                          }
                                      }
                                    }
                                }
                            }
                        }
                    }
                }
               --node;
            }
        }
      {
        nanos_err_t nanos_err;
        nanos_wd_t nanos_wd_ = nanos_current_wd();
        nanos_err = nanos_wg_wait_completion(nanos_wd_, 0);
        if (nanos_err != NANOS_OK)
          {
            nanos_handle_error(nanos_err);
          }
      }
      for (i = 0; i <= from;  ++i)
        {
          (*a[i]).tmp = 0;
        }
    }
}
extern size_t fwrite(const void *__restrict __ptr, size_t __size, size_t __n, FILE *__restrict __s);
static void kad_save1(FILE *fp, const kad_node_t *p)
{
  fwrite(&(*p).ext_label, 4, 1, fp);
  fwrite(&(*p).ext_flag, 4, 1, fp);
  fwrite(&(*p).flag, 1, 1, fp);
  fwrite(&(*p).n_child, 4, 1, fp);
  if ((*p).n_child)
    {
      int32_t j;
      int32_t pre = (*p).pre ? (*(*p).pre).tmp :  -1;
      fwrite(&(*p).op, 2, 1, fp);
      for (j = 0; j < (*p).n_child;  ++j)
        {
          fwrite(&(*(*p).child[j]).tmp, 4, 1, fp);
        }
      fwrite(&pre, 4, 1, fp);
      fwrite(&(*p).ptr_size, 4, 1, fp);
      if ((*p).ptr_size > 0 && (*p).ptr)
        {
          fwrite((*p).ptr, (*p).ptr_size, 1, fp);
        }
    }
  else
    {
      fwrite(&(*p).n_d, 1, 1, fp);
      if ((*p).n_d)
        {
          fwrite((*p).d, 4, (*p).n_d, fp);
        }
    }
}
extern size_t fread(void *__restrict __ptr, size_t __size, size_t __n, FILE *__restrict __stream);
static kad_node_t *kad_load1(FILE *fp, kad_node_t **node)
{
  kad_node_t *p;
  p = (kad_node_t *)calloc(1, sizeof(kad_node_t));
  fread(&(*p).ext_label, 4, 1, fp);
  fread(&(*p).ext_flag, 4, 1, fp);
  fread(&(*p).flag, 1, 1, fp);
  fread(&(*p).n_child, 4, 1, fp);
  if ((*p).n_child)
    {
      int32_t j;
      int32_t k;
      (*p).child = (kad_node_t **)calloc((*p).n_child, sizeof(kad_node_t *));
      fread(&(*p).op, 2, 1, fp);
      for (j = 0; j < (*p).n_child;  ++j)
        {
          fread(&k, 4, 1, fp);
          (*p).child[j] = node ? node[k] : 0;
        }
      fread(&k, 4, 1, fp);
      if (k >= 0)
        {
          (*p).pre = node[k];
        }
      fread(&(*p).ptr_size, 4, 1, fp);
      if ((*p).ptr_size > 0)
        {
          (*p).ptr = malloc((*p).ptr_size);
          fread((*p).ptr, (*p).ptr_size, 1, fp);
        }
    }
  else
    {
      fread(&(*p).n_d, 1, 1, fp);
      if ((*p).n_d)
        {
          fread((*p).d, 4, (*p).n_d, fp);
        }
    }
  return p;
}
int kad_save(FILE *fp, int n_node, kad_node_t **node)
{
  int32_t i;
  int32_t k = n_node;
  fwrite(&k, 4, 1, fp);
  for (i = 0; i < n_node;  ++i)
    {
      (*node[i]).tmp = i;
    }
  for (i = 0; i < n_node;  ++i)
    {
      kad_save1(fp, node[i]);
    }
  for (i = 0; i < n_node;  ++i)
    {
      (*node[i]).tmp = 0;
    }
  return 0;
}
kad_node_t **kad_load(FILE *fp, int *_n_node)
{
  int32_t n_node;
  kad_node_t **node;
  int32_t i;
  fread(&n_node, 4, 1, fp);
  node = (kad_node_t **)malloc(n_node * sizeof(kad_node_t *));
  for (i = 0; i < n_node;  ++i)
    {
      kad_node_t *p;
      p = node[i] = kad_load1(fp, node);
      if ((*p).n_child)
        {
          kad_op_list[(*p).op](p, 1);
          kad_op_list[(*p).op](p, 4);
        }
    }
  *_n_node = n_node;
  kad_mark_back(n_node, node);
  return node;
}
extern void *memcpy(void *__restrict __dest, const void *__restrict __src, size_t __n) __attribute__((__nothrow__)) __attribute__((__leaf__)) __attribute__((__nonnull__(1, 2)));
static __inline kad_node_t *kad_dup1(const kad_node_t *p)
{
  kad_node_t *q;
  q = (kad_node_t *)malloc(sizeof(kad_node_t));
  memcpy(q, p, sizeof(kad_node_t));
  ((((*q).pre = 0, (*q).tmp = 0)), (*q).gtmp = 0);
  if ((*p).ptr && (*p).ptr_size > 0)
    {
      if ((((*p).op == 15 || (*p).op == 24) && !((*p).flag & 16)) && (*p).ptr_size == sizeof(kad_rng_t))
        {
          (*q).ptr = kad_rng();
        }
      else
        {
          (*q).ptr = malloc((*p).ptr_size);
          memcpy((*q).ptr, (*p).ptr, (*p).ptr_size);
        }
    }
  if ((*q).n_child)
    {
      (*q).x = (*q).g = 0;
      (*q).child = (kad_node_t **)calloc((*q).n_child, sizeof(kad_node_t *));
    }
  return q;
}
kad_node_t **kad_clone(int n, kad_node_t **v, int batch_size)
{
  kad_node_t **u;
  int i;
  int j;
  u = (kad_node_t **)calloc(n, sizeof(kad_node_t *));
  for (i = 0; i < n;  ++i)
    {
      (*v[i]).tmp = i;
    }
  for (i = 0; i < n;  ++i)
    {
      kad_node_t *q;
      kad_node_t *p = v[i];
      q = u[i] = kad_dup1(p);
      if ((*p).pre)
        {
          (*q).pre = u[(*(*p).pre).tmp];
        }
      if ((*p).n_child)
        {
          for (j = 0; j < (*p).n_child;  ++j)
            {
              (*q).child[j] = u[(*(*p).child[j]).tmp];
            }
        }
      else
        {
          if (!(((*p).n_child == 0 && !((*p).flag & 1)) && !((*p).flag & 2)))
            {
              (*q).x = (float *)malloc(kad_len(p) * sizeof(float));
              memcpy((*q).x, (*p).x, kad_len(p) * sizeof(float));
              (*q).g = 0;
            }
        }
    }
  for (i = 0; i < n;  ++i)
    {
      (*v[i]).tmp = 0;
    }
  kad_sync_dim(n, u, batch_size);
  return u;
}
struct mcc_struct_anon_46;
typedef struct mcc_struct_anon_46 nodes_t;
struct  mcc_struct_anon_46
{
  int32_t n;
  int32_t m;
  kad_node_t **v;
};
static __inline void push_nodes(nodes_t *w, kad_node_t *p)
{
  if ((*w).n == (*w).m)
    {
      (*w).m = (*w).m ? (*w).m << 1 : 16;
      (*w).v = (kad_node_t **)realloc((*w).v, (*w).m * sizeof(kad_node_t *));
    }
  (*w).v[(*w).n++] = p;
}
static void kad_unroll_helper(int n_v, kad_node_t **v, int i_pivot, kad_node_t **t, int len, nodes_t *w)
{
  uint8_t *flag;
  int i;
  int j;
  kad_node_t **aux;
  int l;
  static const char __MERCURIUM_PRETTY_FUNCTION__[79L] = "void kad_unroll_helper(int, kad_node_t **, int, kad_node_t **, int, nodes_t *)";
  ((*v[i_pivot]).n_child == 1 && (*v[i_pivot]).flag & 4) && t[i_pivot] == 0 ? (void)0 : __assert_fail("((v[i_pivot])->n_child == 1 && ((v[i_pivot])->flag & 0x4)) && t[i_pivot] == 0", "kautodiff_parallel.c", 875, __MERCURIUM_PRETTY_FUNCTION__);
  t[i_pivot] = kad_dup1(v[i_pivot]);
  (*t[i_pivot]).n_child = len;
  (*t[i_pivot]).child = (kad_node_t **)realloc((*t[i_pivot]).child, len * sizeof(kad_node_t *));
  flag = (uint8_t *)calloc(n_v, 1);
  for ((i = i_pivot, flag[i] = 16); i >= 0;  --i)
    {
      if (i < i_pivot && ((*v[i]).n_child == 1 && (*v[i]).flag & 4))
        {
          continue;
        }
      if (flag[i] & 16)
        {
          for (j = 0; j < (*v[i]).n_child;  ++j)
            {
              flag[(*(*v[i]).child[j]).tmp] = 16;
            }
        }
    }
  for (i = 0; i < i_pivot;  ++i)
    {
      if (!(flag[i] & 16))
        {
          continue;
        }
      if ((((*v[i]).n_child == 0 && (*v[i]).flag & 1) || ((*v[i]).n_child == 0 && (*v[i]).flag & 2)) || ((*v[i]).n_child == 1 && (*v[i]).flag & 4))
        {
          flag[i] |= 1;
        }
      if ((*v[i]).pre)
        {
          flag[(*(*v[i]).pre).tmp] |= 2;
        }
    }
  flag[(*(*v[i_pivot]).child[0]).tmp] |= 4;
  aux = (kad_node_t **)calloc(n_v, sizeof(kad_node_t *));
  for (l = 0; l < len;  ++l)
    {
      for (i = 0; i < i_pivot;  ++i)
        {
          if (!(flag[i] & 16) || (flag[i] & 3 && t[i]))
            {
              continue;
            }
          t[i] = kad_dup1(v[i]);
          if ((*v[i]).n_child)
            {
              for (j = 0; j < (*v[i]).n_child;  ++j)
                {
                  (*t[i]).child[j] = t[(*(*v[i]).child[j]).tmp];
                }
            }
          if (flag[i] & 4)
            {
              (*t[i_pivot]).child[l] = t[i];
            }
          if (l == 0 && flag[i] & 2)
            {
              aux[i] = t[i];
            }
          if ((*v[i]).pre)
            {
              t[(*(*v[i]).pre).tmp] = t[i];
              if (l == len - 1)
                {
                  (*t[i]).pre = aux[(*(*v[i]).pre).tmp];
                }
            }
          push_nodes(w, t[i]);
        }
    }
  push_nodes(w, t[i_pivot]);
  free(aux);
  free(flag);
}
int kad_n_pivots(int n_v, kad_node_t **v)
{
  int i;
  int n_pivots = 0;
  for (i = 0; i < n_v;  ++i)
    {
      if ((*v[i]).n_child == 1 && (*v[i]).flag & 4)
        {
           ++n_pivots;
        }
    }
  return n_pivots;
}
kad_node_t **kad_unroll(int n_v, kad_node_t **v, int *new_n, int *len)
{
  kad_node_t **t;
  int i;
  int j;
  int n_pivots = 0;
  nodes_t w = {.n = 0, .m = 0, .v = 0};
  t = (kad_node_t **)calloc(n_v, sizeof(kad_node_t *));
  n_pivots = kad_n_pivots(n_v, v);
  for (i = 0; i < n_v;  ++i)
    {
      (*v[i]).tmp = i;
    }
  if (n_pivots)
    {
      int *i_pivots;
      int k;
      i_pivots = (int *)calloc(n_pivots, sizeof(int));
      for (i = k = 0; i < n_v;  ++i)
        {
          if ((*v[i]).n_child == 1 && (*v[i]).flag & 4)
            {
              i_pivots[k++] = i;
            }
        }
      for (i = 0; i < n_pivots;  ++i)
        {
          kad_unroll_helper(n_v, v, i_pivots[i], t, len[i], &w);
        }
      free(i_pivots);
    }
  for (i = 0; i < n_v;  ++i)
    {
      if (t[i])
        {
          continue;
        }
      t[i] = kad_dup1(v[i]);
      if ((*v[i]).n_child)
        {
          for (j = 0; j < (*v[i]).n_child;  ++j)
            {
              (*t[i]).child[j] = t[(*(*v[i]).child[j]).tmp];
            }
        }
      push_nodes(&w, t[i]);
    }
  free(t);
  for (i = 0; i < n_v;  ++i)
    {
      (*v[i]).tmp = 0;
    }
  for (i = 0; i < w.n;  ++i)
    {
      if ((*w.v[i]).n_child > 0)
        {
          kad_op_list[(*w.v[i]).op](w.v[i], 4);
        }
    }
  kad_allocate_internal(w.n, w.v);
  *new_n = w.n;
  return w.v;
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) void _mm_empty(void)
{
  __builtin_ia32_emms();
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) void _m_empty(void)
{
  _mm_empty();
}
typedef __attribute__((vector_size(8))) __attribute__((__may_alias__)) int __m64;
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_cvtsi32_si64(int __i)
{
  return (__m64)__builtin_ia32_vec_init_v2si(__i, 0);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _m_from_int(int __i)
{
  return _mm_cvtsi32_si64(__i);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _m_from_int64(long long int __i)
{
  return (__m64)__i;
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_cvtsi64_m64(long long int __i)
{
  return (__m64)__i;
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_cvtsi64x_si64(long long int __i)
{
  return (__m64)__i;
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_set_pi64x(long long int __i)
{
  return (__m64)__i;
}
typedef __attribute__((vector_size(8))) __attribute__((__may_alias__)) int __v2si;
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) int _mm_cvtsi64_si32(__m64 __i)
{
  return __builtin_ia32_vec_ext_v2si((__v2si)__i, 0);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) int _m_to_int(__m64 __i)
{
  return _mm_cvtsi64_si32(__i);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) long long int _m_to_int64(__m64 __i)
{
  return (long long int)__i;
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) long long int _mm_cvtm64_si64(__m64 __i)
{
  return (long long int)__i;
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) long long int _mm_cvtsi64_si64x(__m64 __i)
{
  return (long long int)__i;
}
typedef __attribute__((vector_size(8))) __attribute__((__may_alias__)) short int __v4hi;
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_packs_pi16(__m64 __m1, __m64 __m2)
{
  return (__m64)__builtin_ia32_packsswb((__v4hi)__m1, (__v4hi)__m2);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _m_packsswb(__m64 __m1, __m64 __m2)
{
  return _mm_packs_pi16(__m1, __m2);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_packs_pi32(__m64 __m1, __m64 __m2)
{
  return (__m64)__builtin_ia32_packssdw((__v2si)__m1, (__v2si)__m2);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _m_packssdw(__m64 __m1, __m64 __m2)
{
  return _mm_packs_pi32(__m1, __m2);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_packs_pu16(__m64 __m1, __m64 __m2)
{
  return (__m64)__builtin_ia32_packuswb((__v4hi)__m1, (__v4hi)__m2);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _m_packuswb(__m64 __m1, __m64 __m2)
{
  return _mm_packs_pu16(__m1, __m2);
}
typedef __attribute__((vector_size(8))) __attribute__((__may_alias__)) char __v8qi;
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_unpackhi_pi8(__m64 __m1, __m64 __m2)
{
  return (__m64)__builtin_ia32_punpckhbw((__v8qi)__m1, (__v8qi)__m2);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _m_punpckhbw(__m64 __m1, __m64 __m2)
{
  return _mm_unpackhi_pi8(__m1, __m2);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_unpackhi_pi16(__m64 __m1, __m64 __m2)
{
  return (__m64)__builtin_ia32_punpckhwd((__v4hi)__m1, (__v4hi)__m2);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _m_punpckhwd(__m64 __m1, __m64 __m2)
{
  return _mm_unpackhi_pi16(__m1, __m2);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_unpackhi_pi32(__m64 __m1, __m64 __m2)
{
  return (__m64)__builtin_ia32_punpckhdq((__v2si)__m1, (__v2si)__m2);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _m_punpckhdq(__m64 __m1, __m64 __m2)
{
  return _mm_unpackhi_pi32(__m1, __m2);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_unpacklo_pi8(__m64 __m1, __m64 __m2)
{
  return (__m64)__builtin_ia32_punpcklbw((__v8qi)__m1, (__v8qi)__m2);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _m_punpcklbw(__m64 __m1, __m64 __m2)
{
  return _mm_unpacklo_pi8(__m1, __m2);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_unpacklo_pi16(__m64 __m1, __m64 __m2)
{
  return (__m64)__builtin_ia32_punpcklwd((__v4hi)__m1, (__v4hi)__m2);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _m_punpcklwd(__m64 __m1, __m64 __m2)
{
  return _mm_unpacklo_pi16(__m1, __m2);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_unpacklo_pi32(__m64 __m1, __m64 __m2)
{
  return (__m64)__builtin_ia32_punpckldq((__v2si)__m1, (__v2si)__m2);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _m_punpckldq(__m64 __m1, __m64 __m2)
{
  return _mm_unpacklo_pi32(__m1, __m2);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_add_pi8(__m64 __m1, __m64 __m2)
{
  return (__m64)__builtin_ia32_paddb((__v8qi)__m1, (__v8qi)__m2);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _m_paddb(__m64 __m1, __m64 __m2)
{
  return _mm_add_pi8(__m1, __m2);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_add_pi16(__m64 __m1, __m64 __m2)
{
  return (__m64)__builtin_ia32_paddw((__v4hi)__m1, (__v4hi)__m2);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _m_paddw(__m64 __m1, __m64 __m2)
{
  return _mm_add_pi16(__m1, __m2);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_add_pi32(__m64 __m1, __m64 __m2)
{
  return (__m64)__builtin_ia32_paddd((__v2si)__m1, (__v2si)__m2);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _m_paddd(__m64 __m1, __m64 __m2)
{
  return _mm_add_pi32(__m1, __m2);
}
typedef __attribute__((vector_size(8))) __attribute__((__may_alias__)) long long int __v1di;
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_add_si64(__m64 __m1, __m64 __m2)
{
  return (__m64)__builtin_ia32_paddq((__v1di)__m1, (__v1di)__m2);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_adds_pi8(__m64 __m1, __m64 __m2)
{
  return (__m64)__builtin_ia32_paddsb((__v8qi)__m1, (__v8qi)__m2);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _m_paddsb(__m64 __m1, __m64 __m2)
{
  return _mm_adds_pi8(__m1, __m2);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_adds_pi16(__m64 __m1, __m64 __m2)
{
  return (__m64)__builtin_ia32_paddsw((__v4hi)__m1, (__v4hi)__m2);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _m_paddsw(__m64 __m1, __m64 __m2)
{
  return _mm_adds_pi16(__m1, __m2);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_adds_pu8(__m64 __m1, __m64 __m2)
{
  return (__m64)__builtin_ia32_paddusb((__v8qi)__m1, (__v8qi)__m2);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _m_paddusb(__m64 __m1, __m64 __m2)
{
  return _mm_adds_pu8(__m1, __m2);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_adds_pu16(__m64 __m1, __m64 __m2)
{
  return (__m64)__builtin_ia32_paddusw((__v4hi)__m1, (__v4hi)__m2);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _m_paddusw(__m64 __m1, __m64 __m2)
{
  return _mm_adds_pu16(__m1, __m2);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_sub_pi8(__m64 __m1, __m64 __m2)
{
  return (__m64)__builtin_ia32_psubb((__v8qi)__m1, (__v8qi)__m2);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _m_psubb(__m64 __m1, __m64 __m2)
{
  return _mm_sub_pi8(__m1, __m2);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_sub_pi16(__m64 __m1, __m64 __m2)
{
  return (__m64)__builtin_ia32_psubw((__v4hi)__m1, (__v4hi)__m2);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _m_psubw(__m64 __m1, __m64 __m2)
{
  return _mm_sub_pi16(__m1, __m2);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_sub_pi32(__m64 __m1, __m64 __m2)
{
  return (__m64)__builtin_ia32_psubd((__v2si)__m1, (__v2si)__m2);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _m_psubd(__m64 __m1, __m64 __m2)
{
  return _mm_sub_pi32(__m1, __m2);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_sub_si64(__m64 __m1, __m64 __m2)
{
  return (__m64)__builtin_ia32_psubq((__v1di)__m1, (__v1di)__m2);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_subs_pi8(__m64 __m1, __m64 __m2)
{
  return (__m64)__builtin_ia32_psubsb((__v8qi)__m1, (__v8qi)__m2);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _m_psubsb(__m64 __m1, __m64 __m2)
{
  return _mm_subs_pi8(__m1, __m2);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_subs_pi16(__m64 __m1, __m64 __m2)
{
  return (__m64)__builtin_ia32_psubsw((__v4hi)__m1, (__v4hi)__m2);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _m_psubsw(__m64 __m1, __m64 __m2)
{
  return _mm_subs_pi16(__m1, __m2);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_subs_pu8(__m64 __m1, __m64 __m2)
{
  return (__m64)__builtin_ia32_psubusb((__v8qi)__m1, (__v8qi)__m2);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _m_psubusb(__m64 __m1, __m64 __m2)
{
  return _mm_subs_pu8(__m1, __m2);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_subs_pu16(__m64 __m1, __m64 __m2)
{
  return (__m64)__builtin_ia32_psubusw((__v4hi)__m1, (__v4hi)__m2);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _m_psubusw(__m64 __m1, __m64 __m2)
{
  return _mm_subs_pu16(__m1, __m2);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_madd_pi16(__m64 __m1, __m64 __m2)
{
  return (__m64)__builtin_ia32_pmaddwd((__v4hi)__m1, (__v4hi)__m2);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _m_pmaddwd(__m64 __m1, __m64 __m2)
{
  return _mm_madd_pi16(__m1, __m2);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_mulhi_pi16(__m64 __m1, __m64 __m2)
{
  return (__m64)__builtin_ia32_pmulhw((__v4hi)__m1, (__v4hi)__m2);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _m_pmulhw(__m64 __m1, __m64 __m2)
{
  return _mm_mulhi_pi16(__m1, __m2);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_mullo_pi16(__m64 __m1, __m64 __m2)
{
  return (__m64)__builtin_ia32_pmullw((__v4hi)__m1, (__v4hi)__m2);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _m_pmullw(__m64 __m1, __m64 __m2)
{
  return _mm_mullo_pi16(__m1, __m2);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_sll_pi16(__m64 __m, __m64 __count)
{
  return (__m64)__builtin_ia32_psllw((__v4hi)__m, (__v4hi)__count);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _m_psllw(__m64 __m, __m64 __count)
{
  return _mm_sll_pi16(__m, __count);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_slli_pi16(__m64 __m, int __count)
{
  return (__m64)__builtin_ia32_psllwi((__v4hi)__m, __count);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _m_psllwi(__m64 __m, int __count)
{
  return _mm_slli_pi16(__m, __count);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_sll_pi32(__m64 __m, __m64 __count)
{
  return (__m64)__builtin_ia32_pslld((__v2si)__m, (__v2si)__count);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _m_pslld(__m64 __m, __m64 __count)
{
  return _mm_sll_pi32(__m, __count);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_slli_pi32(__m64 __m, int __count)
{
  return (__m64)__builtin_ia32_pslldi((__v2si)__m, __count);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _m_pslldi(__m64 __m, int __count)
{
  return _mm_slli_pi32(__m, __count);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_sll_si64(__m64 __m, __m64 __count)
{
  return (__m64)__builtin_ia32_psllq((__v1di)__m, (__v1di)__count);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _m_psllq(__m64 __m, __m64 __count)
{
  return _mm_sll_si64(__m, __count);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_slli_si64(__m64 __m, int __count)
{
  return (__m64)__builtin_ia32_psllqi((__v1di)__m, __count);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _m_psllqi(__m64 __m, int __count)
{
  return _mm_slli_si64(__m, __count);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_sra_pi16(__m64 __m, __m64 __count)
{
  return (__m64)__builtin_ia32_psraw((__v4hi)__m, (__v4hi)__count);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _m_psraw(__m64 __m, __m64 __count)
{
  return _mm_sra_pi16(__m, __count);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_srai_pi16(__m64 __m, int __count)
{
  return (__m64)__builtin_ia32_psrawi((__v4hi)__m, __count);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _m_psrawi(__m64 __m, int __count)
{
  return _mm_srai_pi16(__m, __count);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_sra_pi32(__m64 __m, __m64 __count)
{
  return (__m64)__builtin_ia32_psrad((__v2si)__m, (__v2si)__count);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _m_psrad(__m64 __m, __m64 __count)
{
  return _mm_sra_pi32(__m, __count);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_srai_pi32(__m64 __m, int __count)
{
  return (__m64)__builtin_ia32_psradi((__v2si)__m, __count);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _m_psradi(__m64 __m, int __count)
{
  return _mm_srai_pi32(__m, __count);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_srl_pi16(__m64 __m, __m64 __count)
{
  return (__m64)__builtin_ia32_psrlw((__v4hi)__m, (__v4hi)__count);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _m_psrlw(__m64 __m, __m64 __count)
{
  return _mm_srl_pi16(__m, __count);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_srli_pi16(__m64 __m, int __count)
{
  return (__m64)__builtin_ia32_psrlwi((__v4hi)__m, __count);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _m_psrlwi(__m64 __m, int __count)
{
  return _mm_srli_pi16(__m, __count);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_srl_pi32(__m64 __m, __m64 __count)
{
  return (__m64)__builtin_ia32_psrld((__v2si)__m, (__v2si)__count);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _m_psrld(__m64 __m, __m64 __count)
{
  return _mm_srl_pi32(__m, __count);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_srli_pi32(__m64 __m, int __count)
{
  return (__m64)__builtin_ia32_psrldi((__v2si)__m, __count);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _m_psrldi(__m64 __m, int __count)
{
  return _mm_srli_pi32(__m, __count);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_srl_si64(__m64 __m, __m64 __count)
{
  return (__m64)__builtin_ia32_psrlq((__v1di)__m, (__v1di)__count);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _m_psrlq(__m64 __m, __m64 __count)
{
  return _mm_srl_si64(__m, __count);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_srli_si64(__m64 __m, int __count)
{
  return (__m64)__builtin_ia32_psrlqi((__v1di)__m, __count);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _m_psrlqi(__m64 __m, int __count)
{
  return _mm_srli_si64(__m, __count);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_and_si64(__m64 __m1, __m64 __m2)
{
  return __builtin_ia32_pand(__m1, __m2);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _m_pand(__m64 __m1, __m64 __m2)
{
  return _mm_and_si64(__m1, __m2);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_andnot_si64(__m64 __m1, __m64 __m2)
{
  return __builtin_ia32_pandn(__m1, __m2);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _m_pandn(__m64 __m1, __m64 __m2)
{
  return _mm_andnot_si64(__m1, __m2);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_or_si64(__m64 __m1, __m64 __m2)
{
  return __builtin_ia32_por(__m1, __m2);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _m_por(__m64 __m1, __m64 __m2)
{
  return _mm_or_si64(__m1, __m2);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_xor_si64(__m64 __m1, __m64 __m2)
{
  return __builtin_ia32_pxor(__m1, __m2);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _m_pxor(__m64 __m1, __m64 __m2)
{
  return _mm_xor_si64(__m1, __m2);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_cmpeq_pi8(__m64 __m1, __m64 __m2)
{
  return (__m64)__builtin_ia32_pcmpeqb((__v8qi)__m1, (__v8qi)__m2);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _m_pcmpeqb(__m64 __m1, __m64 __m2)
{
  return _mm_cmpeq_pi8(__m1, __m2);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_cmpgt_pi8(__m64 __m1, __m64 __m2)
{
  return (__m64)__builtin_ia32_pcmpgtb((__v8qi)__m1, (__v8qi)__m2);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _m_pcmpgtb(__m64 __m1, __m64 __m2)
{
  return _mm_cmpgt_pi8(__m1, __m2);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_cmpeq_pi16(__m64 __m1, __m64 __m2)
{
  return (__m64)__builtin_ia32_pcmpeqw((__v4hi)__m1, (__v4hi)__m2);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _m_pcmpeqw(__m64 __m1, __m64 __m2)
{
  return _mm_cmpeq_pi16(__m1, __m2);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_cmpgt_pi16(__m64 __m1, __m64 __m2)
{
  return (__m64)__builtin_ia32_pcmpgtw((__v4hi)__m1, (__v4hi)__m2);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _m_pcmpgtw(__m64 __m1, __m64 __m2)
{
  return _mm_cmpgt_pi16(__m1, __m2);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_cmpeq_pi32(__m64 __m1, __m64 __m2)
{
  return (__m64)__builtin_ia32_pcmpeqd((__v2si)__m1, (__v2si)__m2);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _m_pcmpeqd(__m64 __m1, __m64 __m2)
{
  return _mm_cmpeq_pi32(__m1, __m2);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_cmpgt_pi32(__m64 __m1, __m64 __m2)
{
  return (__m64)__builtin_ia32_pcmpgtd((__v2si)__m1, (__v2si)__m2);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _m_pcmpgtd(__m64 __m1, __m64 __m2)
{
  return _mm_cmpgt_pi32(__m1, __m2);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_setzero_si64(void)
{
  return (__m64)0LL;
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_set_pi32(int __i1, int __i0)
{
  return (__m64)__builtin_ia32_vec_init_v2si(__i0, __i1);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_set_pi16(short int __w3, short int __w2, short int __w1, short int __w0)
{
  return (__m64)__builtin_ia32_vec_init_v4hi(__w0, __w1, __w2, __w3);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_set_pi8(char __b7, char __b6, char __b5, char __b4, char __b3, char __b2, char __b1, char __b0)
{
  return (__m64)__builtin_ia32_vec_init_v8qi(__b0, __b1, __b2, __b3, __b4, __b5, __b6, __b7);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_setr_pi32(int __i0, int __i1)
{
  return _mm_set_pi32(__i1, __i0);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_setr_pi16(short int __w0, short int __w1, short int __w2, short int __w3)
{
  return _mm_set_pi16(__w3, __w2, __w1, __w0);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_setr_pi8(char __b0, char __b1, char __b2, char __b3, char __b4, char __b5, char __b6, char __b7)
{
  return _mm_set_pi8(__b7, __b6, __b5, __b4, __b3, __b2, __b1, __b0);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_set1_pi32(int __i)
{
  return _mm_set_pi32(__i, __i);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_set1_pi16(short int __w)
{
  return _mm_set_pi16(__w, __w, __w, __w);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_set1_pi8(char __b)
{
  return _mm_set_pi8(__b, __b, __b, __b, __b, __b, __b, __b);
}
extern int posix_memalign(void **__memptr, size_t __alignment, size_t __size) __attribute__((__nothrow__)) __attribute__((__leaf__)) __attribute__((__nonnull__(1)));
static __inline void *_mm_malloc(size_t size, size_t alignment)
{
  void *ptr;
  if (alignment == 1)
    {
      return malloc(size);
    }
  if (alignment == 2 || (sizeof(void *) == 8 && alignment == 4))
    {
      alignment = sizeof(void *);
    }
  if (posix_memalign(&ptr, alignment, size) == 0)
    {
      return ptr;
    }
  else
    {
      return (void *)0;
    }
}
static __inline void _mm_free(void *ptr)
{
  free(ptr);
}
typedef __attribute__((vector_size(16))) __attribute__((__may_alias__)) float __m128;
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128 _mm_setzero_ps(void)
{
  return (__m128){0.000000000000000000000000e+00f, 0.000000000000000000000000e+00f, 0.000000000000000000000000e+00f, 0.000000000000000000000000e+00f};
}
typedef __attribute__((vector_size(16))) __attribute__((__may_alias__)) float __v4sf;
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128 _mm_add_ss(__m128 __A, __m128 __B)
{
  return (__m128)__builtin_ia32_addss((__v4sf)__A, (__v4sf)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128 _mm_sub_ss(__m128 __A, __m128 __B)
{
  return (__m128)__builtin_ia32_subss((__v4sf)__A, (__v4sf)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128 _mm_mul_ss(__m128 __A, __m128 __B)
{
  return (__m128)__builtin_ia32_mulss((__v4sf)__A, (__v4sf)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128 _mm_div_ss(__m128 __A, __m128 __B)
{
  return (__m128)__builtin_ia32_divss((__v4sf)__A, (__v4sf)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128 _mm_sqrt_ss(__m128 __A)
{
  return (__m128)__builtin_ia32_sqrtss((__v4sf)__A);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128 _mm_rcp_ss(__m128 __A)
{
  return (__m128)__builtin_ia32_rcpss((__v4sf)__A);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128 _mm_rsqrt_ss(__m128 __A)
{
  return (__m128)__builtin_ia32_rsqrtss((__v4sf)__A);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128 _mm_min_ss(__m128 __A, __m128 __B)
{
  return (__m128)__builtin_ia32_minss((__v4sf)__A, (__v4sf)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128 _mm_max_ss(__m128 __A, __m128 __B)
{
  return (__m128)__builtin_ia32_maxss((__v4sf)__A, (__v4sf)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128 _mm_add_ps(__m128 __A, __m128 __B)
{
  return (__m128)__builtin_ia32_addps((__v4sf)__A, (__v4sf)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128 _mm_sub_ps(__m128 __A, __m128 __B)
{
  return (__m128)__builtin_ia32_subps((__v4sf)__A, (__v4sf)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128 _mm_mul_ps(__m128 __A, __m128 __B)
{
  return (__m128)__builtin_ia32_mulps((__v4sf)__A, (__v4sf)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128 _mm_div_ps(__m128 __A, __m128 __B)
{
  return (__m128)__builtin_ia32_divps((__v4sf)__A, (__v4sf)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128 _mm_sqrt_ps(__m128 __A)
{
  return (__m128)__builtin_ia32_sqrtps((__v4sf)__A);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128 _mm_rcp_ps(__m128 __A)
{
  return (__m128)__builtin_ia32_rcpps((__v4sf)__A);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128 _mm_rsqrt_ps(__m128 __A)
{
  return (__m128)__builtin_ia32_rsqrtps((__v4sf)__A);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128 _mm_min_ps(__m128 __A, __m128 __B)
{
  return (__m128)__builtin_ia32_minps((__v4sf)__A, (__v4sf)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128 _mm_max_ps(__m128 __A, __m128 __B)
{
  return (__m128)__builtin_ia32_maxps((__v4sf)__A, (__v4sf)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128 _mm_and_ps(__m128 __A, __m128 __B)
{
  return __builtin_ia32_andps(__A, __B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128 _mm_andnot_ps(__m128 __A, __m128 __B)
{
  return __builtin_ia32_andnps(__A, __B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128 _mm_or_ps(__m128 __A, __m128 __B)
{
  return __builtin_ia32_orps(__A, __B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128 _mm_xor_ps(__m128 __A, __m128 __B)
{
  return __builtin_ia32_xorps(__A, __B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128 _mm_cmpeq_ss(__m128 __A, __m128 __B)
{
  return (__m128)__builtin_ia32_cmpeqss((__v4sf)__A, (__v4sf)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128 _mm_cmplt_ss(__m128 __A, __m128 __B)
{
  return (__m128)__builtin_ia32_cmpltss((__v4sf)__A, (__v4sf)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128 _mm_cmple_ss(__m128 __A, __m128 __B)
{
  return (__m128)__builtin_ia32_cmpless((__v4sf)__A, (__v4sf)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128 _mm_cmpgt_ss(__m128 __A, __m128 __B)
{
  return (__m128)__builtin_ia32_movss((__v4sf)__A, (__v4sf)__builtin_ia32_cmpltss((__v4sf)__B, (__v4sf)__A));
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128 _mm_cmpge_ss(__m128 __A, __m128 __B)
{
  return (__m128)__builtin_ia32_movss((__v4sf)__A, (__v4sf)__builtin_ia32_cmpless((__v4sf)__B, (__v4sf)__A));
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128 _mm_cmpneq_ss(__m128 __A, __m128 __B)
{
  return (__m128)__builtin_ia32_cmpneqss((__v4sf)__A, (__v4sf)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128 _mm_cmpnlt_ss(__m128 __A, __m128 __B)
{
  return (__m128)__builtin_ia32_cmpnltss((__v4sf)__A, (__v4sf)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128 _mm_cmpnle_ss(__m128 __A, __m128 __B)
{
  return (__m128)__builtin_ia32_cmpnless((__v4sf)__A, (__v4sf)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128 _mm_cmpngt_ss(__m128 __A, __m128 __B)
{
  return (__m128)__builtin_ia32_movss((__v4sf)__A, (__v4sf)__builtin_ia32_cmpnltss((__v4sf)__B, (__v4sf)__A));
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128 _mm_cmpnge_ss(__m128 __A, __m128 __B)
{
  return (__m128)__builtin_ia32_movss((__v4sf)__A, (__v4sf)__builtin_ia32_cmpnless((__v4sf)__B, (__v4sf)__A));
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128 _mm_cmpord_ss(__m128 __A, __m128 __B)
{
  return (__m128)__builtin_ia32_cmpordss((__v4sf)__A, (__v4sf)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128 _mm_cmpunord_ss(__m128 __A, __m128 __B)
{
  return (__m128)__builtin_ia32_cmpunordss((__v4sf)__A, (__v4sf)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128 _mm_cmpeq_ps(__m128 __A, __m128 __B)
{
  return (__m128)__builtin_ia32_cmpeqps((__v4sf)__A, (__v4sf)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128 _mm_cmplt_ps(__m128 __A, __m128 __B)
{
  return (__m128)__builtin_ia32_cmpltps((__v4sf)__A, (__v4sf)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128 _mm_cmple_ps(__m128 __A, __m128 __B)
{
  return (__m128)__builtin_ia32_cmpleps((__v4sf)__A, (__v4sf)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128 _mm_cmpgt_ps(__m128 __A, __m128 __B)
{
  return (__m128)__builtin_ia32_cmpgtps((__v4sf)__A, (__v4sf)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128 _mm_cmpge_ps(__m128 __A, __m128 __B)
{
  return (__m128)__builtin_ia32_cmpgeps((__v4sf)__A, (__v4sf)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128 _mm_cmpneq_ps(__m128 __A, __m128 __B)
{
  return (__m128)__builtin_ia32_cmpneqps((__v4sf)__A, (__v4sf)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128 _mm_cmpnlt_ps(__m128 __A, __m128 __B)
{
  return (__m128)__builtin_ia32_cmpnltps((__v4sf)__A, (__v4sf)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128 _mm_cmpnle_ps(__m128 __A, __m128 __B)
{
  return (__m128)__builtin_ia32_cmpnleps((__v4sf)__A, (__v4sf)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128 _mm_cmpngt_ps(__m128 __A, __m128 __B)
{
  return (__m128)__builtin_ia32_cmpngtps((__v4sf)__A, (__v4sf)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128 _mm_cmpnge_ps(__m128 __A, __m128 __B)
{
  return (__m128)__builtin_ia32_cmpngeps((__v4sf)__A, (__v4sf)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128 _mm_cmpord_ps(__m128 __A, __m128 __B)
{
  return (__m128)__builtin_ia32_cmpordps((__v4sf)__A, (__v4sf)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128 _mm_cmpunord_ps(__m128 __A, __m128 __B)
{
  return (__m128)__builtin_ia32_cmpunordps((__v4sf)__A, (__v4sf)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) int _mm_comieq_ss(__m128 __A, __m128 __B)
{
  return __builtin_ia32_comieq((__v4sf)__A, (__v4sf)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) int _mm_comilt_ss(__m128 __A, __m128 __B)
{
  return __builtin_ia32_comilt((__v4sf)__A, (__v4sf)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) int _mm_comile_ss(__m128 __A, __m128 __B)
{
  return __builtin_ia32_comile((__v4sf)__A, (__v4sf)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) int _mm_comigt_ss(__m128 __A, __m128 __B)
{
  return __builtin_ia32_comigt((__v4sf)__A, (__v4sf)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) int _mm_comige_ss(__m128 __A, __m128 __B)
{
  return __builtin_ia32_comige((__v4sf)__A, (__v4sf)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) int _mm_comineq_ss(__m128 __A, __m128 __B)
{
  return __builtin_ia32_comineq((__v4sf)__A, (__v4sf)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) int _mm_ucomieq_ss(__m128 __A, __m128 __B)
{
  return __builtin_ia32_ucomieq((__v4sf)__A, (__v4sf)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) int _mm_ucomilt_ss(__m128 __A, __m128 __B)
{
  return __builtin_ia32_ucomilt((__v4sf)__A, (__v4sf)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) int _mm_ucomile_ss(__m128 __A, __m128 __B)
{
  return __builtin_ia32_ucomile((__v4sf)__A, (__v4sf)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) int _mm_ucomigt_ss(__m128 __A, __m128 __B)
{
  return __builtin_ia32_ucomigt((__v4sf)__A, (__v4sf)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) int _mm_ucomige_ss(__m128 __A, __m128 __B)
{
  return __builtin_ia32_ucomige((__v4sf)__A, (__v4sf)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) int _mm_ucomineq_ss(__m128 __A, __m128 __B)
{
  return __builtin_ia32_ucomineq((__v4sf)__A, (__v4sf)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) int _mm_cvtss_si32(__m128 __A)
{
  return __builtin_ia32_cvtss2si((__v4sf)__A);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) int _mm_cvt_ss2si(__m128 __A)
{
  return _mm_cvtss_si32(__A);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) long long int _mm_cvtss_si64(__m128 __A)
{
  return __builtin_ia32_cvtss2si64((__v4sf)__A);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) long long int _mm_cvtss_si64x(__m128 __A)
{
  return __builtin_ia32_cvtss2si64((__v4sf)__A);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_cvtps_pi32(__m128 __A)
{
  return (__m64)__builtin_ia32_cvtps2pi((__v4sf)__A);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_cvt_ps2pi(__m128 __A)
{
  return _mm_cvtps_pi32(__A);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) int _mm_cvttss_si32(__m128 __A)
{
  return __builtin_ia32_cvttss2si((__v4sf)__A);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) int _mm_cvtt_ss2si(__m128 __A)
{
  return _mm_cvttss_si32(__A);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) long long int _mm_cvttss_si64(__m128 __A)
{
  return __builtin_ia32_cvttss2si64((__v4sf)__A);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) long long int _mm_cvttss_si64x(__m128 __A)
{
  return __builtin_ia32_cvttss2si64((__v4sf)__A);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_cvttps_pi32(__m128 __A)
{
  return (__m64)__builtin_ia32_cvttps2pi((__v4sf)__A);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_cvtt_ps2pi(__m128 __A)
{
  return _mm_cvttps_pi32(__A);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128 _mm_cvtsi32_ss(__m128 __A, int __B)
{
  return (__m128)__builtin_ia32_cvtsi2ss((__v4sf)__A, __B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128 _mm_cvt_si2ss(__m128 __A, int __B)
{
  return _mm_cvtsi32_ss(__A, __B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128 _mm_cvtsi64_ss(__m128 __A, long long int __B)
{
  return (__m128)__builtin_ia32_cvtsi642ss((__v4sf)__A, __B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128 _mm_cvtsi64x_ss(__m128 __A, long long int __B)
{
  return (__m128)__builtin_ia32_cvtsi642ss((__v4sf)__A, __B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128 _mm_cvtpi32_ps(__m128 __A, __m64 __B)
{
  return (__m128)__builtin_ia32_cvtpi2ps((__v4sf)__A, (__v2si)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128 _mm_cvt_pi2ps(__m128 __A, __m64 __B)
{
  return _mm_cvtpi32_ps(__A, __B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128 _mm_cvtpi16_ps(__m64 __A)
{
  __v4hi __sign;
  __v2si __losi;
  __v2si __hisi;
  __v4sf __zero;
  __v4sf __ra;
  __v4sf __rb;
  __sign = __builtin_ia32_pcmpgtw((__v4hi)0LL, (__v4hi)__A);
  __losi = (__v2si)__builtin_ia32_punpcklwd((__v4hi)__A, __sign);
  __hisi = (__v2si)__builtin_ia32_punpckhwd((__v4hi)__A, __sign);
  __zero = (__v4sf)_mm_setzero_ps();
  __ra = __builtin_ia32_cvtpi2ps(__zero, __losi);
  __rb = __builtin_ia32_cvtpi2ps(__ra, __hisi);
  return (__m128)__builtin_ia32_movlhps(__ra, __rb);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128 _mm_cvtpu16_ps(__m64 __A)
{
  __v2si __losi;
  __v2si __hisi;
  __v4sf __zero;
  __v4sf __ra;
  __v4sf __rb;
  __losi = (__v2si)__builtin_ia32_punpcklwd((__v4hi)__A, (__v4hi)0LL);
  __hisi = (__v2si)__builtin_ia32_punpckhwd((__v4hi)__A, (__v4hi)0LL);
  __zero = (__v4sf)_mm_setzero_ps();
  __ra = __builtin_ia32_cvtpi2ps(__zero, __losi);
  __rb = __builtin_ia32_cvtpi2ps(__ra, __hisi);
  return (__m128)__builtin_ia32_movlhps(__ra, __rb);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128 _mm_cvtpi8_ps(__m64 __A)
{
  __v8qi __sign;
  __sign = __builtin_ia32_pcmpgtb((__v8qi)0LL, (__v8qi)__A);
  __A = (__m64)__builtin_ia32_punpcklbw((__v8qi)__A, __sign);
  return _mm_cvtpi16_ps(__A);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128 _mm_cvtpu8_ps(__m64 __A)
{
  __A = (__m64)__builtin_ia32_punpcklbw((__v8qi)__A, (__v8qi)0LL);
  return _mm_cvtpu16_ps(__A);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128 _mm_cvtpi32x2_ps(__m64 __A, __m64 __B)
{
  __v4sf __zero = (__v4sf)_mm_setzero_ps();
  __v4sf __sfa = __builtin_ia32_cvtpi2ps(__zero, (__v2si)__A);
  __v4sf __sfb = __builtin_ia32_cvtpi2ps(__sfa, (__v2si)__B);
  return (__m128)__builtin_ia32_movlhps(__sfa, __sfb);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_cvtps_pi16(__m128 __A)
{
  __v4sf __hisf = (__v4sf)__A;
  __v4sf __losf = __builtin_ia32_movhlps(__hisf, __hisf);
  __v2si __hisi = __builtin_ia32_cvtps2pi(__hisf);
  __v2si __losi = __builtin_ia32_cvtps2pi(__losf);
  return (__m64)__builtin_ia32_packssdw(__hisi, __losi);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_cvtps_pi8(__m128 __A)
{
  __v4hi __tmp = (__v4hi)_mm_cvtps_pi16(__A);
  return (__m64)__builtin_ia32_packsswb(__tmp, (__v4hi)0LL);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128 _mm_shuffle_ps(__m128 __A, __m128 __B, const int __mask)
{
  return (__m128)__builtin_ia32_shufps((__v4sf)__A, (__v4sf)__B, __mask);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128 _mm_unpackhi_ps(__m128 __A, __m128 __B)
{
  return (__m128)__builtin_ia32_unpckhps((__v4sf)__A, (__v4sf)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128 _mm_unpacklo_ps(__m128 __A, __m128 __B)
{
  return (__m128)__builtin_ia32_unpcklps((__v4sf)__A, (__v4sf)__B);
}
typedef __attribute__((vector_size(8))) __attribute__((__may_alias__)) float __v2sf;
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128 _mm_loadh_pi(__m128 __A, const __m64 *__P)
{
  return (__m128)__builtin_ia32_loadhps((__v4sf)__A, (const __v2sf *)__P);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) void _mm_storeh_pi(__m64 *__P, __m128 __A)
{
  __builtin_ia32_storehps((__v2sf *)__P, (__v4sf)__A);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128 _mm_movehl_ps(__m128 __A, __m128 __B)
{
  return (__m128)__builtin_ia32_movhlps((__v4sf)__A, (__v4sf)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128 _mm_movelh_ps(__m128 __A, __m128 __B)
{
  return (__m128)__builtin_ia32_movlhps((__v4sf)__A, (__v4sf)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128 _mm_loadl_pi(__m128 __A, const __m64 *__P)
{
  return (__m128)__builtin_ia32_loadlps((__v4sf)__A, (const __v2sf *)__P);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) void _mm_storel_pi(__m64 *__P, __m128 __A)
{
  __builtin_ia32_storelps((__v2sf *)__P, (__v4sf)__A);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) int _mm_movemask_ps(__m128 __A)
{
  return __builtin_ia32_movmskps((__v4sf)__A);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) unsigned int _mm_getcsr(void)
{
  return __builtin_ia32_stmxcsr();
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) unsigned int _MM_GET_EXCEPTION_STATE(void)
{
  return _mm_getcsr() & 63;
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) unsigned int _MM_GET_EXCEPTION_MASK(void)
{
  return _mm_getcsr() & 8064;
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) unsigned int _MM_GET_ROUNDING_MODE(void)
{
  return _mm_getcsr() & 24576;
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) unsigned int _MM_GET_FLUSH_ZERO_MODE(void)
{
  return _mm_getcsr() & 32768;
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) void _mm_setcsr(unsigned int __I)
{
  __builtin_ia32_ldmxcsr(__I);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) void _MM_SET_EXCEPTION_STATE(unsigned int __mask)
{
  _mm_setcsr((_mm_getcsr() & ~63) | __mask);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) void _MM_SET_EXCEPTION_MASK(unsigned int __mask)
{
  _mm_setcsr((_mm_getcsr() & ~8064) | __mask);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) void _MM_SET_ROUNDING_MODE(unsigned int __mode)
{
  _mm_setcsr((_mm_getcsr() & ~24576) | __mode);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) void _MM_SET_FLUSH_ZERO_MODE(unsigned int __mode)
{
  _mm_setcsr((_mm_getcsr() & ~32768) | __mode);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128 _mm_set_ss(float __F)
{
  return (__m128)(__v4sf){__F, 0.000000000000000000000000e+00f, 0.000000000000000000000000e+00f, 0.000000000000000000000000e+00f};
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128 _mm_set1_ps(float __F)
{
  return (__m128)(__v4sf){__F, __F, __F, __F};
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128 _mm_set_ps1(float __F)
{
  return _mm_set1_ps(__F);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128 _mm_load_ss(const float *__P)
{
  return _mm_set_ss(*__P);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128 _mm_load1_ps(const float *__P)
{
  return _mm_set1_ps(*__P);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128 _mm_load_ps1(const float *__P)
{
  return _mm_load1_ps(__P);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128 _mm_load_ps(const float *__P)
{
  return (__m128)*((__v4sf *)__P);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128 _mm_loadu_ps(const float *__P)
{
  return (__m128)__builtin_ia32_loadups(__P);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128 _mm_loadr_ps(const float *__P)
{
  __v4sf __tmp = *((__v4sf *)__P);
  return (__m128)__builtin_ia32_shufps(__tmp, __tmp, ((0 << 6 | 1 << 4) | 2 << 2) | 3);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128 _mm_set_ps(const float __Z, const float __Y, const float __X, const float __W)
{
  return (__m128)(__v4sf){__W, __X, __Y, __Z};
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128 _mm_setr_ps(float __Z, float __Y, float __X, float __W)
{
  return (__m128)(__v4sf){__Z, __Y, __X, __W};
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) void _mm_store_ss(float *__P, __m128 __A)
{
  *__P = __builtin_ia32_vec_ext_v4sf((__v4sf)__A, 0);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) float _mm_cvtss_f32(__m128 __A)
{
  return __builtin_ia32_vec_ext_v4sf((__v4sf)__A, 0);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) void _mm_store_ps(float *__P, __m128 __A)
{
  *((__v4sf *)__P) = (__v4sf)__A;
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) void _mm_storeu_ps(float *__P, __m128 __A)
{
  __builtin_ia32_storeups(__P, (__v4sf)__A);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) void _mm_store1_ps(float *__P, __m128 __A)
{
  __v4sf __va = (__v4sf)__A;
  __v4sf __tmp = __builtin_ia32_shufps(__va, __va, ((0 << 6 | 0 << 4) | 0 << 2) | 0);
  _mm_storeu_ps(__P, __tmp);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) void _mm_store_ps1(float *__P, __m128 __A)
{
  _mm_store1_ps(__P, __A);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) void _mm_storer_ps(float *__P, __m128 __A)
{
  __v4sf __va = (__v4sf)__A;
  __v4sf __tmp = __builtin_ia32_shufps(__va, __va, ((0 << 6 | 1 << 4) | 2 << 2) | 3);
  _mm_store_ps(__P, __tmp);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128 _mm_move_ss(__m128 __A, __m128 __B)
{
  return (__m128)__builtin_ia32_movss((__v4sf)__A, (__v4sf)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) int _mm_extract_pi16(const __m64 __A, const int __N)
{
  return __builtin_ia32_vec_ext_v4hi((__v4hi)__A, __N);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) int _m_pextrw(const __m64 __A, const int __N)
{
  return _mm_extract_pi16(__A, __N);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_insert_pi16(const __m64 __A, const int __D, const int __N)
{
  return (__m64)__builtin_ia32_vec_set_v4hi((__v4hi)__A, __D, __N);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _m_pinsrw(const __m64 __A, const int __D, const int __N)
{
  return _mm_insert_pi16(__A, __D, __N);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_max_pi16(__m64 __A, __m64 __B)
{
  return (__m64)__builtin_ia32_pmaxsw((__v4hi)__A, (__v4hi)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _m_pmaxsw(__m64 __A, __m64 __B)
{
  return _mm_max_pi16(__A, __B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_max_pu8(__m64 __A, __m64 __B)
{
  return (__m64)__builtin_ia32_pmaxub((__v8qi)__A, (__v8qi)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _m_pmaxub(__m64 __A, __m64 __B)
{
  return _mm_max_pu8(__A, __B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_min_pi16(__m64 __A, __m64 __B)
{
  return (__m64)__builtin_ia32_pminsw((__v4hi)__A, (__v4hi)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _m_pminsw(__m64 __A, __m64 __B)
{
  return _mm_min_pi16(__A, __B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_min_pu8(__m64 __A, __m64 __B)
{
  return (__m64)__builtin_ia32_pminub((__v8qi)__A, (__v8qi)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _m_pminub(__m64 __A, __m64 __B)
{
  return _mm_min_pu8(__A, __B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) int _mm_movemask_pi8(__m64 __A)
{
  return __builtin_ia32_pmovmskb((__v8qi)__A);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) int _m_pmovmskb(__m64 __A)
{
  return _mm_movemask_pi8(__A);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_mulhi_pu16(__m64 __A, __m64 __B)
{
  return (__m64)__builtin_ia32_pmulhuw((__v4hi)__A, (__v4hi)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _m_pmulhuw(__m64 __A, __m64 __B)
{
  return _mm_mulhi_pu16(__A, __B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_shuffle_pi16(__m64 __A, const int __N)
{
  return (__m64)__builtin_ia32_pshufw((__v4hi)__A, __N);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _m_pshufw(__m64 __A, const int __N)
{
  return _mm_shuffle_pi16(__A, __N);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) void _mm_maskmove_si64(__m64 __A, __m64 __N, char *__P)
{
  __builtin_ia32_maskmovq((__v8qi)__A, (__v8qi)__N, __P);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) void _m_maskmovq(__m64 __A, __m64 __N, char *__P)
{
  _mm_maskmove_si64(__A, __N, __P);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_avg_pu8(__m64 __A, __m64 __B)
{
  return (__m64)__builtin_ia32_pavgb((__v8qi)__A, (__v8qi)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _m_pavgb(__m64 __A, __m64 __B)
{
  return _mm_avg_pu8(__A, __B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_avg_pu16(__m64 __A, __m64 __B)
{
  return (__m64)__builtin_ia32_pavgw((__v4hi)__A, (__v4hi)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _m_pavgw(__m64 __A, __m64 __B)
{
  return _mm_avg_pu16(__A, __B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_sad_pu8(__m64 __A, __m64 __B)
{
  return (__m64)__builtin_ia32_psadbw((__v8qi)__A, (__v8qi)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _m_psadbw(__m64 __A, __m64 __B)
{
  return _mm_sad_pu8(__A, __B);
}
enum _mm_hint
{
  _MM_HINT_T0 = 3,
  _MM_HINT_T1 = 2,
  _MM_HINT_T2 = 1,
  _MM_HINT_NTA = 0
};
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) void _mm_prefetch(const void *__P, enum _mm_hint __I)
{
  __builtin_prefetch(__P, 0, __I);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) void _mm_stream_pi(__m64 *__P, __m64 __A)
{
  __builtin_ia32_movntq((unsigned long long int *)__P, (unsigned long long int)__A);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) void _mm_stream_ps(float *__P, __m128 __A)
{
  __builtin_ia32_movntps(__P, (__v4sf)__A);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) void _mm_sfence(void)
{
  __builtin_ia32_sfence();
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) void _mm_pause(void)
{
  __builtin_ia32_pause();
}
typedef __attribute__((vector_size(16))) __attribute__((__may_alias__)) double __m128d;
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128d _mm_set_sd(double __F)
{
  return (__m128d){__F, 0.00000000000000000000000000000000000000000000000000000e+00};
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128d _mm_set1_pd(double __F)
{
  return (__m128d){__F, __F};
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128d _mm_set_pd1(double __F)
{
  return _mm_set1_pd(__F);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128d _mm_set_pd(double __W, double __X)
{
  return (__m128d){__X, __W};
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128d _mm_setr_pd(double __W, double __X)
{
  return (__m128d){__W, __X};
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128d _mm_setzero_pd(void)
{
  return (__m128d){0.00000000000000000000000000000000000000000000000000000e+00, 0.00000000000000000000000000000000000000000000000000000e+00};
}
typedef __attribute__((vector_size(16))) __attribute__((__may_alias__)) double __v2df;
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128d _mm_move_sd(__m128d __A, __m128d __B)
{
  return (__m128d)__builtin_ia32_movsd((__v2df)__A, (__v2df)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128d _mm_load_pd(const double *__P)
{
  return *((__m128d *)__P);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128d _mm_loadu_pd(const double *__P)
{
  return __builtin_ia32_loadupd(__P);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128d _mm_load1_pd(const double *__P)
{
  return _mm_set1_pd(*__P);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128d _mm_load_sd(const double *__P)
{
  return _mm_set_sd(*__P);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128d _mm_load_pd1(const double *__P)
{
  return _mm_load1_pd(__P);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128d _mm_loadr_pd(const double *__P)
{
  __m128d __tmp = _mm_load_pd(__P);
  return __builtin_ia32_shufpd(__tmp, __tmp, 0 << 1 | 1);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) void _mm_store_pd(double *__P, __m128d __A)
{
  *((__m128d *)__P) = __A;
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) void _mm_storeu_pd(double *__P, __m128d __A)
{
  __builtin_ia32_storeupd(__P, __A);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) void _mm_store_sd(double *__P, __m128d __A)
{
  *__P = __builtin_ia32_vec_ext_v2df(__A, 0);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) double _mm_cvtsd_f64(__m128d __A)
{
  return __builtin_ia32_vec_ext_v2df(__A, 0);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) void _mm_storel_pd(double *__P, __m128d __A)
{
  _mm_store_sd(__P, __A);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) void _mm_storeh_pd(double *__P, __m128d __A)
{
  *__P = __builtin_ia32_vec_ext_v2df(__A, 1);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) void _mm_store1_pd(double *__P, __m128d __A)
{
  _mm_store_pd(__P, __builtin_ia32_shufpd(__A, __A, 0 << 1 | 0));
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) void _mm_store_pd1(double *__P, __m128d __A)
{
  _mm_store1_pd(__P, __A);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) void _mm_storer_pd(double *__P, __m128d __A)
{
  _mm_store_pd(__P, __builtin_ia32_shufpd(__A, __A, 0 << 1 | 1));
}
typedef __attribute__((vector_size(16))) __attribute__((__may_alias__)) long long int __m128i;
typedef __attribute__((vector_size(16))) __attribute__((__may_alias__)) int __v4si;
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) int _mm_cvtsi128_si32(__m128i __A)
{
  return __builtin_ia32_vec_ext_v4si((__v4si)__A, 0);
}
typedef __attribute__((vector_size(16))) __attribute__((__may_alias__)) long long int __v2di;
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) long long int _mm_cvtsi128_si64(__m128i __A)
{
  return __builtin_ia32_vec_ext_v2di((__v2di)__A, 0);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) long long int _mm_cvtsi128_si64x(__m128i __A)
{
  return __builtin_ia32_vec_ext_v2di((__v2di)__A, 0);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128d _mm_add_pd(__m128d __A, __m128d __B)
{
  return (__m128d)__builtin_ia32_addpd((__v2df)__A, (__v2df)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128d _mm_add_sd(__m128d __A, __m128d __B)
{
  return (__m128d)__builtin_ia32_addsd((__v2df)__A, (__v2df)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128d _mm_sub_pd(__m128d __A, __m128d __B)
{
  return (__m128d)__builtin_ia32_subpd((__v2df)__A, (__v2df)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128d _mm_sub_sd(__m128d __A, __m128d __B)
{
  return (__m128d)__builtin_ia32_subsd((__v2df)__A, (__v2df)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128d _mm_mul_pd(__m128d __A, __m128d __B)
{
  return (__m128d)__builtin_ia32_mulpd((__v2df)__A, (__v2df)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128d _mm_mul_sd(__m128d __A, __m128d __B)
{
  return (__m128d)__builtin_ia32_mulsd((__v2df)__A, (__v2df)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128d _mm_div_pd(__m128d __A, __m128d __B)
{
  return (__m128d)__builtin_ia32_divpd((__v2df)__A, (__v2df)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128d _mm_div_sd(__m128d __A, __m128d __B)
{
  return (__m128d)__builtin_ia32_divsd((__v2df)__A, (__v2df)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128d _mm_sqrt_pd(__m128d __A)
{
  return (__m128d)__builtin_ia32_sqrtpd((__v2df)__A);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128d _mm_sqrt_sd(__m128d __A, __m128d __B)
{
  __v2df __tmp = __builtin_ia32_movsd((__v2df)__A, (__v2df)__B);
  return (__m128d)__builtin_ia32_sqrtsd((__v2df)__tmp);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128d _mm_min_pd(__m128d __A, __m128d __B)
{
  return (__m128d)__builtin_ia32_minpd((__v2df)__A, (__v2df)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128d _mm_min_sd(__m128d __A, __m128d __B)
{
  return (__m128d)__builtin_ia32_minsd((__v2df)__A, (__v2df)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128d _mm_max_pd(__m128d __A, __m128d __B)
{
  return (__m128d)__builtin_ia32_maxpd((__v2df)__A, (__v2df)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128d _mm_max_sd(__m128d __A, __m128d __B)
{
  return (__m128d)__builtin_ia32_maxsd((__v2df)__A, (__v2df)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128d _mm_and_pd(__m128d __A, __m128d __B)
{
  return (__m128d)__builtin_ia32_andpd((__v2df)__A, (__v2df)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128d _mm_andnot_pd(__m128d __A, __m128d __B)
{
  return (__m128d)__builtin_ia32_andnpd((__v2df)__A, (__v2df)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128d _mm_or_pd(__m128d __A, __m128d __B)
{
  return (__m128d)__builtin_ia32_orpd((__v2df)__A, (__v2df)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128d _mm_xor_pd(__m128d __A, __m128d __B)
{
  return (__m128d)__builtin_ia32_xorpd((__v2df)__A, (__v2df)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128d _mm_cmpeq_pd(__m128d __A, __m128d __B)
{
  return (__m128d)__builtin_ia32_cmpeqpd((__v2df)__A, (__v2df)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128d _mm_cmplt_pd(__m128d __A, __m128d __B)
{
  return (__m128d)__builtin_ia32_cmpltpd((__v2df)__A, (__v2df)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128d _mm_cmple_pd(__m128d __A, __m128d __B)
{
  return (__m128d)__builtin_ia32_cmplepd((__v2df)__A, (__v2df)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128d _mm_cmpgt_pd(__m128d __A, __m128d __B)
{
  return (__m128d)__builtin_ia32_cmpgtpd((__v2df)__A, (__v2df)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128d _mm_cmpge_pd(__m128d __A, __m128d __B)
{
  return (__m128d)__builtin_ia32_cmpgepd((__v2df)__A, (__v2df)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128d _mm_cmpneq_pd(__m128d __A, __m128d __B)
{
  return (__m128d)__builtin_ia32_cmpneqpd((__v2df)__A, (__v2df)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128d _mm_cmpnlt_pd(__m128d __A, __m128d __B)
{
  return (__m128d)__builtin_ia32_cmpnltpd((__v2df)__A, (__v2df)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128d _mm_cmpnle_pd(__m128d __A, __m128d __B)
{
  return (__m128d)__builtin_ia32_cmpnlepd((__v2df)__A, (__v2df)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128d _mm_cmpngt_pd(__m128d __A, __m128d __B)
{
  return (__m128d)__builtin_ia32_cmpngtpd((__v2df)__A, (__v2df)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128d _mm_cmpnge_pd(__m128d __A, __m128d __B)
{
  return (__m128d)__builtin_ia32_cmpngepd((__v2df)__A, (__v2df)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128d _mm_cmpord_pd(__m128d __A, __m128d __B)
{
  return (__m128d)__builtin_ia32_cmpordpd((__v2df)__A, (__v2df)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128d _mm_cmpunord_pd(__m128d __A, __m128d __B)
{
  return (__m128d)__builtin_ia32_cmpunordpd((__v2df)__A, (__v2df)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128d _mm_cmpeq_sd(__m128d __A, __m128d __B)
{
  return (__m128d)__builtin_ia32_cmpeqsd((__v2df)__A, (__v2df)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128d _mm_cmplt_sd(__m128d __A, __m128d __B)
{
  return (__m128d)__builtin_ia32_cmpltsd((__v2df)__A, (__v2df)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128d _mm_cmple_sd(__m128d __A, __m128d __B)
{
  return (__m128d)__builtin_ia32_cmplesd((__v2df)__A, (__v2df)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128d _mm_cmpgt_sd(__m128d __A, __m128d __B)
{
  return (__m128d)__builtin_ia32_movsd((__v2df)__A, (__v2df)__builtin_ia32_cmpltsd((__v2df)__B, (__v2df)__A));
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128d _mm_cmpge_sd(__m128d __A, __m128d __B)
{
  return (__m128d)__builtin_ia32_movsd((__v2df)__A, (__v2df)__builtin_ia32_cmplesd((__v2df)__B, (__v2df)__A));
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128d _mm_cmpneq_sd(__m128d __A, __m128d __B)
{
  return (__m128d)__builtin_ia32_cmpneqsd((__v2df)__A, (__v2df)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128d _mm_cmpnlt_sd(__m128d __A, __m128d __B)
{
  return (__m128d)__builtin_ia32_cmpnltsd((__v2df)__A, (__v2df)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128d _mm_cmpnle_sd(__m128d __A, __m128d __B)
{
  return (__m128d)__builtin_ia32_cmpnlesd((__v2df)__A, (__v2df)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128d _mm_cmpngt_sd(__m128d __A, __m128d __B)
{
  return (__m128d)__builtin_ia32_movsd((__v2df)__A, (__v2df)__builtin_ia32_cmpnltsd((__v2df)__B, (__v2df)__A));
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128d _mm_cmpnge_sd(__m128d __A, __m128d __B)
{
  return (__m128d)__builtin_ia32_movsd((__v2df)__A, (__v2df)__builtin_ia32_cmpnlesd((__v2df)__B, (__v2df)__A));
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128d _mm_cmpord_sd(__m128d __A, __m128d __B)
{
  return (__m128d)__builtin_ia32_cmpordsd((__v2df)__A, (__v2df)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128d _mm_cmpunord_sd(__m128d __A, __m128d __B)
{
  return (__m128d)__builtin_ia32_cmpunordsd((__v2df)__A, (__v2df)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) int _mm_comieq_sd(__m128d __A, __m128d __B)
{
  return __builtin_ia32_comisdeq((__v2df)__A, (__v2df)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) int _mm_comilt_sd(__m128d __A, __m128d __B)
{
  return __builtin_ia32_comisdlt((__v2df)__A, (__v2df)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) int _mm_comile_sd(__m128d __A, __m128d __B)
{
  return __builtin_ia32_comisdle((__v2df)__A, (__v2df)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) int _mm_comigt_sd(__m128d __A, __m128d __B)
{
  return __builtin_ia32_comisdgt((__v2df)__A, (__v2df)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) int _mm_comige_sd(__m128d __A, __m128d __B)
{
  return __builtin_ia32_comisdge((__v2df)__A, (__v2df)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) int _mm_comineq_sd(__m128d __A, __m128d __B)
{
  return __builtin_ia32_comisdneq((__v2df)__A, (__v2df)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) int _mm_ucomieq_sd(__m128d __A, __m128d __B)
{
  return __builtin_ia32_ucomisdeq((__v2df)__A, (__v2df)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) int _mm_ucomilt_sd(__m128d __A, __m128d __B)
{
  return __builtin_ia32_ucomisdlt((__v2df)__A, (__v2df)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) int _mm_ucomile_sd(__m128d __A, __m128d __B)
{
  return __builtin_ia32_ucomisdle((__v2df)__A, (__v2df)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) int _mm_ucomigt_sd(__m128d __A, __m128d __B)
{
  return __builtin_ia32_ucomisdgt((__v2df)__A, (__v2df)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) int _mm_ucomige_sd(__m128d __A, __m128d __B)
{
  return __builtin_ia32_ucomisdge((__v2df)__A, (__v2df)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) int _mm_ucomineq_sd(__m128d __A, __m128d __B)
{
  return __builtin_ia32_ucomisdneq((__v2df)__A, (__v2df)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_set_epi64x(long long int __q1, long long int __q0)
{
  return (__m128i)(__v2di){__q0, __q1};
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_set_epi64(__m64 __q1, __m64 __q0)
{
  return _mm_set_epi64x((long long int)__q1, (long long int)__q0);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_set_epi32(int __q3, int __q2, int __q1, int __q0)
{
  return (__m128i)(__v4si){__q0, __q1, __q2, __q3};
}
typedef __attribute__((vector_size(16))) __attribute__((__may_alias__)) short int __v8hi;
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_set_epi16(short int __q7, short int __q6, short int __q5, short int __q4, short int __q3, short int __q2, short int __q1, short int __q0)
{
  return (__m128i)(__v8hi){__q0, __q1, __q2, __q3, __q4, __q5, __q6, __q7};
}
typedef __attribute__((vector_size(16))) __attribute__((__may_alias__)) char __v16qi;
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_set_epi8(char __q15, char __q14, char __q13, char __q12, char __q11, char __q10, char __q09, char __q08, char __q07, char __q06, char __q05, char __q04, char __q03, char __q02, char __q01, char __q00)
{
  return (__m128i)(__v16qi){__q00, __q01, __q02, __q03, __q04, __q05, __q06, __q07, __q08, __q09, __q10, __q11, __q12, __q13, __q14, __q15};
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_set1_epi64x(long long int __A)
{
  return _mm_set_epi64x(__A, __A);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_set1_epi64(__m64 __A)
{
  return _mm_set_epi64(__A, __A);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_set1_epi32(int __A)
{
  return _mm_set_epi32(__A, __A, __A, __A);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_set1_epi16(short int __A)
{
  return _mm_set_epi16(__A, __A, __A, __A, __A, __A, __A, __A);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_set1_epi8(char __A)
{
  return _mm_set_epi8(__A, __A, __A, __A, __A, __A, __A, __A, __A, __A, __A, __A, __A, __A, __A, __A);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_setr_epi64(__m64 __q0, __m64 __q1)
{
  return _mm_set_epi64(__q1, __q0);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_setr_epi32(int __q0, int __q1, int __q2, int __q3)
{
  return _mm_set_epi32(__q3, __q2, __q1, __q0);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_setr_epi16(short int __q0, short int __q1, short int __q2, short int __q3, short int __q4, short int __q5, short int __q6, short int __q7)
{
  return _mm_set_epi16(__q7, __q6, __q5, __q4, __q3, __q2, __q1, __q0);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_setr_epi8(char __q00, char __q01, char __q02, char __q03, char __q04, char __q05, char __q06, char __q07, char __q08, char __q09, char __q10, char __q11, char __q12, char __q13, char __q14, char __q15)
{
  return _mm_set_epi8(__q15, __q14, __q13, __q12, __q11, __q10, __q09, __q08, __q07, __q06, __q05, __q04, __q03, __q02, __q01, __q00);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_load_si128(const __m128i *__P)
{
  return *__P;
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_loadu_si128(const __m128i *__P)
{
  return (__m128i)__builtin_ia32_loaddqu((const char *)__P);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_loadl_epi64(const __m128i *__P)
{
  return _mm_set_epi64((__m64)0LL, *((__m64 *)__P));
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) void _mm_store_si128(__m128i *__P, __m128i __B)
{
  *__P = __B;
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) void _mm_storeu_si128(__m128i *__P, __m128i __B)
{
  __builtin_ia32_storedqu((char *)__P, (__v16qi)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) void _mm_storel_epi64(__m128i *__P, __m128i __B)
{
  *((long long int *)__P) = __builtin_ia32_vec_ext_v2di((__v2di)__B, 0);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_movepi64_pi64(__m128i __B)
{
  return (__m64)__builtin_ia32_vec_ext_v2di((__v2di)__B, 0);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_movpi64_epi64(__m64 __A)
{
  return _mm_set_epi64((__m64)0LL, __A);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_move_epi64(__m128i __A)
{
  return (__m128i)__builtin_ia32_movq128((__v2di)__A);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_setzero_si128(void)
{
  return (__m128i)(__v4si){0, 0, 0, 0};
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128d _mm_cvtepi32_pd(__m128i __A)
{
  return (__m128d)__builtin_ia32_cvtdq2pd((__v4si)__A);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128 _mm_cvtepi32_ps(__m128i __A)
{
  return (__m128)__builtin_ia32_cvtdq2ps((__v4si)__A);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_cvtpd_epi32(__m128d __A)
{
  return (__m128i)__builtin_ia32_cvtpd2dq((__v2df)__A);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_cvtpd_pi32(__m128d __A)
{
  return (__m64)__builtin_ia32_cvtpd2pi((__v2df)__A);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128 _mm_cvtpd_ps(__m128d __A)
{
  return (__m128)__builtin_ia32_cvtpd2ps((__v2df)__A);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_cvttpd_epi32(__m128d __A)
{
  return (__m128i)__builtin_ia32_cvttpd2dq((__v2df)__A);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_cvttpd_pi32(__m128d __A)
{
  return (__m64)__builtin_ia32_cvttpd2pi((__v2df)__A);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128d _mm_cvtpi32_pd(__m64 __A)
{
  return (__m128d)__builtin_ia32_cvtpi2pd((__v2si)__A);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_cvtps_epi32(__m128 __A)
{
  return (__m128i)__builtin_ia32_cvtps2dq((__v4sf)__A);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_cvttps_epi32(__m128 __A)
{
  return (__m128i)__builtin_ia32_cvttps2dq((__v4sf)__A);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128d _mm_cvtps_pd(__m128 __A)
{
  return (__m128d)__builtin_ia32_cvtps2pd((__v4sf)__A);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) int _mm_cvtsd_si32(__m128d __A)
{
  return __builtin_ia32_cvtsd2si((__v2df)__A);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) long long int _mm_cvtsd_si64(__m128d __A)
{
  return __builtin_ia32_cvtsd2si64((__v2df)__A);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) long long int _mm_cvtsd_si64x(__m128d __A)
{
  return __builtin_ia32_cvtsd2si64((__v2df)__A);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) int _mm_cvttsd_si32(__m128d __A)
{
  return __builtin_ia32_cvttsd2si((__v2df)__A);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) long long int _mm_cvttsd_si64(__m128d __A)
{
  return __builtin_ia32_cvttsd2si64((__v2df)__A);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) long long int _mm_cvttsd_si64x(__m128d __A)
{
  return __builtin_ia32_cvttsd2si64((__v2df)__A);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128 _mm_cvtsd_ss(__m128 __A, __m128d __B)
{
  return (__m128)__builtin_ia32_cvtsd2ss((__v4sf)__A, (__v2df)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128d _mm_cvtsi32_sd(__m128d __A, int __B)
{
  return (__m128d)__builtin_ia32_cvtsi2sd((__v2df)__A, __B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128d _mm_cvtsi64_sd(__m128d __A, long long int __B)
{
  return (__m128d)__builtin_ia32_cvtsi642sd((__v2df)__A, __B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128d _mm_cvtsi64x_sd(__m128d __A, long long int __B)
{
  return (__m128d)__builtin_ia32_cvtsi642sd((__v2df)__A, __B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128d _mm_cvtss_sd(__m128d __A, __m128 __B)
{
  return (__m128d)__builtin_ia32_cvtss2sd((__v2df)__A, (__v4sf)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128d _mm_shuffle_pd(__m128d __A, __m128d __B, const int __mask)
{
  return (__m128d)__builtin_ia32_shufpd((__v2df)__A, (__v2df)__B, __mask);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128d _mm_unpackhi_pd(__m128d __A, __m128d __B)
{
  return (__m128d)__builtin_ia32_unpckhpd((__v2df)__A, (__v2df)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128d _mm_unpacklo_pd(__m128d __A, __m128d __B)
{
  return (__m128d)__builtin_ia32_unpcklpd((__v2df)__A, (__v2df)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128d _mm_loadh_pd(__m128d __A, const double *__B)
{
  return (__m128d)__builtin_ia32_loadhpd((__v2df)__A, __B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128d _mm_loadl_pd(__m128d __A, const double *__B)
{
  return (__m128d)__builtin_ia32_loadlpd((__v2df)__A, __B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) int _mm_movemask_pd(__m128d __A)
{
  return __builtin_ia32_movmskpd((__v2df)__A);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_packs_epi16(__m128i __A, __m128i __B)
{
  return (__m128i)__builtin_ia32_packsswb128((__v8hi)__A, (__v8hi)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_packs_epi32(__m128i __A, __m128i __B)
{
  return (__m128i)__builtin_ia32_packssdw128((__v4si)__A, (__v4si)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_packus_epi16(__m128i __A, __m128i __B)
{
  return (__m128i)__builtin_ia32_packuswb128((__v8hi)__A, (__v8hi)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_unpackhi_epi8(__m128i __A, __m128i __B)
{
  return (__m128i)__builtin_ia32_punpckhbw128((__v16qi)__A, (__v16qi)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_unpackhi_epi16(__m128i __A, __m128i __B)
{
  return (__m128i)__builtin_ia32_punpckhwd128((__v8hi)__A, (__v8hi)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_unpackhi_epi32(__m128i __A, __m128i __B)
{
  return (__m128i)__builtin_ia32_punpckhdq128((__v4si)__A, (__v4si)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_unpackhi_epi64(__m128i __A, __m128i __B)
{
  return (__m128i)__builtin_ia32_punpckhqdq128((__v2di)__A, (__v2di)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_unpacklo_epi8(__m128i __A, __m128i __B)
{
  return (__m128i)__builtin_ia32_punpcklbw128((__v16qi)__A, (__v16qi)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_unpacklo_epi16(__m128i __A, __m128i __B)
{
  return (__m128i)__builtin_ia32_punpcklwd128((__v8hi)__A, (__v8hi)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_unpacklo_epi32(__m128i __A, __m128i __B)
{
  return (__m128i)__builtin_ia32_punpckldq128((__v4si)__A, (__v4si)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_unpacklo_epi64(__m128i __A, __m128i __B)
{
  return (__m128i)__builtin_ia32_punpcklqdq128((__v2di)__A, (__v2di)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_add_epi8(__m128i __A, __m128i __B)
{
  return (__m128i)__builtin_ia32_paddb128((__v16qi)__A, (__v16qi)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_add_epi16(__m128i __A, __m128i __B)
{
  return (__m128i)__builtin_ia32_paddw128((__v8hi)__A, (__v8hi)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_add_epi32(__m128i __A, __m128i __B)
{
  return (__m128i)__builtin_ia32_paddd128((__v4si)__A, (__v4si)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_add_epi64(__m128i __A, __m128i __B)
{
  return (__m128i)__builtin_ia32_paddq128((__v2di)__A, (__v2di)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_adds_epi8(__m128i __A, __m128i __B)
{
  return (__m128i)__builtin_ia32_paddsb128((__v16qi)__A, (__v16qi)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_adds_epi16(__m128i __A, __m128i __B)
{
  return (__m128i)__builtin_ia32_paddsw128((__v8hi)__A, (__v8hi)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_adds_epu8(__m128i __A, __m128i __B)
{
  return (__m128i)__builtin_ia32_paddusb128((__v16qi)__A, (__v16qi)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_adds_epu16(__m128i __A, __m128i __B)
{
  return (__m128i)__builtin_ia32_paddusw128((__v8hi)__A, (__v8hi)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_sub_epi8(__m128i __A, __m128i __B)
{
  return (__m128i)__builtin_ia32_psubb128((__v16qi)__A, (__v16qi)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_sub_epi16(__m128i __A, __m128i __B)
{
  return (__m128i)__builtin_ia32_psubw128((__v8hi)__A, (__v8hi)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_sub_epi32(__m128i __A, __m128i __B)
{
  return (__m128i)__builtin_ia32_psubd128((__v4si)__A, (__v4si)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_sub_epi64(__m128i __A, __m128i __B)
{
  return (__m128i)__builtin_ia32_psubq128((__v2di)__A, (__v2di)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_subs_epi8(__m128i __A, __m128i __B)
{
  return (__m128i)__builtin_ia32_psubsb128((__v16qi)__A, (__v16qi)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_subs_epi16(__m128i __A, __m128i __B)
{
  return (__m128i)__builtin_ia32_psubsw128((__v8hi)__A, (__v8hi)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_subs_epu8(__m128i __A, __m128i __B)
{
  return (__m128i)__builtin_ia32_psubusb128((__v16qi)__A, (__v16qi)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_subs_epu16(__m128i __A, __m128i __B)
{
  return (__m128i)__builtin_ia32_psubusw128((__v8hi)__A, (__v8hi)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_madd_epi16(__m128i __A, __m128i __B)
{
  return (__m128i)__builtin_ia32_pmaddwd128((__v8hi)__A, (__v8hi)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_mulhi_epi16(__m128i __A, __m128i __B)
{
  return (__m128i)__builtin_ia32_pmulhw128((__v8hi)__A, (__v8hi)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_mullo_epi16(__m128i __A, __m128i __B)
{
  return (__m128i)__builtin_ia32_pmullw128((__v8hi)__A, (__v8hi)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_mul_su32(__m64 __A, __m64 __B)
{
  return (__m64)__builtin_ia32_pmuludq((__v2si)__A, (__v2si)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_mul_epu32(__m128i __A, __m128i __B)
{
  return (__m128i)__builtin_ia32_pmuludq128((__v4si)__A, (__v4si)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_slli_epi16(__m128i __A, int __B)
{
  return (__m128i)__builtin_ia32_psllwi128((__v8hi)__A, __B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_slli_epi32(__m128i __A, int __B)
{
  return (__m128i)__builtin_ia32_pslldi128((__v4si)__A, __B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_slli_epi64(__m128i __A, int __B)
{
  return (__m128i)__builtin_ia32_psllqi128((__v2di)__A, __B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_srai_epi16(__m128i __A, int __B)
{
  return (__m128i)__builtin_ia32_psrawi128((__v8hi)__A, __B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_srai_epi32(__m128i __A, int __B)
{
  return (__m128i)__builtin_ia32_psradi128((__v4si)__A, __B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_bsrli_si128(__m128i __A, const int __N)
{
  return (__m128i)__builtin_ia32_psrldqi128(__A, __N * 8);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_bslli_si128(__m128i __A, const int __N)
{
  return (__m128i)__builtin_ia32_pslldqi128(__A, __N * 8);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_srli_si128(__m128i __A, const int __N)
{
  return (__m128i)__builtin_ia32_psrldqi128(__A, __N * 8);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_slli_si128(__m128i __A, const int __N)
{
  return (__m128i)__builtin_ia32_pslldqi128(__A, __N * 8);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_srli_epi16(__m128i __A, int __B)
{
  return (__m128i)__builtin_ia32_psrlwi128((__v8hi)__A, __B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_srli_epi32(__m128i __A, int __B)
{
  return (__m128i)__builtin_ia32_psrldi128((__v4si)__A, __B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_srli_epi64(__m128i __A, int __B)
{
  return (__m128i)__builtin_ia32_psrlqi128((__v2di)__A, __B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_sll_epi16(__m128i __A, __m128i __B)
{
  return (__m128i)__builtin_ia32_psllw128((__v8hi)__A, (__v8hi)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_sll_epi32(__m128i __A, __m128i __B)
{
  return (__m128i)__builtin_ia32_pslld128((__v4si)__A, (__v4si)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_sll_epi64(__m128i __A, __m128i __B)
{
  return (__m128i)__builtin_ia32_psllq128((__v2di)__A, (__v2di)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_sra_epi16(__m128i __A, __m128i __B)
{
  return (__m128i)__builtin_ia32_psraw128((__v8hi)__A, (__v8hi)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_sra_epi32(__m128i __A, __m128i __B)
{
  return (__m128i)__builtin_ia32_psrad128((__v4si)__A, (__v4si)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_srl_epi16(__m128i __A, __m128i __B)
{
  return (__m128i)__builtin_ia32_psrlw128((__v8hi)__A, (__v8hi)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_srl_epi32(__m128i __A, __m128i __B)
{
  return (__m128i)__builtin_ia32_psrld128((__v4si)__A, (__v4si)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_srl_epi64(__m128i __A, __m128i __B)
{
  return (__m128i)__builtin_ia32_psrlq128((__v2di)__A, (__v2di)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_and_si128(__m128i __A, __m128i __B)
{
  return (__m128i)__builtin_ia32_pand128((__v2di)__A, (__v2di)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_andnot_si128(__m128i __A, __m128i __B)
{
  return (__m128i)__builtin_ia32_pandn128((__v2di)__A, (__v2di)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_or_si128(__m128i __A, __m128i __B)
{
  return (__m128i)__builtin_ia32_por128((__v2di)__A, (__v2di)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_xor_si128(__m128i __A, __m128i __B)
{
  return (__m128i)__builtin_ia32_pxor128((__v2di)__A, (__v2di)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_cmpeq_epi8(__m128i __A, __m128i __B)
{
  return (__m128i)__builtin_ia32_pcmpeqb128((__v16qi)__A, (__v16qi)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_cmpeq_epi16(__m128i __A, __m128i __B)
{
  return (__m128i)__builtin_ia32_pcmpeqw128((__v8hi)__A, (__v8hi)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_cmpeq_epi32(__m128i __A, __m128i __B)
{
  return (__m128i)__builtin_ia32_pcmpeqd128((__v4si)__A, (__v4si)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_cmplt_epi8(__m128i __A, __m128i __B)
{
  return (__m128i)__builtin_ia32_pcmpgtb128((__v16qi)__B, (__v16qi)__A);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_cmplt_epi16(__m128i __A, __m128i __B)
{
  return (__m128i)__builtin_ia32_pcmpgtw128((__v8hi)__B, (__v8hi)__A);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_cmplt_epi32(__m128i __A, __m128i __B)
{
  return (__m128i)__builtin_ia32_pcmpgtd128((__v4si)__B, (__v4si)__A);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_cmpgt_epi8(__m128i __A, __m128i __B)
{
  return (__m128i)__builtin_ia32_pcmpgtb128((__v16qi)__A, (__v16qi)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_cmpgt_epi16(__m128i __A, __m128i __B)
{
  return (__m128i)__builtin_ia32_pcmpgtw128((__v8hi)__A, (__v8hi)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_cmpgt_epi32(__m128i __A, __m128i __B)
{
  return (__m128i)__builtin_ia32_pcmpgtd128((__v4si)__A, (__v4si)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) int _mm_extract_epi16(const __m128i __A, const int __N)
{
  return (unsigned short int)__builtin_ia32_vec_ext_v8hi((__v8hi)__A, __N);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_insert_epi16(const __m128i __A, const int __D, const int __N)
{
  return (__m128i)__builtin_ia32_vec_set_v8hi((__v8hi)__A, __D, __N);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_max_epi16(__m128i __A, __m128i __B)
{
  return (__m128i)__builtin_ia32_pmaxsw128((__v8hi)__A, (__v8hi)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_max_epu8(__m128i __A, __m128i __B)
{
  return (__m128i)__builtin_ia32_pmaxub128((__v16qi)__A, (__v16qi)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_min_epi16(__m128i __A, __m128i __B)
{
  return (__m128i)__builtin_ia32_pminsw128((__v8hi)__A, (__v8hi)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_min_epu8(__m128i __A, __m128i __B)
{
  return (__m128i)__builtin_ia32_pminub128((__v16qi)__A, (__v16qi)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) int _mm_movemask_epi8(__m128i __A)
{
  return __builtin_ia32_pmovmskb128((__v16qi)__A);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_mulhi_epu16(__m128i __A, __m128i __B)
{
  return (__m128i)__builtin_ia32_pmulhuw128((__v8hi)__A, (__v8hi)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_shufflehi_epi16(__m128i __A, const int __mask)
{
  return (__m128i)__builtin_ia32_pshufhw((__v8hi)__A, __mask);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_shufflelo_epi16(__m128i __A, const int __mask)
{
  return (__m128i)__builtin_ia32_pshuflw((__v8hi)__A, __mask);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_shuffle_epi32(__m128i __A, const int __mask)
{
  return (__m128i)__builtin_ia32_pshufd((__v4si)__A, __mask);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) void _mm_maskmoveu_si128(__m128i __A, __m128i __B, char *__C)
{
  __builtin_ia32_maskmovdqu((__v16qi)__A, (__v16qi)__B, __C);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_avg_epu8(__m128i __A, __m128i __B)
{
  return (__m128i)__builtin_ia32_pavgb128((__v16qi)__A, (__v16qi)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_avg_epu16(__m128i __A, __m128i __B)
{
  return (__m128i)__builtin_ia32_pavgw128((__v8hi)__A, (__v8hi)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_sad_epu8(__m128i __A, __m128i __B)
{
  return (__m128i)__builtin_ia32_psadbw128((__v16qi)__A, (__v16qi)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) void _mm_stream_si32(int *__A, int __B)
{
  __builtin_ia32_movnti(__A, __B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) void _mm_stream_si64(long long int *__A, long long int __B)
{
  __builtin_ia32_movnti64(__A, __B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) void _mm_stream_si128(__m128i *__A, __m128i __B)
{
  __builtin_ia32_movntdq((__m128i *)__A, (__v2di)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) void _mm_stream_pd(double *__A, __m128d __B)
{
  __builtin_ia32_movntpd(__A, (__v2df)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) void _mm_clflush(const void *__A)
{
  __builtin_ia32_clflush(__A);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) void _mm_lfence(void)
{
  __builtin_ia32_lfence();
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) void _mm_mfence(void)
{
  __builtin_ia32_mfence();
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_cvtsi32_si128(int __A)
{
  return _mm_set_epi32(0, 0, 0, __A);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_cvtsi64_si128(long long int __A)
{
  return _mm_set_epi64x(0, __A);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_cvtsi64x_si128(long long int __A)
{
  return _mm_set_epi64x(0, __A);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128 _mm_castpd_ps(__m128d __A)
{
  return (__m128)__A;
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_castpd_si128(__m128d __A)
{
  return (__m128i)__A;
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128d _mm_castps_pd(__m128 __A)
{
  return (__m128d)__A;
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_castps_si128(__m128 __A)
{
  return (__m128i)__A;
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128 _mm_castsi128_ps(__m128i __A)
{
  return (__m128)__A;
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128d _mm_castsi128_pd(__m128i __A)
{
  return (__m128d)__A;
}
static __inline float kad_sdot(int n, const float *x, const float *y)
{
  __m128 vs1;
  __m128 vs2;
  int i;
  float s;
  float t[4L];
  int n8 = n >> 3 << 3;
  vs1 = _mm_setzero_ps();
  vs2 = _mm_setzero_ps();
  for (i = 0; i < n8; i += 8)
    {
      __m128 vx1;
      __m128 vx2;
      __m128 vy1;
      __m128 vy2;
      vx1 = _mm_loadu_ps(&x[i]);
      vx2 = _mm_loadu_ps(&x[i + 4]);
      vy1 = _mm_loadu_ps(&y[i]);
      vy2 = _mm_loadu_ps(&y[i + 4]);
      vs1 = _mm_add_ps(vs1, _mm_mul_ps(vx1, vy1));
      vs2 = _mm_add_ps(vs2, _mm_mul_ps(vx2, vy2));
    }
  for (s = 0.00000000000000000000000000000000000000000000000000000e+00; i < n;  ++i)
    {
      s += x[i] * y[i];
    }
  _mm_storeu_ps(t, vs1);
  s += t[0] + t[1] + t[2] + t[3];
  _mm_storeu_ps(t, vs2);
  s += t[0] + t[1] + t[2] + t[3];
  return s;
}
static __inline void kad_saxpy_inlined(int n, float a, const float *x, float *y)
{
  __m128 va;
  int i;
  int n8 = n >> 3 << 3;
  va = _mm_set1_ps(a);
  for (i = 0; i < n8; i += 8)
    {
      __m128 vx1;
      __m128 vx2;
      __m128 vy1;
      __m128 vy2;
      __m128 vt1;
      __m128 vt2;
      vx1 = _mm_loadu_ps(&x[i]);
      vx2 = _mm_loadu_ps(&x[i + 4]);
      vy1 = _mm_loadu_ps(&y[i]);
      vy2 = _mm_loadu_ps(&y[i + 4]);
      vt1 = _mm_add_ps(_mm_mul_ps(va, vx1), vy1);
      vt2 = _mm_add_ps(_mm_mul_ps(va, vx2), vy2);
      _mm_storeu_ps(&y[i], vt1);
      _mm_storeu_ps(&y[i + 4], vt2);
    }
  for (; i < n;  ++i)
    {
      y[i] += a * x[i];
    }
}
void kad_vec_mul_sum(int n, float *a, const float *b, const float *c)
{
  int i;
  for (i = 0; i < n;  ++i)
    {
      a[i] += b[i] * c[i];
    }
}
void kad_saxpy(int n, float a, const float *x, float *y)
{
  kad_saxpy_inlined(n, a, x, y);
}
enum CBLAS_LAYOUT
{
  CblasRowMajor = 101,
  CblasColMajor = 102
};
enum CBLAS_TRANSPOSE
{
  CblasNoTrans = 111,
  CblasTrans = 112,
  CblasConjTrans = 113
};
typedef enum CBLAS_LAYOUT CBLAS_LAYOUT;
typedef enum CBLAS_TRANSPOSE CBLAS_TRANSPOSE;
void cblas_sgemm(const CBLAS_LAYOUT Layout, const CBLAS_TRANSPOSE TransA, const CBLAS_TRANSPOSE TransB, const long long int M, const long long int N, const long long int K, const float alpha, const float *A, const long long int lda, const float *B, const long long int ldb, const float beta, float *C, const long long int ldc);
void kad_sgemm_simple(int trans_A, int trans_B, int M, int N, int K, const float *A, const float *B, float *C)
{
  cblas_sgemm(CblasRowMajor, trans_A ? CblasTrans : CblasNoTrans, trans_B ? CblasTrans : CblasNoTrans, M, N, K, 1.000000000000000000000000e+00f, A, trans_A ? M : K, B, trans_B ? K : N, 1.000000000000000000000000e+00f, C, N);
}
static kad_rng_t kad_rng_dat = {.s = {[0] = 5833679380957638813LLU, [1] = 10520313552068553934LLU}, .n_gset = 0.00000000000000000000000000000000000000000000000000000e+00, .n_iset = 0, .lock = 0};
static __inline uint64_t kad_splitmix64(uint64_t x)
{
  uint64_t z = x += 11400714819323198485LLU;
  z = (z ^ z >> 30) * 13787848793156543929LLU;
  z = (z ^ z >> 27) * 10723151780598845931LLU;
  return z ^ z >> 31;
}
static __inline uint64_t kad_xoroshiro128plus_next(kad_rng_t *r)
{
  const uint64_t s0 = (*r).s[0];
  uint64_t s1 = (*r).s[1];
  const uint64_t result = s0 + s1;
  s1 ^= s0;
  (*r).s[0] = ((s0 << 55 | s0 >> 9) ^ s1) ^ s1 << 14;
  (*r).s[1] = s0 << 36 | s0 >> 28;
  return result;
}
static __inline void kad_xoroshiro128plus_jump(kad_rng_t *r)
{
  int i;
  int b;
  static const unsigned long int JUMP[2L] = {[0] = 13739361407582206667LLU, [1] = 15594563132006766882LLU};
  uint64_t s0 = 0;
  uint64_t s1 = 0;
  for (i = 0; i < 2;  ++i)
    {
      for (b = 0; b < 64; b++)
        {
          if (JUMP[i] & 1LLU << b)
            {
              (s0 ^= (*r).s[0], s1 ^= (*r).s[1]);
            }
          kad_xoroshiro128plus_next(r);
        }
    }
  ((*r).s[0] = s0, (*r).s[1] = s1);
}
void kad_srand(void *d, uint64_t seed)
{
  kad_rng_t *r = d ? (kad_rng_t *)d : &kad_rng_dat;
  ((*r).n_gset = 0.00000000000000000000000000000000000000000000000000000e+00, (*r).n_iset = 0);
  (*r).s[0] = kad_splitmix64(seed);
  (*r).s[1] = kad_splitmix64((*r).s[0]);
}
void *kad_rng(void)
{
  kad_rng_t *r;
  r = (kad_rng_t *)calloc(1, sizeof(kad_rng_t));
  kad_xoroshiro128plus_jump(&kad_rng_dat);
  ((*r).s[0] = kad_rng_dat.s[0], (*r).s[1] = kad_rng_dat.s[1]);
  return r;
}
uint64_t kad_rand(void *d)
{
  return kad_xoroshiro128plus_next(d ? (kad_rng_t *)d : &kad_rng_dat);
}
double kad_drand(void *d)
{
  union  mcc_union_anon_49
  {
    uint64_t i;
    double d;
  };
  union mcc_union_anon_49 u;
  u.i = 1023LLU << 52 | kad_xoroshiro128plus_next(d ? (kad_rng_t *)d : &kad_rng_dat) >> 12;
  return u.d - 1.00000000000000000000000000000000000000000000000000000e+00;
}
extern double sqrt(double __x) __attribute__((__nothrow__)) __attribute__((__leaf__));
extern double log(double __x) __attribute__((__nothrow__)) __attribute__((__leaf__));
double kad_drand_normal(void *d)
{
  kad_rng_t *r = d ? (kad_rng_t *)d : &kad_rng_dat;
  if ((*r).n_iset == 0)
    {
      double v1;
      double v2;
      double rsq;
      double fac;
      do
        {
          v1 = 2.00000000000000000000000000000000000000000000000000000e+00 * kad_drand(d) - 1.00000000000000000000000000000000000000000000000000000e+00;
          v2 = 2.00000000000000000000000000000000000000000000000000000e+00 * kad_drand(d) - 1.00000000000000000000000000000000000000000000000000000e+00;
          rsq = v1 * v1 + v2 * v2;
        }
      while (rsq >= 1.00000000000000000000000000000000000000000000000000000e+00 || rsq == 0.00000000000000000000000000000000000000000000000000000e+00);
      fac = sqrt( -2.00000000000000000000000000000000000000000000000000000e+00 * log(rsq) / rsq);
      (*r).n_gset = v1 * fac;
      (*r).n_iset = 1;
      return v2 * fac;
    }
  else
    {
      (*r).n_iset = 0;
      return (*r).n_gset;
    }
}
static __inline void kad_copy_dim1(kad_node_t *dst, const kad_node_t *src)
{
  (*dst).n_d = (*src).n_d;
  if ((*src).n_d)
    {
      memcpy((*dst).d, (*src).d, (*src).n_d * sizeof(int));
    }
}
int kad_op_add(kad_node_t *p, int action)
{
  kad_node_t *q[2L];
  int n0;
  int n1;
  int i;
  static const char __MERCURIUM_PRETTY_FUNCTION__[34L] = "int kad_op_add(kad_node_t *, int)";
  (q[0] = (*p).child[0], n0 = kad_len(q[0]));
  (q[1] = (*p).child[1], n1 = kad_len(q[1]));
  if (action == 4)
    {
      if (n0 % n1 != 0)
        {
          return  -1;
        }
      kad_copy_dim1(p, q[0]);
    }
  else
    {
      if (action == 2)
        {
          n0 >= n1 ? (void)0 : __assert_fail("n0 >= n1", "kautodiff_parallel.c", 1173, __MERCURIUM_PRETTY_FUNCTION__);
          memcpy((*p).x, (*q[0]).x, n0 * sizeof(float));
          for (i = 0; i < n0; i += n1)
            {
              kad_saxpy(n1, 1.000000000000000000000000e+00f, (*q[1]).x, (*p).x + i);
            }
        }
      else
        {
          if (action == 3)
            {
              if ((*q[0]).flag & 1)
                {
                  kad_saxpy(n0, 1.000000000000000000000000e+00f, (*p).g, (*q[0]).g);
                }
              if ((*q[1]).flag & 1)
                {
                  for (i = 0; i < n0; i += n1)
                    {
                      kad_saxpy(n1, 1.000000000000000000000000e+00f, (*p).g + i, (*q[1]).g);
                    }
                }
            }
        }
    }
  return 0;
}
int kad_op_sub(kad_node_t *p, int action)
{
  kad_node_t *q[2L];
  int n0;
  int n1;
  int i;
  static const char __MERCURIUM_PRETTY_FUNCTION__[34L] = "int kad_op_sub(kad_node_t *, int)";
  (q[0] = (*p).child[0], n0 = kad_len(q[0]));
  (q[1] = (*p).child[1], n1 = kad_len(q[1]));
  if (action == 4)
    {
      if (n0 % n1 != 0)
        {
          return  -1;
        }
      kad_copy_dim1(p, q[0]);
    }
  else
    {
      if (action == 2)
        {
          n0 >= n1 ? (void)0 : __assert_fail("n0 >= n1", "kautodiff_parallel.c", 1197, __MERCURIUM_PRETTY_FUNCTION__);
          memcpy((*p).x, (*q[0]).x, n0 * sizeof(float));
          for (i = 0; i < n0; i += n1)
            {
              kad_saxpy(n1,  -1.000000000000000000000000e+00f, (*q[1]).x, (*p).x + i);
            }
        }
      else
        {
          if (action == 3)
            {
              if ((*q[0]).flag & 1)
                {
                  kad_saxpy(n0, 1.000000000000000000000000e+00f, (*p).g, (*q[0]).g);
                }
              if ((*q[1]).flag & 1)
                {
                  for (i = 0; i < n0; i += n1)
                    {
                      kad_saxpy(n1,  -1.000000000000000000000000e+00f, (*p).g + i, (*q[1]).g);
                    }
                }
            }
        }
    }
  return 0;
}
int kad_op_mul(kad_node_t *p, int action)
{
  kad_node_t *q[2L];
  int n0;
  int n1;
  int i;
  static const char __MERCURIUM_PRETTY_FUNCTION__[34L] = "int kad_op_mul(kad_node_t *, int)";
  (q[0] = (*p).child[0], n0 = kad_len(q[0]));
  (q[1] = (*p).child[1], n1 = kad_len(q[1]));
  if (action == 4)
    {
      if (n0 % n1 != 0)
        {
          return  -1;
        }
      kad_copy_dim1(p, q[0]);
    }
  else
    {
      if (action == 2)
        {
          n0 >= n1 ? (void)0 : __assert_fail("n0 >= n1", "kautodiff_parallel.c", 1221, __MERCURIUM_PRETTY_FUNCTION__);
          memset((*p).x, 0, n0 * sizeof(float));
          if ((*q[0]).x != 0 && (*q[1]).x != 0)
            {
              for (i = 0; i < n0; i += n1)
                {
                  kad_vec_mul_sum(n1, (*p).x + i, (*q[0]).x + i, (*q[1]).x);
                }
            }
        }
      else
        {
          if (action == 3)
            {
              if ((*q[0]).flag & 1 && (*q[1]).x)
                {
                  for (i = 0; i < n0; i += n1)
                    {
                      kad_vec_mul_sum(n1, (*q[0]).g + i, (*p).g + i, (*q[1]).x);
                    }
                }
              if ((*q[1]).flag & 1 && (*q[0]).x)
                {
                  for (i = 0; i < n0; i += n1)
                    {
                      kad_vec_mul_sum(n1, (*q[1]).g, (*p).g + i, (*q[0]).x + i);
                    }
                }
            }
        }
    }
  return 0;
}
int kad_op_cmul(kad_node_t *p, int action)
{
  kad_node_t *q[2L];
  int n_col;
  int i;
  int n_a_row;
  int n_b_row;
  int n_a_col = 1;
  int n_b_col = 1;
  (q[0] = (*p).child[0], q[1] = (*p).child[1]);
  n_col = (*q[0]).d[(*q[0]).n_d - 1] > (*q[1]).d[(*q[1]).n_d - 1] ? (*q[0]).d[(*q[0]).n_d - 1] : (*q[1]).d[(*q[1]).n_d - 1];
  for (i = (*q[0]).n_d - 1; i >= 0;  --i)
    {
      if (n_a_col < n_col)
        {
          n_a_col *= (*q[0]).d[i];
        }
    }
  for (i = (*q[1]).n_d - 1; i >= 0;  --i)
    {
      if (n_b_col < n_col)
        {
          n_b_col *= (*q[1]).d[i];
        }
    }
  (n_a_row = kad_len(q[0]) / n_a_col, n_b_row = kad_len(q[1]) / n_b_col);
  if (action == 4)
    {
      if (n_a_col != n_b_col)
        {
          return  -1;
        }
      ((((*p).n_d = 2, (*p).d[0] = n_a_row)), (*p).d[1] = n_b_row);
    }
  else
    {
      if (action == 2)
        {
          memset((*p).x, 0, n_a_row * n_b_row * sizeof(float));
          if ((*q[0]).x && (*q[1]).x)
            {
              kad_sgemm_simple(0, 1, n_a_row, n_b_row, n_col, (*q[0]).x, (*q[1]).x, (*p).x);
            }
        }
      else
        {
          if (action == 3)
            {
              if ((*q[0]).flag & 1 && (*q[1]).x)
                {
                  kad_sgemm_simple(0, 0, n_a_row, n_col, n_b_row, (*p).g, (*q[1]).x, (*q[0]).g);
                }
              if ((*q[1]).flag & 1 && (*q[0]).x)
                {
                  kad_sgemm_simple(1, 0, n_b_row, n_col, n_a_row, (*p).g, (*q[0]).x, (*q[1]).g);
                }
            }
        }
    }
  return 0;
}
int kad_op_matmul(kad_node_t *p, int action)
{
  kad_node_t *q[2L];
  int n_a_row;
  int n_b_row;
  int n_a_col;
  int n_b_col;
  q[0] = (*p).child[0];
  q[1] = (*p).child[1];
  n_a_row = (*q[0]).n_d == 1 ? 1 : (*q[0]).d[0];
  n_b_row = (*q[1]).n_d == 1 ? 1 : (*q[1]).d[0];
  n_a_col = kad_len(q[0]) / n_a_row;
  n_b_col = kad_len(q[1]) / n_b_row;
  if (action == 4)
    {
      if (n_a_col != n_b_row)
        {
          return  -1;
        }
      ((((*p).n_d = 2, (*p).d[0] = n_a_row)), (*p).d[1] = n_b_col);
    }
  else
    {
      if (action == 2)
        {
          memset((*p).x, 0, n_a_row * n_b_col * sizeof(float));
          if ((*q[0]).x && (*q[1]).x)
            {
              kad_sgemm_simple(0, 0, n_a_row, n_b_col, n_a_col, (*q[0]).x, (*q[1]).x, (*p).x);
            }
        }
      else
        {
          if (action == 3)
            {
              if ((*q[0]).flag & 1 && (*q[1]).x)
                {
                  kad_sgemm_simple(0, 1, n_a_row, n_a_col, n_b_col, (*p).g, (*q[1]).x, (*q[0]).g);
                }
              if ((*q[1]).flag & 1 && (*q[0]).x)
                {
                  kad_sgemm_simple(1, 0, n_b_row, n_b_col, n_a_row, (*q[0]).x, (*p).g, (*q[1]).g);
                }
            }
        }
    }
  return 0;
}
int kad_op_square(kad_node_t *p, int action)
{
  int n;
  int i;
  kad_node_t *q = (*p).child[0];
  n = kad_len(q);
  if (action == 4)
    {
      kad_copy_dim1(p, q);
    }
  else
    {
      if (action == 2)
        {
          for (i = 0; i < n;  ++i)
            {
              (*p).x[i] = (*q).x[i] * (*q).x[i];
            }
        }
      else
        {
          if (action == 3 && (*q).flag & 1)
            {
              for (i = 0; i < n;  ++i)
                {
                  (*q).g[i] += (*p).g[i] * ((*q).x[i] + (*q).x[i]);
                }
            }
        }
    }
  return 0;
}
int kad_op_1minus(kad_node_t *p, int action)
{
  int n;
  int i;
  kad_node_t *q = (*p).child[0];
  n = kad_len(q);
  if (action == 4)
    {
      kad_copy_dim1(p, q);
    }
  else
    {
      if (action == 2)
        {
          for (i = 0; i < n;  ++i)
            {
              (*p).x[i] = 1.000000000000000000000000e+00f - (*q).x[i];
            }
        }
      else
        {
          if (action == 3 && (*q).flag & 1)
            {
              kad_saxpy(n,  -1.000000000000000000000000e+00f, (*p).g, (*q).g);
            }
        }
    }
  return 0;
}
extern float expf(float __x) __attribute__((__nothrow__)) __attribute__((__leaf__));
int kad_op_exp(kad_node_t *p, int action)
{
  int n;
  int i;
  kad_node_t *q = (*p).child[0];
  n = kad_len(q);
  if (action == 4)
    {
      kad_copy_dim1(p, q);
    }
  else
    {
      if (action == 2)
        {
          for (i = 0; i < n;  ++i)
            {
              (*p).x[i] = expf((*q).x[i]);
            }
        }
      else
        {
          if (action == 3 && (*q).flag & 1)
            {
              for (i = 0; i < n;  ++i)
                {
                  (*q).g[i] += (*p).g[i] * (*p).x[i];
                }
            }
        }
    }
  return 0;
}
extern float logf(float __x) __attribute__((__nothrow__)) __attribute__((__leaf__));
int kad_op_log(kad_node_t *p, int action)
{
  int n;
  int i;
  kad_node_t *q = (*p).child[0];
  n = kad_len(q);
  if (action == 4)
    {
      kad_copy_dim1(p, q);
    }
  else
    {
      if (action == 2)
        {
          for (i = 0; i < n;  ++i)
            {
              (*p).x[i] = logf((*q).x[i]);
            }
        }
      else
        {
          if (action == 3 && (*q).flag & 1)
            {
              for (i = 0; i < n;  ++i)
                {
                  (*q).g[i] += (*p).g[i] / (*q).x[i];
                }
            }
        }
    }
  return 0;
}
int kad_op_reduce_sum(kad_node_t *p, int action)
{
  int axis;
  int i;
  int d0;
  int d1;
  int j;
  int k;
  static const char __MERCURIUM_PRETTY_FUNCTION__[41L] = "int kad_op_reduce_sum(kad_node_t *, int)";
  kad_node_t *q = (*p).child[0];
  (*p).ptr ? (void)0 : __assert_fail("p->ptr", "kautodiff_parallel.c", 1359, __MERCURIUM_PRETTY_FUNCTION__);
  axis = *((int32_t *)(*p).ptr);
  if (axis < 0 || axis >= (*q).n_d)
    {
      return  -1;
    }
  for ((i = 0, d0 = 1); i < axis;  ++i)
    {
      d0 *= (*q).d[i];
    }
  for ((i = axis + 1, d1 = 1); i < (*q).n_d;  ++i)
    {
      d1 *= (*q).d[i];
    }
  if (action == 4)
    {
      (*p).n_d = (*q).n_d - 1;
      for (i = j = 0; i < (*q).n_d;  ++i)
        {
          if (i != axis)
            {
              (*p).d[j++] = (*q).d[i];
            }
        }
    }
  else
    {
      if (action == 2)
        {
          memset((*p).x, 0, kad_len(p) * sizeof(float));
          for (i = 0; i < d0;  ++i)
            {
              for (j = 0; j < (*q).d[axis];  ++j)
                {
                  for (k = 0; k < d1;  ++k)
                    {
                      (*p).x[i * d1 + k] += (*q).x[(i * (*q).d[axis] + j) * d1 + k];
                    }
                }
            }
        }
      else
        {
          if (action == 3 && (*q).flag & 1)
            {
              for (i = 0; i < d0;  ++i)
                {
                  for (j = 0; j < (*q).d[axis];  ++j)
                    {
                      for (k = 0; k < d1;  ++k)
                        {
                          (*q).g[(i * (*q).d[axis] + j) * d1 + k] += (*p).g[i * d1 + k];
                        }
                    }
                }
            }
        }
    }
  return 0;
}
int kad_op_reduce_mean(kad_node_t *p, int action)
{
  int axis;
  int i;
  int d0;
  int d1;
  int j;
  int k;
  static const char __MERCURIUM_PRETTY_FUNCTION__[42L] = "int kad_op_reduce_mean(kad_node_t *, int)";
  kad_node_t *q = (*p).child[0];
  (*p).ptr ? (void)0 : __assert_fail("p->ptr", "kautodiff_parallel.c", 1388, __MERCURIUM_PRETTY_FUNCTION__);
  axis = *((int32_t *)(*p).ptr);
  if (axis < 0 || axis >= (*q).n_d)
    {
      return  -1;
    }
  for ((i = 0, d0 = 1); i < axis;  ++i)
    {
      d0 *= (*q).d[i];
    }
  for ((i = axis + 1, d1 = 1); i < (*q).n_d;  ++i)
    {
      d1 *= (*q).d[i];
    }
  if (action == 4)
    {
      (*p).n_d = (*q).n_d - 1;
      for (i = j = 0; i < (*q).n_d;  ++i)
        {
          if (i != axis)
            {
              (*p).d[j++] = (*q).d[i];
            }
        }
    }
  else
    {
      if (action == 2)
        {
          float t = 1.000000000000000000000000e+00f / (*q).d[axis];
          memset((*p).x, 0, kad_len(p) * sizeof(float));
          for (i = 0; i < d0;  ++i)
            {
              for (j = 0; j < (*q).d[axis];  ++j)
                {
                  for (k = 0; k < d1;  ++k)
                    {
                      (*p).x[i * d1 + k] += t * (*q).x[(i * (*q).d[axis] + j) * d1 + k];
                    }
                }
            }
        }
      else
        {
          if (action == 3 && (*q).flag & 1)
            {
              float t = 1.000000000000000000000000e+00f / (*q).d[axis];
              for (i = 0; i < d0;  ++i)
                {
                  for (j = 0; j < (*q).d[axis];  ++j)
                    {
                      for (k = 0; k < d1;  ++k)
                        {
                          (*q).g[(i * (*q).d[axis] + j) * d1 + k] += t * (*p).g[i * d1 + k];
                        }
                    }
                }
            }
        }
    }
  return 0;
}
int kad_op_dropout(kad_node_t *p, int action)
{
  int n;
  int i;
  static const char __MERCURIUM_PRETTY_FUNCTION__[38L] = "int kad_op_dropout(kad_node_t *, int)";
  kad_node_t *q = (*p).child[0];
  (*(*p).child[1]).n_d == 0 ? (void)0 : __assert_fail("p->child[1]->n_d == 0", "kautodiff_parallel.c", 1420, __MERCURIUM_PRETTY_FUNCTION__);
  n = kad_len(q);
  if (action == 4)
    {
      kad_copy_dim1(p, q);
    }
  else
    {
      if (action == 1)
        {
          if ((*(*p).child[0]).flag & 1)
            {
              (*p).gtmp = realloc((*p).gtmp, n);
            }
        }
      else
        {
          if (action == 2)
            {
              float r = ((*q).n_child == 0 && (*q).flag & 2) || ((*q).n_child == 0 && (*q).flag & 1) ? 0.000000000000000000000000e+00f : *(*(*p).child[1]).x;
              float z = 1.000000000000000000000000e+00f / (1.000000000000000000000000e+00f - r);
              uint8_t *flag = (uint8_t *)(*p).gtmp;
              for (i = 0; i < n;  ++i)
                {
                  int kept = kad_drand((*p).ptr) >= r;
                  (*p).x[i] = kept ? (*q).x[i] * z : 0.000000000000000000000000e+00f;
                  if (flag)
                    {
                      flag[i] = kept;
                    }
                }
            }
          else
            {
              if (action == 3 && (*(*p).child[0]).flag & 1)
                {
                  float r = ((*q).n_child == 0 && (*q).flag & 2) || ((*q).n_child == 0 && (*q).flag & 1) ? 0.000000000000000000000000e+00f : *(*(*p).child[1]).x;
                  float z = 1.000000000000000000000000e+00f / (1.000000000000000000000000e+00f - r);
                  uint8_t *flag = (uint8_t *)(*p).gtmp;
                  for (i = 0; i < n;  ++i)
                    {
                      if (flag[i])
                        {
                          (*q).g[i] += z * (*p).g[i];
                        }
                    }
                }
            }
        }
    }
  return 0;
}
int kad_op_sample_normal(kad_node_t *p, int action)
{
  int n;
  int i;
  kad_node_t *q = (*p).child[0];
  n = kad_len(q);
  if (action == 4)
    {
      kad_copy_dim1(p, q);
    }
  else
    {
      if (action == 1)
        {
          if ((*(*p).child[0]).flag & 1)
            {
              (*p).gtmp = realloc((*p).gtmp, n * sizeof(float));
            }
        }
      else
        {
          if (action == 2)
            {
              float *r = (float *)(*p).gtmp;
              for (i = 0; i < n;  ++i)
                {
                  float z;
                  z = (float)kad_drand_normal((*p).ptr);
                  (*p).x[i] = (*q).x[i] * z;
                  if (r)
                    {
                      r[i] = z;
                    }
                }
            }
          else
            {
              if (action == 3 && (*(*p).child[0]).flag & 1)
                {
                  float *r = (float *)(*p).gtmp;
                  for (i = 0; i < n;  ++i)
                    {
                      (*q).g[i] += (*p).g[i] * r[i];
                    }
                }
            }
        }
    }
  return 0;
}
int kad_op_slice(kad_node_t *p, int action)
{
  int32_t *aux;
  int axis;
  int32_t *range;
  int i;
  int d0;
  int d1;
  static const char __MERCURIUM_PRETTY_FUNCTION__[36L] = "int kad_op_slice(kad_node_t *, int)";
  kad_node_t *q = (*p).child[0];
  (*p).ptr ? (void)0 : __assert_fail("p->ptr", "kautodiff_parallel.c", 1476, __MERCURIUM_PRETTY_FUNCTION__);
  (((aux = (int32_t *)(*p).ptr, axis = aux[0])), range = aux + 1);
  if (axis < 0 || axis >= (*q).n_d)
    {
      return  -1;
    }
  for ((i = 0, d0 = 1); i < axis;  ++i)
    {
      d0 *= (*q).d[i];
    }
  for ((i = axis + 1, d1 = 1); i < (*q).n_d;  ++i)
    {
      d1 *= (*q).d[i];
    }
  if (action == 4)
    {
      if ((range[0] >= range[1] || range[0] < 0) || range[1] > (*q).d[axis])
        {
          return  -1;
        }
      kad_copy_dim1(p, q);
      (*p).d[axis] = range[1] - range[0];
    }
  else
    {
      if (action == 2)
        {
          for (i = 0; i < d0;  ++i)
            {
              memcpy(&(*p).x[i * (*p).d[axis] * d1], &(*q).x[(i * (*q).d[axis] + range[0]) * d1], (range[1] - range[0]) * d1 * sizeof(float));
            }
        }
      else
        {
          if (action == 3 && (*q).flag & 1)
            {
              for (i = 0; i < d0;  ++i)
                {
                  kad_saxpy((range[1] - range[0]) * d1, 1.000000000000000000000000e+00f, &(*p).g[i * (*p).d[axis] * d1], &(*q).g[(i * (*q).d[axis] + range[0]) * d1]);
                }
            }
        }
    }
  return 0;
}
int kad_op_concat(kad_node_t *p, int action)
{
  int32_t *aux;
  int axis;
  int i;
  int d0;
  int d1;
  int j;
  int k;
  static const char __MERCURIUM_PRETTY_FUNCTION__[37L] = "int kad_op_concat(kad_node_t *, int)";
  kad_node_t *q = (*p).child[0];
  (*p).ptr ? (void)0 : __assert_fail("p->ptr", "kautodiff_parallel.c", 1501, __MERCURIUM_PRETTY_FUNCTION__);
  (aux = (int32_t *)(*p).ptr, axis = aux[0]);
  for ((i = 0, d0 = 1); i < axis;  ++i)
    {
      d0 *= (*q).d[i];
    }
  for ((i = axis + 1, d1 = 1); i < (*q).n_d;  ++i)
    {
      d1 *= (*q).d[i];
    }
  if (action == 4)
    {
      for (i = 1; i < (*p).n_child;  ++i)
        {
          if ((*(*p).child[i]).n_d != (*q).n_d)
            {
              return  -1;
            }
          for (j = 0; j < (*q).n_d;  ++j)
            {
              if (j != axis && (*q).d[j] != (*(*p).child[i]).d[j])
                {
                  return  -1;
                }
            }
        }
      kad_copy_dim1(p, q);
      for (i = 1; i < (*p).n_child;  ++i)
        {
          (*p).d[axis] += (*(*p).child[i]).d[axis];
        }
    }
  else
    {
      if (action == 2)
        {
          for (i = 0; i < d0;  ++i)
            {
              for (j = k = 0; j < (*p).n_child;  ++j)
                {
                  q = (*p).child[j];
                  memcpy(&(*p).x[(i * (*p).d[axis] + k) * d1], &(*q).x[i * (*q).d[axis] * d1], (*q).d[axis] * d1 * sizeof(float));
                  k += (*q).d[axis];
                }
            }
        }
      else
        {
          if (action == 3)
            {
              for (i = 0; i < d0;  ++i)
                {
                  for (j = k = 0; j < (*p).n_child;  ++j)
                    {
                      q = (*p).child[j];
                      if (!((*q).flag & 1))
                        {
                          continue;
                        }
                      kad_saxpy((*q).d[axis] * d1, 1.000000000000000000000000e+00f, &(*p).g[(i * (*p).d[axis] + k) * d1], &(*q).g[i * (*q).d[axis] * d1]);
                      k += (*q).d[axis];
                    }
                }
            }
        }
    }
  return 0;
}
int kad_op_reshape(kad_node_t *p, int action)
{
  kad_node_t *q = (*p).child[0];
  if (action == 4)
    {
      if ((*p).ptr)
        {
          int i;
          int32_t *aux = (int32_t *)(*p).ptr;
          int len = 1;
          int n_missing = 0;
          (*p).n_d = (*p).ptr_size / 4;
          for (i = 0; i < (*p).n_d;  ++i)
            {
              (*p).d[i] = aux[i];
            }
          for (i = 0; i < (*p).n_d;  ++i)
            {
              if ((*p).d[i] <= 0)
                {
                   ++n_missing;
                }
              else
                {
                  len *= (*p).d[i];
                }
            }
          if (n_missing == 0 && len != kad_len(q))
            {
              return  -1;
            }
          if (n_missing > 1)
            {
              for (i = 0; i < (*p).n_d;  ++i)
                {
                  if ((*p).d[i] <= 0 && i < (*q).n_d)
                    {
                      ((*p).d[i] = (*q).d[i], len *= (*p).d[i]);
                      if ( --n_missing == 1)
                        {
                          break;
                        }
                    }
                }
              if (n_missing > 1)
                {
                  return  -1;
                }
            }
          if (n_missing == 1)
            {
              if (kad_len(q) % len != 0)
                {
                  return  -1;
                }
              for (i = 0; i < (*p).n_d;  ++i)
                {
                  if ((*p).d[i] <= 0)
                    {
                      (*p).d[i] = kad_len(q) / len;
                    }
                }
            }
        }
      else
        {
          kad_copy_dim1(p, q);
        }
    }
  else
    {
      if (action == 2)
        {
          memcpy((*p).x, (*q).x, kad_len(p) * sizeof(float));
        }
      else
        {
          if (action == 3 && (*q).flag & 1)
            {
              kad_saxpy(kad_len(p), 1.000000000000000000000000e+00f, (*p).g, (*q).g);
            }
        }
    }
  return 0;
}
int kad_op_reverse(kad_node_t *p, int action)
{
  int axis;
  int i;
  int d0;
  int n;
  int d1;
  int j;
  static const char __MERCURIUM_PRETTY_FUNCTION__[38L] = "int kad_op_reverse(kad_node_t *, int)";
  kad_node_t *q = (*p).child[0];
  axis = (*p).ptr ? *((int32_t *)(*p).ptr) : 0;
  if (axis < 0)
    {
      axis += (*q).n_d;
    }
  axis >= 0 && axis < (*q).n_d ? (void)0 : __assert_fail("axis >= 0 && axis < q->n_d", "kautodiff_parallel.c", 1576, __MERCURIUM_PRETTY_FUNCTION__);
  for ((i = 0, d0 = 1); i < axis;  ++i)
    {
      d0 *= (*q).d[i];
    }
  n = (*q).d[axis];
  for ((i = axis + 1, d1 = 1); i < (*q).n_d;  ++i)
    {
      d1 *= (*q).d[i];
    }
  if (action == 4)
    {
      kad_copy_dim1(p, q);
    }
  else
    {
      if (action == 2)
        {
          for (i = 0; i < d0;  ++i)
            {
              for (j = 0; j < n;  ++j)
                {
                  memcpy(&(*p).x[(i * n + n - 1 - j) * d1], &(*q).x[(i * n + j) * d1], d1 * sizeof(float));
                }
            }
        }
      else
        {
          if (action == 3 && (*q).flag & 1)
            {
              for (i = 0; i < d0;  ++i)
                {
                  for (j = 0; j < n;  ++j)
                    {
                      kad_saxpy(d1, 1.000000000000000000000000e+00f, &(*p).g[(i * n + n - 1 - j) * d1], &(*q).g[(i * n + j) * d1]);
                    }
                }
            }
        }
    }
  return 0;
}
int kad_op_mse(kad_node_t *p, int action)
{
  int n;
  int i;
  kad_node_t *y1 = (*p).child[0];
  kad_node_t *y0 = (*p).child[1];
  n = kad_len(y0);
  if (action == 4)
    {
      if (n != kad_len(y1))
        {
          return  -1;
        }
      (*p).n_d = 0;
    }
  else
    {
      if (action == 2)
        {
          double cost = 0.00000000000000000000000000000000000000000000000000000e+00;
          for (i = 0; i < n;  ++i)
            {
              cost += ((*y1).x[i] - (*y0).x[i]) * ((*y1).x[i] - (*y0).x[i]);
            }
          (*p).x[0] = (float)(cost / n);
        }
      else
        {
          if (action == 3 && (*y1).flag & 1)
            {
              float t = 2.000000000000000000000000e+00f * (*p).g[0] / n;
              for (i = 0; i < n;  ++i)
                {
                  (*y1).g[i] += t * ((*y1).x[i] - (*y0).x[i]);
                }
            }
        }
    }
  return 0;
}
int kad_op_ce_bin(kad_node_t *p, int action)
{
  int n;
  int i;
  static const float tiny = 9.999999717180685365747195e-10f;
  kad_node_t *y1 = (*p).child[0];
  kad_node_t *y0 = (*p).child[1];
  n = kad_len(y0);
  if (action == 4)
    {
      if (n != kad_len(y1))
        {
          return  -1;
        }
      (*p).n_d = 0;
    }
  else
    {
      if (action == 2)
        {
          double cost = 0.00000000000000000000000000000000000000000000000000000e+00;
          for (i = 0; i < n;  ++i)
            {
              if ((*y0).x[i] > 0.000000000000000000000000e+00f)
                {
                  cost += (*y0).x[i] * log((*y0).x[i] / ((*y1).x[i] > tiny ? (*y1).x[i] : tiny));
                }
              if (1.000000000000000000000000e+00f - (*y0).x[i] > 0.000000000000000000000000e+00f)
                {
                  cost += (1.000000000000000000000000e+00f - (*y0).x[i]) * log((1.000000000000000000000000e+00f - (*y0).x[i]) / (1.000000000000000000000000e+00f - (*y1).x[i] > tiny ? 1.000000000000000000000000e+00f - (*y1).x[i] : tiny));
                }
            }
          (*p).x[0] = (float)(cost / n);
        }
      else
        {
          if (action == 3 && (*y1).flag & 1)
            {
              float t = (*p).g[0] / n;
              for (i = 0; i < n;  ++i)
                {
                  if ((*y0).x[i] > 0.000000000000000000000000e+00f)
                    {
                      (*y1).g[i] -= t * (*y0).x[i] / ((*y1).x[i] > tiny ? (*y1).x[i] : tiny);
                    }
                  if (1.000000000000000000000000e+00f - (*y0).x[i] > 0.000000000000000000000000e+00f)
                    {
                      (*y1).g[i] += t * (1.000000000000000000000000e+00f - (*y0).x[i]) / (1.000000000000000000000000e+00f - (*y1).x[i] > tiny ? 1.000000000000000000000000e+00f - (*y1).x[i] : tiny);
                    }
                }
            }
        }
    }
  return 0;
}
int kad_op_ce_bin_neg(kad_node_t *p, int action)
{
  int n;
  int i;
  static const float tiny = 9.999999717180685365747195e-10f;
  kad_node_t *y1 = (*p).child[0];
  kad_node_t *y0 = (*p).child[1];
  n = kad_len(y0);
  if (action == 4)
    {
      if (n != kad_len(y1))
        {
          return  -1;
        }
      (*p).n_d = 0;
    }
  else
    {
      if (action == 2)
        {
          double cost = 0.00000000000000000000000000000000000000000000000000000e+00;
          for (i = 0; i < n;  ++i)
            {
              if (1.000000000000000000000000e+00f + (*y0).x[i] > 0.000000000000000000000000e+00f)
                {
                  cost += 5.000000000000000000000000e-01f * (1.000000000000000000000000e+00f + (*y0).x[i]) * log((1.000000000000000000000000e+00f + (*y0).x[i]) / (1.000000000000000000000000e+00f + (*y1).x[i] > tiny ? 1.000000000000000000000000e+00f + (*y1).x[i] : tiny));
                }
              if (1.000000000000000000000000e+00f - (*y0).x[i] > 0.000000000000000000000000e+00f)
                {
                  cost += 5.000000000000000000000000e-01f * (1.000000000000000000000000e+00f - (*y0).x[i]) * log((1.000000000000000000000000e+00f - (*y0).x[i]) / (1.000000000000000000000000e+00f - (*y1).x[i] > tiny ? 1.000000000000000000000000e+00f - (*y1).x[i] : tiny));
                }
            }
          (*p).x[0] = (float)(cost / n);
        }
      else
        {
          if (action == 3 && (*y1).flag & 1)
            {
              float t = (*p).g[0] / n;
              for (i = 0; i < n;  ++i)
                {
                  if (1.000000000000000000000000e+00f + (*y0).x[i] > 0.000000000000000000000000e+00f)
                    {
                      (*y1).g[i] -= 5.000000000000000000000000e-01f * t * (1.000000000000000000000000e+00f + (*y0).x[i]) / (1.000000000000000000000000e+00f + (*y1).x[i] > tiny ? 1.000000000000000000000000e+00f + (*y1).x[i] : tiny);
                    }
                  if (1.000000000000000000000000e+00f - (*y0).x[i] > 0.000000000000000000000000e+00f)
                    {
                      (*y1).g[i] += 5.000000000000000000000000e-01f * t * (1.000000000000000000000000e+00f - (*y0).x[i]) / (1.000000000000000000000000e+00f - (*y1).x[i] > tiny ? 1.000000000000000000000000e+00f - (*y1).x[i] : tiny);
                    }
                }
            }
        }
    }
  return 0;
}
int kad_op_ce_multi(kad_node_t *p, int action)
{
  int n1;
  int d0;
  int j;
  int i;
  static const char __MERCURIUM_PRETTY_FUNCTION__[39L] = "int kad_op_ce_multi(kad_node_t *, int)";
  static const float tiny = 9.999999717180685365747195e-10f;
  kad_node_t *y1 = (*p).child[0];
  kad_node_t *y0 = (*p).child[1];
  kad_node_t *c = 0;
  n1 = (*y0).d[(*y0).n_d - 1];
  d0 = kad_len(y0) / n1;
  if ((*p).n_child == 3)
    {
      c = (*p).child[2];
      (*c).n_d == 1 && (*c).d[0] == n1 ? (void)0 : __assert_fail("c->n_d == 1 && c->d[0] == n1", "kautodiff_parallel.c", 1695, __MERCURIUM_PRETTY_FUNCTION__);
    }
  if (action == 4)
    {
      if (kad_len(y0) != kad_len(y1) || (*y0).d[(*y0).n_d - 1] != (*y1).d[(*y1).n_d - 1])
        {
          return  -1;
        }
      (*p).n_d = 0;
    }
  else
    {
      if (action == 2)
        {
          double cost = 0.00000000000000000000000000000000000000000000000000000e+00;
          if (c == 0)
            {
              for (j = 0; j < d0;  ++j)
                {
                  float *x1 = &(*y1).x[j * n1];
                  float *x0 = &(*y0).x[j * n1];
                  for (i = 0; i < n1;  ++i)
                    {
                      if (x0[i] > 0.000000000000000000000000e+00f)
                        {
                          cost += x0[i] * log(x0[i] / (x1[i] > tiny ? x1[i] : tiny));
                        }
                    }
                }
            }
          else
            {
              for (j = 0; j < d0;  ++j)
                {
                  float *x1 = &(*y1).x[j * n1];
                  float *x0 = &(*y0).x[j * n1];
                  for (i = 0; i < n1;  ++i)
                    {
                      if (x0[i] > 0.000000000000000000000000e+00f)
                        {
                          cost += (*c).x[i] * x0[i] * log(x0[i] / (x1[i] > tiny ? x1[i] : tiny));
                        }
                    }
                }
            }
          (*p).x[0] = (float)(cost / d0);
        }
      else
        {
          if (action == 3 && (*y1).flag & 1)
            {
              float t = (*p).g[0] / d0;
              if (c == 0)
                {
                  for (j = 0; j < d0;  ++j)
                    {
                      float *g = &(*y1).g[j * n1];
                      float *x1 = &(*y1).x[j * n1];
                      float *x0 = &(*y0).x[j * n1];
                      for (i = 0; i < n1;  ++i)
                        {
                          g[i] -= t * x0[i] / (x1[i] > tiny ? x1[i] : tiny);
                        }
                    }
                }
              else
                {
                  for (j = 0; j < d0;  ++j)
                    {
                      float *g = &(*y1).g[j * n1];
                      float *x1 = &(*y1).x[j * n1];
                      float *x0 = &(*y0).x[j * n1];
                      for (i = 0; i < n1;  ++i)
                        {
                          g[i] -= t * (*c).x[i] * x0[i] / (x1[i] > tiny ? x1[i] : tiny);
                        }
                    }
                }
            }
        }
    }
  return 0;
}
int kad_op_stdnorm(kad_node_t *p, int action)
{
  int n;
  int m;
  int j;
  int i;
  static const char __MERCURIUM_PRETTY_FUNCTION__[38L] = "int kad_op_stdnorm(kad_node_t *, int)";
  kad_node_t *q = (*p).child[0];
  (*q).n_d > 0 ? (void)0 : __assert_fail("q->n_d > 0", "kautodiff_parallel.c", 1743, __MERCURIUM_PRETTY_FUNCTION__);
  n = (*q).d[(*q).n_d - 1];
  m = kad_len(q) / n;
  if (action == 4)
    {
      kad_copy_dim1(p, q);
    }
  else
    {
      if (action == 1)
        {
          (*p).gtmp = realloc((*p).gtmp, m * sizeof(float));
        }
      else
        {
          if (action == 2)
            {
              float *si = (float *)(*p).gtmp;
              for (j = 0; j < m;  ++j)
                {
                  double s;
                  float avg;
                  float std_inv;
                  float *px = &(*p).x[j * n];
                  float *qx = &(*q).x[j * n];
                  for ((i = 0, s = 0.00000000000000000000000000000000000000000000000000000e+00); i < n;  ++i)
                    {
                      s += qx[i];
                    }
                  avg = (float)(s / n);
                  for (i = 0; i < n;  ++i)
                    {
                      px[i] = qx[i] - avg;
                    }
                  for ((i = 0, s = 0.00000000000000000000000000000000000000000000000000000e+00); i < n;  ++i)
                    {
                      s += px[i] * px[i];
                    }
                  std_inv = s == 0.00000000000000000000000000000000000000000000000000000e+00 ? 1.000000000000000000000000e+00f : (float)(1.00000000000000000000000000000000000000000000000000000e+00 / sqrt(s / n));
                  for (i = 0; i < n;  ++i)
                    {
                      px[i] *= std_inv;
                    }
                  si[j] = std_inv;
                }
            }
          else
            {
              if (action == 3 && (*q).flag & 1)
                {
                  float *si = (float *)(*p).gtmp;
                  for (j = 0; j < m;  ++j)
                    {
                      double s;
                      double t;
                      float *pg = &(*p).g[j * n];
                      float *qg = &(*q).g[j * n];
                      float *px = &(*p).x[j * n];
                      float std_inv = si[j];
                      for ((i = 0, s = t = 0.00000000000000000000000000000000000000000000000000000e+00); i < n;  ++i)
                        {
                          (s += pg[i], t += px[i] * pg[i]);
                        }
                      (s /= n, t /= n);
                      for (i = 0; i < n;  ++i)
                        {
                          qg[i] += std_inv * (pg[i] - s - px[i] * t);
                        }
                    }
                }
            }
        }
    }
  return 0;
}
int kad_op_sigm(kad_node_t *p, int action)
{
  int n;
  int i;
  kad_node_t *q = (*p).child[0];
  n = kad_len(q);
  if (action == 4)
    {
      kad_copy_dim1(p, q);
    }
  else
    {
      if (action == 2)
        {
          for (i = 0; i < n;  ++i)
            {
              (*p).x[i] = 1.000000000000000000000000e+00f / (1.000000000000000000000000e+00f + expf( -(*q).x[i]));
            }
        }
      else
        {
          if (action == 3 && (*q).flag & 1)
            {
              for (i = 0; i < n;  ++i)
                {
                  (*q).g[i] += (*p).g[i] * ((*p).x[i] * (1.000000000000000000000000e+00f - (*p).x[i]));
                }
            }
        }
    }
  return 0;
}
int kad_op_tanh(kad_node_t *p, int action)
{
  int n;
  int i;
  kad_node_t *q = (*p).child[0];
  n = kad_len(q);
  if (action == 4)
    {
      kad_copy_dim1(p, q);
    }
  else
    {
      if (action == 2)
        {
          for (i = 0; i < n;  ++i)
            {
              if ((*q).x[i] <  -2.000000000000000000000000e+01f)
                {
                  (*p).x[i] =  -1.000000000000000000000000e+00f;
                }
              else
                {
                  float y;
                  y = expf( -2.000000000000000000000000e+00f * (*q).x[i]);
                  (*p).x[i] = (1.000000000000000000000000e+00f - y) / (1.000000000000000000000000e+00f + y);
                }
            }
        }
      else
        {
          if (action == 3 && (*q).flag & 1)
            {
              for (i = 0; i < n;  ++i)
                {
                  (*q).g[i] += (*p).g[i] * (1.000000000000000000000000e+00f - (*p).x[i] * (*p).x[i]);
                }
            }
        }
    }
  return 0;
}
int kad_op_relu(kad_node_t *p, int action)
{
  int n;
  int i;
  kad_node_t *q = (*p).child[0];
  n = kad_len(q);
  if (action == 4)
    {
      kad_copy_dim1(p, q);
    }
  else
    {
      if (action == 2)
        {
          for (i = 0; i < n;  ++i)
            {
              (*p).x[i] = (*q).x[i] > 0.000000000000000000000000e+00f ? (*q).x[i] : 0.000000000000000000000000e+00f;
            }
        }
      else
        {
          if (action == 3 && (*q).flag & 1)
            {
              for (i = 0; i < n;  ++i)
                {
                  if ((*q).x[i] > 0.000000000000000000000000e+00f)
                    {
                      (*q).g[i] += (*p).g[i];
                    }
                }
            }
        }
    }
  return 0;
}
extern float sinf(float __x) __attribute__((__nothrow__)) __attribute__((__leaf__));
extern float cosf(float __x) __attribute__((__nothrow__)) __attribute__((__leaf__));
int kad_op_sin(kad_node_t *p, int action)
{
  int n;
  int i;
  kad_node_t *q = (*p).child[0];
  n = kad_len(q);
  if (action == 4)
    {
      kad_copy_dim1(p, q);
    }
  else
    {
      if (action == 2)
        {
          for (i = 0; i < n;  ++i)
            {
              (*p).x[i] = sinf((*q).x[i]);
            }
        }
      else
        {
          if (action == 3 && (*q).flag & 1)
            {
              for (i = 0; i < n;  ++i)
                {
                  (*q).g[i] += (*p).g[i] * cosf((*q).x[i]);
                }
            }
        }
    }
  return 0;
}
int kad_op_softmax(kad_node_t *p, int action)
{
  int n1;
  int d0;
  int j;
  int i;
  kad_node_t *q = (*p).child[0];
  n1 = (*q).d[(*q).n_d - 1];
  d0 = kad_len(q) / n1;
  if (action == 4)
    {
      kad_copy_dim1(p, q);
    }
  else
    {
      if (action == 2)
        {
          for (j = 0; j < d0;  ++j)
            {
              float max;
              float s;
              float *x = &(*q).x[j * n1];
              float *y = &(*p).x[j * n1];
              for ((i = 0, max =  -3.402823466385288598117042e+38f); i < n1;  ++i)
                {
                  max = max > x[i] ? max : x[i];
                }
              for ((i = 0, s = 0.000000000000000000000000e+00f); i < n1;  ++i)
                {
                  y[i] = expf(x[i] - max);
                  s += y[i];
                }
              for ((i = 0, s = 1.000000000000000000000000e+00f / s); i < n1;  ++i)
                {
                  y[i] *= s;
                }
            }
        }
      else
        {
          if (action == 3 && (*q).flag & 1)
            {
              for (j = 0; j < d0;  ++j)
                {
                  float s;
                  float *g = &(*p).g[j * n1];
                  float *y = &(*p).x[j * n1];
                  float *h = &(*q).g[j * n1];
                  for ((i = 0, s = 0.000000000000000000000000e+00f); i < n1;  ++i)
                    {
                      s += g[i] * y[i];
                    }
                  for (i = 0; i < n1;  ++i)
                    {
                      h[i] += y[i] * (g[i] - s);
                    }
                }
            }
        }
    }
  return 0;
}
int kad_op_avg(kad_node_t *p, int action)
{
  float tmp;
  kad_node_t *q;
  int n;
  int i;
  static const char __MERCURIUM_PRETTY_FUNCTION__[34L] = "int kad_op_avg(kad_node_t *, int)";
  (*p).n_child > 0 ? (void)0 : __assert_fail("p->n_child > 0", "kautodiff_parallel.c", 1895, __MERCURIUM_PRETTY_FUNCTION__);
  tmp = 1.000000000000000000000000e+00f / (*p).n_child;
  q = (*p).child[0];
  n = kad_len(q);
  if (action == 4)
    {
      for (i = 1; i < (*p).n_child;  ++i)
        {
          if (kad_len((*p).child[i]) != n)
            {
              return  -1;
            }
        }
      kad_copy_dim1(p, q);
    }
  else
    {
      if (action == 2)
        {
          memcpy((*p).x, (*q).x, n * sizeof(float));
          for (i = 1; i < (*p).n_child;  ++i)
            {
              kad_saxpy(n, 1.000000000000000000000000e+00f, (*(*p).child[i]).x, (*p).x);
            }
          for (i = 0; i < n;  ++i)
            {
              (*p).x[i] *= tmp;
            }
        }
      else
        {
          if (action == 3)
            {
              for (i = 0; i < (*p).n_child;  ++i)
                {
                  if ((*(*p).child[i]).flag & 1)
                    {
                      kad_saxpy(n, tmp, (*p).g, (*(*p).child[i]).g);
                    }
                }
            }
        }
    }
  return 0;
}
int kad_op_max(kad_node_t *p, int action)
{
  int n;
  int i;
  kad_node_t *q = (*p).child[0];
  n = kad_len(q);
  if (action == 4)
    {
      int *max_j;
      for (i = 1; i < (*p).n_child;  ++i)
        {
          if (kad_len((*p).child[i]) != n)
            {
              return  -1;
            }
        }
      kad_copy_dim1(p, q);
      max_j = (int *)calloc(n, sizeof(int));
      (*p).gtmp = max_j;
    }
  else
    {
      if (action == 2)
        {
          int j;
          int *max_j = (int *)(*p).gtmp;
          memset(max_j, 0, n * sizeof(int));
          memcpy((*p).x, (*q).x, n * sizeof(float));
          for (j = 1; j < (*p).n_child;  ++j)
            {
              for ((i = 0, q = (*p).child[j]); i < n;  ++i)
                {
                  if ((*q).x[i] > (*p).x[i])
                    {
                      ((*p).x[i] = (*q).x[i], max_j[i] = j);
                    }
                }
            }
        }
      else
        {
          if (action == 3)
            {
              int *max_j = (int *)(*p).gtmp;
              for (i = 0; i < n;  ++i)
                {
                  (*(*p).child[max_j[i]]).g[i] += (*p).g[i];
                }
            }
        }
    }
  return 0;
}
int kad_op_stack(kad_node_t *p, int action)
{
  kad_node_t *q;
  int n;
  int i;
  static const char __MERCURIUM_PRETTY_FUNCTION__[36L] = "int kad_op_stack(kad_node_t *, int)";
  int axis = 0;
  (*p).n_child > 0 ? (void)0 : __assert_fail("p->n_child > 0", "kautodiff_parallel.c", 1948, __MERCURIUM_PRETTY_FUNCTION__);
  q = (*p).child[0];
  n = kad_len(q);
  if (action == 4)
    {
      for (i = 1; i < (*p).n_child;  ++i)
        {
          if (kad_len((*p).child[i]) != n)
            {
              return  -1;
            }
        }
      (*p).n_d = (*q).n_d + 1;
      for (i = 0; i < axis;  ++i)
        {
          (*p).d[i] = (*q).d[i];
        }
      (*p).d[axis] = (*p).n_child;
      for (; i < (*q).n_d;  ++i)
        {
          (*p).d[i + 1] = (*q).d[i];
        }
    }
  else
    {
      if (action == 2)
        {
          for (i = 0; i < (*p).n_child;  ++i)
            {
              memcpy(&(*p).x[i * n], (*(*p).child[i]).x, n * sizeof(float));
            }
        }
      else
        {
          if (action == 3)
            {
              for (i = 0; i < (*p).n_child;  ++i)
                {
                  if ((*(*p).child[i]).flag & 1)
                    {
                      kad_saxpy(n, 1.000000000000000000000000e+00f, &(*p).g[i * n], (*(*p).child[i]).g);
                    }
                }
            }
        }
    }
  return 0;
}
int kad_op_select(kad_node_t *p, int action)
{
  int which;
  kad_node_t *q;
  int n;
  int i;
  static const char __MERCURIUM_PRETTY_FUNCTION__[37L] = "int kad_op_select(kad_node_t *, int)";
  which = *((int32_t *)(*p).ptr);
  if (which < 0)
    {
      which += (*p).n_child;
    }
  which >= 0 && which < (*p).n_child ? (void)0 : __assert_fail("which >= 0 && which < p->n_child", "kautodiff_parallel.c", 1976, __MERCURIUM_PRETTY_FUNCTION__);
  q = (*p).child[which];
  n = kad_len(q);
  if (action == 4)
    {
      for (i = 0; i < (*p).n_child;  ++i)
        {
          if ((*(*p).child[i]).n_d != (*q).n_d || kad_len((*p).child[i]) != n)
            {
              break;
            }
        }
      if (i < (*p).n_child)
        {
          return  -1;
        }
      kad_copy_dim1(p, q);
    }
  else
    {
      if (action == 2)
        {
          memcpy((*p).x, (*q).x, n * sizeof(float));
        }
      else
        {
          if (action == 3 && (*q).flag & 1)
            {
              kad_saxpy(n, 1.000000000000000000000000e+00f, (*p).g, (*q).g);
            }
        }
    }
  return 0;
}
static void conv_rot180(int d0, int d1, float *x)
{
  int i;
  int j;
  for (i = 0; i < d0;  ++i)
    {
      float tmp;
      float *xi = &x[i * d1];
      for (j = 0; j < d1 >> 1;  ++j)
        {
          (((tmp = xi[j], xi[j] = xi[d1 - 1 - j])), xi[d1 - 1 - j] = tmp);
        }
    }
}
static void conv2d_move_1to3(int *d, const float *x, float *y)
{
  int i;
  int j;
  int k;
  int l;
  for (i = 0; i < d[0];  ++i)
    {
      for (j = 0; j < d[1];  ++j)
        {
          for (k = 0; k < d[2];  ++k)
            {
              int ik = (i * d[2] + k) * d[3];
              int ijk = ((i * d[1] + j) * d[2] + k) * d[3];
              for (l = 0; l < d[3];  ++l)
                {
                  y[(ik + l) * d[1] + j] = x[ijk + l];
                }
            }
        }
    }
}
static void conv2d_add_3to1(int *d, const float *y, float *x)
{
  int i;
  int j;
  int k;
  int l;
  for (i = 0; i < d[0];  ++i)
    {
      for (j = 0; j < d[1];  ++j)
        {
          for (k = 0; k < d[2];  ++k)
            {
              int ik = (i * d[2] + k) * d[3];
              int ijk = ((i * d[1] + j) * d[2] + k) * d[3];
              for (l = 0; l < d[3];  ++l)
                {
                  x[ijk + l] += y[(ik + l) * d[1] + j];
                }
            }
        }
    }
}
int kad_op_conv2d(kad_node_t *p, int action)
{
  conv_conf_t *aux = (conv_conf_t *)(*p).ptr;
  kad_node_t *q = (*p).child[0];
  kad_node_t *w = (*p).child[1];
  float *t = 0;
  float *q1 = 0;
  float *w1 = 0;
  float *x_padded = 0;
  int algo_switch = 0;
  if (action == 2 || action == 3)
    {
      if ((*w).d[3] * (*w).d[1] < 16)
        {
          t = (float *)malloc((*p).d[3] * sizeof(float));
          x_padded = aux[1].pad[0] + aux[1].pad[1] > 0 ? (float *)calloc((*q).d[3] + aux[1].pad[0] + aux[1].pad[1], sizeof(float)) : 0;
        }
      else
        {
          q1 = (float *)malloc(kad_len(q) * sizeof(float));
          w1 = (float *)malloc(kad_len(w) * sizeof(float));
          x_padded = aux[1].pad[0] + aux[1].pad[1] > 0 ? (float *)calloc(((*q).d[3] + aux[1].pad[0] + aux[1].pad[1]) * (*q).d[1], sizeof(float)) : 0;
          algo_switch = 1;
        }
    }
  if (action == 4)
    {
      if ((*q).n_d != 4 || (*w).n_d != 4)
        {
          return  -1;
        }
      if ((*q).d[1] != (*w).d[1])
        {
          return  -1;
        }
      (*p).n_d = 4;
      ((((((*p).d[0] = (*q).d[0], (*p).d[1] = (*w).d[0])), (*p).d[2] = ((*q).d[2] - (*&aux[0]).kernel_size + (*&aux[0]).pad[0] + (*&aux[0]).pad[1]) / (*&aux[0]).stride + 1)), (*p).d[3] = ((*q).d[3] - (*&aux[1]).kernel_size + (*&aux[1]).pad[0] + (*&aux[1]).pad[1]) / (*&aux[1]).stride + 1);
    }
  else
    {
      if (action == 2)
        {
          conv_rot180((*w).d[0] * (*w).d[1], (*w).d[2] * (*w).d[3], (*w).x);
          memset((*p).x, 0, kad_len(p) * sizeof(float));
          if (!algo_switch)
            {
              do
                {
                  int n;
                  int c1;
                  int c0;
                  int k;
                  int i;
                  int ii;
                  for (n = 0; n < (*q).d[0];  ++n)
                    {
                      for (c1 = 0; c1 < (*w).d[0];  ++c1)
                        {
                          for (c0 = 0; c0 < (*w).d[1];  ++c0)
                            {
                              for (k = 0; k < (*w).d[2];  ++k)
                                {
                                  float *_ww = &(*w).x[((c1 * (*w).d[1] + c0) * (*w).d[2] + k) * (*w).d[3]];
                                  for ((i = 0, ii = k - aux[0].pad[0]); (i < (*p).d[2] && ii >= 0) && ii < (*q).d[2]; ( ++i, ii += aux[0].stride))
                                    {
                                      float *_xx = &(*q).x[((n * (*q).d[1] + c0) * (*q).d[2] + ii) * (*q).d[3]];
                                      float *_yy = &(*p).x[((n * (*p).d[1] + c1) * (*p).d[2] + i) * (*p).d[3]];
                                      if (x_padded)
                                        {
                                          memcpy(x_padded + aux[1].pad[0], _xx, (*q).d[3] * sizeof(float));
                                          _xx = x_padded + aux[1].pad[0];
                                        }
                                      do
                                        {
                                          int l;
                                          int j;
                                          if (aux[1].stride > 1)
                                            {
                                              for (l = 0; l < (*w).d[3];  ++l)
                                                {
                                                  const float *xl = &_xx[l - aux[1].pad[0]];
                                                  for (j = 0; j < (*p).d[3]; ( ++j, xl += aux[1].stride))
                                                    {
                                                      t[j] = *xl;
                                                    }
                                                  kad_saxpy((*p).d[3], _ww[l], t, _yy);
                                                }
                                            }
                                          else
                                            {
                                              for (l = 0; l < (*w).d[3];  ++l)
                                                {
                                                  kad_saxpy((*p).d[3], _ww[l], &_xx[l - aux[1].pad[0]], _yy);
                                                }
                                            }
                                        }
                                      while (0);
                                    }
                                }
                            }
                        }
                    }
                }
              while (0);
            }
          else
            {
              conv2d_move_1to3((*q).d, (*q).x, q1);
              conv2d_move_1to3((*w).d, (*w).x, w1);
              do
                {
                  int n;
                  int c1;
                  int k;
                  int i;
                  int ii;
                  int j;
                  int j_skip = aux[1].stride * (*q).d[1];
                  int m = (*w).d[3] * (*w).d[1];
                  for (n = 0; n < (*q).d[0];  ++n)
                    {
                      for (c1 = 0; c1 < (*w).d[0];  ++c1)
                        {
                          for (k = 0; k < (*w).d[2];  ++k)
                            {
                              float *_ww = &w1[(c1 * (*w).d[2] + k) * m];
                              for ((i = 0, ii = k - aux[0].pad[0]); (i < (*p).d[2] && ii >= 0) && ii < (*q).d[2]; ( ++i, ii += aux[0].stride))
                                {
                                  float *_xx = &q1[(n * (*q).d[2] + ii) * (*q).d[3] * (*q).d[1]];
                                  float *_yy = &(*p).x[((n * (*p).d[1] + c1) * (*p).d[2] + i) * (*p).d[3]];
                                  if (x_padded)
                                    {
                                      memcpy(x_padded + aux[1].pad[0] * (*q).d[1], _xx, (*q).d[3] * (*q).d[1] * sizeof(float));
                                      _xx = x_padded;
                                    }
                                  for (j = 0; j < (*p).d[3]; ((( ++j, _xx += j_skip)),  ++_yy))
                                    {
                                      *_yy += kad_sdot(m, _ww, _xx);
                                    }
                                }
                            }
                        }
                    }
                }
              while (0);
            }
          conv_rot180((*w).d[0] * (*w).d[1], (*w).d[2] * (*w).d[3], (*w).x);
        }
      else
        {
          if (action == 3)
            {
              if ((*(*p).child[0]).flag & 1)
                {
                  conv_rot180((*w).d[0] * (*w).d[1], (*w).d[2] * (*w).d[3], (*w).x);
                  if (!algo_switch)
                    {
                      do
                        {
                          int n;
                          int c1;
                          int c0;
                          int k;
                          int i;
                          int ii;
                          for (n = 0; n < (*q).d[0];  ++n)
                            {
                              for (c1 = 0; c1 < (*w).d[0];  ++c1)
                                {
                                  for (c0 = 0; c0 < (*w).d[1];  ++c0)
                                    {
                                      for (k = 0; k < (*w).d[2];  ++k)
                                        {
                                          float *_ww = &(*w).x[((c1 * (*w).d[1] + c0) * (*w).d[2] + k) * (*w).d[3]];
                                          for ((i = 0, ii = k - aux[0].pad[0]); (i < (*p).d[2] && ii >= 0) && ii < (*q).d[2]; ( ++i, ii += aux[0].stride))
                                            {
                                              float *_xx = &(*q).g[((n * (*q).d[1] + c0) * (*q).d[2] + ii) * (*q).d[3]];
                                              float *_yy = &(*p).g[((n * (*p).d[1] + c1) * (*p).d[2] + i) * (*p).d[3]];
                                              if (x_padded)
                                                {
                                                  memcpy(x_padded + aux[1].pad[0], _xx, (*q).d[3] * sizeof(float));
                                                  _xx = x_padded + aux[1].pad[0];
                                                }
                                              do
                                                {
                                                  int l;
                                                  int j;
                                                  if (aux[1].stride > 1)
                                                    {
                                                      for (l = 0; l < (*w).d[3];  ++l)
                                                        {
                                                          float *xl = &_xx[l - aux[1].pad[0]];
                                                          memset(t, 0, (*p).d[3] * sizeof(float));
                                                          kad_saxpy((*p).d[3], _ww[l], _yy, t);
                                                          for (j = 0; j < (*p).d[3]; ( ++j, xl += aux[1].stride))
                                                            {
                                                              *xl += t[j];
                                                            }
                                                        }
                                                    }
                                                  else
                                                    {
                                                      for (l = 0; l < (*w).d[3];  ++l)
                                                        {
                                                          kad_saxpy((*p).d[3], _ww[l], _yy, &_xx[l - aux[1].pad[0]]);
                                                        }
                                                    }
                                                }
                                              while (0);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                      while (0);
                    }
                  else
                    {
                      memset(q1, 0, kad_len(q) * sizeof(float));
                      conv2d_move_1to3((*w).d, (*w).x, w1);
                      do
                        {
                          int n;
                          int c1;
                          int k;
                          int i;
                          int ii;
                          int j;
                          int j_skip = aux[1].stride * (*q).d[1];
                          int m = (*w).d[3] * (*w).d[1];
                          for (n = 0; n < (*q).d[0];  ++n)
                            {
                              for (c1 = 0; c1 < (*w).d[0];  ++c1)
                                {
                                  for (k = 0; k < (*w).d[2];  ++k)
                                    {
                                      float *_ww = &w1[(c1 * (*w).d[2] + k) * m];
                                      for ((i = 0, ii = k - aux[0].pad[0]); (i < (*p).d[2] && ii >= 0) && ii < (*q).d[2]; ( ++i, ii += aux[0].stride))
                                        {
                                          float *_xx = &q1[(n * (*q).d[2] + ii) * (*q).d[3] * (*q).d[1]];
                                          float *_yy = &(*p).g[((n * (*p).d[1] + c1) * (*p).d[2] + i) * (*p).d[3]];
                                          if (x_padded)
                                            {
                                              memcpy(x_padded + aux[1].pad[0] * (*q).d[1], _xx, (*q).d[3] * (*q).d[1] * sizeof(float));
                                              _xx = x_padded;
                                            }
                                          for (j = 0; j < (*p).d[3]; ((( ++j, _xx += j_skip)),  ++_yy))
                                            {
                                              kad_saxpy(m, *_yy, _ww, _xx);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                      while (0);
                      conv2d_add_3to1((*q).d, q1, (*q).g);
                    }
                  conv_rot180((*w).d[0] * (*w).d[1], (*w).d[2] * (*w).d[3], (*w).x);
                }
              if ((*(*p).child[1]).flag & 1)
                {
                  conv_rot180((*w).d[0] * (*w).d[1], (*w).d[2] * (*w).d[3], (*w).g);
                  if (!algo_switch)
                    {
                      do
                        {
                          int n;
                          int c1;
                          int c0;
                          int k;
                          int i;
                          int ii;
                          for (n = 0; n < (*q).d[0];  ++n)
                            {
                              for (c1 = 0; c1 < (*w).d[0];  ++c1)
                                {
                                  for (c0 = 0; c0 < (*w).d[1];  ++c0)
                                    {
                                      for (k = 0; k < (*w).d[2];  ++k)
                                        {
                                          float *_ww = &(*w).g[((c1 * (*w).d[1] + c0) * (*w).d[2] + k) * (*w).d[3]];
                                          for ((i = 0, ii = k - aux[0].pad[0]); (i < (*p).d[2] && ii >= 0) && ii < (*q).d[2]; ( ++i, ii += aux[0].stride))
                                            {
                                              float *_xx = &(*q).x[((n * (*q).d[1] + c0) * (*q).d[2] + ii) * (*q).d[3]];
                                              float *_yy = &(*p).g[((n * (*p).d[1] + c1) * (*p).d[2] + i) * (*p).d[3]];
                                              if (x_padded)
                                                {
                                                  memcpy(x_padded + aux[1].pad[0], _xx, (*q).d[3] * sizeof(float));
                                                  _xx = x_padded + aux[1].pad[0];
                                                }
                                              do
                                                {
                                                  int l;
                                                  int j;
                                                  if (aux[1].stride > 1)
                                                    {
                                                      for (l = 0; l < (*w).d[3];  ++l)
                                                        {
                                                          const float *xl = &_xx[l - aux[1].pad[0]];
                                                          for (j = 0; j < (*p).d[3]; ( ++j, xl += aux[1].stride))
                                                            {
                                                              t[j] = *xl;
                                                            }
                                                          _ww[l] += kad_sdot((*p).d[3], _yy, t);
                                                        }
                                                    }
                                                  else
                                                    {
                                                      for (l = 0; l < (*w).d[3];  ++l)
                                                        {
                                                          _ww[l] += kad_sdot((*p).d[3], _yy, &_xx[l - aux[1].pad[0]]);
                                                        }
                                                    }
                                                }
                                              while (0);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                      while (0);
                    }
                  else
                    {
                      conv2d_move_1to3((*q).d, (*q).x, q1);
                      memset(w1, 0, kad_len(w) * sizeof(float));
                      do
                        {
                          int n;
                          int c1;
                          int k;
                          int i;
                          int ii;
                          int j;
                          int j_skip = aux[1].stride * (*q).d[1];
                          int m = (*w).d[3] * (*w).d[1];
                          for (n = 0; n < (*q).d[0];  ++n)
                            {
                              for (c1 = 0; c1 < (*w).d[0];  ++c1)
                                {
                                  for (k = 0; k < (*w).d[2];  ++k)
                                    {
                                      float *_ww = &w1[(c1 * (*w).d[2] + k) * m];
                                      for ((i = 0, ii = k - aux[0].pad[0]); (i < (*p).d[2] && ii >= 0) && ii < (*q).d[2]; ( ++i, ii += aux[0].stride))
                                        {
                                          float *_xx = &q1[(n * (*q).d[2] + ii) * (*q).d[3] * (*q).d[1]];
                                          float *_yy = &(*p).g[((n * (*p).d[1] + c1) * (*p).d[2] + i) * (*p).d[3]];
                                          if (x_padded)
                                            {
                                              memcpy(x_padded + aux[1].pad[0] * (*q).d[1], _xx, (*q).d[3] * (*q).d[1] * sizeof(float));
                                              _xx = x_padded;
                                            }
                                          for (j = 0; j < (*p).d[3]; ((( ++j, _xx += j_skip)),  ++_yy))
                                            {
                                              kad_saxpy(m, *_yy, _xx, _ww);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                      while (0);
                      conv2d_add_3to1((*w).d, w1, (*w).g);
                    }
                  conv_rot180((*w).d[0] * (*w).d[1], (*w).d[2] * (*w).d[3], (*w).g);
                }
            }
        }
    }
  free(t);
  free(q1);
  free(w1);
  free(x_padded);
  return 0;
}
int kad_op_max2d(kad_node_t *p, int action)
{
  conv_conf_t *aux = (conv_conf_t *)(*p).ptr;
  kad_node_t *q = (*p).child[0];
  if (action == 4)
    {
      if ((*q).n_d != 4)
        {
          return  -1;
        }
      (*p).n_d = 4;
      ((((((*p).d[0] = (*q).d[0], (*p).d[1] = (*q).d[1])), (*p).d[2] = ((*q).d[2] - (*&aux[0]).kernel_size + (*&aux[0]).pad[0] + (*&aux[0]).pad[1]) / (*&aux[0]).stride + 1)), (*p).d[3] = ((*q).d[3] - (*&aux[1]).kernel_size + (*&aux[1]).pad[0] + (*&aux[1]).pad[1]) / (*&aux[1]).stride + 1);
    }
  else
    {
      if (action == 1)
        {
          (*p).gtmp = realloc((*p).gtmp, kad_len(p) * sizeof(int));
        }
      else
        {
          if (action == 2)
            {
              int len;
              int i;
              int t;
              int rest = 1;
              int *f = (int *)(*p).gtmp;
              len = kad_len(p);
              for (i = 0; i < len;  ++i)
                {
                  (*p).x[i] =  -3.402823466385288598117042e+38f;
                }
              for (i = 0; i < (*p).n_d - 2;  ++i)
                {
                  rest *= (*p).d[i];
                }
              for (t = 0; t < rest;  ++t)
                {
                  int i;
                  int k;
                  int l;
                  int j;
                  int p_row = (*p).d[(*p).n_d - 2];
                  int p_col = (*p).d[(*p).n_d - 1];
                  for (i = 0; i < p_row;  ++i)
                    {
                      int u = (t * p_row + i) * p_col;
                      for (k = 0; k < aux[0].kernel_size;  ++k)
                        {
                          int v0;
                          int v_end;
                          int v;
                          int ii = i * aux[0].stride + k - aux[0].pad[0];
                          if (ii < 0 || ii >= (*q).d[(*p).n_d - 2])
                            {
                              continue;
                            }
                          v0 = (t * (*q).d[(*p).n_d - 2] + ii) * (*q).d[(*p).n_d - 1];
                          v_end = v0 + (*q).d[(*p).n_d - 1];
                          for (l = 0; l < aux[1].kernel_size;  ++l)
                            {
                              for ((j = 0, v = v0 + (l > aux[1].pad[0] ? l - aux[1].pad[0] : 0)); j < p_col && v < v_end; ( ++j, v += aux[1].stride))
                                {
                                  if ((*p).x[u + j] < (*q).x[v])
                                    {
                                      ((*p).x[u + j] = (*q).x[v], f[u + j] = v);
                                    }
                                }
                            }
                        }
                    }
                }
            }
          else
            {
              if (action == 3)
                {
                  int len;
                  int i;
                  int *f = (int *)(*p).gtmp;
                  len = kad_len(p);
                  for (i = 0; i < len;  ++i)
                    {
                      (*q).g[f[i]] += (*p).g[i];
                    }
                }
            }
        }
    }
  return 0;
}
static void conv1d_move_1to2(int *d, const float *x, float *y)
{
  int k;
  int j;
  int i;
  for (k = 0; k < d[0];  ++k)
    {
      for (j = 0; j < d[1];  ++j)
        {
          for (i = 0; i < d[2];  ++i)
            {
              y[(k * d[2] + i) * d[1] + j] = x[(k * d[1] + j) * d[2] + i];
            }
        }
    }
}
static void conv1d_add_2to1(int *d, const float *y, float *x)
{
  int k;
  int j;
  int i;
  for (k = 0; k < d[0];  ++k)
    {
      for (j = 0; j < d[1];  ++j)
        {
          for (i = 0; i < d[2];  ++i)
            {
              x[(k * d[1] + j) * d[2] + i] += y[(k * d[2] + i) * d[1] + j];
            }
        }
    }
}
int kad_op_conv1d(kad_node_t *p, int action)
{
  conv_conf_t *aux = (conv_conf_t *)(*p).ptr;
  kad_node_t *q = (*p).child[0];
  kad_node_t *w = (*p).child[1];
  float *t = 0;
  float *q1 = 0;
  float *w1 = 0;
  float *x_padded = 0;
  int algo_switch = 0;
  if (action == 2 || action == 3)
    {
      if ((*w).d[2] * (*w).d[1] < 32)
        {
          t = (float *)malloc((*p).d[2] * sizeof(float));
          x_padded = (*aux).pad[0] + (*aux).pad[1] > 0 ? (float *)calloc((*q).d[2] + (*aux).pad[0] + (*aux).pad[1], sizeof(float)) : 0;
        }
      else
        {
          q1 = (float *)malloc(kad_len(q) * sizeof(float));
          w1 = (float *)malloc(kad_len(w) * sizeof(float));
          x_padded = (*aux).pad[0] + (*aux).pad[1] > 0 ? (float *)calloc(((*q).d[2] + (*aux).pad[0] + (*aux).pad[1]) * (*q).d[1], sizeof(float)) : 0;
          algo_switch = 1;
        }
    }
  if (action == 4)
    {
      if ((*q).n_d != 3 || (*w).n_d != 3)
        {
          return  -1;
        }
      if ((*q).d[1] != (*w).d[1])
        {
          return  -1;
        }
      (*p).n_d = 3;
      ((((*p).d[0] = (*q).d[0], (*p).d[1] = (*w).d[0])), (*p).d[2] = ((*q).d[2] - (*aux).kernel_size + (*aux).pad[0] + (*aux).pad[1]) / (*aux).stride + 1);
    }
  else
    {
      if (action == 2)
        {
          conv_rot180((*w).d[0] * (*w).d[1], (*w).d[2], (*w).x);
          memset((*p).x, 0, kad_len(p) * sizeof(float));
          if (!algo_switch)
            {
              do
                {
                  int n;
                  int c1;
                  int c0;
                  for (n = 0; n < (*q).d[0];  ++n)
                    {
                      for (c1 = 0; c1 < (*w).d[0];  ++c1)
                        {
                          for (c0 = 0; c0 < (*w).d[1];  ++c0)
                            {
                              float *_ww = &(*w).x[(c1 * (*w).d[1] + c0) * (*w).d[2]];
                              float *_xx = &(*q).x[(n * (*q).d[1] + c0) * (*q).d[2]];
                              float *_yy = &(*p).x[(n * (*p).d[1] + c1) * (*p).d[2]];
                              if (x_padded)
                                {
                                  memcpy(x_padded + (*aux).pad[0], _xx, (*q).d[2] * sizeof(float));
                                  _xx = x_padded + (*aux).pad[0];
                                }
                              do
                                {
                                  int l;
                                  int j;
                                  if ((*aux).stride > 1)
                                    {
                                      for (l = 0; l < (*w).d[2];  ++l)
                                        {
                                          const float *xl = &_xx[l - (*aux).pad[0]];
                                          for (j = 0; j < (*p).d[2]; ( ++j, xl += (*aux).stride))
                                            {
                                              t[j] = *xl;
                                            }
                                          kad_saxpy((*p).d[2], _ww[l], t, _yy);
                                        }
                                    }
                                  else
                                    {
                                      for (l = 0; l < (*w).d[2];  ++l)
                                        {
                                          kad_saxpy((*p).d[2], _ww[l], &_xx[l - (*aux).pad[0]], _yy);
                                        }
                                    }
                                }
                              while (0);
                            }
                        }
                    }
                }
              while (0);
            }
          else
            {
              conv1d_move_1to2((*q).d, (*q).x, q1);
              conv1d_move_1to2((*w).d, (*w).x, w1);
              do
                {
                  int n;
                  int c1;
                  int j;
                  int j_skip = (*aux).stride * (*q).d[1];
                  int m = (*w).d[2] * (*w).d[1];
                  for (n = 0; n < (*q).d[0];  ++n)
                    {
                      for (c1 = 0; c1 < (*w).d[0];  ++c1)
                        {
                          float *_ww = &w1[c1 * m];
                          float *_xx = &q1[n * (*q).d[1] * (*q).d[2]];
                          float *_yy = &(*p).x[(n * (*p).d[1] + c1) * (*p).d[2]];
                          if (x_padded)
                            {
                              memcpy(x_padded + (*aux).pad[0] * (*q).d[1], _xx, (*q).d[2] * (*q).d[1] * sizeof(float));
                              _xx = x_padded;
                            }
                          for (j = 0; j < (*p).d[2]; ((( ++j, _xx += j_skip)),  ++_yy))
                            {
                              *_yy += kad_sdot(m, _ww, _xx);
                            }
                        }
                    }
                }
              while (0);
            }
          conv_rot180((*w).d[0] * (*w).d[1], (*w).d[2], (*w).x);
        }
      else
        {
          if (action == 3)
            {
              if ((*(*p).child[0]).flag & 1)
                {
                  conv_rot180((*w).d[0] * (*w).d[1], (*w).d[2], (*w).x);
                  if (!algo_switch)
                    {
                      do
                        {
                          int n;
                          int c1;
                          int c0;
                          for (n = 0; n < (*q).d[0];  ++n)
                            {
                              for (c1 = 0; c1 < (*w).d[0];  ++c1)
                                {
                                  for (c0 = 0; c0 < (*w).d[1];  ++c0)
                                    {
                                      float *_ww = &(*w).x[(c1 * (*w).d[1] + c0) * (*w).d[2]];
                                      float *_xx = &(*q).g[(n * (*q).d[1] + c0) * (*q).d[2]];
                                      float *_yy = &(*p).g[(n * (*p).d[1] + c1) * (*p).d[2]];
                                      if (x_padded)
                                        {
                                          memcpy(x_padded + (*aux).pad[0], _xx, (*q).d[2] * sizeof(float));
                                          _xx = x_padded + (*aux).pad[0];
                                        }
                                      do
                                        {
                                          int l;
                                          int j;
                                          if ((*aux).stride > 1)
                                            {
                                              for (l = 0; l < (*w).d[2];  ++l)
                                                {
                                                  float *xl = &_xx[l - (*aux).pad[0]];
                                                  memset(t, 0, (*p).d[2] * sizeof(float));
                                                  kad_saxpy((*p).d[2], _ww[l], _yy, t);
                                                  for (j = 0; j < (*p).d[2]; ( ++j, xl += (*aux).stride))
                                                    {
                                                      *xl += t[j];
                                                    }
                                                }
                                            }
                                          else
                                            {
                                              for (l = 0; l < (*w).d[2];  ++l)
                                                {
                                                  kad_saxpy((*p).d[2], _ww[l], _yy, &_xx[l - (*aux).pad[0]]);
                                                }
                                            }
                                        }
                                      while (0);
                                    }
                                }
                            }
                        }
                      while (0);
                    }
                  else
                    {
                      memset(q1, 0, kad_len(q) * sizeof(float));
                      conv1d_move_1to2((*w).d, (*w).x, w1);
                      do
                        {
                          int n;
                          int c1;
                          int j;
                          int j_skip = (*aux).stride * (*q).d[1];
                          int m = (*w).d[2] * (*w).d[1];
                          for (n = 0; n < (*q).d[0];  ++n)
                            {
                              for (c1 = 0; c1 < (*w).d[0];  ++c1)
                                {
                                  float *_ww = &w1[c1 * m];
                                  float *_xx = &q1[n * (*q).d[1] * (*q).d[2]];
                                  float *_yy = &(*p).g[(n * (*p).d[1] + c1) * (*p).d[2]];
                                  if (x_padded)
                                    {
                                      memcpy(x_padded + (*aux).pad[0] * (*q).d[1], _xx, (*q).d[2] * (*q).d[1] * sizeof(float));
                                      _xx = x_padded;
                                    }
                                  for (j = 0; j < (*p).d[2]; ((( ++j, _xx += j_skip)),  ++_yy))
                                    {
                                      kad_saxpy(m, *_yy, _ww, _xx);
                                    }
                                }
                            }
                        }
                      while (0);
                      conv1d_add_2to1((*q).d, q1, (*q).g);
                    }
                  conv_rot180((*w).d[0] * (*w).d[1], (*w).d[2], (*w).x);
                }
              if ((*(*p).child[1]).flag & 1)
                {
                  conv_rot180((*w).d[0] * (*w).d[1], (*w).d[2], (*w).g);
                  if (!algo_switch)
                    {
                      do
                        {
                          int n;
                          int c1;
                          int c0;
                          for (n = 0; n < (*q).d[0];  ++n)
                            {
                              for (c1 = 0; c1 < (*w).d[0];  ++c1)
                                {
                                  for (c0 = 0; c0 < (*w).d[1];  ++c0)
                                    {
                                      float *_ww = &(*w).g[(c1 * (*w).d[1] + c0) * (*w).d[2]];
                                      float *_xx = &(*q).x[(n * (*q).d[1] + c0) * (*q).d[2]];
                                      float *_yy = &(*p).g[(n * (*p).d[1] + c1) * (*p).d[2]];
                                      if (x_padded)
                                        {
                                          memcpy(x_padded + (*aux).pad[0], _xx, (*q).d[2] * sizeof(float));
                                          _xx = x_padded + (*aux).pad[0];
                                        }
                                      do
                                        {
                                          int l;
                                          int j;
                                          if ((*aux).stride > 1)
                                            {
                                              for (l = 0; l < (*w).d[2];  ++l)
                                                {
                                                  const float *xl = &_xx[l - (*aux).pad[0]];
                                                  for (j = 0; j < (*p).d[2]; ( ++j, xl += (*aux).stride))
                                                    {
                                                      t[j] = *xl;
                                                    }
                                                  _ww[l] += kad_sdot((*p).d[2], _yy, t);
                                                }
                                            }
                                          else
                                            {
                                              for (l = 0; l < (*w).d[2];  ++l)
                                                {
                                                  _ww[l] += kad_sdot((*p).d[2], _yy, &_xx[l - (*aux).pad[0]]);
                                                }
                                            }
                                        }
                                      while (0);
                                    }
                                }
                            }
                        }
                      while (0);
                    }
                  else
                    {
                      conv1d_move_1to2((*q).d, (*q).x, q1);
                      memset(w1, 0, kad_len(w) * sizeof(float));
                      do
                        {
                          int n;
                          int c1;
                          int j;
                          int j_skip = (*aux).stride * (*q).d[1];
                          int m = (*w).d[2] * (*w).d[1];
                          for (n = 0; n < (*q).d[0];  ++n)
                            {
                              for (c1 = 0; c1 < (*w).d[0];  ++c1)
                                {
                                  float *_ww = &w1[c1 * m];
                                  float *_xx = &q1[n * (*q).d[1] * (*q).d[2]];
                                  float *_yy = &(*p).g[(n * (*p).d[1] + c1) * (*p).d[2]];
                                  if (x_padded)
                                    {
                                      memcpy(x_padded + (*aux).pad[0] * (*q).d[1], _xx, (*q).d[2] * (*q).d[1] * sizeof(float));
                                      _xx = x_padded;
                                    }
                                  for (j = 0; j < (*p).d[2]; ((( ++j, _xx += j_skip)),  ++_yy))
                                    {
                                      kad_saxpy(m, *_yy, _xx, _ww);
                                    }
                                }
                            }
                        }
                      while (0);
                      conv1d_add_2to1((*w).d, w1, (*w).g);
                    }
                  conv_rot180((*w).d[0] * (*w).d[1], (*w).d[2], (*w).g);
                }
            }
        }
    }
  free(t);
  free(q1);
  free(w1);
  free(x_padded);
  return 0;
}
int kad_op_max1d(kad_node_t *p, int action)
{
  conv_conf_t *aux = (conv_conf_t *)(*p).ptr;
  kad_node_t *q = (*p).child[0];
  if (action == 4)
    {
      if ((*q).n_d != 3)
        {
          return  -1;
        }
      (*p).n_d = 3;
      ((((*p).d[0] = (*q).d[0], (*p).d[1] = (*q).d[1])), (*p).d[2] = ((*q).d[2] - (*aux).kernel_size + (*aux).pad[0] + (*aux).pad[1]) / (*aux).stride + 1);
    }
  else
    {
      if (action == 1)
        {
          (*p).gtmp = realloc((*p).gtmp, kad_len(p) * sizeof(int));
        }
      else
        {
          if (action == 2)
            {
              int len;
              int i;
              int t;
              int rest = 1;
              int *f = (int *)(*p).gtmp;
              len = kad_len(p);
              for (i = 0; i < len;  ++i)
                {
                  (*p).x[i] =  -3.402823466385288598117042e+38f;
                }
              for (i = 0; i < (*p).n_d - 1;  ++i)
                {
                  rest *= (*p).d[i];
                }
              for (t = 0; t < rest;  ++t)
                {
                  int l;
                  int j;
                  int v;
                  int p_width = (*p).d[(*p).n_d - 1];
                  int u = t * p_width;
                  int v0 = t * (*q).d[(*p).n_d - 1];
                  int v_end = v0 + (*q).d[(*p).n_d - 1];
                  for (l = 0; l < (*aux).kernel_size;  ++l)
                    {
                      for ((j = 0, v = v0 + (l > (*aux).pad[0] ? l - (*aux).pad[0] : 0)); j < p_width && v < v_end; ( ++j, v += (*aux).stride))
                        {
                          if ((*p).x[u + j] < (*q).x[v])
                            {
                              ((*p).x[u + j] = (*q).x[v], f[u + j] = v);
                            }
                        }
                    }
                }
            }
          else
            {
              if (action == 3)
                {
                  int len;
                  int i;
                  int *f = (int *)(*p).gtmp;
                  len = kad_len(p);
                  for (i = 0; i < len;  ++i)
                    {
                      (*q).g[f[i]] += (*p).g[i];
                    }
                }
            }
        }
    }
  return 0;
}
int kad_op_avg1d(kad_node_t *p, int action)
{
  conv_conf_t *aux = (conv_conf_t *)(*p).ptr;
  kad_node_t *q = (*p).child[0];
  if (action == 4)
    {
      if ((*q).n_d != 3)
        {
          return  -1;
        }
      (*p).n_d = 3;
      ((((*p).d[0] = (*q).d[0], (*p).d[1] = (*q).d[1])), (*p).d[2] = ((*q).d[2] - (*aux).kernel_size + (*aux).pad[0] + (*aux).pad[1]) / (*aux).stride + 1);
    }
  else
    {
      if (action == 1)
        {
          (*p).gtmp = realloc((*p).gtmp, kad_len(p) * sizeof(int));
        }
      else
        {
          if (action == 2)
            {
              int len;
              int i;
              int t;
              int rest = 1;
              int *f = (int *)(*p).gtmp;
              len = kad_len(p);
              for (i = 0; i < len;  ++i)
                {
                  ((*p).x[i] = 0.000000000000000000000000e+00f, f[i] = 0);
                }
              for (i = 0; i < (*p).n_d - 1;  ++i)
                {
                  rest *= (*p).d[i];
                }
              for (t = 0; t < rest;  ++t)
                {
                  int l;
                  int j;
                  int v;
                  int p_width = (*p).d[(*p).n_d - 1];
                  int u = t * p_width;
                  int v0 = t * (*q).d[(*p).n_d - 1];
                  int v_end = v0 + (*q).d[(*p).n_d - 1];
                  for (l = 0; l < (*aux).kernel_size;  ++l)
                    {
                      for ((j = 0, v = v0 + (l > (*aux).pad[0] ? l - (*aux).pad[0] : 0)); j < p_width && v < v_end; ( ++j, v += (*aux).stride))
                        {
                          ((*p).x[u + j] += (*q).x[v],  ++f[u + j]);
                        }
                    }
                }
              for (i = 0; i < len;  ++i)
                {
                  (*p).x[i] /= f[i];
                }
            }
          else
            {
              if (action == 3)
                {
                  int i;
                  int t;
                  int rest = 1;
                  int *f = (int *)(*p).gtmp;
                  for (i = 0; i < (*p).n_d - 1;  ++i)
                    {
                      rest *= (*p).d[i];
                    }
                  for (t = 0; t < rest;  ++t)
                    {
                      int l;
                      int j;
                      int v;
                      int p_width = (*p).d[(*p).n_d - 1];
                      int u = t * p_width;
                      int v0 = t * (*q).d[(*p).n_d - 1];
                      int v_end = v0 + (*q).d[(*p).n_d - 1];
                      for (l = 0; l < (*aux).kernel_size;  ++l)
                        {
                          for ((j = 0, v = v0 + (l > (*aux).pad[0] ? l - (*aux).pad[0] : 0)); j < p_width && v < v_end; ( ++j, v += (*aux).stride))
                            {
                              (*q).g[v] += (*p).g[u + j] / f[u + j];
                            }
                        }
                    }
                }
            }
        }
    }
  return 0;
}
char *kad_op_name[64L] = {[0] = 0, [1] = "add", [2] = "mul", [3] = "cmul", [4] = "ce_bin_neg", [5] = "square", [6] = "sigm", [7] = "tanh", [8] = "relu", [9] = "matmul", [10] = "avg", [11] = "1minus", [12] = "select", [13] = "ce_multi", [14] = "softmax", [15] = "dropout", [16] = "conv2d", [17] = "max2d", [18] = "conv1d", [19] = "max1d", [20] = "slice", [21] = "max", [22] = "ce_bin", [23] = "sub", [24] = "sample_normal", [25] = "reduce_sum", [26] = "reduce_mean", [27] = "log", [28] = "avg1d", [29] = "mse", [30] = "reshape", [31] = "concat", [32] = "stdnorm", [33] = "exp", [34] = "sin", [35] = "stack", [36] = "reverse"};
void kad_trap_fe(void)
{
  _MM_SET_EXCEPTION_MASK(_MM_GET_EXCEPTION_MASK() & ~(128 | 512));
}
extern int fprintf(FILE *__restrict __stream, const char *__restrict __format, ...);
extern int fputs(const char *__restrict __s, FILE *__restrict __stream);
extern int fputc(int __c, FILE *__stream);
void kad_print_graph(FILE *fp, int n, kad_node_t **v)
{
  int i;
  int j;
  for (i = 0; i < n;  ++i)
    {
      (*v[i]).tmp = i;
    }
  for (i = 0; i < n;  ++i)
    {
      kad_node_t *p = v[i];
      fprintf(fp, "%d\t%x:%x\t%d\t", i, (*p).flag, (*p).ext_flag, (*p).ext_label);
      if ((*p).pre)
        {
          fprintf(fp, "%d\t", (*(*p).pre).tmp);
        }
      else
        {
          fprintf(fp, ".\t");
        }
      fputs("[", fp);
      for (j = 0; j < (*p).n_d;  ++j)
        {
          if (j)
            {
              fputc(',', fp);
            }
          fprintf(fp, "%d", (*p).d[j]);
        }
      fprintf(fp, "]\t");
      if ((*p).n_child)
        {
          fprintf(fp, "%s(", kad_op_name[(*p).op]);
          for (j = 0; j < (*p).n_child;  ++j)
            {
              if (j)
                {
                  fputc(',', fp);
                }
              fprintf(fp, "$%d", (*(*p).child[j]).tmp);
            }
          fprintf(fp, ")");
        }
      else
        {
          fprintf(fp, "%s", ((*p).n_child == 0 && !((*p).flag & 1)) && !((*p).flag & 2) ? "feed" : (*p).n_child == 0 && (*p).flag & 1 ? "var" : (*p).n_child == 0 && (*p).flag & 2 ? "const" : "N/A");
        }
      fputc('\n', fp);
    }
  for (i = 0; i < n;  ++i)
    {
      (*v[i]).tmp = 0;
    }
}
static void kad_add_delta(int n, kad_node_t **a, float c, float *delta)
{
  int i;
  int k;
  for (i = k = 0; i < n;  ++i)
    {
      if ((*a[i]).n_child == 0 && (*a[i]).flag & 1)
        {
          kad_saxpy(kad_len(a[i]), c, &delta[k], (*a[i]).x);
          k += kad_len(a[i]);
        }
    }
}
extern struct _IO_FILE *stderr;
extern double fabs(double __x) __attribute__((__nothrow__)) __attribute__((__leaf__)) __attribute__((__const__));
extern float fabsf(float __x) __attribute__((__nothrow__)) __attribute__((__leaf__)) __attribute__((__const__));
void kad_check_grad(int n, kad_node_t **a, int from, int layers, int ulen, int *start, int *end, int parallel)
{
  int n_var;
  float *g0;
  float f0;
  int i;
  int k;
  float *delta;
  float f_plus;
  float f_minus;
  float s0;
  float s1;
  float rel_err;
  float p_m_err;
  const float eps = 9.999999747378751635551453e-06f;
  const float rel = 1.000000011686097423080355e-07f / eps;
  n_var = kad_size_var(n, a);
  g0 = (float *)calloc(n_var, sizeof(float));
  f0 = *kad_eval_at(n, a, from, layers, ulen, start, end, parallel);
  kad_grad(n, a, from, layers, ulen, start, end, parallel);
  for (i = k = 0; i < n;  ++i)
    {
      if ((*a[i]).n_child == 0 && (*a[i]).flag & 1)
        {
          memcpy(&g0[k], (*a[i]).g, kad_len(a[i]) * sizeof(float));
          k += kad_len(a[i]);
        }
    }
  delta = (float *)calloc(n_var, sizeof(float));
  for (k = 0; k < n_var;  ++k)
    {
      delta[k] = (float)kad_drand(0) * eps;
    }
  kad_add_delta(n, a, 1.000000000000000000000000e+00f, delta);
  f_plus = *kad_eval_at(n, a, from, layers, ulen, start, end, parallel);
  kad_add_delta(n, a,  -2.000000000000000000000000e+00f, delta);
  f_minus = *kad_eval_at(n, a, from, layers, ulen, start, end, parallel);
  kad_add_delta(n, a, 1.000000000000000000000000e+00f, delta);
  s0 = kad_sdot(n_var, g0, delta);
  s1 = 5.000000000000000000000000e-01f * (f_plus - f_minus);
  fprintf(stderr, "Gradient check -- %g <=> %g @ %g -- ", s0 / eps, s1 / eps, f0);
  if (fabs(s1) >= rel * eps)
    {
      rel_err = fabsf(fabsf(s0) - fabsf(s1)) / (fabsf(s0) + fabsf(s1));
      p_m_err = fabsf(f_plus + f_minus - 2.000000000000000000000000e+00f * f0) / fabsf(f_plus - f_minus);
      fprintf(stderr, "rel_err:%g p_m_err:%g -- ", rel_err, p_m_err);
      if (rel_err >= rel && rel_err > p_m_err)
        {
          fprintf(stderr, "failed\n");
        }
      else
        {
          fprintf(stderr, "passed\n");
        }
    }
  else
    {
      fprintf(stderr, "skipped\n");
    }
  free(delta);
  free(g0);
}
static void smp_ol_kad_eval_marked_0_unpacked(kad_node_t **a, int s, int e, int *end, int node, int layers)
{
  {
    ompss_forward_prop(s, e, a, 2);
  }
}
static void smp_ol_kad_eval_marked_0(struct nanos_args_0_t *const args)
{
  {
    smp_ol_kad_eval_marked_0_unpacked((*args).a, (*args).s, (*args).e, (*args).end, (*args).node, (*args).layers);
  }
}
static void smp_ol_kad_eval_marked_1_unpacked(kad_node_t **a, int s, int e)
{
  {
    ompss_forward_prop(s, e, a, 2);
  }
}
static void smp_ol_kad_eval_marked_1(struct nanos_args_1_t *const args)
{
  {
    smp_ol_kad_eval_marked_1_unpacked((*args).a, (*args).s, (*args).e);
  }
}
static void smp_ol_kad_eval_marked_2_unpacked(kad_node_t **a, int s, int e, int *end, int node, int layers)
{
  {
    ompss_forward_prop(s, e, a, 2);
  }
}
static void smp_ol_kad_eval_marked_2(struct nanos_args_2_t *const args)
{
  {
    smp_ol_kad_eval_marked_2_unpacked((*args).a, (*args).s, (*args).e, (*args).end, (*args).node, (*args).layers);
  }
}
static void smp_ol_kad_eval_marked_3_unpacked(kad_node_t **a, int s, int e)
{
  {
    ompss_forward_prop(s, e, a, 2);
  }
}
static void smp_ol_kad_eval_marked_3(struct nanos_args_3_t *const args)
{
  {
    smp_ol_kad_eval_marked_3_unpacked((*args).a, (*args).s, (*args).e);
  }
}
static void smp_ol_kad_grad_4_unpacked(kad_node_t **a, int from, int (**const kad_op_list)(kad_node_t *, int))
{
  {
    {
      (*a[from]).g[0] = 1.000000000000000000000000e+00f;
      kad_op_list[(*a[from]).op](a[from], 3);
    }
  }
}
static void smp_ol_kad_grad_4(struct nanos_args_4_t *const args)
{
  {
    smp_ol_kad_grad_4_unpacked((*args).a, (*args).from, *(*args).kad_op_list);
  }
}
static void smp_ol_kad_grad_5_unpacked(kad_node_t **a, int e, int s, int i_d)
{
  {
    ompss_backward_prop(e, s, a, 3);
  }
}
static void smp_ol_kad_grad_5(struct nanos_args_5_t *const args)
{
  {
    smp_ol_kad_grad_5_unpacked((*args).a, (*args).e, (*args).s, (*args).i_d);
  }
}
static void smp_ol_kad_grad_6_unpacked(kad_node_t **a, int e, int s, int from, int i_d)
{
  {
    ompss_backward_prop(e, s, a, 3);
  }
}
static void smp_ol_kad_grad_6(struct nanos_args_6_t *const args)
{
  {
    smp_ol_kad_grad_6_unpacked((*args).a, (*args).e, (*args).s, (*args).from, (*args).i_d);
  }
}
static void smp_ol_kad_grad_7_unpacked(kad_node_t **a, int e, int s, int i_d)
{
  {
    ompss_backward_prop(e, s, a, 3);
  }
}
static void smp_ol_kad_grad_7(struct nanos_args_7_t *const args)
{
  {
    smp_ol_kad_grad_7_unpacked((*args).a, (*args).e, (*args).s, (*args).i_d);
  }
}
static void smp_ol_kad_grad_8_unpacked(kad_node_t **a, int e, int s)
{
  {
    ompss_backward_prop(e, s, a, 3);
  }
}
static void smp_ol_kad_grad_8(struct nanos_args_8_t *const args)
{
  {
    smp_ol_kad_grad_8_unpacked((*args).a, (*args).e, (*args).s);
  }
}
static void smp_ol_kad_grad_9_unpacked(kad_node_t **a, int e, int s, int from)
{
  {
    ompss_backward_prop(e, s, a, 3);
  }
}
static void smp_ol_kad_grad_9(struct nanos_args_9_t *const args)
{
  {
    smp_ol_kad_grad_9_unpacked((*args).a, (*args).e, (*args).s, (*args).from);
  }
}
static void smp_ol_kad_grad_10_unpacked(kad_node_t **a, int e, int s)
{
  {
    ompss_backward_prop(e, s, a, 3);
  }
}
static void smp_ol_kad_grad_10(struct nanos_args_10_t *const args)
{
  {
    smp_ol_kad_grad_10_unpacked((*args).a, (*args).e, (*args).s);
  }
}
static void smp_ol_kad_grad_11_unpacked(kad_node_t **a, int e, int s, int i_d)
{
  {
    ompss_backward_prop(e, s, a, 3);
  }
}
static void smp_ol_kad_grad_11(struct nanos_args_11_t *const args)
{
  {
    smp_ol_kad_grad_11_unpacked((*args).a, (*args).e, (*args).s, (*args).i_d);
  }
}
static void smp_ol_kad_grad_12_unpacked(kad_node_t **a, int e, int s, int from, int i_d)
{
  {
    ompss_backward_prop(e, s, a, 3);
  }
}
static void smp_ol_kad_grad_12(struct nanos_args_12_t *const args)
{
  {
    smp_ol_kad_grad_12_unpacked((*args).a, (*args).e, (*args).s, (*args).from, (*args).i_d);
  }
}
static void smp_ol_kad_grad_13_unpacked(kad_node_t **a, int e, int s, int i_d)
{
  {
    ompss_backward_prop(e, s, a, 3);
  }
}
static void smp_ol_kad_grad_13(struct nanos_args_13_t *const args)
{
  {
    smp_ol_kad_grad_13_unpacked((*args).a, (*args).e, (*args).s, (*args).i_d);
  }
}
