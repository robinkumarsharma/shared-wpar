#!/bin/bash
#SBATCH --job-name="next-char"
#SBATCH --time=00:10:00
#SBATCH -D .
#SBATCH --output=LSTM_%J.out
#SBATCH --error=LSTM_%J.err
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=1
#SBATCH --exclusive
#SBATCH --qos=debug

moudule purge
module load gcc/4.8.5
module load impi/2019.5
module load mkl/2019.4
module load ompss 

# W-par run code
export NX_ARGS="--summary --smp-workers=48"
./d_textgen_parallel -P -t 6 -B 64 -l 12 -u 100 -r 0.001 -n 128 -m 1 -M "lstm" 10000_wiki
# Seq (Sequential) run code
export NX_ARGS="--summary --smp-workers=48"
./d_textgen_parallel -t 6 -B 64 -l 12 -u 100 -r 0.001 -n 128 -m 1 -M "lstm" 10000_wiki
