/*
   RNN Next Character Prediction
   -  RNN / LSTM/ GRU
   -  Layer and NO Layer Normalization
   -  diagonal parallel (-P)
   -  ompss mini mini batch division

   Author - robin sharma (robinkeralaam@gmail.com)
 */

#include <math.h>
#include <stdio.h>
#include <float.h>
#include <assert.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include "sys/time.h"
#include "kann_parallel.h"


long long values[2];

typedef struct {
    int len, n_char, n_para, *para_len;
    uint8_t *data, **para;
    int c2i[256];
} tg_data_t;

#define kv_roundup32(x) (--(x), (x)|=(x)>>1, (x)|=(x)>>2, (x)|=(x)>>4, (x)|=(x)>>8,(x)|=(x)>>16, ++(x))

uint8_t *tg_read_file(const char *fn, int *_len)
{
    const int buf_len = 0x10000;
    unsigned long int len = 0, max = 0, l;
    FILE *fp;
    uint8_t *buf, *s = 0;

    fp = fn && strcmp(fn, "-")? fopen(fn, "rb") : stdin;
    buf = (uint8_t*)malloc(buf_len);
    while ((l = fread(buf, 1, buf_len, fp)) > 0) {
        if (len + l > max) {
            max = len + buf_len;
            kv_roundup32(max);
            s = (uint8_t*)realloc(s, max);
        }
        memcpy(&s[len], buf, l);
        len += l;
    }
    s = (uint8_t*)realloc(s, len);
    *_len = len;
    fclose(fp);
    free(buf);
    return s;
}

tg_data_t *tg_init(const char *fn)
{
    int i, j, st, k;
    tg_data_t *tg;
    tg = (tg_data_t*)calloc(1, sizeof(tg_data_t));
    tg->data = tg_read_file(fn, &tg->len);
    for (i = 0; i < tg->len; ++i)
        tg->c2i[tg->data[i]] = 1;
    for (i = j = 0; i < 256; ++i)
        if (tg->c2i[i] == 0) tg->c2i[i] = -1;
        else tg->c2i[i] = j++;
    tg->n_char = j;
    for (i = 1, st = 0, tg->n_para = 0; i < tg->len; ++i)
        if (tg->data[i] == '\n' && tg->data[i-1] == '\n' && i - st > 1)
            ++tg->n_para, st = i + 1;
    if (i - st > 1) ++tg->n_para;
    tg->para = (uint8_t**)calloc(tg->n_para, sizeof(uint8_t*));
    tg->para_len = (int*)calloc(tg->n_para, sizeof(int));
    for (i = 1, st = k = 0; i < tg->len; ++i)
        if (tg->data[i] == '\n' && tg->data[i-1] == '\n' && i - st > 1)
            tg->para[k] = &tg->data[st], tg->para_len[k++] = i - st, st = i + 1;
    if (i - st > 1) tg->para[k] = &tg->data[st], tg->para_len[k++] = i - st;
    for (i = 0; i < tg->len; ++i)
        tg->data[i] = tg->c2i[tg->data[i]];
    return tg;
}

void tg_save(const char *fn, kann_t *ann, const int c2i[256])
{
    FILE *fp;
    fp = fn && strcmp(fn, "-")? fopen(fn, "wb") : stdout;
    kann_save_fp(fp, ann);
    fwrite(c2i, sizeof(int), 256, fp);
    fclose(fp);
}

kann_t *tg_load(const char *fn, int c2i[256])
{
    FILE *fp;
    kann_t *ann;
    fp = fn && strcmp(fn, "-")? fopen(fn, "rb") : stdin;
    ann = kann_load_fp(fp);
    fread(c2i, sizeof(int), 256, fp);
    fclose(fp);
    return ann;
}

void tg_gen(FILE *fp, kann_t *ann, float temp, int len, const int c2i[256], const char *seed)
{
    int i, c, n_char, i2c[256], i_temp;
    float x[256];
    memset(i2c, 0, 256 * sizeof(int));
    for (i = 0; i < 256; ++i)
        if (c2i[i] >= 0) i2c[c2i[i]] = i;
    n_char = kann_dim_in(ann);
    i_temp = kann_find(ann, 0, -1);
    if (i_temp >= 0) ann->v[i_temp]->x[0] = 1.0f / temp;
    kann_rnn_start(ann);
    for (c = 0; c < ann->n; ++c) {
        kad_node_t *p = ann->v[c];
        if (p->pre) {
            int l = kad_len(p);
            for (i = 0; i < l; ++i)
                p->x[i] = 2.0 * kann_drand() - 1.0;
        }
    }
    if (seed) {
        const char *p;
        for (p = seed; *p; ++p) {
            const float *y;
            float max = -1.0f;
            int max_c = -1;
            c = c2i[(int)*p];
            assert(c >= 0);
            memset(x, 0, n_char * sizeof(float));
            x[c] = 1.0f;
            y = kann_apply1(ann, x);
            for (c = 0; c < n_char; ++c)
                if (max < y[c]) max = y[c], max_c = c;
            c = max_c;
        }
        fprintf(fp, "%s%c", seed, i2c[c]);
    } else c = c2i[(int)' '];
    for (i = 0; i < len; ++i) {
        float s, r;
        const float *y;
        memset(x, 0, n_char * sizeof(float));
        x[c] = 1.0f;
        y = kann_apply1(ann, x);
        r = kann_drand();
        for (c = 0, s = 0.0f; c < n_char; ++c)
            if (s + y[c] >= r) break;
            else s += y[c];
        fputc(i2c[c], fp);
    }
    fputc('\n', fp);
    kann_rnn_end(ann);
    if (i_temp >= 0) ann->v[i_temp]->x[0] = 1.0f;
}

float tg_perplexity(kann_t *ann, const tg_data_t *tg)
{
    const float tiny = 1e-6;
    float x[256], p;
    double loss = 0.0;
    int i;
    kann_rnn_start(ann);
    for (i = 0; i < tg->len - 1; ++i) {
        const float *y;
        memset(x, 0, 256 * sizeof(float));
        x[tg->data[i]] = 1.0f;
        y = kann_apply1(ann, x);
        p = y[tg->data[i+1]];
        loss += logf(p > tiny? p : tiny);
    }
    kann_rnn_end(ann);
    return (float)exp(-loss / (tg->len - 1));
}

int tg_urnn_start(kann_t *ann, int batch_size)
{
    int i, j, n, cnt = 0;
    for (i = 0; i < ann->n; ++i) {
        kad_node_t *p = ann->v[i];
        if (p->pre && p->n_d >= 2 && p->pre->n_d == p->n_d && p->pre->n_child == 0 && kad_len(p)/p->d[0] == kad_len(p->pre)/p->pre->d[0])
            p->pre->flag = 0;
    }
    kann_set_batch_size(ann, batch_size);
    for (i = 0; i < ann->n; ++i) {
        kad_node_t *p = ann->v[i];
        if (p->pre && p->n_d >= 2 && p->pre->n_d == p->n_d && p->pre->n_child == 0 && kad_len(p) == kad_len(p->pre)) {
            kad_node_t *q = p->pre;
            n = kad_len(p) / p->d[0];
            memset(p->x, 0, p->d[0] * n * sizeof(float));
            if (q->x)
                for (j = 0; j < p->d[0]; ++j)
                    memcpy(&p->x[j * n], q->x, n * sizeof(float));
            q->x = p->x;
            ++cnt;
        }
    }
    return cnt;
}

//Fill the Start and end index of each layer node for GRU
void GRU_OmpSs_start_end(int *start, int *end, int unstate, int layers, int layer_norm){
    int l,u; 
    if(layer_norm){  // Layer normalization
        for( u = 0; u < unstate;++u){  
            for(l = 0; l < layers; ++l){
                if(u == 0){
                    if(l == 0){
                        start[u*layers + l] = 0; 
                        end[u*layers + l] = 63;
                    }else if(l == (layers-1)){
                        start[u*layers + l] = end[u*layers + l - 1] + 1; 
                        end[u*layers + l] = start[u*layers + l] + 72 - 1; 
                    }else if(l < (layers-1)){
                        start[u*layers + l] = end[u*layers + l - 1] + 1; 
                        end[u*layers + l] = start[u*layers + l] + 63 - 1; 
                    }    
                }else if(u>0){
                    if(l == 0){
                        start[u*layers + l] = end[u*layers + l - 1] + 1; 
                        end[u*layers + l] = start[u*layers + l] + 41 - 1; 
                    }else if(l == (layers-1)){
                        start[u*layers + l] = end[u*layers + l - 1] + 1; 
                        end[u*layers + l] = start[u*layers + l] + 46 - 1; 
                    }else if(l < (layers-1)){
                        start[u*layers + l] = end[u*layers + l - 1] + 1; 
                        end[u*layers + l] = start[u*layers + l] + 40 - 1; 
                    }    
                }    
            }    
        }    
    }else if (!layer_norm){   // No Layer normalization
        for( u = 0; u < unstate;++u){
            for(l = 0; l < layers; ++l){
                if(u == 0){
                    if(l == 0){
                        start[u*layers + l] = 0; 
                        end[u*layers + l] = 33;
                    }else if(l == (layers-1)){
                        start[u*layers + l] = end[u*layers + l - 1] + 1; 
                        end[u*layers + l] = start[u*layers + l] + 32 + 9; 
                    }else if(l < (layers-1)){
                        start[u*layers + l] = end[u*layers + l - 1] + 1; 
                        end[u*layers + l] = start[u*layers + l] + 32; 
                    }    
                }else if(u>0){
                    if(l == 0){
                        start[u*layers + l] = end[u*layers + l - 1] + 1; 
                        end[u*layers + l] = start[u*layers + l] + 22; 
                    }else if(l == (layers-1)){
                        start[u*layers + l] = end[u*layers + l - 1] + 1; 
                        end[u*layers + l] = start[u*layers + l] + 21 + 6; 
                    }else if(l < (layers-1)){
                        start[u*layers + l] = end[u*layers + l - 1] + 1; 
                        end[u*layers + l] = start[u*layers + l] + 21; 
                    }    
                }    
            }    
        }    
    }
} 

//Fill the Start and end index of each layer node for LSTM
void LSTM_OmpSs_start_end(int *start, int *end, int unstate, int layers, int layer_norm){
    int l,u; 
    for( u = 0; u < unstate;++u){  // Layer normalization
        for(l = 0; l < layers; ++l){
            if(layer_norm){
                if(u == 0){
                    if(l == 0){
                        start[u*layers + l] = 0; 
                        end[u*layers + l] = 87;
                    }else if(l == (layers-1)){
                        start[u*layers + l] = end[u*layers + l - 1] + 1; 
                        end[u*layers + l] = start[u*layers + l] + 95; 
                    }else if(l < (layers-1)){
                        start[u*layers + l] = end[u*layers + l - 1] + 1; 
                        end[u*layers + l] = start[u*layers + l] + 86; 
                    }    
                }else if(u>0){
                    if(l == 0){
                        start[u*layers + l] = end[u*layers + l - 1] + 1; 
                        end[u*layers + l] = start[u*layers + l] + 54; 
                    }else if(l == (layers-1)){
                        start[u*layers + l] = end[u*layers + l - 1] + 1; 
                        end[u*layers + l] = start[u*layers + l] + 59; 
                    }else if(l < (layers-1)){
                        start[u*layers + l] = end[u*layers + l - 1] + 1; 
                        end[u*layers + l] = start[u*layers + l] + 53; 
                    }    
                }    
            }else{      // NO Layer normalization
                if(u == 0){
                    if(l == 0){
                        start[u*layers + l] = 0; 
                        end[u*layers + l] = 42;
                    }else if(l == (layers-1)){
                        start[u*layers + l] = end[u*layers + l - 1] + 1; 
                        end[u*layers + l] = start[u*layers + l] + 50; 
                    }else if(l < (layers-1)){
                        start[u*layers + l] = end[u*layers + l - 1] + 1; 
                        end[u*layers + l] = start[u*layers + l] + 41; 
                    }    
                }else if(u>0){
                    if(l == 0){
                        start[u*layers + l] = end[u*layers + l - 1] + 1; 
                        end[u*layers + l] = start[u*layers + l] + 27; 
                    }else if(l == (layers-1)){
                        start[u*layers + l] = end[u*layers + l - 1] + 1; 
                        end[u*layers + l] = start[u*layers + l] + 32; 
                    }else if(l < (layers-1)){
                        start[u*layers + l] = end[u*layers + l - 1] + 1; 
                        end[u*layers + l] = start[u*layers + l] + 26; 
                    }    
                }    
            }
        }    
    }
}

//Fill the Start and end index of each layer node for RNN
void RNN_OmpSs_start_end(int *start, int *end, int unstate, int layers, int layer_norm){
    int l,u; 
    if(layer_norm){    // Layer normalization
        for( u = 0; u < unstate;++u){  
            for(l = 0; l < layers; ++l){
                if(u == 0){
                    if(l == 0){
                        start[u*layers + l] = 0; 
                        end[u*layers + l] = 22;
                    }else if(l == (layers-1)){
                        start[u*layers + l] = end[u*layers + l - 1] + 1; 
                        end[u*layers + l] = start[u*layers + l] + 30; 
                    }else if(l < (layers-1)){
                        start[u*layers + l] = end[u*layers + l - 1] + 1; 
                        end[u*layers + l] = start[u*layers + l] + 21; 
                    }    
                }else if(u>0){
                    if(l == 0){
                        start[u*layers + l] = end[u*layers + l - 1] + 1; 
                        end[u*layers + l] = start[u*layers + l] + 13; 
                    }else if(l == (layers-1)){
                        start[u*layers + l] = end[u*layers + l - 1] + 1; 
                        end[u*layers + l] = start[u*layers + l] + 18; 
                    }else if(l < (layers-1)){
                        start[u*layers + l] = end[u*layers + l - 1] + 1; 
                        end[u*layers + l] = start[u*layers + l] + 12; 
                    }    
                }    
            }    
        }    
    }else if (!layer_norm){    // No Layer normalization
        for( u = 0; u < unstate;++u){  
            for(l = 0; l < layers; ++l){
                if(u == 0){
                    if(l == 0){
                        start[u*layers + l] = 0; 
                        end[u*layers + l] = 12;
                    }else if(l == (layers-1)){
                        start[u*layers + l] = end[u*layers + l - 1] + 1; 
                        end[u*layers + l] = start[u*layers + l] + 11 + 9; 
                    }else if(l < (layers-1)){
                        start[u*layers + l] = end[u*layers + l - 1] + 1; 
                        end[u*layers + l] = start[u*layers + l] + 11; 
                    }    
                }else if(u>0){
                    if(l == 0){
                        start[u*layers + l] = end[u*layers + l - 1] + 1; 
                        end[u*layers + l] = start[u*layers + l] + 7; 
                    }else if(l == (layers-1)){
                        start[u*layers + l] = end[u*layers + l - 1] + 1; 
                        end[u*layers + l] = start[u*layers + l] + 6 + 6; 
                    }else if(l < (layers-1)){
                        start[u*layers + l] = end[u*layers + l - 1] + 1; 
                        end[u*layers + l] = start[u*layers + l] + 6; 
                    }    
                }    
            }    
        }    
    }
}

void tg_train(kann_t *ann, const tg_data_t *tg, float lr, int layers, int ulen, int vlen, int cs, int mbs,int max_epoch, float grad_clip, const char *fn, int n_threads, float frac_val, int model, int layer_norm, int parallel, int num_batches)
{
    int i, epoch, u, n_var, n_char;
    float **x, **y, *r;
    int *CG_start, *CG_end;
    const uint8_t **p;
    kann_t *ua;
    struct timeval start, end;
    double test_timetaken = 0, val_timetaken = 0, tot_test_timetaken = 0, tot_val_timetaken = 0;

    n_char = kann_dim_in(ann);
    x = (float**)calloc(ulen, sizeof(float*));
    y = (float**)calloc(ulen, sizeof(float*));
    for (u = 0; u < ulen; ++u) {
        x[u] = (float*)calloc(n_char * mbs, sizeof(float));
        y[u] = (float*)calloc(n_char * mbs, sizeof(float));
    }
    n_var = kann_size_var(ann);
    r = (float*)calloc(n_var, sizeof(float));
    p = (const uint8_t**)calloc(mbs, sizeof(const uint8_t*));

    CG_start = (int*)calloc(layers*ulen,sizeof(int));
    CG_end = (int*)calloc(layers*ulen,sizeof(int));

    if(model == 2){ // GRU
        GRU_OmpSs_start_end(CG_start,CG_end,ulen,layers, layer_norm);
    }else if(model == 1){ // LSTM
        LSTM_OmpSs_start_end(CG_start,CG_end,ulen,layers, layer_norm);
    }else if(model == 0){ // RNN
        RNN_OmpSs_start_end(CG_start,CG_end,ulen,layers, layer_norm);
    }

    ua = kann_unroll(ann, ulen);
    tg_urnn_start(ua, mbs);
    ompss_mt(ua, n_threads, mbs);  // Batch division among the cores using OMPSS
    kann_feed_bind(ua, KANN_F_IN,  100, x);
    kann_feed_bind(ua, KANN_F_TRUTH, 0, y);

    for (epoch = 0; epoch < max_epoch; ++epoch) {
        double train_cost = 0.0, val_cost = 0.0 ; // training and validation cost
        int train_err = 0, val_err=0;
        double time_taken = 0;
        test_timetaken = 0.0, val_timetaken = 0.0;

        int c, j, b, t_ctot = 0, t_tot = 0, v_tot = 0, v_ctot=0;

        for (b = 0; b < mbs; ++b){
            p[b] = tg->data ;
        }
        for (c = 0; c < cs; ++c) {
            int ce_len = c? ulen : ulen - vlen;
            for (u = 0; u < ulen; ++u) {
                memset(x[u], 0, mbs * n_char * sizeof(float));
                memset(y[u], 0, mbs * n_char * sizeof(float));
            }
            for (b = 0; b < mbs; ++b) {
                for (u = 0; u < ulen; ++u) {
                    x[u][b * n_char + p[b][u-1]] = 1.0f;
                    y[u][b * n_char + p[b][u]] = 1.0f;
                }
                p[b+1] += ulen;
            }
            // Training cost
            kann_switch(ua, 1);
            for(j=0;j<num_batches;++j){
                //start timer
                gettimeofday(&start, NULL);

                train_cost += kann_cost(ua, 0, 1, layers, ulen, CG_start, CG_end, parallel) * ulen * mbs;
                //fprintf(stderr,"Iteration : %d , Train_cost : %f\n",j,train_cost);
                train_err += kann_class_error(ua, &b);
                t_tot += ce_len * mbs, t_ctot += b;
                if (grad_clip > 0.0f) kann_grad_clip(grad_clip, n_var, ua->g);
                kann_RMSprop(n_var, lr, 0, 0.9f, ua->g, ua->x, r);

                //stop timer
                gettimeofday(&end, NULL);
                if(j>49){
                    time_taken = 0;
                    time_taken = (end.tv_sec - start.tv_sec);
                    time_taken = (time_taken*1e3 + (end.tv_usec - start.tv_usec)*1e-3);
                    test_timetaken += time_taken;
                }
            }

            //validation cost
            kann_switch(ua,0);
            for (j = 0; j < num_batches; ++j){
                //start timer
                gettimeofday(&start, NULL);
                val_cost += kann_cost(ua, 0, 0, layers, ulen, CG_start, CG_end, parallel) * ulen * mbs;
                //stop timer
                gettimeofday(&end, NULL);

                if(j>49){
                    time_taken = 0;
                    time_taken = (end.tv_sec - start.tv_sec);
                    time_taken = (time_taken*1e3 + (end.tv_usec - start.tv_usec)*1e-3);
                    val_timetaken += time_taken;
                }

                val_err += kann_class_error(ua, &b);
                v_tot += ce_len * mbs, v_ctot += b;
            }
        }

        tot_test_timetaken += test_timetaken;
        tot_val_timetaken += val_timetaken;
        fprintf(stderr, "epoch: %d; T-cost: %g (C-accuracy: %.2f%%) MeanTime [ms] : %.2f",epoch+1,train_cost/t_tot, (100 - (100.0* train_err / t_ctot)), test_timetaken/(num_batches-50));
        fprintf(stderr, "; V-cost: %g (V-accuracy: %.2f%%) Time [ms] : %.2f\n",val_cost/v_tot,(100-(100.0*val_err / v_ctot)), val_timetaken/(num_batches-50));
        if (fn) tg_save(fn, ann, tg->c2i);
    }
    fprintf(stderr, "\nT-totaltime [ms] : %.2f ; V-totaltime [ms] : %.2f \n",tot_test_timetaken,tot_val_timetaken);
    fprintf(stderr, "\nAverage T-totaltime [ms] : %.2f ; Average V-totaltime [ms] : %.2f\n",tot_test_timetaken/max_epoch,tot_val_timetaken/max_epoch);
    kann_delete_unrolled(ua);

    for (u = 0; u < ulen; ++u) {
        free(x[u]); free(y[u]);
    }
    free(r); free(y); free(x); free(p);
}

static kann_t *model_gen(int model, int n_char, int n_h_layers, int n_h_neurons, float h_dropout, int use_norm)
{
    int i, flag = use_norm? KANN_RNN_NORM : 0;
    kad_node_t *t, *t1;
    t = kann_layer_input(n_char), t->ext_label = 100;
    for (i = 0; i < n_h_layers; ++i) {
        if (model == 0) t = kann_layer_rnn(t, n_h_neurons, flag);
        else if (model == 1) t = kann_layer_lstm(t, n_h_neurons, flag);
        else if (model == 2) t = kann_layer_gru(t, n_h_neurons, flag);
        t = kann_layer_dropout(t, h_dropout);
    }
    t = kann_layer_dense(t, n_char);
    t1 = kann_new_scalar(KAD_CONST, 1.0f), t1->ext_label = -1; // -1 is for backward compatibility
    t = kad_mul(t, t1); // t1 is the inverse of temperature
    t = kad_softmax(t), t->ext_flag |= KANN_F_OUT;
    t1 = kad_feed(2, 1, n_char), t1->ext_flag |= KANN_F_TRUTH;
    t = kad_ce_multi(t, t1), t->ext_flag |= KANN_F_COST;
    return kann_new(t, 0);
}

int main(int argc, char *argv[])
{
    int c, seed = 11, ulen = 70, vlen = 10, n_h_layers = 1, n_h_neurons = 128, model = 2, max_epoch
        = 1, mbs = 64, c2i[256];
    int len_gen = 1000, use_norm = 1, batch_len = 1000000, n_threads = 1, cal_perp = 0, cs = 1, parallel=0, num_batches=100;
    float h_dropout = 0.1f, temp = 0.5f, lr = 0.01f, grad_clip = 10.0f, frac_val = 0.1f;
    kann_t *ann = 0;
    char *fn_in = 0, *fn_out = 0, *prefix = 0;

    while ((c = getopt(argc, argv, "n:l:s:r:m:B:o:i:d:b:T:M:u:L:Pg:Np:t:x:av:c:f")) >= 0) {
        if (c == 'n') n_h_neurons = atoi(optarg);
        else if (c == 'l') n_h_layers = atoi(optarg);
        else if (c == 's') seed = atoi(optarg);
        else if (c == 'i') fn_in = optarg;
        else if (c == 'o') fn_out = optarg;
        else if (c == 'r') lr = atof(optarg);
        else if (c == 'm') max_epoch = atoi(optarg);
        else if (c == 'B') mbs = atoi(optarg);
        else if (c == 'd') h_dropout = atof(optarg);
        else if (c == 'T') temp = atof(optarg);
        else if (c == 'c') cs = atoi(optarg);
        else if (c == 'u') ulen = atoi(optarg);
        else if (c == 'v') vlen = atoi(optarg);
        else if (c == 'L') len_gen = atoi(optarg);
        else if (c == 'g') grad_clip = atof(optarg);
        else if (c == 'N') use_norm = 0;
        else if (c == 'P') parallel = 1;
        else if (c == 'p') prefix = optarg;
        else if (c == 'b') batch_len = atoi(optarg);
        else if (c == 't') n_threads = atoi(optarg);
        else if (c == 'a') cal_perp = 1;
        else if (c == 'x') num_batches = atoi(optarg);
        else if (c == 'f') frac_val = atof(optarg);
        else if (c == 'M') {
            if (strcmp(optarg, "rnn") == 0) model = 0;
            else if (strcmp(optarg, "lstm") == 0) model = 1;
            else if (strcmp(optarg, "gru") == 0) model = 2;
        }
    }
    fprintf(stderr,"Parallel %d - Model : %d, Layers ; %d, Unrolling length : %d, hidden_size : %d, batch_size : %d, thread : %d, dropout : %.3f, lr : %.3f,norm : %d, num_batches : %d \n ", parallel, model, n_h_layers, ulen,  n_h_neurons,mbs, n_threads, h_dropout, lr,use_norm,num_batches);

    if (vlen >= ulen) vlen = ulen - 1;
    if (argc == optind && fn_in == 0) {
        FILE *fp = stdout;
        fprintf(fp, "Usage: textgen [options] <in.txt>\n");
        fprintf(fp, "Options:\n");
        fprintf(fp, "  Model construction:\n");
        fprintf(fp, "    -i FILE     read trained model from FILE []\n");
        fprintf(fp, "    -o FILE     save trained model to FILE []\n");
        fprintf(fp, "    -s INT      random seed [%d]\n", seed);
        fprintf(fp, "    -l INT      number of hidden layers [%d]\n", n_h_layers);
        fprintf(fp, "    -n INT      number of hidden neurons per layer [%d]\n", n_h_neurons);
        fprintf(fp, "    -M STR      model: rnn, lstm or gru [gru]\n");
        fprintf(fp, "    -N          don't use layer normalization\n");
        fprintf(fp, "  Model training:\n");
        fprintf(fp, "    -r FLOAT    learning rate [%g]\n", lr);
        fprintf(fp, "    -d FLOAT    dropout at the hidden layer(s) [%g]\n", h_dropout);
        fprintf(fp, "    -m INT      max number of epochs [%d]\n", max_epoch);
        fprintf(fp, "    -B INT      mini-batch size [%d]\n", mbs);
        fprintf(fp, "    -u INT      max unroll [%d]\n", ulen);
        fprintf(fp, "    -v INT      burn-in length [%d]\n", vlen);
        fprintf(fp, "    -g FLOAT    gradient clipping threshold [%g]\n", grad_clip);
        fprintf(fp, "    -c INT      size of a batch [%d]\n", batch_len);
        fprintf(fp, "    -x INT      Num of batchs [%d]\n", num_batches);
        fprintf(fp, "    -b          use minibatch (run faster but converge slower)\n");
        fprintf(fp, "    -f FLOAT    fraction value [%g]\n", frac_val);
        fprintf(fp, "    -a          compute perplexity at the end\n");
        fprintf(fp, "  Text generation:\n");
        fprintf(fp, "    -p STR      prefix []\n");
        fprintf(fp, "    -T FLOAT    temperature [%g]\n", temp);
        fprintf(fp, "    -L INT      length of text to generate [%d]\n", len_gen);
        return 1;
    }

    fprintf(stderr, "Command line:");
    for (c = 0; c < argc; ++c)
        fprintf(stderr, " %s", argv[c]);
    fprintf(stderr, "\n");
    kann_srand(seed);
    kad_trap_fe();
    if (fn_in) ann = tg_load(fn_in, c2i);

    if (argc - optind >= 1) { // train
        tg_data_t *tg;
        tg = tg_init(argv[optind]);
        fprintf(stderr, "Read %d paragraphs and %d characters; alphabet size %d\n", tg->n_para, tg->len, tg->n_char);
        if (!ann) ann = model_gen(model, tg->n_char, n_h_layers, n_h_neurons, h_dropout, use_norm);
        tg_train(ann, tg, lr, n_h_layers, ulen, vlen, cs, mbs, max_epoch, grad_clip, fn_out,n_threads, frac_val, model, use_norm, parallel, num_batches);
        if (cal_perp) fprintf(stderr, "Character-level perplexity: %g\n", tg_perplexity(ann, tg));
        free(tg->data); free(tg);
    } else tg_gen(stdout, ann, temp, len_gen, c2i, prefix);

    kann_delete(ann);
    return 0;
}
