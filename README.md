#ICS'2020 Submission

## Getting Started
```sh
# acquire source code and compile
git clone https://gitlab.com/robinkumarsharma/w-par.git
cd W-Par; make

## Two ways to execute

# 1. Run the script in ./examples
sbatch scr.sh


## OR

# 2.
 
# W-Par (Wavefront-Parallized) recurrent neural network run code
 ./examples/d_textgen_parallel -P -t 6 -B 64 -l 2 -u 100 -r 0.001 -n 128 -m 1 -M "lstm" ./examples/10000_wiki
# Seq (Sequential) run code
 ./examples/d_textgen_parallel -t 6 -B 64 -l 2 -u 100 -r 0.001 -n 128 -m 1 -M "lstm" ./examples/10000_wiki
```

### Parameters
* -P : Parallel implementation
* -t : Number of mini-batches to divide Batch Size to run in parallel
* -B : Batch Size
* -l : Number of layers
* -u : Sequnce or Unrolling length
* -r : Learning rate
* -n : Hidden Size/Number of neurons
* -m : Number of Epoch
* -M : model ["lstm"/"gru"/"rnn"]
* 10000\_wiki : A part of the wikipedia dataset

### Sample Results
We use the RNNs varient LSTM network to predict the next character on the set of wikipedia dataset and following are the same results for execution time of single batch:

|Task       |Parallel/Sequential|Machine|Device   |Training time [ms] | Validation time [ms]      |Command line |
|:----------|:------------|:-------------|:--------|:--------|:----------|:------------|
|Next-Char Prediction  | Parallel    |Linux  |48 CPU    | 342.52  | 117.52 |./examples/d\_textgen\_parallel -P -t 6 -B 64 -l 12 -u 100 -r 0.001 -n 128 -m 1 -M "rnn" ./examples/10000\_wiki
|  | Sequential    |Linux  |48 CPU    | 1098.96  | 571.29 |./examples/d\_textgen\_parallel -t 6 -B 64 -l 12 -u 100 -r 0.001 -n 128 -m 1 -M "rnn" ./examples/10000\_wiki

The training time [ms] in parallel run is **342.52** and in sequential run is **1098.96** . Due to model parallelism, there is speed up of **3.20** considering sequential execution against parallel execution.

Sample Results can be found at: ./examples/Sample\_Results .

### Requirements
Following are the requirements to execute the above commands:

* Linux x86-64 platform
* GNU C/C++ compiler versions 8.1.0
* [OmpSs-1.0][ompss1.0]
* Sequential Intel MKL library 2019.3


[ompss1.0]: https://pm.bsc.es/ftp/ompss/doc/user-guide/
