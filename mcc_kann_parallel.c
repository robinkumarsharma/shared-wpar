extern int signgam;
enum mcc_enum_anon_7
{
  _IEEE_ =  -1,
  _SVID_ = 0,
  _XOPEN_ = 1,
  _POSIX_ = 2,
  _ISOC_ = 3
};
typedef enum mcc_enum_anon_7 _LIB_VERSION_TYPE;
extern _LIB_VERSION_TYPE _LIB_VERSION;
extern __inline __attribute__((__nothrow__)) __attribute__((__leaf__)) __attribute__((__const__)) __attribute__((__always_inline__)) __attribute__((__gnu_inline__)) int __signbitf(float __x)
{
  int __m;
  __asm__ ("pmovmskb %1, %0" : "=r"(__m) : "x"(__x));
  return (__m & 8) != 0;
}
extern __inline __attribute__((__nothrow__)) __attribute__((__leaf__)) __attribute__((__const__)) __attribute__((__always_inline__)) __attribute__((__gnu_inline__)) int __signbit(double __x)
{
  int __m;
  __asm__ ("pmovmskb %1, %0" : "=r"(__m) : "x"(__x));
  return (__m & 128) != 0;
}
extern __inline __attribute__((__nothrow__)) __attribute__((__leaf__)) __attribute__((__const__)) __attribute__((__always_inline__)) __attribute__((__gnu_inline__)) int __signbitl(long double __x)
{
  __extension__ union  mcc_union_anon_19
  {
    __extension__ long double __l;
    __extension__ int __i[3L];
  };
  __extension__ union mcc_union_anon_19 __u = {.__l = __x};
  return (__u.__i[2] & 32768) != 0;
}
static __inline unsigned int __bswap_32(unsigned int __bsx)
{
  return __builtin_bswap32(__bsx);
}
typedef unsigned long int __uint64_t;
static __inline __uint64_t __bswap_64(__uint64_t __bsx)
{
  return __builtin_bswap64(__bsx);
}
typedef unsigned long int size_t;
extern __inline __attribute__((__gnu_inline__)) size_t __strcspn_c1(const char *__s, int __reject)
{
  size_t __result = 0;
  while (__s[__result] != '\000' && __s[__result] != __reject)
    {
       ++__result;
    }
  return __result;
}
extern __inline __attribute__((__gnu_inline__)) size_t __strcspn_c2(const char *__s, int __reject1, int __reject2)
{
  size_t __result = 0;
  while ((__s[__result] != '\000' && __s[__result] != __reject1) && __s[__result] != __reject2)
    {
       ++__result;
    }
  return __result;
}
extern __inline __attribute__((__gnu_inline__)) size_t __strcspn_c3(const char *__s, int __reject1, int __reject2, int __reject3)
{
  size_t __result = 0;
  while (((__s[__result] != '\000' && __s[__result] != __reject1) && __s[__result] != __reject2) && __s[__result] != __reject3)
    {
       ++__result;
    }
  return __result;
}
extern __inline __attribute__((__gnu_inline__)) size_t __strspn_c1(const char *__s, int __accept)
{
  size_t __result = 0;
  while (__s[__result] == __accept)
    {
       ++__result;
    }
  return __result;
}
extern __inline __attribute__((__gnu_inline__)) size_t __strspn_c2(const char *__s, int __accept1, int __accept2)
{
  size_t __result = 0;
  while (__s[__result] == __accept1 || __s[__result] == __accept2)
    {
       ++__result;
    }
  return __result;
}
extern __inline __attribute__((__gnu_inline__)) size_t __strspn_c3(const char *__s, int __accept1, int __accept2, int __accept3)
{
  size_t __result = 0;
  while ((__s[__result] == __accept1 || __s[__result] == __accept2) || __s[__result] == __accept3)
    {
       ++__result;
    }
  return __result;
}
extern __inline __attribute__((__gnu_inline__)) char *__strpbrk_c2(const char *__s, int __accept1, int __accept2)
{
  while ((*__s != '\000' && *__s != __accept1) && *__s != __accept2)
    {
       ++__s;
    }
  return *__s == '\000' ? (void *)0 : (char *)(size_t)__s;
}
extern __inline __attribute__((__gnu_inline__)) char *__strpbrk_c3(const char *__s, int __accept1, int __accept2, int __accept3)
{
  while (((*__s != '\000' && *__s != __accept1) && *__s != __accept2) && *__s != __accept3)
    {
       ++__s;
    }
  return *__s == '\000' ? (void *)0 : (char *)(size_t)__s;
}
extern __inline __attribute__((__gnu_inline__)) char *__strtok_r_1c(char *__s, char __sep, char **__nextp)
{
  char *__result;
  if (__s == (void *)0)
    {
      __s = *__nextp;
    }
  while (*__s == __sep)
    {
       ++__s;
    }
  __result = (void *)0;
  if (*__s != '\000')
    {
      __result = __s++;
      while (*__s != '\000')
        {
          if (*__s++ == __sep)
            {
              __s[ -1] = '\000';
              break;
            }
        }
    }
  *__nextp = __s;
  return __result;
}
extern void *__rawmemchr(const void *__s, int __c);
extern __inline __attribute__((__gnu_inline__)) char *__strsep_1c(char **__s, char __reject)
{
  char *__retval = *__s;
  if (__retval != (void *)0 && (*__s = (__builtin_constant_p(__reject) && !__builtin_constant_p(__retval)) && __reject == '\000' ? (char *)__rawmemchr(__retval, __reject) : __builtin_strchr(__retval, __reject)) != (void *)0)
    {
      *(*__s)++ = '\000';
    }
  return __retval;
}
extern __inline __attribute__((__gnu_inline__)) char *__strsep_2c(char **__s, char __reject1, char __reject2)
{
  char *__retval = *__s;
  if (__retval != (void *)0)
    {
      char *__cp = __retval;
      while (1)
        {
          if (*__cp == '\000')
            {
              __cp = (void *)0;
              break;
            }
          if (*__cp == __reject1 || *__cp == __reject2)
            {
              *__cp++ = '\000';
              break;
            }
           ++__cp;
        }
      *__s = __cp;
    }
  return __retval;
}
extern __inline __attribute__((__gnu_inline__)) char *__strsep_3c(char **__s, char __reject1, char __reject2, char __reject3)
{
  char *__retval = *__s;
  if (__retval != (void *)0)
    {
      char *__cp = __retval;
      while (1)
        {
          if (*__cp == '\000')
            {
              __cp = (void *)0;
              break;
            }
          if ((*__cp == __reject1 || *__cp == __reject2) || *__cp == __reject3)
            {
              *__cp++ = '\000';
              break;
            }
           ++__cp;
        }
      *__s = __cp;
    }
  return __retval;
}
extern long int strtol(const char *__restrict __nptr, char **__restrict __endptr, int __base) __attribute__((__nothrow__)) __attribute__((__leaf__)) __attribute__((__nonnull__(1)));
extern __inline __attribute__((__nothrow__)) __attribute__((__leaf__)) __attribute__((__pure__)) __attribute__((__nonnull__(1))) __attribute__((__gnu_inline__)) int atoi(const char *__nptr)
{
  return (int)strtol(__nptr, (char **)(void *)0, 10);
}
extern __inline __attribute__((__nothrow__)) __attribute__((__leaf__)) __attribute__((__pure__)) __attribute__((__nonnull__(1))) __attribute__((__gnu_inline__)) long int atol(const char *__nptr)
{
  return strtol(__nptr, (char **)(void *)0, 10);
}
extern long long int strtoll(const char *__restrict __nptr, char **__restrict __endptr, int __base) __attribute__((__nothrow__)) __attribute__((__leaf__)) __attribute__((__nonnull__(1)));
__extension__ extern __inline __attribute__((__nothrow__)) __attribute__((__leaf__)) __attribute__((__pure__)) __attribute__((__nonnull__(1))) __attribute__((__gnu_inline__)) long long int atoll(const char *__nptr)
{
  return strtoll(__nptr, (char **)(void *)0, 10);
}
__extension__ extern __inline __attribute__((__nothrow__)) __attribute__((__leaf__)) __attribute__((__const__)) __attribute__((__gnu_inline__)) unsigned int gnu_dev_major(unsigned long long int __dev)
{
  return (__dev >> 8 & 4095) | ((unsigned int)(__dev >> 32) & ~4095);
}
__extension__ extern __inline __attribute__((__nothrow__)) __attribute__((__leaf__)) __attribute__((__const__)) __attribute__((__gnu_inline__)) unsigned int gnu_dev_minor(unsigned long long int __dev)
{
  return (__dev & 255) | ((unsigned int)(__dev >> 12) & ~255);
}
__extension__ extern __inline __attribute__((__nothrow__)) __attribute__((__leaf__)) __attribute__((__const__)) __attribute__((__gnu_inline__)) unsigned long long int gnu_dev_makedev(unsigned int __major, unsigned int __minor)
{
  return (((__minor & 255) | (__major & 4095) << 8) | (unsigned long long int)(__minor & ~255) << 12) | (unsigned long long int)(__major & ~4095) << 32;
}
typedef int (*__compar_fn_t)(const void *, const void *);
extern __inline __attribute__((__nonnull__(1, 2, 5))) __attribute__((__gnu_inline__)) void *bsearch(const void *__key, const void *__base, size_t __nmemb, size_t __size, __compar_fn_t __compar)
{
  size_t __l;
  size_t __u;
  size_t __idx;
  const void *__p;
  int __comparison;
  __l = 0;
  __u = __nmemb;
  while (__l < __u)
    {
      __idx = (__l + __u) / 2;
      __p = (void *)((const char *)__base + __idx * __size);
      __comparison = (*__compar)(__key, __p);
      if (__comparison < 0)
        {
          __u = __idx;
        }
      else
        {
          if (__comparison > 0)
            {
              __l = __idx + 1;
            }
          else
            {
              return (void *)__p;
            }
        }
    }
  return (void *)0;
}
extern double strtod(const char *__restrict __nptr, char **__restrict __endptr) __attribute__((__nothrow__)) __attribute__((__leaf__)) __attribute__((__nonnull__(1)));
extern __inline __attribute__((__nothrow__)) __attribute__((__leaf__)) __attribute__((__pure__)) __attribute__((__nonnull__(1))) __attribute__((__gnu_inline__)) double atof(const char *__nptr)
{
  return strtod(__nptr, (char **)(void *)0);
}
struct _IO_FILE_plus;
extern struct _IO_FILE_plus _IO_2_1_stdin_;
extern struct _IO_FILE_plus _IO_2_1_stdout_;
extern struct _IO_FILE_plus _IO_2_1_stderr_;
struct _IO_FILE;
extern struct _IO_FILE *stdin;
extern struct _IO_FILE *stdout;
extern struct _IO_FILE *stderr;
extern int sys_nerr;
extern const char *const sys_errlist[];
typedef __builtin_va_list __gnuc_va_list;
typedef struct _IO_FILE FILE;
extern int vfprintf(FILE *__restrict __s, const char *__restrict __format, __gnuc_va_list __arg);
extern struct _IO_FILE *stdout;
extern __inline __attribute__((__gnu_inline__)) int vprintf(const char *__restrict __fmt, __gnuc_va_list __arg)
{
  return vfprintf(stdout, __fmt, __arg);
}
typedef struct _IO_FILE _IO_FILE;
extern int _IO_getc(_IO_FILE *__fp);
extern struct _IO_FILE *stdin;
extern __inline __attribute__((__gnu_inline__)) int getchar(void)
{
  return _IO_getc(stdin);
}
struct _IO_marker;
typedef long int __off_t;
typedef void _IO_lock_t;
typedef long int __off64_t;
struct  _IO_FILE
{
  int _flags;
  char *_IO_read_ptr;
  char *_IO_read_end;
  char *_IO_read_base;
  char *_IO_write_base;
  char *_IO_write_ptr;
  char *_IO_write_end;
  char *_IO_buf_base;
  char *_IO_buf_end;
  char *_IO_save_base;
  char *_IO_backup_base;
  char *_IO_save_end;
  struct _IO_marker *_markers;
  struct _IO_FILE *_chain;
  int _fileno;
  int _flags2;
  __off_t _old_offset;
  unsigned short int _cur_column;
  signed char _vtable_offset;
  char _shortbuf[1L];
  _IO_lock_t *_lock;
  __off64_t _offset;
  void *__pad1;
  void *__pad2;
  void *__pad3;
  void *__pad4;
  size_t __pad5;
  int _mode;
  char _unused2[20L];
};
extern int __uflow(_IO_FILE *);
extern __inline __attribute__((__gnu_inline__)) int fgetc_unlocked(FILE *__fp)
{
  return __builtin_expect((*__fp)._IO_read_ptr >= (*__fp)._IO_read_end, 0) ? __uflow(__fp) : *((unsigned char *)(*__fp)._IO_read_ptr++);
}
extern __inline __attribute__((__gnu_inline__)) int getc_unlocked(FILE *__fp)
{
  return __builtin_expect((*__fp)._IO_read_ptr >= (*__fp)._IO_read_end, 0) ? __uflow(__fp) : *((unsigned char *)(*__fp)._IO_read_ptr++);
}
extern __inline __attribute__((__gnu_inline__)) int getchar_unlocked(void)
{
  return __builtin_expect((*stdin)._IO_read_ptr >= (*stdin)._IO_read_end, 0) ? __uflow(stdin) : *((unsigned char *)(*stdin)._IO_read_ptr++);
}
extern int _IO_putc(int __c, _IO_FILE *__fp);
extern __inline __attribute__((__gnu_inline__)) int putchar(int __c)
{
  return _IO_putc(__c, stdout);
}
extern int __overflow(_IO_FILE *, int);
extern __inline __attribute__((__gnu_inline__)) int fputc_unlocked(int __c, FILE *__stream)
{
  return __builtin_expect((*__stream)._IO_write_ptr >= (*__stream)._IO_write_end, 0) ? __overflow(__stream, (unsigned char)__c) : (unsigned char)(*(*__stream)._IO_write_ptr++ = __c);
}
extern __inline __attribute__((__gnu_inline__)) int putc_unlocked(int __c, FILE *__stream)
{
  return __builtin_expect((*__stream)._IO_write_ptr >= (*__stream)._IO_write_end, 0) ? __overflow(__stream, (unsigned char)__c) : (unsigned char)(*(*__stream)._IO_write_ptr++ = __c);
}
extern __inline __attribute__((__gnu_inline__)) int putchar_unlocked(int __c)
{
  return __builtin_expect((*stdout)._IO_write_ptr >= (*stdout)._IO_write_end, 0) ? __overflow(stdout, (unsigned char)__c) : (unsigned char)(*(*stdout)._IO_write_ptr++ = __c);
}
extern __inline __attribute__((__nothrow__)) __attribute__((__leaf__)) __attribute__((__gnu_inline__)) int feof_unlocked(FILE *__stream)
{
  return ((*__stream)._flags & 16) != 0;
}
extern __inline __attribute__((__nothrow__)) __attribute__((__leaf__)) __attribute__((__gnu_inline__)) int ferror_unlocked(FILE *__stream)
{
  return ((*__stream)._flags & 32) != 0;
}
struct kad_node_t;
typedef struct kad_node_t kad_node_t;
typedef int (*kad_op_f)(kad_node_t *, int);
extern kad_op_f kad_op_list[64L];
extern char *kad_op_name[64L];
typedef unsigned char uint8_t;
typedef unsigned short int uint16_t;
typedef int int32_t;
typedef unsigned int uint32_t;
struct  kad_node_t
{
  uint8_t n_d;
  uint8_t flag;
  uint16_t op;
  int32_t n_child;
  int32_t tmp;
  int32_t ptr_size;
  int32_t d[4L];
  int32_t ext_label;
  uint32_t ext_flag;
  float *x;
  float *g;
  void *ptr;
  void *gtmp;
  struct kad_node_t **child;
  struct kad_node_t *pre;
};
static __inline int kad_len(const kad_node_t *p)
{
  int i;
  int n = 1;
  for (i = 0; i < (*p).n_d;  ++i)
    {
      n *= (*p).d[i];
    }
  return n;
}
extern int kann_verbose;
int kann_verbose = 3;
int kad_size_var(int n, kad_node_t *const *v);
extern void *realloc(void *__ptr, size_t __size) __attribute__((__nothrow__)) __attribute__((__leaf__)) __attribute__((__warn_unused_result__));
int kad_size_const(int n, kad_node_t *const *v);
extern void *memset(void *__s, int __c, size_t __n) __attribute__((__nothrow__)) __attribute__((__leaf__)) __attribute__((__nonnull__(1)));
extern void *memcpy(void *__restrict __dest, const void *__restrict __src, size_t __n) __attribute__((__nothrow__)) __attribute__((__leaf__)) __attribute__((__nonnull__(1, 2)));
extern void free(void *__ptr) __attribute__((__nothrow__)) __attribute__((__leaf__));
static void kad_ext_collate(int n, kad_node_t **a, float **_x, float **_g, float **_c)
{
  int n_var;
  float *x;
  float *g;
  float *c;
  int i;
  int j;
  int k;
  int l;
  n_var = kad_size_var(n, a);
  x = *_x = (float *)realloc(*_x, n_var * sizeof(float));
  g = *_g = (float *)realloc(*_g, n_var * sizeof(float));
  c = *_c = (float *)realloc(*_c, kad_size_const(n, a) * sizeof(float));
  memset(g, 0, n_var * sizeof(float));
  for (i = j = k = 0; i < n;  ++i)
    {
      kad_node_t *v = a[i];
      if ((*v).n_child == 0 && (*v).flag & 1)
        {
          l = kad_len(v);
          memcpy(&x[j], (*v).x, l * sizeof(float));
          free((*v).x);
          (*v).x = &x[j];
          (*v).g = &g[j];
          j += l;
        }
      else
        {
          if ((*v).n_child == 0 && (*v).flag & 2)
            {
              l = kad_len(v);
              memcpy(&c[k], (*v).x, l * sizeof(float));
              free((*v).x);
              (*v).x = &c[k];
              k += l;
            }
        }
    }
}
static void kad_ext_sync(int n, kad_node_t **a, float *x, float *g, float *c)
{
  int i;
  int j;
  int k;
  for (i = j = k = 0; i < n;  ++i)
    {
      kad_node_t *v = a[i];
      if ((*v).n_child == 0 && (*v).flag & 1)
        {
          (*v).x = &x[j];
          (*v).g = &g[j];
          j += kad_len(v);
        }
      else
        {
          if ((*v).n_child == 0 && (*v).flag & 2)
            {
              (*v).x = &c[k];
              k += kad_len(v);
            }
        }
    }
}
struct mcc_struct_anon_43;
typedef struct mcc_struct_anon_43 kann_t;
typedef __gnuc_va_list va_list;
extern void *malloc(size_t __size) __attribute__((__nothrow__)) __attribute__((__leaf__)) __attribute__((__malloc__));
extern void *calloc(size_t __nmemb, size_t __size) __attribute__((__nothrow__)) __attribute__((__leaf__)) __attribute__((__malloc__));
struct  mcc_struct_anon_43
{
  int n;
  kad_node_t **v;
  float *x;
  float *g;
  float *c;
  void *mt;
};
kad_node_t **kad_compile_array(int *n_node, int n_roots, kad_node_t **roots);
kad_node_t *kad_avg(int n, kad_node_t **x);
kann_t *kann_new(kad_node_t *cost, int n_rest, ...)
{
  va_list ap;
  kad_node_t **roots;
  int i;
  kann_t *a;
  int n_roots = 1 + n_rest;
  int has_pivot = 0;
  int has_recur = 0;
  if ((*cost).n_d != 0)
    {
      return 0;
    }
  __builtin_va_start(ap, n_rest);
  roots = (kad_node_t **)malloc((n_roots + 1) * sizeof(kad_node_t *));
  for (i = 0; i < n_rest;  ++i)
    {
      roots[i] = (__builtin_va_arg(ap, kad_node_t *));
    }
  roots[i++] = cost;
  __builtin_va_end(ap);
  (*cost).ext_flag |= 8;
  a = (kann_t *)calloc(1, sizeof(kann_t));
  (*a).v = kad_compile_array(&(*a).n, n_roots, roots);
  for (i = 0; i < (*a).n;  ++i)
    {
      if ((*(*a).v[i]).pre)
        {
          has_recur = 1;
        }
      if ((*(*a).v[i]).n_child == 1 && (*(*a).v[i]).flag & 4)
        {
          has_pivot = 1;
        }
    }
  if (has_recur && !has_pivot)
    {
      (*cost).ext_flag &= ~8;
      (roots[n_roots - 1] = cost = kad_avg(1, &cost), (*cost).ext_flag |= 8);
      free((*a).v);
      (*a).v = kad_compile_array(&(*a).n, n_roots, roots);
    }
  kad_ext_collate((*a).n, (*a).v, &(*a).x, &(*a).g, &(*a).c);
  free(roots);
  return a;
}
kad_node_t **kad_clone(int n, kad_node_t **v, int batch_size);
kann_t *kann_clone(kann_t *a, int batch_size)
{
  kann_t *b;
  b = (kann_t *)calloc(1, sizeof(kann_t));
  (*b).n = (*a).n;
  (*b).v = kad_clone((*a).n, (*a).v, batch_size);
  kad_ext_collate((*b).n, (*b).v, &(*b).x, &(*b).g, &(*b).c);
  return b;
}
kad_node_t **kad_unroll(int n_v, kad_node_t **v, int *new_n, int *len);
kann_t *kann_unroll_array(kann_t *a, int *len)
{
  kann_t *b;
  b = (kann_t *)calloc(1, sizeof(kann_t));
  ((((*b).x = (*a).x, (*b).g = (*a).g)), (*b).c = (*a).c);
  (*b).v = kad_unroll((*a).n, (*a).v, &(*b).n, len);
  return b;
}
int kad_n_pivots(int n_v, kad_node_t **v);
kann_t *kann_unroll(kann_t *a, ...)
{
  int n_pivots;
  int *len;
  va_list ap;
  int i;
  kann_t *b;
  n_pivots = kad_n_pivots((*a).n, (*a).v);
  len = (int *)calloc(n_pivots, sizeof(int));
  __builtin_va_start(ap, a);
  for (i = 0; i < n_pivots;  ++i)
    {
      len[i] = (__builtin_va_arg(ap, int));
    }
  __builtin_va_end(ap);
  b = kann_unroll_array(a, len);
  free(len);
  return b;
}
void ompss_mt(kann_t *ann, int n_threads, int max_batch_size);
void kad_delete(int n, kad_node_t **a);
void kann_delete_unrolled(kann_t *a)
{
  if (a && (*a).mt)
    {
      ompss_mt(a, 0, 0);
    }
  if (a && (*a).v)
    {
      kad_delete((*a).n, (*a).v);
    }
  free(a);
}
void kann_delete(kann_t *a)
{
  if (a == 0)
    {
      return ;
    }
  free((*a).x);
  free((*a).g);
  free((*a).c);
  kann_delete_unrolled(a);
}
static void kann_switch_core(kann_t *a, int is_train)
{
  int i;
  for (i = 0; i < (*a).n;  ++i)
    {
      if ((*(*a).v[i]).op == 12 && (*(*a).v[i]).n_child == 2)
        {
          *((int32_t *)(*(*a).v[i]).ptr) = !!is_train;
        }
    }
}
int kann_find(const kann_t *a, uint32_t ext_flag, int32_t ext_label)
{
  int i;
  int k;
  int r =  -1;
  for (i = k = 0; i < (*a).n;  ++i)
    {
      if ((ext_flag == 0 || (*(*a).v[i]).ext_flag & ext_flag) && (ext_label == 0 || (*(*a).v[i]).ext_label == ext_label))
        {
          ( ++k, r = i);
        }
    }
  return k == 1 ? r : k == 0 ?  -1 :  -2;
}
int kann_feed_bind(kann_t *a, uint32_t ext_flag, int32_t ext_label, float **x)
{
  int i;
  int k;
  if (x == 0)
    {
      return 0;
    }
  for (i = k = 0; i < (*a).n;  ++i)
    {
      if (((((*(*a).v[i]).n_child == 0 && !((*(*a).v[i]).flag & 1)) && !((*(*a).v[i]).flag & 2)) && (ext_flag == 0 || (*(*a).v[i]).ext_flag & ext_flag)) && (ext_label == 0 || (*(*a).v[i]).ext_label == ext_label))
        {
          (*(*a).v[i]).x = x[k++];
        }
    }
  return k;
}
int kann_feed_dim(const kann_t *a, uint32_t ext_flag, int32_t ext_label)
{
  int i;
  int k;
  int n = 0;
  for (i = k = 0; i < (*a).n;  ++i)
    {
      if (((((*(*a).v[i]).n_child == 0 && !((*(*a).v[i]).flag & 1)) && !((*(*a).v[i]).flag & 2)) && (ext_flag == 0 || (*(*a).v[i]).ext_flag & ext_flag)) && (ext_label == 0 || (*(*a).v[i]).ext_label == ext_label))
        {
          ( ++k, n = (*(*a).v[i]).n_d > 1 ? kad_len((*a).v[i]) / (*(*a).v[i]).d[0] : (*(*a).v[i]).n_d == 1 ? (*(*a).v[i]).d[0] : 1);
        }
    }
  return k == 1 ? n : k == 0 ?  -1 :  -2;
}
extern void __assert_fail(const char *__assertion, const char *__file, unsigned int __line, const char *__function) __attribute__((__nothrow__)) __attribute__((__leaf__)) __attribute__((__noreturn__));
const float *kad_eval_at(int n, kad_node_t **a, int from, int layers, int ulen, int *start, int *end, int parallel);
void kad_grad(int n, kad_node_t **a, int from, int layers, int ulen, int *start, int *end, int parallel);
static float kann_cost_core(kann_t *a, int cost_label, int cal_grad, int layers, int ulen, int *start, int *end, int parallel)
{
  int i_cost;
  float cost;
  static const char __MERCURIUM_PRETTY_FUNCTION__[70L] = "float kann_cost_core(kann_t *, int, int, int, int, int *, int *, int)";
  i_cost = kann_find(a, 8, cost_label);
  i_cost >= 0 ? (void)0 : __assert_fail("i_cost >= 0", "kann_parallel.c", 187, __MERCURIUM_PRETTY_FUNCTION__);
  cost = *kad_eval_at((*a).n, (*a).v, i_cost, layers, ulen, start, end, parallel);
  if (cal_grad)
    {
      kad_grad((*a).n, (*a).v, i_cost, layers, ulen, start, end, parallel);
    }
  return cost;
}
int kad_sync_dim(int n, kad_node_t **v, int batch_size);
void kann_rnn_start(kann_t *a)
{
  int i;
  kad_sync_dim((*a).n, (*a).v, 1);
  for (i = 0; i < (*a).n;  ++i)
    {
      kad_node_t *p = (*a).v[i];
      if ((*p).pre)
        {
          kad_node_t *q = (*p).pre;
          if ((*q).x)
            {
              memcpy((*p).x, (*q).x, kad_len(p) * sizeof(float));
            }
          else
            {
              memset((*p).x, 0, kad_len(p) * sizeof(float));
            }
          if ((*q).n_child > 0)
            {
              free((*q).x);
            }
          (*q).x = (*p).x;
        }
    }
}
void kann_rnn_end(kann_t *a)
{
  int i;
  kad_ext_sync((*a).n, (*a).v, (*a).x, (*a).g, (*a).c);
  for (i = 0; i < (*a).n;  ++i)
    {
      if ((*(*a).v[i]).pre && (*(*(*a).v[i]).pre).n_child > 0)
        {
          (*(*(*a).v[i]).pre).x = (float *)calloc(kad_len((*(*a).v[i]).pre), sizeof(float));
        }
    }
}
static int kann_class_error_core(const kann_t *ann, int *base)
{
  int i;
  int n;
  int m;
  int j;
  int off;
  int k;
  int n_err = 0;
  for ((i = 0, *base = 0); i < (*ann).n;  ++i)
    {
      kad_node_t *p = (*ann).v[i];
      if ((((*p).op == 13 && ((*p).n_child == 2 || (*p).n_child == 3)) || ((*p).op == 22 && (*p).n_child == 2)) && (*p).n_d == 0)
        {
          kad_node_t *x = (*p).child[0];
          kad_node_t *t = (*p).child[1];
          (n = (*t).d[(*t).n_d - 1], m = kad_len(t) / n);
          for (j = off = 0; j < m; ( ++j, off += n))
            {
              float t_sum = 0.000000000000000000000000e+00f;
              float t_min = 1.000000000000000000000000e+00f;
              float t_max = 0.000000000000000000000000e+00f;
              float x_max = 0.000000000000000000000000e+00f;
              float x_min = 1.000000000000000000000000e+00f;
              int x_max_k =  -1;
              int t_max_k =  -1;
              for (k = 0; k < n;  ++k)
                {
                  float xk = (*x).x[off + k];
                  float tk = (*t).x[off + k];
                  t_sum += tk;
                  t_min = t_min < tk ? t_min : tk;
                  x_min = x_min < xk ? x_min : xk;
                  if (t_max < tk)
                    {
                      (t_max = tk, t_max_k = k);
                    }
                  if (x_max < xk)
                    {
                      (x_max = xk, x_max_k = k);
                    }
                }
              if (((t_sum - 1.000000000000000000000000e+00f == 0 && t_min >= 0.000000000000000000000000e+00f) && x_min >= 0.000000000000000000000000e+00f) && x_max <= 1.000000000000000000000000e+00f)
                {
                   ++*base;
                  n_err += x_max_k != t_max_k;
                }
            }
        }
    }
  return n_err;
}
extern char *__tzname[2L];
extern int __daylight;
extern long int __timezone;
extern char *tzname[2L];
extern int daylight;
extern long int timezone;
typedef unsigned long int pthread_t;
extern __inline __attribute__((__nothrow__)) __attribute__((__leaf__)) __attribute__((__const__)) __attribute__((__gnu_inline__)) int pthread_equal(pthread_t __thread1, pthread_t __thread2)
{
  return __thread1 == __thread2;
}
struct ompss_batch;
typedef struct ompss_batch ompss_batch;
struct mcc_struct_anon_48;
typedef struct mcc_struct_anon_48 worker;
struct  ompss_batch
{
  int n_threads;
  int max_batch_size;
  int cal_grad;
  int cost_label;
  int eval_out;
  int start;
  int end;
  worker *mt;
};
struct  mcc_struct_anon_48
{
  kann_t *a;
  float cost;
  int action;
  struct ompss_batch *g;
};
void ompss_mt(kann_t *ann, int n_threads, int max_batch_size)
{
  ompss_batch *mt;
  int i;
  int k;
  if (n_threads > 1)
    {
      if (n_threads > max_batch_size)
        {
          n_threads = max_batch_size;
        }
      if (n_threads <= 1)
        {
          return ;
        }
      mt = (ompss_batch *)calloc(1, sizeof(ompss_batch));
      ((*mt).n_threads = n_threads, (*mt).max_batch_size = max_batch_size);
      (*mt).mt = (worker *)calloc(n_threads, sizeof(worker));
      for (i = k = 0; i < n_threads;  ++i)
        {
          int size = (max_batch_size - k) / (n_threads - i);
          (*mt).mt[i].a = kann_clone(ann, size);
          (*mt).mt[i].g = mt;
          k += size;
        }
      (*ann).mt = mt;
    }
  else
    {
      (*ann).mt = 0;
      return ;
    }
}
static void ompss_mt_kickoff(kann_t *a, int cost_label, int cal_grad, int eval_out)
{
  int B;
  int n_var;
  int i;
  int k;
  int j;
  static const char __MERCURIUM_PRETTY_FUNCTION__[47L] = "void ompss_mt_kickoff(kann_t *, int, int, int)";
  ompss_batch *mt = (ompss_batch *)(*a).mt;
  B = kad_sync_dim((*a).n, (*a).v,  -1);
  B <= (*mt).max_batch_size ? (void)0 : __assert_fail("B <= mt->max_batch_size", "kann_parallel.c", 329, __MERCURIUM_PRETTY_FUNCTION__);
  n_var = kad_size_var((*a).n, (*a).v);
  ((((*mt).cost_label = cost_label, (*mt).cal_grad = cal_grad)), (*mt).eval_out = eval_out);
  for (i = k = 0; i < (*mt).n_threads;  ++i)
    {
      int size = (B - k) / ((*mt).n_threads - i);
      for (j = 0; j < (*a).n;  ++j)
        {
          if (((*(*a).v[j]).n_child == 0 && !((*(*a).v[j]).flag & 1)) && !((*(*a).v[j]).flag & 2))
            {
              (*(*(*mt).mt[i].a).v[j]).x = &(*(*a).v[j]).x[k * kad_len((*a).v[j]) / (*(*a).v[j]).d[0]];
            }
        }
      kad_sync_dim((*(*mt).mt[i].a).n, (*(*mt).mt[i].a).v, size);
      k += size;
      memcpy((*(*mt).mt[i].a).x, (*a).x, n_var * sizeof(float));
      (*mt).mt[i].action = 1;
    }
}
static void *mt_worker(void *data)
{
}
struct mtaux_t;
typedef struct mtaux_t mtaux_t;
union mcc_union_anon_29;
typedef union mcc_union_anon_29 pthread_mutex_t;
extern int pthread_mutex_lock(pthread_mutex_t *__mutex) __attribute__((__nothrow__)) __attribute__((__nonnull__(1)));
struct __pthread_internal_list;
struct  __pthread_internal_list
{
  struct __pthread_internal_list *__prev;
  struct __pthread_internal_list *__next;
};
typedef struct __pthread_internal_list __pthread_list_t;
struct  __pthread_mutex_s
{
  int __lock;
  unsigned int __count;
  int __owner;
  unsigned int __nusers;
  int __kind;
  short int __spins;
  short int __elision;
  __pthread_list_t __list;
};
union  mcc_union_anon_29
{
  struct __pthread_mutex_s __data;
  char __size[40L];
  long int __align;
};
struct  mcc_struct_anon_32
{
  int __lock;
  unsigned int __futex;
  __extension__ unsigned long long int __total_seq;
  __extension__ unsigned long long int __wakeup_seq;
  __extension__ unsigned long long int __woken_seq;
  void *__mutex;
  unsigned int __nwaiters;
  unsigned int __broadcast_seq;
};
union  mcc_union_anon_31
{
  struct mcc_struct_anon_32 __data;
  char __size[48L];
  __extension__ long long int __align;
};
typedef union mcc_union_anon_31 pthread_cond_t;
struct mcc_struct_anon_47;
typedef struct mcc_struct_anon_47 mtaux1_t;
struct  mtaux_t
{
  int n_threads;
  int max_batch_size;
  int cal_grad;
  int cost_label;
  int eval_out;
  volatile int n_idle;
  pthread_mutex_t mtx;
  pthread_cond_t cv;
  mtaux1_t *mt;
};
struct  mcc_struct_anon_47
{
  kann_t *a;
  float cost;
  int action;
  pthread_t tid;
  struct mtaux_t *g;
};
extern int pthread_cond_broadcast(pthread_cond_t *__cond) __attribute__((__nothrow__)) __attribute__((__nonnull__(1)));
extern int pthread_mutex_unlock(pthread_mutex_t *__mutex) __attribute__((__nothrow__)) __attribute__((__nonnull__(1)));
extern int pthread_join(pthread_t __th, void **__thread_return);
extern int pthread_cond_destroy(pthread_cond_t *__cond) __attribute__((__nothrow__)) __attribute__((__leaf__)) __attribute__((__nonnull__(1)));
extern int pthread_mutex_destroy(pthread_mutex_t *__mutex) __attribute__((__nothrow__)) __attribute__((__leaf__)) __attribute__((__nonnull__(1)));
static void mt_destroy(mtaux_t *mt)
{
  int i;
  pthread_mutex_lock(&(*mt).mtx);
  (*mt).n_idle = 0;
  for (i = 1; i < (*mt).n_threads;  ++i)
    {
      (*mt).mt[i].action =  -1;
    }
  pthread_cond_broadcast(&(*mt).cv);
  pthread_mutex_unlock(&(*mt).mtx);
  for (i = 1; i < (*mt).n_threads;  ++i)
    {
      pthread_join((*mt).mt[i].tid, 0);
    }
  for (i = 0; i < (*mt).n_threads;  ++i)
    {
      kann_delete((*mt).mt[i].a);
    }
  free((*mt).mt);
  pthread_cond_destroy(&(*mt).cv);
  pthread_mutex_destroy(&(*mt).mtx);
  free(mt);
}
union mcc_union_anon_30;
typedef union mcc_union_anon_30 pthread_mutexattr_t;
extern int pthread_mutex_init(pthread_mutex_t *__mutex, const pthread_mutexattr_t *__mutexattr) __attribute__((__nothrow__)) __attribute__((__leaf__)) __attribute__((__nonnull__(1)));
union mcc_union_anon_33;
typedef union mcc_union_anon_33 pthread_condattr_t;
extern int pthread_cond_init(pthread_cond_t *__restrict __cond, const pthread_condattr_t *__restrict __cond_attr) __attribute__((__nothrow__)) __attribute__((__leaf__)) __attribute__((__nonnull__(1)));
union pthread_attr_t;
typedef union pthread_attr_t pthread_attr_t;
extern int pthread_create(pthread_t *__restrict __newthread, const pthread_attr_t *__restrict __attr, void *(*__start_routine)(void *), void *__restrict __arg) __attribute__((__nothrow__)) __attribute__((__nonnull__(1, 3)));
void kann_mt(kann_t *ann, int n_threads, int max_batch_size)
{
  mtaux_t *mt;
  int i;
  int k;
  if (n_threads <= 1)
    {
      if ((*ann).mt)
        {
          mt_destroy((mtaux_t *)(*ann).mt);
        }
      (*ann).mt = 0;
      return ;
    }
  if (n_threads > max_batch_size)
    {
      n_threads = max_batch_size;
    }
  if (n_threads <= 1)
    {
      return ;
    }
  mt = (mtaux_t *)calloc(1, sizeof(mtaux_t));
  ((*mt).n_threads = n_threads, (*mt).max_batch_size = max_batch_size);
  pthread_mutex_init(&(*mt).mtx, 0);
  pthread_cond_init(&(*mt).cv, 0);
  (*mt).mt = (mtaux1_t *)calloc(n_threads, sizeof(mtaux1_t));
  for (i = k = 0; i < n_threads;  ++i)
    {
      int size = (max_batch_size - k) / (n_threads - i);
      (*mt).mt[i].a = kann_clone(ann, size);
      (*mt).mt[i].g = mt;
      k += size;
    }
  for (i = 1; i < n_threads;  ++i)
    {
      pthread_create(&(*mt).mt[i].tid, 0, mt_worker, &(*mt).mt[i]);
    }
  while ((*mt).n_idle < n_threads - 1)
    {
      ;
    }
  (*ann).mt = mt;
}
static void mt_kickoff(kann_t *a, int cost_label, int cal_grad, int eval_out)
{
  int B;
  int n_var;
  int i;
  int k;
  int j;
  static const char __MERCURIUM_PRETTY_FUNCTION__[41L] = "void mt_kickoff(kann_t *, int, int, int)";
  mtaux_t *mt = (mtaux_t *)(*a).mt;
  B = kad_sync_dim((*a).n, (*a).v,  -1);
  B <= (*mt).max_batch_size ? (void)0 : __assert_fail("B <= mt->max_batch_size", "kann_parallel.c", 419, __MERCURIUM_PRETTY_FUNCTION__);
  n_var = kad_size_var((*a).n, (*a).v);
  pthread_mutex_lock(&(*mt).mtx);
  ((((*mt).cost_label = cost_label, (*mt).cal_grad = cal_grad)), (*mt).eval_out = eval_out);
  for (i = k = 0; i < (*mt).n_threads;  ++i)
    {
      int size = (B - k) / ((*mt).n_threads - i);
      for (j = 0; j < (*a).n;  ++j)
        {
          if (((*(*a).v[j]).n_child == 0 && !((*(*a).v[j]).flag & 1)) && !((*(*a).v[j]).flag & 2))
            {
              (*(*(*mt).mt[i].a).v[j]).x = &(*(*a).v[j]).x[k * kad_len((*a).v[j]) / (*(*a).v[j]).d[0]];
            }
        }
      kad_sync_dim((*(*mt).mt[i].a).n, (*(*mt).mt[i].a).v, size);
      k += size;
      memcpy((*(*mt).mt[i].a).x, (*a).x, n_var * sizeof(float));
      (*mt).mt[i].action = 1;
    }
  (*mt).n_idle = 0;
  pthread_cond_broadcast(&(*mt).cv);
  pthread_mutex_unlock(&(*mt).mtx);
}
void ompss_kann_cost_core(kann_t *a, int cost_label, int cal_grad, int x, int layers, int ulen, int *start, int *end, int parallel)
{
  ompss_batch *mt = (ompss_batch *)(*a).mt;
  (*mt).mt[x].cost = kann_cost_core((*mt).mt[x].a, cost_label, cal_grad, layers, ulen, start, end, parallel);
}
enum mcc_enum_anon_5
{
  NANOS_OK = 0,
  NANOS_UNKNOWN_ERR = 1,
  NANOS_UNIMPLEMENTED = 2,
  NANOS_ENOMEM = 3,
  NANOS_INVALID_PARAM = 4,
  NANOS_INVALID_REQUEST = 5
};
typedef enum mcc_enum_anon_5 nanos_err_t;
extern nanos_err_t nanos_in_final(_Bool *result);
extern void nanos_handle_error(nanos_err_t err);
struct  mcc_struct_anon_15
{
  void (*outline)(void *);
};
typedef struct mcc_struct_anon_15 nanos_smp_args_t;
struct  nanos_args_0_t
{
  kann_t *a;
  int cost_label;
  int cal_grad;
  int i;
  int layers;
  int ulen;
  int *start;
  int *end;
  int parallel;
};
static void smp_ol_kann_cost_0(struct nanos_args_0_t *const args);
struct  mcc_struct_anon_11
{
  _Bool mandatory_creation:1;
  _Bool tied:1;
  _Bool clear_chunk:1;
  _Bool reserved0:1;
  _Bool reserved1:1;
  _Bool reserved2:1;
  _Bool reserved3:1;
  _Bool reserved4:1;
};
typedef struct mcc_struct_anon_11 nanos_wd_props_t;
struct  nanos_const_wd_definition_tag
{
  nanos_wd_props_t props;
  size_t data_alignment;
  size_t num_copies;
  size_t num_devices;
  size_t num_dimensions;
  const char *description;
};
typedef struct nanos_const_wd_definition_tag nanos_const_wd_definition_t;
struct  mcc_struct_anon_14
{
  void *(*factory)(void *);
  void *arg;
};
typedef struct mcc_struct_anon_14 nanos_device_t;
struct  nanos_const_wd_definition_1
{
  nanos_const_wd_definition_t base;
  nanos_device_t devices[1L];
};
extern void *nanos_smp_factory(void *args);
struct  mcc_struct_anon_12
{
  _Bool is_final:1;
  _Bool is_recover:1;
  _Bool is_implicit:1;
  _Bool reserved3:1;
  _Bool reserved4:1;
  _Bool reserved5:1;
  _Bool reserved6:1;
  _Bool reserved7:1;
};
typedef struct mcc_struct_anon_12 nanos_wd_dyn_flags_t;
typedef void *nanos_thread_t;
struct  mcc_struct_anon_13
{
  nanos_wd_dyn_flags_t flags;
  nanos_thread_t tie_to;
  int priority;
  void *callback;
  void *arguments;
};
typedef struct mcc_struct_anon_13 nanos_wd_dyn_props_t;
typedef void *nanos_wd_t;
struct mcc_struct_anon_4;
typedef struct mcc_struct_anon_4 nanos_copy_data_internal_t;
typedef nanos_copy_data_internal_t nanos_copy_data_t;
struct mcc_struct_anon_0;
typedef struct mcc_struct_anon_0 nanos_region_dimension_internal_t;
typedef void *nanos_wg_t;
extern nanos_err_t nanos_create_wd_compact(nanos_wd_t *wd, nanos_const_wd_definition_t *const_data, nanos_wd_dyn_props_t *dyn_props, size_t data_size, void **data, nanos_wg_t wg, nanos_copy_data_t **copies, nanos_region_dimension_internal_t **dimensions);
extern nanos_wd_t nanos_current_wd(void);
struct mcc_struct_anon_2;
typedef struct mcc_struct_anon_2 nanos_data_access_internal_t;
typedef nanos_data_access_internal_t nanos_data_access_t;
typedef void *nanos_team_t;
extern nanos_err_t nanos_submit(nanos_wd_t wd, size_t num_data_accesses, nanos_data_access_t *data_accesses, nanos_team_t team);
struct  mcc_struct_anon_1
{
  _Bool input:1;
  _Bool output:1;
  _Bool can_rename:1;
  _Bool concurrent:1;
  _Bool commutative:1;
};
typedef struct mcc_struct_anon_1 nanos_access_type_internal_t;
typedef long int ptrdiff_t;
struct  mcc_struct_anon_2
{
  void *address;
  nanos_access_type_internal_t flags;
  short int dimension_count;
  const nanos_region_dimension_internal_t *dimensions;
  ptrdiff_t offset;
};
typedef void (*nanos_translate_args_t)(void *, nanos_wd_t);
extern nanos_err_t nanos_create_wd_and_run_compact(nanos_const_wd_definition_t *const_data, nanos_wd_dyn_props_t *dyn_props, size_t data_size, void *data, size_t num_data_accesses, nanos_data_access_t *data_accesses, nanos_copy_data_t *copies, nanos_region_dimension_internal_t *dimensions, nanos_translate_args_t translate_args);
extern nanos_err_t nanos_wg_wait_completion(nanos_wg_t wg, _Bool avoid_flush);
void kad_saxpy(int n, float a, const float *x, float *y);
float kann_cost(kann_t *a, int cost_label, int cal_grad, int layers, int ulen, int *start, int *end, int parallel)
{
  int B;
  int n_var;
  int i;
  int k;
  float cost;
  int j;
  ompss_batch *mt = (ompss_batch *)(*a).mt;
  if (mt == 0)
    {
      return kann_cost_core(a, cost_label, cal_grad, layers, ulen, start, end, parallel);
    }
  B = kad_sync_dim((*a).n, (*a).v,  -1);
  n_var = kad_size_var((*a).n, (*a).v);
  int n_threads = (*mt).n_threads;
  ompss_mt_kickoff(a, cost_label, cal_grad, 0);
  for (i = 0; i < n_threads;  ++i)
    {
      {
        _Bool mcc_is_in_final;
        nanos_err_t mcc_err_in_final = nanos_in_final(&mcc_is_in_final);
        if (mcc_err_in_final != NANOS_OK)
          {
            nanos_handle_error(mcc_err_in_final);
          }
        if (mcc_is_in_final)
          {
            ompss_kann_cost_core(a, cost_label, cal_grad, i, layers, ulen, start, end, parallel);
          }
        else
          {
            {
              nanos_wd_dyn_props_t nanos_wd_dyn_props;
              struct nanos_args_0_t *ol_args;
              nanos_err_t nanos_err;
              struct nanos_args_0_t imm_args;
              nanos_data_access_t dependences[1L];
              static nanos_smp_args_t smp_ol_kann_cost_0_args = {.outline = (void (*)(void *))(void (*)(struct nanos_args_0_t *))&smp_ol_kann_cost_0};
              static struct nanos_const_wd_definition_1 nanos_wd_const_data = {.base = {.props = {.mandatory_creation = 0, .tied = 0, .clear_chunk = 0, .reserved0 = 0, .reserved1 = 0, .reserved2 = 0, .reserved3 = 0, .reserved4 = 0}, .data_alignment = __alignof__(struct nanos_args_0_t), .num_copies = 0, .num_devices = 1, .num_dimensions = 0, .description = 0}, .devices = {[0] = {.factory = &nanos_smp_factory, .arg = &smp_ol_kann_cost_0_args}}};
              nanos_wd_dyn_props.tie_to = 0;
              nanos_wd_dyn_props.priority = 0;
              nanos_wd_dyn_props.flags.is_final = 0;
              nanos_wd_dyn_props.flags.is_implicit = 0;
              nanos_wd_dyn_props.flags.is_recover = 0;
              ol_args = (struct nanos_args_0_t *)0;
              nanos_wd_t nanos_wd_ = (nanos_wd_t)0;
              nanos_err = nanos_create_wd_compact(&nanos_wd_, &nanos_wd_const_data.base, &nanos_wd_dyn_props, sizeof(struct nanos_args_0_t), (void **)&ol_args, nanos_current_wd(), (nanos_copy_data_t **)0, (nanos_region_dimension_internal_t **)0);
              if (nanos_err != NANOS_OK)
                {
                  nanos_handle_error(nanos_err);
                }
              if (nanos_wd_ != (nanos_wd_t)0)
                {
                  (*ol_args).a = a;
                  (*ol_args).cost_label = cost_label;
                  (*ol_args).cal_grad = cal_grad;
                  (*ol_args).i = i;
                  (*ol_args).layers = layers;
                  (*ol_args).ulen = ulen;
                  (*ol_args).start = start;
                  (*ol_args).end = end;
                  (*ol_args).parallel = parallel;
                  nanos_err = nanos_submit(nanos_wd_, 0, &dependences[0], (nanos_team_t)0);
                  if (nanos_err != NANOS_OK)
                    {
                      nanos_handle_error(nanos_err);
                    }
                }
              else
                {
                  imm_args.a = a;
                  imm_args.cost_label = cost_label;
                  imm_args.cal_grad = cal_grad;
                  imm_args.i = i;
                  imm_args.layers = layers;
                  imm_args.ulen = ulen;
                  imm_args.start = start;
                  imm_args.end = end;
                  imm_args.parallel = parallel;
                  nanos_err = nanos_create_wd_and_run_compact(&nanos_wd_const_data.base, &nanos_wd_dyn_props, sizeof(struct nanos_args_0_t), &imm_args, 0, &dependences[0], (nanos_copy_data_t *)0, (nanos_region_dimension_internal_t *)0, (nanos_translate_args_t)0);
                  if (nanos_err != NANOS_OK)
                    {
                      nanos_handle_error(nanos_err);
                    }
                }
            }
          }
      }
    }
  {
    nanos_err_t nanos_err;
    nanos_wd_t nanos_wd_ = nanos_current_wd();
    nanos_err = nanos_wg_wait_completion(nanos_wd_, 0);
    if (nanos_err != NANOS_OK)
      {
        nanos_handle_error(nanos_err);
      }
  }
  memset((*a).g, 0, n_var * sizeof(float));
  for ((i = k = 0, cost = 0.000000000000000000000000e+00f); i < (*mt).n_threads;  ++i)
    {
      int size = (B - k) / ((*mt).n_threads - i);
      cost += (*mt).mt[i].cost * size / B;
      kad_saxpy(n_var, (float)size / B, (*(*mt).mt[i].a).g, (*a).g);
      k += size;
    }
  for (j = 0; j < (*a).n;  ++j)
    {
      kad_node_t *p = (*a).v[j];
      if (((*p).pre && (*p).n_d >= 2) && (*p).d[0] == B)
        {
          for (i = k = 0; i < (*mt).n_threads;  ++i)
            {
              kad_node_t *q = (*(*mt).mt[i].a).v[j];
              memcpy(&(*p).x[k], (*q).x, kad_len(q) * sizeof(float));
              k += kad_len(q);
            }
        }
    }
  return cost;
}
int kann_class_error(const kann_t *ann, int *base)
{
  int i;
  ompss_batch *mt = (ompss_batch *)(*ann).mt;
  int n_err = 0;
  int b = 0;
  if (mt == 0)
    {
      return kann_class_error_core(ann, base);
    }
  *base = 0;
  for (i = 0; i < (*mt).n_threads;  ++i)
    {
      n_err += kann_class_error_core((*mt).mt[i].a, &b);
      *base += b;
    }
  return n_err;
}
void kann_switch(kann_t *ann, int is_train)
{
  int i;
  ompss_batch *mt = (ompss_batch *)(*ann).mt;
  if (mt == 0)
    {
      kann_switch_core(ann, is_train);
      return ;
    }
  for (i = 0; i < (*mt).n_threads;  ++i)
    {
      kann_switch_core((*mt).mt[i].a, is_train);
    }
}
extern size_t fwrite(const void *__restrict __ptr, size_t __size, size_t __n, FILE *__restrict __s);
int kad_save(FILE *fp, int n_node, kad_node_t **node);
void kann_save_fp(FILE *fp, kann_t *ann)
{
  kad_sync_dim((*ann).n, (*ann).v, 1);
  fwrite("KAN\001", 1, 4, fp);
  kad_save(fp, (*ann).n, (*ann).v);
  fwrite((*ann).x, sizeof(float), kad_size_var((*ann).n, (*ann).v), fp);
  fwrite((*ann).c, sizeof(float), kad_size_const((*ann).n, (*ann).v), fp);
}
extern size_t strlen(const char *__s) __attribute__((__nothrow__)) __attribute__((__leaf__)) __attribute__((__pure__)) __attribute__((__nonnull__(1)));
extern FILE *fopen(const char *__restrict __filename, const char *__restrict __modes);
extern int fclose(FILE *__stream);
void kann_save(const char *fn, kann_t *ann)
{
  FILE *fp;
  fp = fn &&  ({
    size_t __s1_len;
    size_t __s2_len;
    (__builtin_constant_p(fn) && __builtin_constant_p("-")) && ((((__s1_len = strlen(fn), __s2_len = strlen("-"))), (!((size_t)(const void *)(fn + 1) - (size_t)(const void *)fn == 1) || __s1_len >= 4) && (!((size_t)(const void *)("-" + 1) - (size_t)(const void *)"-" == 1) || __s2_len >= 4))) ? __builtin_strcmp(fn, "-") : (__builtin_constant_p(fn) && (size_t)(const void *)(fn + 1) - (size_t)(const void *)fn == 1) && ((__s1_len = strlen(fn), __s1_len < 4)) ? __builtin_constant_p("-") && (size_t)(const void *)("-" + 1) - (size_t)(const void *)"-" == 1 ? __builtin_strcmp(fn, "-") :  ({
      const unsigned char *__s2 = (const unsigned char *)(const char *)"-";
      int __result = ((const unsigned char *)(const char *)fn)[0] - __s2[0];
      if (__s1_len > 0 && __result == 0)
        {
          __result = ((const unsigned char *)(const char *)fn)[1] - __s2[1];
          if (__s1_len > 1 && __result == 0)
            {
              __result = ((const unsigned char *)(const char *)fn)[2] - __s2[2];
              if (__s1_len > 2 && __result == 0)
                {
                  __result = ((const unsigned char *)(const char *)fn)[3] - __s2[3];
                }
            }
        }
      __result;
    })  : (__builtin_constant_p("-") && (size_t)(const void *)("-" + 1) - (size_t)(const void *)"-" == 1) && ((__s2_len = strlen("-"), __s2_len < 4)) ? __builtin_constant_p(fn) && (size_t)(const void *)(fn + 1) - (size_t)(const void *)fn == 1 ? __builtin_strcmp(fn, "-") :  - ({
      const unsigned char *__s2 = (const unsigned char *)(const char *)fn;
      int __result = ((const unsigned char *)(const char *)"-")[0] - __s2[0];
      if (__s2_len > 0 && __result == 0)
        {
          __result = ((const unsigned char *)(const char *)"-")[1] - __s2[1];
          if (__s2_len > 1 && __result == 0)
            {
              __result = ((const unsigned char *)(const char *)"-")[2] - __s2[2];
              if (__s2_len > 2 && __result == 0)
                {
                  __result = ((const unsigned char *)(const char *)"-")[3] - __s2[3];
                }
            }
        }
      __result;
    })  : __builtin_strcmp(fn, "-");
  })  ? fopen(fn, "wb") : stdout;
  kann_save_fp(fp, ann);
  fclose(fp);
}
extern size_t fread(void *__restrict __ptr, size_t __size, size_t __n, FILE *__restrict __stream);
extern int strncmp(const char *__s1, const char *__s2, size_t __n) __attribute__((__nothrow__)) __attribute__((__leaf__)) __attribute__((__pure__)) __attribute__((__nonnull__(1, 2)));
kad_node_t **kad_load(FILE *fp, int *_n_node);
kann_t *kann_load_fp(FILE *fp)
{
  char magic[4L];
  kann_t *ann;
  int n_var;
  int n_const;
  fread(magic, 1, 4, fp);
  if ((__builtin_constant_p(4) && ((__builtin_constant_p(magic) && strlen(magic) < (size_t)4) || (__builtin_constant_p("KAN\001") && strlen("KAN\001") < (size_t)4)) ?  ({
  size_t __s1_len;
  size_t __s2_len;
  (__builtin_constant_p(magic) && __builtin_constant_p("KAN\001")) && ((((__s1_len = strlen(magic), __s2_len = strlen("KAN\001"))), (!((size_t)(const void *)(magic + 1) - (size_t)(const void *)magic == 1) || __s1_len >= 4) && (!((size_t)(const void *)("KAN\001" + 1) - (size_t)(const void *)"KAN\001" == 1) || __s2_len >= 4))) ? __builtin_strcmp(magic, "KAN\001") : (__builtin_constant_p(magic) && (size_t)(const void *)(magic + 1) - (size_t)(const void *)magic == 1) && ((__s1_len = strlen(magic), __s1_len < 4)) ? __builtin_constant_p("KAN\001") && (size_t)(const void *)("KAN\001" + 1) - (size_t)(const void *)"KAN\001" == 1 ? __builtin_strcmp(magic, "KAN\001") :  ({
    const unsigned char *__s2 = (const unsigned char *)(const char *)"KAN\001";
    int __result = ((const unsigned char *)(const char *)magic)[0] - __s2[0];
    if (__s1_len > 0 && __result == 0)
      {
        __result = ((const unsigned char *)(const char *)magic)[1] - __s2[1];
        if (__s1_len > 1 && __result == 0)
          {
            __result = ((const unsigned char *)(const char *)magic)[2] - __s2[2];
            if (__s1_len > 2 && __result == 0)
              {
                __result = ((const unsigned char *)(const char *)magic)[3] - __s2[3];
              }
          }
      }
    __result;
  })  : (__builtin_constant_p("KAN\001") && (size_t)(const void *)("KAN\001" + 1) - (size_t)(const void *)"KAN\001" == 1) && ((__s2_len = strlen("KAN\001"), __s2_len < 4)) ? __builtin_constant_p(magic) && (size_t)(const void *)(magic + 1) - (size_t)(const void *)magic == 1 ? __builtin_strcmp(magic, "KAN\001") :  - ({
    const unsigned char *__s2 = (const unsigned char *)(const char *)magic;
    int __result = ((const unsigned char *)(const char *)"KAN\001")[0] - __s2[0];
    if (__s2_len > 0 && __result == 0)
      {
        __result = ((const unsigned char *)(const char *)"KAN\001")[1] - __s2[1];
        if (__s2_len > 1 && __result == 0)
          {
            __result = ((const unsigned char *)(const char *)"KAN\001")[2] - __s2[2];
            if (__s2_len > 2 && __result == 0)
              {
                __result = ((const unsigned char *)(const char *)"KAN\001")[3] - __s2[3];
              }
          }
      }
    __result;
  })  : __builtin_strcmp(magic, "KAN\001");
})  : strncmp(magic, "KAN\001", 4)) != 0)
    {
      fclose(fp);
      return 0;
    }
  ann = (kann_t *)calloc(1, sizeof(kann_t));
  (*ann).v = kad_load(fp, &(*ann).n);
  n_var = kad_size_var((*ann).n, (*ann).v);
  n_const = kad_size_const((*ann).n, (*ann).v);
  (*ann).x = (float *)malloc(n_var * sizeof(float));
  (*ann).g = (float *)calloc(n_var, sizeof(float));
  (*ann).c = (float *)malloc(n_const * sizeof(float));
  fread((*ann).x, sizeof(float), n_var, fp);
  fread((*ann).c, sizeof(float), n_const, fp);
  kad_ext_sync((*ann).n, (*ann).v, (*ann).x, (*ann).g, (*ann).c);
  return ann;
}
kann_t *kann_load(const char *fn)
{
  FILE *fp;
  kann_t *ann;
  fp = fn &&  ({
    size_t __s1_len;
    size_t __s2_len;
    (__builtin_constant_p(fn) && __builtin_constant_p("-")) && ((((__s1_len = strlen(fn), __s2_len = strlen("-"))), (!((size_t)(const void *)(fn + 1) - (size_t)(const void *)fn == 1) || __s1_len >= 4) && (!((size_t)(const void *)("-" + 1) - (size_t)(const void *)"-" == 1) || __s2_len >= 4))) ? __builtin_strcmp(fn, "-") : (__builtin_constant_p(fn) && (size_t)(const void *)(fn + 1) - (size_t)(const void *)fn == 1) && ((__s1_len = strlen(fn), __s1_len < 4)) ? __builtin_constant_p("-") && (size_t)(const void *)("-" + 1) - (size_t)(const void *)"-" == 1 ? __builtin_strcmp(fn, "-") :  ({
      const unsigned char *__s2 = (const unsigned char *)(const char *)"-";
      int __result = ((const unsigned char *)(const char *)fn)[0] - __s2[0];
      if (__s1_len > 0 && __result == 0)
        {
          __result = ((const unsigned char *)(const char *)fn)[1] - __s2[1];
          if (__s1_len > 1 && __result == 0)
            {
              __result = ((const unsigned char *)(const char *)fn)[2] - __s2[2];
              if (__s1_len > 2 && __result == 0)
                {
                  __result = ((const unsigned char *)(const char *)fn)[3] - __s2[3];
                }
            }
        }
      __result;
    })  : (__builtin_constant_p("-") && (size_t)(const void *)("-" + 1) - (size_t)(const void *)"-" == 1) && ((__s2_len = strlen("-"), __s2_len < 4)) ? __builtin_constant_p(fn) && (size_t)(const void *)(fn + 1) - (size_t)(const void *)fn == 1 ? __builtin_strcmp(fn, "-") :  - ({
      const unsigned char *__s2 = (const unsigned char *)(const char *)fn;
      int __result = ((const unsigned char *)(const char *)"-")[0] - __s2[0];
      if (__s2_len > 0 && __result == 0)
        {
          __result = ((const unsigned char *)(const char *)"-")[1] - __s2[1];
          if (__s2_len > 1 && __result == 0)
            {
              __result = ((const unsigned char *)(const char *)"-")[2] - __s2[2];
              if (__s2_len > 2 && __result == 0)
                {
                  __result = ((const unsigned char *)(const char *)"-")[3] - __s2[3];
                }
            }
        }
      __result;
    })  : __builtin_strcmp(fn, "-");
  })  ? fopen(fn, "rb") : stdin;
  ann = kann_load_fp(fp);
  fclose(fp);
  return ann;
}
typedef struct kad_node_t *kad_node_p;
extern double sqrt(double __x) __attribute__((__nothrow__)) __attribute__((__leaf__));
double kad_drand_normal(void *d);
kad_node_t *kann_new_leaf_array(int *offset, kad_node_p *par, uint8_t flag, float x0_01, int n_d, int32_t *d)
{
  kad_node_t *p;
  int len;
  int i;
  int off = offset && par ? *offset :  -1;
  if (off >= 0 && par[off])
    {
      return par[(*offset)++];
    }
  p = (kad_node_t *)calloc(1, sizeof(kad_node_t));
  ((*p).n_d = n_d, (*p).flag = flag);
  memcpy((*p).d, d, n_d * sizeof(int32_t));
  len = kad_len(p);
  (*p).x = (float *)calloc(len, sizeof(float));
  if ((*p).n_d <= 1)
    {
      for (i = 0; i < len;  ++i)
        {
          (*p).x[i] = x0_01;
        }
    }
  else
    {
      double sdev_inv;
      sdev_inv = 1.00000000000000000000000000000000000000000000000000000e+00 / sqrt((double)len / (*p).d[0]);
      for (i = 0; i < len;  ++i)
        {
          (*p).x[i] = (float)(kad_drand_normal(0) * sdev_inv);
        }
    }
  if (off >= 0)
    {
      (par[off] = p,  ++*offset);
    }
  return p;
}
kad_node_t *kann_new_leaf2(int *offset, kad_node_p *par, uint8_t flag, float x0_01, int n_d, ...)
{
  va_list ap;
  int32_t i;
  int32_t d[4L];
  __builtin_va_start(ap, n_d);
  for (i = 0; i < n_d;  ++i)
    {
      d[i] = (__builtin_va_arg(ap, int));
    }
  __builtin_va_end(ap);
  return kann_new_leaf_array(offset, par, flag, x0_01, n_d, d);
}
kad_node_t *kad_add(kad_node_t *x, kad_node_t *y);
kad_node_t *kad_cmul(kad_node_t *x, kad_node_t *y);
kad_node_t *kann_layer_dense2(int *offset, kad_node_p *par, kad_node_t *in, int n1)
{
  int n0;
  kad_node_t *w;
  kad_node_t *b;
  n0 = (*in).n_d >= 2 ? kad_len(in) / (*in).d[0] : kad_len(in);
  w = kann_new_leaf2(offset, par, 1, 0.000000000000000000000000e+00f, 2, n1, n0);
  b = kann_new_leaf2(offset, par, 1, 0.000000000000000000000000e+00f, 1, n1);
  return kad_add(kad_cmul(in, w), b);
}
kad_node_t *kad_dropout(kad_node_t *x, kad_node_t *r);
kad_node_t *kad_switch(int n, kad_node_t **p);
kad_node_t *kann_layer_dropout2(int *offset, kad_node_p *par, kad_node_t *t, float r)
{
  kad_node_t *cr;
  kad_node_t *x[2L];
  cr = kann_new_leaf2(offset, par, 2, r, 0);
  (x[0] = t, x[1] = kad_dropout(t, cr));
  return kad_switch(2, x);
}
kad_node_t *kad_mul(kad_node_t *x, kad_node_t *y);
kad_node_t *kad_stdnorm(kad_node_t *x);
kad_node_t *kann_layer_layernorm2(int *offset, kad_node_t **par, kad_node_t *in)
{
  int n0;
  kad_node_t *alpha;
  kad_node_t *beta;
  n0 = (*in).n_d >= 2 ? kad_len(in) / (*in).d[0] : kad_len(in);
  alpha = kann_new_leaf2(offset, par, 1, 1.000000000000000000000000e+00f, 1, n0);
  beta = kann_new_leaf2(offset, par, 1, 0.000000000000000000000000e+00f, 1, n0);
  return kad_add(kad_mul(kad_stdnorm(in), alpha), beta);
}
static __inline kad_node_t *cmul_norm2(int *offset, kad_node_t **par, kad_node_t *x, kad_node_t *w, int use_norm)
{
  return use_norm ? kann_layer_layernorm2(offset, par, kad_cmul(x, w)) : kad_cmul(x, w);
}
kad_node_t *kad_tanh(kad_node_t *x);
kad_node_t *kann_layer_rnn2(int *offset, kad_node_t **par, kad_node_t *in, kad_node_t *h0, int rnn_flag)
{
  kad_node_t *u;
  kad_node_t *b;
  kad_node_t *t;
  int n0;
  kad_node_t *w;
  kad_node_t *out;
  int n1 = (*h0).d[(*h0).n_d - 1];
  int use_norm = !!(rnn_flag & 2);
  u = kann_new_leaf2(offset, par, 1, 0.000000000000000000000000e+00f, 2, n1, n1);
  b = kann_new_leaf2(offset, par, 1, 0.000000000000000000000000e+00f, 1, n1);
  t = cmul_norm2(offset, par, h0, u, use_norm);
  if (in)
    {
      n0 = (*in).n_d >= 2 ? kad_len(in) / (*in).d[0] : kad_len(in);
      w = kann_new_leaf2(offset, par, 1, 0.000000000000000000000000e+00f, 2, n1, n0);
      t = kad_add(cmul_norm2(offset, par, in, w, use_norm), t);
    }
  out = kad_tanh(kad_add(t, b));
  (*out).pre = h0;
  return out;
}
kad_node_t *kad_sigm(kad_node_t *x);
kad_node_t *kad_1minus(kad_node_t *x);
kad_node_t *kann_layer_gru2(int *offset, kad_node_t **par, kad_node_t *in, kad_node_t *h0, int rnn_flag)
{
  kad_node_t *u;
  kad_node_t *b;
  kad_node_t *t;
  kad_node_t *w;
  kad_node_t *z;
  kad_node_t *r;
  kad_node_t *s;
  kad_node_t *out;
  int n0 = 0;
  int n1 = (*h0).d[(*h0).n_d - 1];
  int use_norm = !!(rnn_flag & 2);
  if (in)
    {
      n0 = (*in).n_d >= 2 ? kad_len(in) / (*in).d[0] : kad_len(in);
    }
  u = kann_new_leaf2(offset, par, 1, 0.000000000000000000000000e+00f, 2, n1, n1);
  b = kann_new_leaf2(offset, par, 1, 0.000000000000000000000000e+00f, 1, n1);
  t = cmul_norm2(offset, par, h0, u, use_norm);
  if (in)
    {
      w = kann_new_leaf2(offset, par, 1, 0.000000000000000000000000e+00f, 2, n1, n0);
      t = kad_add(cmul_norm2(offset, par, in, w, use_norm), t);
    }
  z = kad_sigm(kad_add(t, b));
  u = kann_new_leaf2(offset, par, 1, 0.000000000000000000000000e+00f, 2, n1, n1);
  b = kann_new_leaf2(offset, par, 1, 0.000000000000000000000000e+00f, 1, n1);
  t = cmul_norm2(offset, par, h0, u, use_norm);
  if (in)
    {
      w = kann_new_leaf2(offset, par, 1, 0.000000000000000000000000e+00f, 2, n1, n0);
      t = kad_add(cmul_norm2(offset, par, in, w, use_norm), t);
    }
  r = kad_sigm(kad_add(t, b));
  u = kann_new_leaf2(offset, par, 1, 0.000000000000000000000000e+00f, 2, n1, n1);
  b = kann_new_leaf2(offset, par, 1, 0.000000000000000000000000e+00f, 1, n1);
  t = cmul_norm2(offset, par, kad_mul(r, h0), u, use_norm);
  if (in)
    {
      w = kann_new_leaf2(offset, par, 1, 0.000000000000000000000000e+00f, 2, n1, n0);
      t = kad_add(cmul_norm2(offset, par, in, w, use_norm), t);
    }
  s = kad_tanh(kad_add(t, b));
  out = kad_add(kad_mul(kad_1minus(z), s), kad_mul(z, h0));
  (*out).pre = h0;
  return out;
}
kad_node_t *kann_new_leaf(uint8_t flag, float x0_01, int n_d, ...)
{
  va_list ap;
  int32_t i;
  int32_t d[4L];
  __builtin_va_start(ap, n_d);
  for (i = 0; i < n_d;  ++i)
    {
      d[i] = (__builtin_va_arg(ap, int));
    }
  __builtin_va_end(ap);
  return kann_new_leaf_array(0, 0, flag, x0_01, n_d, d);
}
kad_node_t *kann_new_scalar(uint8_t flag, float x)
{
  return kann_new_leaf(flag, x, 0);
}
kad_node_t *kann_new_weight(int n_row, int n_col)
{
  return kann_new_leaf(1, 0.000000000000000000000000e+00f, 2, n_row, n_col);
}
kad_node_t *kann_new_vec(int n, float x)
{
  return kann_new_leaf(1, x, 1, n);
}
kad_node_t *kann_new_bias(int n)
{
  return kann_new_vec(n, 0.000000000000000000000000e+00f);
}
kad_node_t *kann_new_weight_conv2d(int n_out, int n_in, int k_row, int k_col)
{
  return kann_new_leaf(1, 0.000000000000000000000000e+00f, 4, n_out, n_in, k_row, k_col);
}
kad_node_t *kann_new_weight_conv1d(int n_out, int n_in, int kernel_len)
{
  return kann_new_leaf(1, 0.000000000000000000000000e+00f, 3, n_out, n_in, kernel_len);
}
kad_node_t *kad_feed(int n_d, ...);
kad_node_t *kann_layer_input(int n1)
{
  kad_node_t *t;
  (t = kad_feed(2, 1, n1), (*t).ext_flag |= 1);
  return t;
}
kad_node_t *kann_layer_dense(kad_node_t *in, int n1)
{
  return kann_layer_dense2(0, 0, in, n1);
}
kad_node_t *kann_layer_dropout(kad_node_t *t, float r)
{
  return kann_layer_dropout2(0, 0, t, r);
}
kad_node_t *kann_layer_layernorm(kad_node_t *in)
{
  return kann_layer_layernorm2(0, 0, in);
}
kad_node_t *kad_var(float *x, float *g, int n_d, ...);
kad_node_t *kad_const(float *x, int n_d, ...);
kad_node_t *kann_layer_rnn(kad_node_t *in, int n1, int rnn_flag)
{
  kad_node_t *h0;
  h0 = rnn_flag & 1 ? kad_var(0, 0, 2, 1, n1) : kad_const(0, 2, 1, n1);
  (*h0).x = (float *)calloc(n1, sizeof(float));
  return kann_layer_rnn2(0, 0, in, h0, rnn_flag);
}
kad_node_t *kann_layer_gru(kad_node_t *in, int n1, int rnn_flag)
{
  kad_node_t *h0;
  h0 = rnn_flag & 1 ? kad_var(0, 0, 2, 1, n1) : kad_const(0, 2, 1, n1);
  (*h0).x = (float *)calloc(n1, sizeof(float));
  return kann_layer_gru2(0, 0, in, h0, rnn_flag);
}
static kad_node_t *kann_cmul_norm(kad_node_t *x, kad_node_t *w)
{
  return kann_layer_layernorm(kad_cmul(x, w));
}
kad_node_t *kann_layer_lstm(kad_node_t *in, int n1, int rnn_flag)
{
  int n0;
  kad_node_t *h0;
  kad_node_t *c0;
  kad_node_t *w;
  kad_node_t *u;
  kad_node_t *b;
  kad_node_t *i;
  kad_node_t *f;
  kad_node_t *o;
  kad_node_t *g;
  kad_node_t *c;
  kad_node_t *out;
  kad_node_t *(*cmul)(kad_node_t *, kad_node_t *) = rnn_flag & 2 ? kann_cmul_norm : kad_cmul;
  n0 = (*in).n_d >= 2 ? kad_len(in) / (*in).d[0] : kad_len(in);
  h0 = rnn_flag & 1 ? kad_var(0, 0, 2, 1, n1) : kad_const(0, 2, 1, n1);
  (*h0).x = (float *)calloc(n1, sizeof(float));
  c0 = rnn_flag & 1 ? kad_var(0, 0, 2, 1, n1) : kad_const(0, 2, 1, n1);
  (*c0).x = (float *)calloc(n1, sizeof(float));
  w = kann_new_weight(n1, n0);
  u = kann_new_weight(n1, n1);
  b = kann_new_bias(n1);
  i = kad_sigm(kad_add(kad_add((*cmul)(in, w), (*cmul)(h0, u)), b));
  w = kann_new_weight(n1, n0);
  u = kann_new_weight(n1, n1);
  b = kann_new_vec(n1, 1.000000000000000000000000e+00f);
  f = kad_sigm(kad_add(kad_add((*cmul)(in, w), (*cmul)(h0, u)), b));
  w = kann_new_weight(n1, n0);
  u = kann_new_weight(n1, n1);
  b = kann_new_bias(n1);
  o = kad_sigm(kad_add(kad_add((*cmul)(in, w), (*cmul)(h0, u)), b));
  w = kann_new_weight(n1, n0);
  u = kann_new_weight(n1, n1);
  b = kann_new_bias(n1);
  g = kad_tanh(kad_add(kad_add((*cmul)(in, w), (*cmul)(h0, u)), b));
  c = kad_add(kad_mul(f, c0), kad_mul(g, i));
  (*c).pre = c0;
  if (rnn_flag & 2)
    {
      c = kann_layer_layernorm(c);
    }
  out = kad_mul(kad_tanh(c), o);
  (*out).pre = h0;
  return out;
}
kad_node_t *kad_conv2d(kad_node_t *x, kad_node_t *w, int r_stride, int c_stride, int r_pad, int c_pad);
kad_node_t *kann_layer_conv2d(kad_node_t *in, int n_flt, int k_rows, int k_cols, int stride_r, int stride_c, int pad_r, int pad_c)
{
  kad_node_t *w;
  w = kann_new_weight_conv2d(n_flt, (*in).d[1], k_rows, k_cols);
  return kad_conv2d(in, w, stride_r, stride_c, pad_r, pad_c);
}
kad_node_t *kad_conv1d(kad_node_t *x, kad_node_t *w, int stride, int pad);
kad_node_t *kann_layer_conv1d(kad_node_t *in, int n_flt, int k_size, int stride, int pad)
{
  kad_node_t *w;
  w = kann_new_weight_conv1d(n_flt, (*in).d[1], k_size);
  return kad_conv1d(in, w, stride, pad);
}
kad_node_t *kad_mse(kad_node_t *x, kad_node_t *y);
kad_node_t *kad_ce_bin(kad_node_t *x, kad_node_t *y);
kad_node_t *kad_ce_bin_neg(kad_node_t *x, kad_node_t *y);
kad_node_t *kad_softmax(kad_node_t *x);
kad_node_t *kad_ce_multi(kad_node_t *x, kad_node_t *y);
kad_node_t *kann_layer_cost(kad_node_t *t, int n_out, int cost_type)
{
  static const char __MERCURIUM_PRETTY_FUNCTION__[52L] = "kad_node_t *kann_layer_cost(kad_node_t *, int, int)";
  kad_node_t *cost = 0;
  kad_node_t *truth = 0;
  ((cost_type == 1 || cost_type == 2) || cost_type == 3) || cost_type == 4 ? (void)0 : __assert_fail("cost_type == 1 || cost_type == 2 || cost_type == 3 || cost_type == 4", "kann_parallel.c", 841, __MERCURIUM_PRETTY_FUNCTION__);
  t = kann_layer_dense(t, n_out);
  (truth = kad_feed(2, 1, n_out), (*truth).ext_flag |= 4);
  if (cost_type == 4)
    {
      cost = kad_mse(t, truth);
    }
  else
    {
      if (cost_type == 1)
        {
          t = kad_sigm(t);
          cost = kad_ce_bin(t, truth);
        }
      else
        {
          if (cost_type == 3)
            {
              t = kad_tanh(t);
              cost = kad_ce_bin_neg(t, truth);
            }
          else
            {
              if (cost_type == 2)
                {
                  t = kad_softmax(t);
                  cost = kad_ce_multi(t, truth);
                }
            }
        }
    }
  ((*t).ext_flag |= 2, (*cost).ext_flag |= 8);
  return cost;
}
double kad_drand(void *d);
void kann_shuffle(int n, int *s)
{
  int i;
  int j;
  int t;
  for (i = 0; i < n;  ++i)
    {
      s[i] = i;
    }
  for (i = n; i > 0;  --i)
    {
      j = (int)(i * kad_drand(0));
      (((t = s[j], s[j] = s[i - 1])), s[i - 1] = t);
    }
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) void _mm_empty(void)
{
  __builtin_ia32_emms();
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) void _m_empty(void)
{
  _mm_empty();
}
typedef __attribute__((vector_size(8))) __attribute__((__may_alias__)) int __m64;
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_cvtsi32_si64(int __i)
{
  return (__m64)__builtin_ia32_vec_init_v2si(__i, 0);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _m_from_int(int __i)
{
  return _mm_cvtsi32_si64(__i);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _m_from_int64(long long int __i)
{
  return (__m64)__i;
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_cvtsi64_m64(long long int __i)
{
  return (__m64)__i;
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_cvtsi64x_si64(long long int __i)
{
  return (__m64)__i;
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_set_pi64x(long long int __i)
{
  return (__m64)__i;
}
typedef __attribute__((vector_size(8))) __attribute__((__may_alias__)) int __v2si;
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) int _mm_cvtsi64_si32(__m64 __i)
{
  return __builtin_ia32_vec_ext_v2si((__v2si)__i, 0);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) int _m_to_int(__m64 __i)
{
  return _mm_cvtsi64_si32(__i);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) long long int _m_to_int64(__m64 __i)
{
  return (long long int)__i;
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) long long int _mm_cvtm64_si64(__m64 __i)
{
  return (long long int)__i;
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) long long int _mm_cvtsi64_si64x(__m64 __i)
{
  return (long long int)__i;
}
typedef __attribute__((vector_size(8))) __attribute__((__may_alias__)) short int __v4hi;
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_packs_pi16(__m64 __m1, __m64 __m2)
{
  return (__m64)__builtin_ia32_packsswb((__v4hi)__m1, (__v4hi)__m2);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _m_packsswb(__m64 __m1, __m64 __m2)
{
  return _mm_packs_pi16(__m1, __m2);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_packs_pi32(__m64 __m1, __m64 __m2)
{
  return (__m64)__builtin_ia32_packssdw((__v2si)__m1, (__v2si)__m2);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _m_packssdw(__m64 __m1, __m64 __m2)
{
  return _mm_packs_pi32(__m1, __m2);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_packs_pu16(__m64 __m1, __m64 __m2)
{
  return (__m64)__builtin_ia32_packuswb((__v4hi)__m1, (__v4hi)__m2);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _m_packuswb(__m64 __m1, __m64 __m2)
{
  return _mm_packs_pu16(__m1, __m2);
}
typedef __attribute__((vector_size(8))) __attribute__((__may_alias__)) char __v8qi;
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_unpackhi_pi8(__m64 __m1, __m64 __m2)
{
  return (__m64)__builtin_ia32_punpckhbw((__v8qi)__m1, (__v8qi)__m2);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _m_punpckhbw(__m64 __m1, __m64 __m2)
{
  return _mm_unpackhi_pi8(__m1, __m2);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_unpackhi_pi16(__m64 __m1, __m64 __m2)
{
  return (__m64)__builtin_ia32_punpckhwd((__v4hi)__m1, (__v4hi)__m2);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _m_punpckhwd(__m64 __m1, __m64 __m2)
{
  return _mm_unpackhi_pi16(__m1, __m2);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_unpackhi_pi32(__m64 __m1, __m64 __m2)
{
  return (__m64)__builtin_ia32_punpckhdq((__v2si)__m1, (__v2si)__m2);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _m_punpckhdq(__m64 __m1, __m64 __m2)
{
  return _mm_unpackhi_pi32(__m1, __m2);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_unpacklo_pi8(__m64 __m1, __m64 __m2)
{
  return (__m64)__builtin_ia32_punpcklbw((__v8qi)__m1, (__v8qi)__m2);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _m_punpcklbw(__m64 __m1, __m64 __m2)
{
  return _mm_unpacklo_pi8(__m1, __m2);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_unpacklo_pi16(__m64 __m1, __m64 __m2)
{
  return (__m64)__builtin_ia32_punpcklwd((__v4hi)__m1, (__v4hi)__m2);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _m_punpcklwd(__m64 __m1, __m64 __m2)
{
  return _mm_unpacklo_pi16(__m1, __m2);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_unpacklo_pi32(__m64 __m1, __m64 __m2)
{
  return (__m64)__builtin_ia32_punpckldq((__v2si)__m1, (__v2si)__m2);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _m_punpckldq(__m64 __m1, __m64 __m2)
{
  return _mm_unpacklo_pi32(__m1, __m2);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_add_pi8(__m64 __m1, __m64 __m2)
{
  return (__m64)__builtin_ia32_paddb((__v8qi)__m1, (__v8qi)__m2);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _m_paddb(__m64 __m1, __m64 __m2)
{
  return _mm_add_pi8(__m1, __m2);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_add_pi16(__m64 __m1, __m64 __m2)
{
  return (__m64)__builtin_ia32_paddw((__v4hi)__m1, (__v4hi)__m2);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _m_paddw(__m64 __m1, __m64 __m2)
{
  return _mm_add_pi16(__m1, __m2);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_add_pi32(__m64 __m1, __m64 __m2)
{
  return (__m64)__builtin_ia32_paddd((__v2si)__m1, (__v2si)__m2);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _m_paddd(__m64 __m1, __m64 __m2)
{
  return _mm_add_pi32(__m1, __m2);
}
typedef __attribute__((vector_size(8))) __attribute__((__may_alias__)) long long int __v1di;
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_add_si64(__m64 __m1, __m64 __m2)
{
  return (__m64)__builtin_ia32_paddq((__v1di)__m1, (__v1di)__m2);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_adds_pi8(__m64 __m1, __m64 __m2)
{
  return (__m64)__builtin_ia32_paddsb((__v8qi)__m1, (__v8qi)__m2);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _m_paddsb(__m64 __m1, __m64 __m2)
{
  return _mm_adds_pi8(__m1, __m2);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_adds_pi16(__m64 __m1, __m64 __m2)
{
  return (__m64)__builtin_ia32_paddsw((__v4hi)__m1, (__v4hi)__m2);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _m_paddsw(__m64 __m1, __m64 __m2)
{
  return _mm_adds_pi16(__m1, __m2);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_adds_pu8(__m64 __m1, __m64 __m2)
{
  return (__m64)__builtin_ia32_paddusb((__v8qi)__m1, (__v8qi)__m2);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _m_paddusb(__m64 __m1, __m64 __m2)
{
  return _mm_adds_pu8(__m1, __m2);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_adds_pu16(__m64 __m1, __m64 __m2)
{
  return (__m64)__builtin_ia32_paddusw((__v4hi)__m1, (__v4hi)__m2);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _m_paddusw(__m64 __m1, __m64 __m2)
{
  return _mm_adds_pu16(__m1, __m2);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_sub_pi8(__m64 __m1, __m64 __m2)
{
  return (__m64)__builtin_ia32_psubb((__v8qi)__m1, (__v8qi)__m2);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _m_psubb(__m64 __m1, __m64 __m2)
{
  return _mm_sub_pi8(__m1, __m2);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_sub_pi16(__m64 __m1, __m64 __m2)
{
  return (__m64)__builtin_ia32_psubw((__v4hi)__m1, (__v4hi)__m2);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _m_psubw(__m64 __m1, __m64 __m2)
{
  return _mm_sub_pi16(__m1, __m2);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_sub_pi32(__m64 __m1, __m64 __m2)
{
  return (__m64)__builtin_ia32_psubd((__v2si)__m1, (__v2si)__m2);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _m_psubd(__m64 __m1, __m64 __m2)
{
  return _mm_sub_pi32(__m1, __m2);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_sub_si64(__m64 __m1, __m64 __m2)
{
  return (__m64)__builtin_ia32_psubq((__v1di)__m1, (__v1di)__m2);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_subs_pi8(__m64 __m1, __m64 __m2)
{
  return (__m64)__builtin_ia32_psubsb((__v8qi)__m1, (__v8qi)__m2);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _m_psubsb(__m64 __m1, __m64 __m2)
{
  return _mm_subs_pi8(__m1, __m2);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_subs_pi16(__m64 __m1, __m64 __m2)
{
  return (__m64)__builtin_ia32_psubsw((__v4hi)__m1, (__v4hi)__m2);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _m_psubsw(__m64 __m1, __m64 __m2)
{
  return _mm_subs_pi16(__m1, __m2);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_subs_pu8(__m64 __m1, __m64 __m2)
{
  return (__m64)__builtin_ia32_psubusb((__v8qi)__m1, (__v8qi)__m2);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _m_psubusb(__m64 __m1, __m64 __m2)
{
  return _mm_subs_pu8(__m1, __m2);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_subs_pu16(__m64 __m1, __m64 __m2)
{
  return (__m64)__builtin_ia32_psubusw((__v4hi)__m1, (__v4hi)__m2);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _m_psubusw(__m64 __m1, __m64 __m2)
{
  return _mm_subs_pu16(__m1, __m2);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_madd_pi16(__m64 __m1, __m64 __m2)
{
  return (__m64)__builtin_ia32_pmaddwd((__v4hi)__m1, (__v4hi)__m2);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _m_pmaddwd(__m64 __m1, __m64 __m2)
{
  return _mm_madd_pi16(__m1, __m2);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_mulhi_pi16(__m64 __m1, __m64 __m2)
{
  return (__m64)__builtin_ia32_pmulhw((__v4hi)__m1, (__v4hi)__m2);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _m_pmulhw(__m64 __m1, __m64 __m2)
{
  return _mm_mulhi_pi16(__m1, __m2);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_mullo_pi16(__m64 __m1, __m64 __m2)
{
  return (__m64)__builtin_ia32_pmullw((__v4hi)__m1, (__v4hi)__m2);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _m_pmullw(__m64 __m1, __m64 __m2)
{
  return _mm_mullo_pi16(__m1, __m2);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_sll_pi16(__m64 __m, __m64 __count)
{
  return (__m64)__builtin_ia32_psllw((__v4hi)__m, (__v4hi)__count);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _m_psllw(__m64 __m, __m64 __count)
{
  return _mm_sll_pi16(__m, __count);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_slli_pi16(__m64 __m, int __count)
{
  return (__m64)__builtin_ia32_psllwi((__v4hi)__m, __count);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _m_psllwi(__m64 __m, int __count)
{
  return _mm_slli_pi16(__m, __count);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_sll_pi32(__m64 __m, __m64 __count)
{
  return (__m64)__builtin_ia32_pslld((__v2si)__m, (__v2si)__count);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _m_pslld(__m64 __m, __m64 __count)
{
  return _mm_sll_pi32(__m, __count);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_slli_pi32(__m64 __m, int __count)
{
  return (__m64)__builtin_ia32_pslldi((__v2si)__m, __count);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _m_pslldi(__m64 __m, int __count)
{
  return _mm_slli_pi32(__m, __count);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_sll_si64(__m64 __m, __m64 __count)
{
  return (__m64)__builtin_ia32_psllq((__v1di)__m, (__v1di)__count);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _m_psllq(__m64 __m, __m64 __count)
{
  return _mm_sll_si64(__m, __count);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_slli_si64(__m64 __m, int __count)
{
  return (__m64)__builtin_ia32_psllqi((__v1di)__m, __count);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _m_psllqi(__m64 __m, int __count)
{
  return _mm_slli_si64(__m, __count);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_sra_pi16(__m64 __m, __m64 __count)
{
  return (__m64)__builtin_ia32_psraw((__v4hi)__m, (__v4hi)__count);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _m_psraw(__m64 __m, __m64 __count)
{
  return _mm_sra_pi16(__m, __count);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_srai_pi16(__m64 __m, int __count)
{
  return (__m64)__builtin_ia32_psrawi((__v4hi)__m, __count);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _m_psrawi(__m64 __m, int __count)
{
  return _mm_srai_pi16(__m, __count);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_sra_pi32(__m64 __m, __m64 __count)
{
  return (__m64)__builtin_ia32_psrad((__v2si)__m, (__v2si)__count);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _m_psrad(__m64 __m, __m64 __count)
{
  return _mm_sra_pi32(__m, __count);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_srai_pi32(__m64 __m, int __count)
{
  return (__m64)__builtin_ia32_psradi((__v2si)__m, __count);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _m_psradi(__m64 __m, int __count)
{
  return _mm_srai_pi32(__m, __count);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_srl_pi16(__m64 __m, __m64 __count)
{
  return (__m64)__builtin_ia32_psrlw((__v4hi)__m, (__v4hi)__count);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _m_psrlw(__m64 __m, __m64 __count)
{
  return _mm_srl_pi16(__m, __count);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_srli_pi16(__m64 __m, int __count)
{
  return (__m64)__builtin_ia32_psrlwi((__v4hi)__m, __count);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _m_psrlwi(__m64 __m, int __count)
{
  return _mm_srli_pi16(__m, __count);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_srl_pi32(__m64 __m, __m64 __count)
{
  return (__m64)__builtin_ia32_psrld((__v2si)__m, (__v2si)__count);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _m_psrld(__m64 __m, __m64 __count)
{
  return _mm_srl_pi32(__m, __count);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_srli_pi32(__m64 __m, int __count)
{
  return (__m64)__builtin_ia32_psrldi((__v2si)__m, __count);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _m_psrldi(__m64 __m, int __count)
{
  return _mm_srli_pi32(__m, __count);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_srl_si64(__m64 __m, __m64 __count)
{
  return (__m64)__builtin_ia32_psrlq((__v1di)__m, (__v1di)__count);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _m_psrlq(__m64 __m, __m64 __count)
{
  return _mm_srl_si64(__m, __count);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_srli_si64(__m64 __m, int __count)
{
  return (__m64)__builtin_ia32_psrlqi((__v1di)__m, __count);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _m_psrlqi(__m64 __m, int __count)
{
  return _mm_srli_si64(__m, __count);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_and_si64(__m64 __m1, __m64 __m2)
{
  return __builtin_ia32_pand(__m1, __m2);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _m_pand(__m64 __m1, __m64 __m2)
{
  return _mm_and_si64(__m1, __m2);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_andnot_si64(__m64 __m1, __m64 __m2)
{
  return __builtin_ia32_pandn(__m1, __m2);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _m_pandn(__m64 __m1, __m64 __m2)
{
  return _mm_andnot_si64(__m1, __m2);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_or_si64(__m64 __m1, __m64 __m2)
{
  return __builtin_ia32_por(__m1, __m2);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _m_por(__m64 __m1, __m64 __m2)
{
  return _mm_or_si64(__m1, __m2);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_xor_si64(__m64 __m1, __m64 __m2)
{
  return __builtin_ia32_pxor(__m1, __m2);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _m_pxor(__m64 __m1, __m64 __m2)
{
  return _mm_xor_si64(__m1, __m2);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_cmpeq_pi8(__m64 __m1, __m64 __m2)
{
  return (__m64)__builtin_ia32_pcmpeqb((__v8qi)__m1, (__v8qi)__m2);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _m_pcmpeqb(__m64 __m1, __m64 __m2)
{
  return _mm_cmpeq_pi8(__m1, __m2);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_cmpgt_pi8(__m64 __m1, __m64 __m2)
{
  return (__m64)__builtin_ia32_pcmpgtb((__v8qi)__m1, (__v8qi)__m2);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _m_pcmpgtb(__m64 __m1, __m64 __m2)
{
  return _mm_cmpgt_pi8(__m1, __m2);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_cmpeq_pi16(__m64 __m1, __m64 __m2)
{
  return (__m64)__builtin_ia32_pcmpeqw((__v4hi)__m1, (__v4hi)__m2);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _m_pcmpeqw(__m64 __m1, __m64 __m2)
{
  return _mm_cmpeq_pi16(__m1, __m2);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_cmpgt_pi16(__m64 __m1, __m64 __m2)
{
  return (__m64)__builtin_ia32_pcmpgtw((__v4hi)__m1, (__v4hi)__m2);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _m_pcmpgtw(__m64 __m1, __m64 __m2)
{
  return _mm_cmpgt_pi16(__m1, __m2);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_cmpeq_pi32(__m64 __m1, __m64 __m2)
{
  return (__m64)__builtin_ia32_pcmpeqd((__v2si)__m1, (__v2si)__m2);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _m_pcmpeqd(__m64 __m1, __m64 __m2)
{
  return _mm_cmpeq_pi32(__m1, __m2);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_cmpgt_pi32(__m64 __m1, __m64 __m2)
{
  return (__m64)__builtin_ia32_pcmpgtd((__v2si)__m1, (__v2si)__m2);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _m_pcmpgtd(__m64 __m1, __m64 __m2)
{
  return _mm_cmpgt_pi32(__m1, __m2);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_setzero_si64(void)
{
  return (__m64)0LL;
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_set_pi32(int __i1, int __i0)
{
  return (__m64)__builtin_ia32_vec_init_v2si(__i0, __i1);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_set_pi16(short int __w3, short int __w2, short int __w1, short int __w0)
{
  return (__m64)__builtin_ia32_vec_init_v4hi(__w0, __w1, __w2, __w3);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_set_pi8(char __b7, char __b6, char __b5, char __b4, char __b3, char __b2, char __b1, char __b0)
{
  return (__m64)__builtin_ia32_vec_init_v8qi(__b0, __b1, __b2, __b3, __b4, __b5, __b6, __b7);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_setr_pi32(int __i0, int __i1)
{
  return _mm_set_pi32(__i1, __i0);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_setr_pi16(short int __w0, short int __w1, short int __w2, short int __w3)
{
  return _mm_set_pi16(__w3, __w2, __w1, __w0);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_setr_pi8(char __b0, char __b1, char __b2, char __b3, char __b4, char __b5, char __b6, char __b7)
{
  return _mm_set_pi8(__b7, __b6, __b5, __b4, __b3, __b2, __b1, __b0);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_set1_pi32(int __i)
{
  return _mm_set_pi32(__i, __i);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_set1_pi16(short int __w)
{
  return _mm_set_pi16(__w, __w, __w, __w);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_set1_pi8(char __b)
{
  return _mm_set_pi8(__b, __b, __b, __b, __b, __b, __b, __b);
}
extern int posix_memalign(void **__memptr, size_t __alignment, size_t __size) __attribute__((__nothrow__)) __attribute__((__leaf__)) __attribute__((__nonnull__(1)));
static __inline void *_mm_malloc(size_t size, size_t alignment)
{
  void *ptr;
  if (alignment == 1)
    {
      return malloc(size);
    }
  if (alignment == 2 || (sizeof(void *) == 8 && alignment == 4))
    {
      alignment = sizeof(void *);
    }
  if (posix_memalign(&ptr, alignment, size) == 0)
    {
      return ptr;
    }
  else
    {
      return (void *)0;
    }
}
static __inline void _mm_free(void *ptr)
{
  free(ptr);
}
typedef __attribute__((vector_size(16))) __attribute__((__may_alias__)) float __m128;
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128 _mm_setzero_ps(void)
{
  return (__m128){0.000000000000000000000000e+00f, 0.000000000000000000000000e+00f, 0.000000000000000000000000e+00f, 0.000000000000000000000000e+00f};
}
typedef __attribute__((vector_size(16))) __attribute__((__may_alias__)) float __v4sf;
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128 _mm_add_ss(__m128 __A, __m128 __B)
{
  return (__m128)__builtin_ia32_addss((__v4sf)__A, (__v4sf)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128 _mm_sub_ss(__m128 __A, __m128 __B)
{
  return (__m128)__builtin_ia32_subss((__v4sf)__A, (__v4sf)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128 _mm_mul_ss(__m128 __A, __m128 __B)
{
  return (__m128)__builtin_ia32_mulss((__v4sf)__A, (__v4sf)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128 _mm_div_ss(__m128 __A, __m128 __B)
{
  return (__m128)__builtin_ia32_divss((__v4sf)__A, (__v4sf)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128 _mm_sqrt_ss(__m128 __A)
{
  return (__m128)__builtin_ia32_sqrtss((__v4sf)__A);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128 _mm_rcp_ss(__m128 __A)
{
  return (__m128)__builtin_ia32_rcpss((__v4sf)__A);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128 _mm_rsqrt_ss(__m128 __A)
{
  return (__m128)__builtin_ia32_rsqrtss((__v4sf)__A);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128 _mm_min_ss(__m128 __A, __m128 __B)
{
  return (__m128)__builtin_ia32_minss((__v4sf)__A, (__v4sf)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128 _mm_max_ss(__m128 __A, __m128 __B)
{
  return (__m128)__builtin_ia32_maxss((__v4sf)__A, (__v4sf)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128 _mm_add_ps(__m128 __A, __m128 __B)
{
  return (__m128)__builtin_ia32_addps((__v4sf)__A, (__v4sf)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128 _mm_sub_ps(__m128 __A, __m128 __B)
{
  return (__m128)__builtin_ia32_subps((__v4sf)__A, (__v4sf)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128 _mm_mul_ps(__m128 __A, __m128 __B)
{
  return (__m128)__builtin_ia32_mulps((__v4sf)__A, (__v4sf)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128 _mm_div_ps(__m128 __A, __m128 __B)
{
  return (__m128)__builtin_ia32_divps((__v4sf)__A, (__v4sf)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128 _mm_sqrt_ps(__m128 __A)
{
  return (__m128)__builtin_ia32_sqrtps((__v4sf)__A);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128 _mm_rcp_ps(__m128 __A)
{
  return (__m128)__builtin_ia32_rcpps((__v4sf)__A);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128 _mm_rsqrt_ps(__m128 __A)
{
  return (__m128)__builtin_ia32_rsqrtps((__v4sf)__A);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128 _mm_min_ps(__m128 __A, __m128 __B)
{
  return (__m128)__builtin_ia32_minps((__v4sf)__A, (__v4sf)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128 _mm_max_ps(__m128 __A, __m128 __B)
{
  return (__m128)__builtin_ia32_maxps((__v4sf)__A, (__v4sf)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128 _mm_and_ps(__m128 __A, __m128 __B)
{
  return __builtin_ia32_andps(__A, __B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128 _mm_andnot_ps(__m128 __A, __m128 __B)
{
  return __builtin_ia32_andnps(__A, __B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128 _mm_or_ps(__m128 __A, __m128 __B)
{
  return __builtin_ia32_orps(__A, __B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128 _mm_xor_ps(__m128 __A, __m128 __B)
{
  return __builtin_ia32_xorps(__A, __B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128 _mm_cmpeq_ss(__m128 __A, __m128 __B)
{
  return (__m128)__builtin_ia32_cmpeqss((__v4sf)__A, (__v4sf)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128 _mm_cmplt_ss(__m128 __A, __m128 __B)
{
  return (__m128)__builtin_ia32_cmpltss((__v4sf)__A, (__v4sf)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128 _mm_cmple_ss(__m128 __A, __m128 __B)
{
  return (__m128)__builtin_ia32_cmpless((__v4sf)__A, (__v4sf)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128 _mm_cmpgt_ss(__m128 __A, __m128 __B)
{
  return (__m128)__builtin_ia32_movss((__v4sf)__A, (__v4sf)__builtin_ia32_cmpltss((__v4sf)__B, (__v4sf)__A));
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128 _mm_cmpge_ss(__m128 __A, __m128 __B)
{
  return (__m128)__builtin_ia32_movss((__v4sf)__A, (__v4sf)__builtin_ia32_cmpless((__v4sf)__B, (__v4sf)__A));
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128 _mm_cmpneq_ss(__m128 __A, __m128 __B)
{
  return (__m128)__builtin_ia32_cmpneqss((__v4sf)__A, (__v4sf)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128 _mm_cmpnlt_ss(__m128 __A, __m128 __B)
{
  return (__m128)__builtin_ia32_cmpnltss((__v4sf)__A, (__v4sf)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128 _mm_cmpnle_ss(__m128 __A, __m128 __B)
{
  return (__m128)__builtin_ia32_cmpnless((__v4sf)__A, (__v4sf)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128 _mm_cmpngt_ss(__m128 __A, __m128 __B)
{
  return (__m128)__builtin_ia32_movss((__v4sf)__A, (__v4sf)__builtin_ia32_cmpnltss((__v4sf)__B, (__v4sf)__A));
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128 _mm_cmpnge_ss(__m128 __A, __m128 __B)
{
  return (__m128)__builtin_ia32_movss((__v4sf)__A, (__v4sf)__builtin_ia32_cmpnless((__v4sf)__B, (__v4sf)__A));
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128 _mm_cmpord_ss(__m128 __A, __m128 __B)
{
  return (__m128)__builtin_ia32_cmpordss((__v4sf)__A, (__v4sf)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128 _mm_cmpunord_ss(__m128 __A, __m128 __B)
{
  return (__m128)__builtin_ia32_cmpunordss((__v4sf)__A, (__v4sf)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128 _mm_cmpeq_ps(__m128 __A, __m128 __B)
{
  return (__m128)__builtin_ia32_cmpeqps((__v4sf)__A, (__v4sf)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128 _mm_cmplt_ps(__m128 __A, __m128 __B)
{
  return (__m128)__builtin_ia32_cmpltps((__v4sf)__A, (__v4sf)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128 _mm_cmple_ps(__m128 __A, __m128 __B)
{
  return (__m128)__builtin_ia32_cmpleps((__v4sf)__A, (__v4sf)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128 _mm_cmpgt_ps(__m128 __A, __m128 __B)
{
  return (__m128)__builtin_ia32_cmpgtps((__v4sf)__A, (__v4sf)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128 _mm_cmpge_ps(__m128 __A, __m128 __B)
{
  return (__m128)__builtin_ia32_cmpgeps((__v4sf)__A, (__v4sf)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128 _mm_cmpneq_ps(__m128 __A, __m128 __B)
{
  return (__m128)__builtin_ia32_cmpneqps((__v4sf)__A, (__v4sf)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128 _mm_cmpnlt_ps(__m128 __A, __m128 __B)
{
  return (__m128)__builtin_ia32_cmpnltps((__v4sf)__A, (__v4sf)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128 _mm_cmpnle_ps(__m128 __A, __m128 __B)
{
  return (__m128)__builtin_ia32_cmpnleps((__v4sf)__A, (__v4sf)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128 _mm_cmpngt_ps(__m128 __A, __m128 __B)
{
  return (__m128)__builtin_ia32_cmpngtps((__v4sf)__A, (__v4sf)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128 _mm_cmpnge_ps(__m128 __A, __m128 __B)
{
  return (__m128)__builtin_ia32_cmpngeps((__v4sf)__A, (__v4sf)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128 _mm_cmpord_ps(__m128 __A, __m128 __B)
{
  return (__m128)__builtin_ia32_cmpordps((__v4sf)__A, (__v4sf)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128 _mm_cmpunord_ps(__m128 __A, __m128 __B)
{
  return (__m128)__builtin_ia32_cmpunordps((__v4sf)__A, (__v4sf)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) int _mm_comieq_ss(__m128 __A, __m128 __B)
{
  return __builtin_ia32_comieq((__v4sf)__A, (__v4sf)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) int _mm_comilt_ss(__m128 __A, __m128 __B)
{
  return __builtin_ia32_comilt((__v4sf)__A, (__v4sf)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) int _mm_comile_ss(__m128 __A, __m128 __B)
{
  return __builtin_ia32_comile((__v4sf)__A, (__v4sf)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) int _mm_comigt_ss(__m128 __A, __m128 __B)
{
  return __builtin_ia32_comigt((__v4sf)__A, (__v4sf)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) int _mm_comige_ss(__m128 __A, __m128 __B)
{
  return __builtin_ia32_comige((__v4sf)__A, (__v4sf)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) int _mm_comineq_ss(__m128 __A, __m128 __B)
{
  return __builtin_ia32_comineq((__v4sf)__A, (__v4sf)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) int _mm_ucomieq_ss(__m128 __A, __m128 __B)
{
  return __builtin_ia32_ucomieq((__v4sf)__A, (__v4sf)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) int _mm_ucomilt_ss(__m128 __A, __m128 __B)
{
  return __builtin_ia32_ucomilt((__v4sf)__A, (__v4sf)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) int _mm_ucomile_ss(__m128 __A, __m128 __B)
{
  return __builtin_ia32_ucomile((__v4sf)__A, (__v4sf)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) int _mm_ucomigt_ss(__m128 __A, __m128 __B)
{
  return __builtin_ia32_ucomigt((__v4sf)__A, (__v4sf)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) int _mm_ucomige_ss(__m128 __A, __m128 __B)
{
  return __builtin_ia32_ucomige((__v4sf)__A, (__v4sf)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) int _mm_ucomineq_ss(__m128 __A, __m128 __B)
{
  return __builtin_ia32_ucomineq((__v4sf)__A, (__v4sf)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) int _mm_cvtss_si32(__m128 __A)
{
  return __builtin_ia32_cvtss2si((__v4sf)__A);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) int _mm_cvt_ss2si(__m128 __A)
{
  return _mm_cvtss_si32(__A);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) long long int _mm_cvtss_si64(__m128 __A)
{
  return __builtin_ia32_cvtss2si64((__v4sf)__A);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) long long int _mm_cvtss_si64x(__m128 __A)
{
  return __builtin_ia32_cvtss2si64((__v4sf)__A);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_cvtps_pi32(__m128 __A)
{
  return (__m64)__builtin_ia32_cvtps2pi((__v4sf)__A);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_cvt_ps2pi(__m128 __A)
{
  return _mm_cvtps_pi32(__A);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) int _mm_cvttss_si32(__m128 __A)
{
  return __builtin_ia32_cvttss2si((__v4sf)__A);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) int _mm_cvtt_ss2si(__m128 __A)
{
  return _mm_cvttss_si32(__A);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) long long int _mm_cvttss_si64(__m128 __A)
{
  return __builtin_ia32_cvttss2si64((__v4sf)__A);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) long long int _mm_cvttss_si64x(__m128 __A)
{
  return __builtin_ia32_cvttss2si64((__v4sf)__A);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_cvttps_pi32(__m128 __A)
{
  return (__m64)__builtin_ia32_cvttps2pi((__v4sf)__A);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_cvtt_ps2pi(__m128 __A)
{
  return _mm_cvttps_pi32(__A);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128 _mm_cvtsi32_ss(__m128 __A, int __B)
{
  return (__m128)__builtin_ia32_cvtsi2ss((__v4sf)__A, __B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128 _mm_cvt_si2ss(__m128 __A, int __B)
{
  return _mm_cvtsi32_ss(__A, __B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128 _mm_cvtsi64_ss(__m128 __A, long long int __B)
{
  return (__m128)__builtin_ia32_cvtsi642ss((__v4sf)__A, __B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128 _mm_cvtsi64x_ss(__m128 __A, long long int __B)
{
  return (__m128)__builtin_ia32_cvtsi642ss((__v4sf)__A, __B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128 _mm_cvtpi32_ps(__m128 __A, __m64 __B)
{
  return (__m128)__builtin_ia32_cvtpi2ps((__v4sf)__A, (__v2si)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128 _mm_cvt_pi2ps(__m128 __A, __m64 __B)
{
  return _mm_cvtpi32_ps(__A, __B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128 _mm_cvtpi16_ps(__m64 __A)
{
  __v4hi __sign;
  __v2si __losi;
  __v2si __hisi;
  __v4sf __zero;
  __v4sf __ra;
  __v4sf __rb;
  __sign = __builtin_ia32_pcmpgtw((__v4hi)0LL, (__v4hi)__A);
  __losi = (__v2si)__builtin_ia32_punpcklwd((__v4hi)__A, __sign);
  __hisi = (__v2si)__builtin_ia32_punpckhwd((__v4hi)__A, __sign);
  __zero = (__v4sf)_mm_setzero_ps();
  __ra = __builtin_ia32_cvtpi2ps(__zero, __losi);
  __rb = __builtin_ia32_cvtpi2ps(__ra, __hisi);
  return (__m128)__builtin_ia32_movlhps(__ra, __rb);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128 _mm_cvtpu16_ps(__m64 __A)
{
  __v2si __losi;
  __v2si __hisi;
  __v4sf __zero;
  __v4sf __ra;
  __v4sf __rb;
  __losi = (__v2si)__builtin_ia32_punpcklwd((__v4hi)__A, (__v4hi)0LL);
  __hisi = (__v2si)__builtin_ia32_punpckhwd((__v4hi)__A, (__v4hi)0LL);
  __zero = (__v4sf)_mm_setzero_ps();
  __ra = __builtin_ia32_cvtpi2ps(__zero, __losi);
  __rb = __builtin_ia32_cvtpi2ps(__ra, __hisi);
  return (__m128)__builtin_ia32_movlhps(__ra, __rb);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128 _mm_cvtpi8_ps(__m64 __A)
{
  __v8qi __sign;
  __sign = __builtin_ia32_pcmpgtb((__v8qi)0LL, (__v8qi)__A);
  __A = (__m64)__builtin_ia32_punpcklbw((__v8qi)__A, __sign);
  return _mm_cvtpi16_ps(__A);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128 _mm_cvtpu8_ps(__m64 __A)
{
  __A = (__m64)__builtin_ia32_punpcklbw((__v8qi)__A, (__v8qi)0LL);
  return _mm_cvtpu16_ps(__A);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128 _mm_cvtpi32x2_ps(__m64 __A, __m64 __B)
{
  __v4sf __zero = (__v4sf)_mm_setzero_ps();
  __v4sf __sfa = __builtin_ia32_cvtpi2ps(__zero, (__v2si)__A);
  __v4sf __sfb = __builtin_ia32_cvtpi2ps(__sfa, (__v2si)__B);
  return (__m128)__builtin_ia32_movlhps(__sfa, __sfb);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_cvtps_pi16(__m128 __A)
{
  __v4sf __hisf = (__v4sf)__A;
  __v4sf __losf = __builtin_ia32_movhlps(__hisf, __hisf);
  __v2si __hisi = __builtin_ia32_cvtps2pi(__hisf);
  __v2si __losi = __builtin_ia32_cvtps2pi(__losf);
  return (__m64)__builtin_ia32_packssdw(__hisi, __losi);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_cvtps_pi8(__m128 __A)
{
  __v4hi __tmp = (__v4hi)_mm_cvtps_pi16(__A);
  return (__m64)__builtin_ia32_packsswb(__tmp, (__v4hi)0LL);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128 _mm_shuffle_ps(__m128 __A, __m128 __B, const int __mask)
{
  return (__m128)__builtin_ia32_shufps((__v4sf)__A, (__v4sf)__B, __mask);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128 _mm_unpackhi_ps(__m128 __A, __m128 __B)
{
  return (__m128)__builtin_ia32_unpckhps((__v4sf)__A, (__v4sf)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128 _mm_unpacklo_ps(__m128 __A, __m128 __B)
{
  return (__m128)__builtin_ia32_unpcklps((__v4sf)__A, (__v4sf)__B);
}
typedef __attribute__((vector_size(8))) __attribute__((__may_alias__)) float __v2sf;
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128 _mm_loadh_pi(__m128 __A, const __m64 *__P)
{
  return (__m128)__builtin_ia32_loadhps((__v4sf)__A, (const __v2sf *)__P);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) void _mm_storeh_pi(__m64 *__P, __m128 __A)
{
  __builtin_ia32_storehps((__v2sf *)__P, (__v4sf)__A);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128 _mm_movehl_ps(__m128 __A, __m128 __B)
{
  return (__m128)__builtin_ia32_movhlps((__v4sf)__A, (__v4sf)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128 _mm_movelh_ps(__m128 __A, __m128 __B)
{
  return (__m128)__builtin_ia32_movlhps((__v4sf)__A, (__v4sf)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128 _mm_loadl_pi(__m128 __A, const __m64 *__P)
{
  return (__m128)__builtin_ia32_loadlps((__v4sf)__A, (const __v2sf *)__P);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) void _mm_storel_pi(__m64 *__P, __m128 __A)
{
  __builtin_ia32_storelps((__v2sf *)__P, (__v4sf)__A);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) int _mm_movemask_ps(__m128 __A)
{
  return __builtin_ia32_movmskps((__v4sf)__A);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) unsigned int _mm_getcsr(void)
{
  return __builtin_ia32_stmxcsr();
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) unsigned int _MM_GET_EXCEPTION_STATE(void)
{
  return _mm_getcsr() & 63;
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) unsigned int _MM_GET_EXCEPTION_MASK(void)
{
  return _mm_getcsr() & 8064;
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) unsigned int _MM_GET_ROUNDING_MODE(void)
{
  return _mm_getcsr() & 24576;
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) unsigned int _MM_GET_FLUSH_ZERO_MODE(void)
{
  return _mm_getcsr() & 32768;
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) void _mm_setcsr(unsigned int __I)
{
  __builtin_ia32_ldmxcsr(__I);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) void _MM_SET_EXCEPTION_STATE(unsigned int __mask)
{
  _mm_setcsr((_mm_getcsr() & ~63) | __mask);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) void _MM_SET_EXCEPTION_MASK(unsigned int __mask)
{
  _mm_setcsr((_mm_getcsr() & ~8064) | __mask);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) void _MM_SET_ROUNDING_MODE(unsigned int __mode)
{
  _mm_setcsr((_mm_getcsr() & ~24576) | __mode);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) void _MM_SET_FLUSH_ZERO_MODE(unsigned int __mode)
{
  _mm_setcsr((_mm_getcsr() & ~32768) | __mode);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128 _mm_set_ss(float __F)
{
  return (__m128)(__v4sf){__F, 0.000000000000000000000000e+00f, 0.000000000000000000000000e+00f, 0.000000000000000000000000e+00f};
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128 _mm_set1_ps(float __F)
{
  return (__m128)(__v4sf){__F, __F, __F, __F};
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128 _mm_set_ps1(float __F)
{
  return _mm_set1_ps(__F);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128 _mm_load_ss(const float *__P)
{
  return _mm_set_ss(*__P);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128 _mm_load1_ps(const float *__P)
{
  return _mm_set1_ps(*__P);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128 _mm_load_ps1(const float *__P)
{
  return _mm_load1_ps(__P);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128 _mm_load_ps(const float *__P)
{
  return (__m128)*((__v4sf *)__P);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128 _mm_loadu_ps(const float *__P)
{
  return (__m128)__builtin_ia32_loadups(__P);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128 _mm_loadr_ps(const float *__P)
{
  __v4sf __tmp = *((__v4sf *)__P);
  return (__m128)__builtin_ia32_shufps(__tmp, __tmp, ((0 << 6 | 1 << 4) | 2 << 2) | 3);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128 _mm_set_ps(const float __Z, const float __Y, const float __X, const float __W)
{
  return (__m128)(__v4sf){__W, __X, __Y, __Z};
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128 _mm_setr_ps(float __Z, float __Y, float __X, float __W)
{
  return (__m128)(__v4sf){__Z, __Y, __X, __W};
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) void _mm_store_ss(float *__P, __m128 __A)
{
  *__P = __builtin_ia32_vec_ext_v4sf((__v4sf)__A, 0);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) float _mm_cvtss_f32(__m128 __A)
{
  return __builtin_ia32_vec_ext_v4sf((__v4sf)__A, 0);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) void _mm_store_ps(float *__P, __m128 __A)
{
  *((__v4sf *)__P) = (__v4sf)__A;
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) void _mm_storeu_ps(float *__P, __m128 __A)
{
  __builtin_ia32_storeups(__P, (__v4sf)__A);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) void _mm_store1_ps(float *__P, __m128 __A)
{
  __v4sf __va = (__v4sf)__A;
  __v4sf __tmp = __builtin_ia32_shufps(__va, __va, ((0 << 6 | 0 << 4) | 0 << 2) | 0);
  _mm_storeu_ps(__P, __tmp);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) void _mm_store_ps1(float *__P, __m128 __A)
{
  _mm_store1_ps(__P, __A);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) void _mm_storer_ps(float *__P, __m128 __A)
{
  __v4sf __va = (__v4sf)__A;
  __v4sf __tmp = __builtin_ia32_shufps(__va, __va, ((0 << 6 | 1 << 4) | 2 << 2) | 3);
  _mm_store_ps(__P, __tmp);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128 _mm_move_ss(__m128 __A, __m128 __B)
{
  return (__m128)__builtin_ia32_movss((__v4sf)__A, (__v4sf)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) int _mm_extract_pi16(const __m64 __A, const int __N)
{
  return __builtin_ia32_vec_ext_v4hi((__v4hi)__A, __N);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) int _m_pextrw(const __m64 __A, const int __N)
{
  return _mm_extract_pi16(__A, __N);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_insert_pi16(const __m64 __A, const int __D, const int __N)
{
  return (__m64)__builtin_ia32_vec_set_v4hi((__v4hi)__A, __D, __N);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _m_pinsrw(const __m64 __A, const int __D, const int __N)
{
  return _mm_insert_pi16(__A, __D, __N);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_max_pi16(__m64 __A, __m64 __B)
{
  return (__m64)__builtin_ia32_pmaxsw((__v4hi)__A, (__v4hi)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _m_pmaxsw(__m64 __A, __m64 __B)
{
  return _mm_max_pi16(__A, __B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_max_pu8(__m64 __A, __m64 __B)
{
  return (__m64)__builtin_ia32_pmaxub((__v8qi)__A, (__v8qi)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _m_pmaxub(__m64 __A, __m64 __B)
{
  return _mm_max_pu8(__A, __B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_min_pi16(__m64 __A, __m64 __B)
{
  return (__m64)__builtin_ia32_pminsw((__v4hi)__A, (__v4hi)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _m_pminsw(__m64 __A, __m64 __B)
{
  return _mm_min_pi16(__A, __B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_min_pu8(__m64 __A, __m64 __B)
{
  return (__m64)__builtin_ia32_pminub((__v8qi)__A, (__v8qi)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _m_pminub(__m64 __A, __m64 __B)
{
  return _mm_min_pu8(__A, __B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) int _mm_movemask_pi8(__m64 __A)
{
  return __builtin_ia32_pmovmskb((__v8qi)__A);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) int _m_pmovmskb(__m64 __A)
{
  return _mm_movemask_pi8(__A);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_mulhi_pu16(__m64 __A, __m64 __B)
{
  return (__m64)__builtin_ia32_pmulhuw((__v4hi)__A, (__v4hi)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _m_pmulhuw(__m64 __A, __m64 __B)
{
  return _mm_mulhi_pu16(__A, __B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_shuffle_pi16(__m64 __A, const int __N)
{
  return (__m64)__builtin_ia32_pshufw((__v4hi)__A, __N);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _m_pshufw(__m64 __A, const int __N)
{
  return _mm_shuffle_pi16(__A, __N);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) void _mm_maskmove_si64(__m64 __A, __m64 __N, char *__P)
{
  __builtin_ia32_maskmovq((__v8qi)__A, (__v8qi)__N, __P);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) void _m_maskmovq(__m64 __A, __m64 __N, char *__P)
{
  _mm_maskmove_si64(__A, __N, __P);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_avg_pu8(__m64 __A, __m64 __B)
{
  return (__m64)__builtin_ia32_pavgb((__v8qi)__A, (__v8qi)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _m_pavgb(__m64 __A, __m64 __B)
{
  return _mm_avg_pu8(__A, __B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_avg_pu16(__m64 __A, __m64 __B)
{
  return (__m64)__builtin_ia32_pavgw((__v4hi)__A, (__v4hi)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _m_pavgw(__m64 __A, __m64 __B)
{
  return _mm_avg_pu16(__A, __B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_sad_pu8(__m64 __A, __m64 __B)
{
  return (__m64)__builtin_ia32_psadbw((__v8qi)__A, (__v8qi)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _m_psadbw(__m64 __A, __m64 __B)
{
  return _mm_sad_pu8(__A, __B);
}
enum _mm_hint
{
  _MM_HINT_T0 = 3,
  _MM_HINT_T1 = 2,
  _MM_HINT_T2 = 1,
  _MM_HINT_NTA = 0
};
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) void _mm_prefetch(const void *__P, enum _mm_hint __I)
{
  __builtin_prefetch(__P, 0, __I);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) void _mm_stream_pi(__m64 *__P, __m64 __A)
{
  __builtin_ia32_movntq((unsigned long long int *)__P, (unsigned long long int)__A);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) void _mm_stream_ps(float *__P, __m128 __A)
{
  __builtin_ia32_movntps(__P, (__v4sf)__A);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) void _mm_sfence(void)
{
  __builtin_ia32_sfence();
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) void _mm_pause(void)
{
  __builtin_ia32_pause();
}
typedef __attribute__((vector_size(16))) __attribute__((__may_alias__)) double __m128d;
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128d _mm_set_sd(double __F)
{
  return (__m128d){__F, 0.00000000000000000000000000000000000000000000000000000e+00};
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128d _mm_set1_pd(double __F)
{
  return (__m128d){__F, __F};
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128d _mm_set_pd1(double __F)
{
  return _mm_set1_pd(__F);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128d _mm_set_pd(double __W, double __X)
{
  return (__m128d){__X, __W};
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128d _mm_setr_pd(double __W, double __X)
{
  return (__m128d){__W, __X};
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128d _mm_setzero_pd(void)
{
  return (__m128d){0.00000000000000000000000000000000000000000000000000000e+00, 0.00000000000000000000000000000000000000000000000000000e+00};
}
typedef __attribute__((vector_size(16))) __attribute__((__may_alias__)) double __v2df;
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128d _mm_move_sd(__m128d __A, __m128d __B)
{
  return (__m128d)__builtin_ia32_movsd((__v2df)__A, (__v2df)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128d _mm_load_pd(const double *__P)
{
  return *((__m128d *)__P);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128d _mm_loadu_pd(const double *__P)
{
  return __builtin_ia32_loadupd(__P);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128d _mm_load1_pd(const double *__P)
{
  return _mm_set1_pd(*__P);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128d _mm_load_sd(const double *__P)
{
  return _mm_set_sd(*__P);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128d _mm_load_pd1(const double *__P)
{
  return _mm_load1_pd(__P);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128d _mm_loadr_pd(const double *__P)
{
  __m128d __tmp = _mm_load_pd(__P);
  return __builtin_ia32_shufpd(__tmp, __tmp, 0 << 1 | 1);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) void _mm_store_pd(double *__P, __m128d __A)
{
  *((__m128d *)__P) = __A;
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) void _mm_storeu_pd(double *__P, __m128d __A)
{
  __builtin_ia32_storeupd(__P, __A);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) void _mm_store_sd(double *__P, __m128d __A)
{
  *__P = __builtin_ia32_vec_ext_v2df(__A, 0);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) double _mm_cvtsd_f64(__m128d __A)
{
  return __builtin_ia32_vec_ext_v2df(__A, 0);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) void _mm_storel_pd(double *__P, __m128d __A)
{
  _mm_store_sd(__P, __A);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) void _mm_storeh_pd(double *__P, __m128d __A)
{
  *__P = __builtin_ia32_vec_ext_v2df(__A, 1);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) void _mm_store1_pd(double *__P, __m128d __A)
{
  _mm_store_pd(__P, __builtin_ia32_shufpd(__A, __A, 0 << 1 | 0));
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) void _mm_store_pd1(double *__P, __m128d __A)
{
  _mm_store1_pd(__P, __A);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) void _mm_storer_pd(double *__P, __m128d __A)
{
  _mm_store_pd(__P, __builtin_ia32_shufpd(__A, __A, 0 << 1 | 1));
}
typedef __attribute__((vector_size(16))) __attribute__((__may_alias__)) long long int __m128i;
typedef __attribute__((vector_size(16))) __attribute__((__may_alias__)) int __v4si;
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) int _mm_cvtsi128_si32(__m128i __A)
{
  return __builtin_ia32_vec_ext_v4si((__v4si)__A, 0);
}
typedef __attribute__((vector_size(16))) __attribute__((__may_alias__)) long long int __v2di;
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) long long int _mm_cvtsi128_si64(__m128i __A)
{
  return __builtin_ia32_vec_ext_v2di((__v2di)__A, 0);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) long long int _mm_cvtsi128_si64x(__m128i __A)
{
  return __builtin_ia32_vec_ext_v2di((__v2di)__A, 0);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128d _mm_add_pd(__m128d __A, __m128d __B)
{
  return (__m128d)__builtin_ia32_addpd((__v2df)__A, (__v2df)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128d _mm_add_sd(__m128d __A, __m128d __B)
{
  return (__m128d)__builtin_ia32_addsd((__v2df)__A, (__v2df)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128d _mm_sub_pd(__m128d __A, __m128d __B)
{
  return (__m128d)__builtin_ia32_subpd((__v2df)__A, (__v2df)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128d _mm_sub_sd(__m128d __A, __m128d __B)
{
  return (__m128d)__builtin_ia32_subsd((__v2df)__A, (__v2df)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128d _mm_mul_pd(__m128d __A, __m128d __B)
{
  return (__m128d)__builtin_ia32_mulpd((__v2df)__A, (__v2df)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128d _mm_mul_sd(__m128d __A, __m128d __B)
{
  return (__m128d)__builtin_ia32_mulsd((__v2df)__A, (__v2df)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128d _mm_div_pd(__m128d __A, __m128d __B)
{
  return (__m128d)__builtin_ia32_divpd((__v2df)__A, (__v2df)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128d _mm_div_sd(__m128d __A, __m128d __B)
{
  return (__m128d)__builtin_ia32_divsd((__v2df)__A, (__v2df)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128d _mm_sqrt_pd(__m128d __A)
{
  return (__m128d)__builtin_ia32_sqrtpd((__v2df)__A);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128d _mm_sqrt_sd(__m128d __A, __m128d __B)
{
  __v2df __tmp = __builtin_ia32_movsd((__v2df)__A, (__v2df)__B);
  return (__m128d)__builtin_ia32_sqrtsd((__v2df)__tmp);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128d _mm_min_pd(__m128d __A, __m128d __B)
{
  return (__m128d)__builtin_ia32_minpd((__v2df)__A, (__v2df)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128d _mm_min_sd(__m128d __A, __m128d __B)
{
  return (__m128d)__builtin_ia32_minsd((__v2df)__A, (__v2df)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128d _mm_max_pd(__m128d __A, __m128d __B)
{
  return (__m128d)__builtin_ia32_maxpd((__v2df)__A, (__v2df)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128d _mm_max_sd(__m128d __A, __m128d __B)
{
  return (__m128d)__builtin_ia32_maxsd((__v2df)__A, (__v2df)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128d _mm_and_pd(__m128d __A, __m128d __B)
{
  return (__m128d)__builtin_ia32_andpd((__v2df)__A, (__v2df)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128d _mm_andnot_pd(__m128d __A, __m128d __B)
{
  return (__m128d)__builtin_ia32_andnpd((__v2df)__A, (__v2df)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128d _mm_or_pd(__m128d __A, __m128d __B)
{
  return (__m128d)__builtin_ia32_orpd((__v2df)__A, (__v2df)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128d _mm_xor_pd(__m128d __A, __m128d __B)
{
  return (__m128d)__builtin_ia32_xorpd((__v2df)__A, (__v2df)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128d _mm_cmpeq_pd(__m128d __A, __m128d __B)
{
  return (__m128d)__builtin_ia32_cmpeqpd((__v2df)__A, (__v2df)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128d _mm_cmplt_pd(__m128d __A, __m128d __B)
{
  return (__m128d)__builtin_ia32_cmpltpd((__v2df)__A, (__v2df)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128d _mm_cmple_pd(__m128d __A, __m128d __B)
{
  return (__m128d)__builtin_ia32_cmplepd((__v2df)__A, (__v2df)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128d _mm_cmpgt_pd(__m128d __A, __m128d __B)
{
  return (__m128d)__builtin_ia32_cmpgtpd((__v2df)__A, (__v2df)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128d _mm_cmpge_pd(__m128d __A, __m128d __B)
{
  return (__m128d)__builtin_ia32_cmpgepd((__v2df)__A, (__v2df)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128d _mm_cmpneq_pd(__m128d __A, __m128d __B)
{
  return (__m128d)__builtin_ia32_cmpneqpd((__v2df)__A, (__v2df)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128d _mm_cmpnlt_pd(__m128d __A, __m128d __B)
{
  return (__m128d)__builtin_ia32_cmpnltpd((__v2df)__A, (__v2df)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128d _mm_cmpnle_pd(__m128d __A, __m128d __B)
{
  return (__m128d)__builtin_ia32_cmpnlepd((__v2df)__A, (__v2df)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128d _mm_cmpngt_pd(__m128d __A, __m128d __B)
{
  return (__m128d)__builtin_ia32_cmpngtpd((__v2df)__A, (__v2df)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128d _mm_cmpnge_pd(__m128d __A, __m128d __B)
{
  return (__m128d)__builtin_ia32_cmpngepd((__v2df)__A, (__v2df)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128d _mm_cmpord_pd(__m128d __A, __m128d __B)
{
  return (__m128d)__builtin_ia32_cmpordpd((__v2df)__A, (__v2df)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128d _mm_cmpunord_pd(__m128d __A, __m128d __B)
{
  return (__m128d)__builtin_ia32_cmpunordpd((__v2df)__A, (__v2df)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128d _mm_cmpeq_sd(__m128d __A, __m128d __B)
{
  return (__m128d)__builtin_ia32_cmpeqsd((__v2df)__A, (__v2df)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128d _mm_cmplt_sd(__m128d __A, __m128d __B)
{
  return (__m128d)__builtin_ia32_cmpltsd((__v2df)__A, (__v2df)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128d _mm_cmple_sd(__m128d __A, __m128d __B)
{
  return (__m128d)__builtin_ia32_cmplesd((__v2df)__A, (__v2df)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128d _mm_cmpgt_sd(__m128d __A, __m128d __B)
{
  return (__m128d)__builtin_ia32_movsd((__v2df)__A, (__v2df)__builtin_ia32_cmpltsd((__v2df)__B, (__v2df)__A));
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128d _mm_cmpge_sd(__m128d __A, __m128d __B)
{
  return (__m128d)__builtin_ia32_movsd((__v2df)__A, (__v2df)__builtin_ia32_cmplesd((__v2df)__B, (__v2df)__A));
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128d _mm_cmpneq_sd(__m128d __A, __m128d __B)
{
  return (__m128d)__builtin_ia32_cmpneqsd((__v2df)__A, (__v2df)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128d _mm_cmpnlt_sd(__m128d __A, __m128d __B)
{
  return (__m128d)__builtin_ia32_cmpnltsd((__v2df)__A, (__v2df)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128d _mm_cmpnle_sd(__m128d __A, __m128d __B)
{
  return (__m128d)__builtin_ia32_cmpnlesd((__v2df)__A, (__v2df)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128d _mm_cmpngt_sd(__m128d __A, __m128d __B)
{
  return (__m128d)__builtin_ia32_movsd((__v2df)__A, (__v2df)__builtin_ia32_cmpnltsd((__v2df)__B, (__v2df)__A));
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128d _mm_cmpnge_sd(__m128d __A, __m128d __B)
{
  return (__m128d)__builtin_ia32_movsd((__v2df)__A, (__v2df)__builtin_ia32_cmpnlesd((__v2df)__B, (__v2df)__A));
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128d _mm_cmpord_sd(__m128d __A, __m128d __B)
{
  return (__m128d)__builtin_ia32_cmpordsd((__v2df)__A, (__v2df)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128d _mm_cmpunord_sd(__m128d __A, __m128d __B)
{
  return (__m128d)__builtin_ia32_cmpunordsd((__v2df)__A, (__v2df)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) int _mm_comieq_sd(__m128d __A, __m128d __B)
{
  return __builtin_ia32_comisdeq((__v2df)__A, (__v2df)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) int _mm_comilt_sd(__m128d __A, __m128d __B)
{
  return __builtin_ia32_comisdlt((__v2df)__A, (__v2df)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) int _mm_comile_sd(__m128d __A, __m128d __B)
{
  return __builtin_ia32_comisdle((__v2df)__A, (__v2df)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) int _mm_comigt_sd(__m128d __A, __m128d __B)
{
  return __builtin_ia32_comisdgt((__v2df)__A, (__v2df)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) int _mm_comige_sd(__m128d __A, __m128d __B)
{
  return __builtin_ia32_comisdge((__v2df)__A, (__v2df)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) int _mm_comineq_sd(__m128d __A, __m128d __B)
{
  return __builtin_ia32_comisdneq((__v2df)__A, (__v2df)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) int _mm_ucomieq_sd(__m128d __A, __m128d __B)
{
  return __builtin_ia32_ucomisdeq((__v2df)__A, (__v2df)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) int _mm_ucomilt_sd(__m128d __A, __m128d __B)
{
  return __builtin_ia32_ucomisdlt((__v2df)__A, (__v2df)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) int _mm_ucomile_sd(__m128d __A, __m128d __B)
{
  return __builtin_ia32_ucomisdle((__v2df)__A, (__v2df)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) int _mm_ucomigt_sd(__m128d __A, __m128d __B)
{
  return __builtin_ia32_ucomisdgt((__v2df)__A, (__v2df)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) int _mm_ucomige_sd(__m128d __A, __m128d __B)
{
  return __builtin_ia32_ucomisdge((__v2df)__A, (__v2df)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) int _mm_ucomineq_sd(__m128d __A, __m128d __B)
{
  return __builtin_ia32_ucomisdneq((__v2df)__A, (__v2df)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_set_epi64x(long long int __q1, long long int __q0)
{
  return (__m128i)(__v2di){__q0, __q1};
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_set_epi64(__m64 __q1, __m64 __q0)
{
  return _mm_set_epi64x((long long int)__q1, (long long int)__q0);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_set_epi32(int __q3, int __q2, int __q1, int __q0)
{
  return (__m128i)(__v4si){__q0, __q1, __q2, __q3};
}
typedef __attribute__((vector_size(16))) __attribute__((__may_alias__)) short int __v8hi;
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_set_epi16(short int __q7, short int __q6, short int __q5, short int __q4, short int __q3, short int __q2, short int __q1, short int __q0)
{
  return (__m128i)(__v8hi){__q0, __q1, __q2, __q3, __q4, __q5, __q6, __q7};
}
typedef __attribute__((vector_size(16))) __attribute__((__may_alias__)) char __v16qi;
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_set_epi8(char __q15, char __q14, char __q13, char __q12, char __q11, char __q10, char __q09, char __q08, char __q07, char __q06, char __q05, char __q04, char __q03, char __q02, char __q01, char __q00)
{
  return (__m128i)(__v16qi){__q00, __q01, __q02, __q03, __q04, __q05, __q06, __q07, __q08, __q09, __q10, __q11, __q12, __q13, __q14, __q15};
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_set1_epi64x(long long int __A)
{
  return _mm_set_epi64x(__A, __A);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_set1_epi64(__m64 __A)
{
  return _mm_set_epi64(__A, __A);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_set1_epi32(int __A)
{
  return _mm_set_epi32(__A, __A, __A, __A);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_set1_epi16(short int __A)
{
  return _mm_set_epi16(__A, __A, __A, __A, __A, __A, __A, __A);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_set1_epi8(char __A)
{
  return _mm_set_epi8(__A, __A, __A, __A, __A, __A, __A, __A, __A, __A, __A, __A, __A, __A, __A, __A);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_setr_epi64(__m64 __q0, __m64 __q1)
{
  return _mm_set_epi64(__q1, __q0);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_setr_epi32(int __q0, int __q1, int __q2, int __q3)
{
  return _mm_set_epi32(__q3, __q2, __q1, __q0);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_setr_epi16(short int __q0, short int __q1, short int __q2, short int __q3, short int __q4, short int __q5, short int __q6, short int __q7)
{
  return _mm_set_epi16(__q7, __q6, __q5, __q4, __q3, __q2, __q1, __q0);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_setr_epi8(char __q00, char __q01, char __q02, char __q03, char __q04, char __q05, char __q06, char __q07, char __q08, char __q09, char __q10, char __q11, char __q12, char __q13, char __q14, char __q15)
{
  return _mm_set_epi8(__q15, __q14, __q13, __q12, __q11, __q10, __q09, __q08, __q07, __q06, __q05, __q04, __q03, __q02, __q01, __q00);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_load_si128(const __m128i *__P)
{
  return *__P;
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_loadu_si128(const __m128i *__P)
{
  return (__m128i)__builtin_ia32_loaddqu((const char *)__P);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_loadl_epi64(const __m128i *__P)
{
  return _mm_set_epi64((__m64)0LL, *((__m64 *)__P));
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) void _mm_store_si128(__m128i *__P, __m128i __B)
{
  *__P = __B;
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) void _mm_storeu_si128(__m128i *__P, __m128i __B)
{
  __builtin_ia32_storedqu((char *)__P, (__v16qi)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) void _mm_storel_epi64(__m128i *__P, __m128i __B)
{
  *((long long int *)__P) = __builtin_ia32_vec_ext_v2di((__v2di)__B, 0);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_movepi64_pi64(__m128i __B)
{
  return (__m64)__builtin_ia32_vec_ext_v2di((__v2di)__B, 0);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_movpi64_epi64(__m64 __A)
{
  return _mm_set_epi64((__m64)0LL, __A);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_move_epi64(__m128i __A)
{
  return (__m128i)__builtin_ia32_movq128((__v2di)__A);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_setzero_si128(void)
{
  return (__m128i)(__v4si){0, 0, 0, 0};
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128d _mm_cvtepi32_pd(__m128i __A)
{
  return (__m128d)__builtin_ia32_cvtdq2pd((__v4si)__A);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128 _mm_cvtepi32_ps(__m128i __A)
{
  return (__m128)__builtin_ia32_cvtdq2ps((__v4si)__A);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_cvtpd_epi32(__m128d __A)
{
  return (__m128i)__builtin_ia32_cvtpd2dq((__v2df)__A);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_cvtpd_pi32(__m128d __A)
{
  return (__m64)__builtin_ia32_cvtpd2pi((__v2df)__A);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128 _mm_cvtpd_ps(__m128d __A)
{
  return (__m128)__builtin_ia32_cvtpd2ps((__v2df)__A);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_cvttpd_epi32(__m128d __A)
{
  return (__m128i)__builtin_ia32_cvttpd2dq((__v2df)__A);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_cvttpd_pi32(__m128d __A)
{
  return (__m64)__builtin_ia32_cvttpd2pi((__v2df)__A);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128d _mm_cvtpi32_pd(__m64 __A)
{
  return (__m128d)__builtin_ia32_cvtpi2pd((__v2si)__A);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_cvtps_epi32(__m128 __A)
{
  return (__m128i)__builtin_ia32_cvtps2dq((__v4sf)__A);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_cvttps_epi32(__m128 __A)
{
  return (__m128i)__builtin_ia32_cvttps2dq((__v4sf)__A);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128d _mm_cvtps_pd(__m128 __A)
{
  return (__m128d)__builtin_ia32_cvtps2pd((__v4sf)__A);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) int _mm_cvtsd_si32(__m128d __A)
{
  return __builtin_ia32_cvtsd2si((__v2df)__A);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) long long int _mm_cvtsd_si64(__m128d __A)
{
  return __builtin_ia32_cvtsd2si64((__v2df)__A);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) long long int _mm_cvtsd_si64x(__m128d __A)
{
  return __builtin_ia32_cvtsd2si64((__v2df)__A);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) int _mm_cvttsd_si32(__m128d __A)
{
  return __builtin_ia32_cvttsd2si((__v2df)__A);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) long long int _mm_cvttsd_si64(__m128d __A)
{
  return __builtin_ia32_cvttsd2si64((__v2df)__A);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) long long int _mm_cvttsd_si64x(__m128d __A)
{
  return __builtin_ia32_cvttsd2si64((__v2df)__A);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128 _mm_cvtsd_ss(__m128 __A, __m128d __B)
{
  return (__m128)__builtin_ia32_cvtsd2ss((__v4sf)__A, (__v2df)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128d _mm_cvtsi32_sd(__m128d __A, int __B)
{
  return (__m128d)__builtin_ia32_cvtsi2sd((__v2df)__A, __B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128d _mm_cvtsi64_sd(__m128d __A, long long int __B)
{
  return (__m128d)__builtin_ia32_cvtsi642sd((__v2df)__A, __B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128d _mm_cvtsi64x_sd(__m128d __A, long long int __B)
{
  return (__m128d)__builtin_ia32_cvtsi642sd((__v2df)__A, __B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128d _mm_cvtss_sd(__m128d __A, __m128 __B)
{
  return (__m128d)__builtin_ia32_cvtss2sd((__v2df)__A, (__v4sf)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128d _mm_shuffle_pd(__m128d __A, __m128d __B, const int __mask)
{
  return (__m128d)__builtin_ia32_shufpd((__v2df)__A, (__v2df)__B, __mask);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128d _mm_unpackhi_pd(__m128d __A, __m128d __B)
{
  return (__m128d)__builtin_ia32_unpckhpd((__v2df)__A, (__v2df)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128d _mm_unpacklo_pd(__m128d __A, __m128d __B)
{
  return (__m128d)__builtin_ia32_unpcklpd((__v2df)__A, (__v2df)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128d _mm_loadh_pd(__m128d __A, const double *__B)
{
  return (__m128d)__builtin_ia32_loadhpd((__v2df)__A, __B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128d _mm_loadl_pd(__m128d __A, const double *__B)
{
  return (__m128d)__builtin_ia32_loadlpd((__v2df)__A, __B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) int _mm_movemask_pd(__m128d __A)
{
  return __builtin_ia32_movmskpd((__v2df)__A);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_packs_epi16(__m128i __A, __m128i __B)
{
  return (__m128i)__builtin_ia32_packsswb128((__v8hi)__A, (__v8hi)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_packs_epi32(__m128i __A, __m128i __B)
{
  return (__m128i)__builtin_ia32_packssdw128((__v4si)__A, (__v4si)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_packus_epi16(__m128i __A, __m128i __B)
{
  return (__m128i)__builtin_ia32_packuswb128((__v8hi)__A, (__v8hi)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_unpackhi_epi8(__m128i __A, __m128i __B)
{
  return (__m128i)__builtin_ia32_punpckhbw128((__v16qi)__A, (__v16qi)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_unpackhi_epi16(__m128i __A, __m128i __B)
{
  return (__m128i)__builtin_ia32_punpckhwd128((__v8hi)__A, (__v8hi)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_unpackhi_epi32(__m128i __A, __m128i __B)
{
  return (__m128i)__builtin_ia32_punpckhdq128((__v4si)__A, (__v4si)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_unpackhi_epi64(__m128i __A, __m128i __B)
{
  return (__m128i)__builtin_ia32_punpckhqdq128((__v2di)__A, (__v2di)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_unpacklo_epi8(__m128i __A, __m128i __B)
{
  return (__m128i)__builtin_ia32_punpcklbw128((__v16qi)__A, (__v16qi)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_unpacklo_epi16(__m128i __A, __m128i __B)
{
  return (__m128i)__builtin_ia32_punpcklwd128((__v8hi)__A, (__v8hi)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_unpacklo_epi32(__m128i __A, __m128i __B)
{
  return (__m128i)__builtin_ia32_punpckldq128((__v4si)__A, (__v4si)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_unpacklo_epi64(__m128i __A, __m128i __B)
{
  return (__m128i)__builtin_ia32_punpcklqdq128((__v2di)__A, (__v2di)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_add_epi8(__m128i __A, __m128i __B)
{
  return (__m128i)__builtin_ia32_paddb128((__v16qi)__A, (__v16qi)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_add_epi16(__m128i __A, __m128i __B)
{
  return (__m128i)__builtin_ia32_paddw128((__v8hi)__A, (__v8hi)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_add_epi32(__m128i __A, __m128i __B)
{
  return (__m128i)__builtin_ia32_paddd128((__v4si)__A, (__v4si)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_add_epi64(__m128i __A, __m128i __B)
{
  return (__m128i)__builtin_ia32_paddq128((__v2di)__A, (__v2di)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_adds_epi8(__m128i __A, __m128i __B)
{
  return (__m128i)__builtin_ia32_paddsb128((__v16qi)__A, (__v16qi)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_adds_epi16(__m128i __A, __m128i __B)
{
  return (__m128i)__builtin_ia32_paddsw128((__v8hi)__A, (__v8hi)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_adds_epu8(__m128i __A, __m128i __B)
{
  return (__m128i)__builtin_ia32_paddusb128((__v16qi)__A, (__v16qi)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_adds_epu16(__m128i __A, __m128i __B)
{
  return (__m128i)__builtin_ia32_paddusw128((__v8hi)__A, (__v8hi)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_sub_epi8(__m128i __A, __m128i __B)
{
  return (__m128i)__builtin_ia32_psubb128((__v16qi)__A, (__v16qi)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_sub_epi16(__m128i __A, __m128i __B)
{
  return (__m128i)__builtin_ia32_psubw128((__v8hi)__A, (__v8hi)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_sub_epi32(__m128i __A, __m128i __B)
{
  return (__m128i)__builtin_ia32_psubd128((__v4si)__A, (__v4si)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_sub_epi64(__m128i __A, __m128i __B)
{
  return (__m128i)__builtin_ia32_psubq128((__v2di)__A, (__v2di)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_subs_epi8(__m128i __A, __m128i __B)
{
  return (__m128i)__builtin_ia32_psubsb128((__v16qi)__A, (__v16qi)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_subs_epi16(__m128i __A, __m128i __B)
{
  return (__m128i)__builtin_ia32_psubsw128((__v8hi)__A, (__v8hi)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_subs_epu8(__m128i __A, __m128i __B)
{
  return (__m128i)__builtin_ia32_psubusb128((__v16qi)__A, (__v16qi)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_subs_epu16(__m128i __A, __m128i __B)
{
  return (__m128i)__builtin_ia32_psubusw128((__v8hi)__A, (__v8hi)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_madd_epi16(__m128i __A, __m128i __B)
{
  return (__m128i)__builtin_ia32_pmaddwd128((__v8hi)__A, (__v8hi)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_mulhi_epi16(__m128i __A, __m128i __B)
{
  return (__m128i)__builtin_ia32_pmulhw128((__v8hi)__A, (__v8hi)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_mullo_epi16(__m128i __A, __m128i __B)
{
  return (__m128i)__builtin_ia32_pmullw128((__v8hi)__A, (__v8hi)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m64 _mm_mul_su32(__m64 __A, __m64 __B)
{
  return (__m64)__builtin_ia32_pmuludq((__v2si)__A, (__v2si)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_mul_epu32(__m128i __A, __m128i __B)
{
  return (__m128i)__builtin_ia32_pmuludq128((__v4si)__A, (__v4si)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_slli_epi16(__m128i __A, int __B)
{
  return (__m128i)__builtin_ia32_psllwi128((__v8hi)__A, __B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_slli_epi32(__m128i __A, int __B)
{
  return (__m128i)__builtin_ia32_pslldi128((__v4si)__A, __B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_slli_epi64(__m128i __A, int __B)
{
  return (__m128i)__builtin_ia32_psllqi128((__v2di)__A, __B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_srai_epi16(__m128i __A, int __B)
{
  return (__m128i)__builtin_ia32_psrawi128((__v8hi)__A, __B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_srai_epi32(__m128i __A, int __B)
{
  return (__m128i)__builtin_ia32_psradi128((__v4si)__A, __B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_bsrli_si128(__m128i __A, const int __N)
{
  return (__m128i)__builtin_ia32_psrldqi128(__A, __N * 8);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_bslli_si128(__m128i __A, const int __N)
{
  return (__m128i)__builtin_ia32_pslldqi128(__A, __N * 8);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_srli_si128(__m128i __A, const int __N)
{
  return (__m128i)__builtin_ia32_psrldqi128(__A, __N * 8);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_slli_si128(__m128i __A, const int __N)
{
  return (__m128i)__builtin_ia32_pslldqi128(__A, __N * 8);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_srli_epi16(__m128i __A, int __B)
{
  return (__m128i)__builtin_ia32_psrlwi128((__v8hi)__A, __B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_srli_epi32(__m128i __A, int __B)
{
  return (__m128i)__builtin_ia32_psrldi128((__v4si)__A, __B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_srli_epi64(__m128i __A, int __B)
{
  return (__m128i)__builtin_ia32_psrlqi128((__v2di)__A, __B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_sll_epi16(__m128i __A, __m128i __B)
{
  return (__m128i)__builtin_ia32_psllw128((__v8hi)__A, (__v8hi)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_sll_epi32(__m128i __A, __m128i __B)
{
  return (__m128i)__builtin_ia32_pslld128((__v4si)__A, (__v4si)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_sll_epi64(__m128i __A, __m128i __B)
{
  return (__m128i)__builtin_ia32_psllq128((__v2di)__A, (__v2di)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_sra_epi16(__m128i __A, __m128i __B)
{
  return (__m128i)__builtin_ia32_psraw128((__v8hi)__A, (__v8hi)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_sra_epi32(__m128i __A, __m128i __B)
{
  return (__m128i)__builtin_ia32_psrad128((__v4si)__A, (__v4si)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_srl_epi16(__m128i __A, __m128i __B)
{
  return (__m128i)__builtin_ia32_psrlw128((__v8hi)__A, (__v8hi)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_srl_epi32(__m128i __A, __m128i __B)
{
  return (__m128i)__builtin_ia32_psrld128((__v4si)__A, (__v4si)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_srl_epi64(__m128i __A, __m128i __B)
{
  return (__m128i)__builtin_ia32_psrlq128((__v2di)__A, (__v2di)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_and_si128(__m128i __A, __m128i __B)
{
  return (__m128i)__builtin_ia32_pand128((__v2di)__A, (__v2di)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_andnot_si128(__m128i __A, __m128i __B)
{
  return (__m128i)__builtin_ia32_pandn128((__v2di)__A, (__v2di)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_or_si128(__m128i __A, __m128i __B)
{
  return (__m128i)__builtin_ia32_por128((__v2di)__A, (__v2di)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_xor_si128(__m128i __A, __m128i __B)
{
  return (__m128i)__builtin_ia32_pxor128((__v2di)__A, (__v2di)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_cmpeq_epi8(__m128i __A, __m128i __B)
{
  return (__m128i)__builtin_ia32_pcmpeqb128((__v16qi)__A, (__v16qi)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_cmpeq_epi16(__m128i __A, __m128i __B)
{
  return (__m128i)__builtin_ia32_pcmpeqw128((__v8hi)__A, (__v8hi)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_cmpeq_epi32(__m128i __A, __m128i __B)
{
  return (__m128i)__builtin_ia32_pcmpeqd128((__v4si)__A, (__v4si)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_cmplt_epi8(__m128i __A, __m128i __B)
{
  return (__m128i)__builtin_ia32_pcmpgtb128((__v16qi)__B, (__v16qi)__A);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_cmplt_epi16(__m128i __A, __m128i __B)
{
  return (__m128i)__builtin_ia32_pcmpgtw128((__v8hi)__B, (__v8hi)__A);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_cmplt_epi32(__m128i __A, __m128i __B)
{
  return (__m128i)__builtin_ia32_pcmpgtd128((__v4si)__B, (__v4si)__A);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_cmpgt_epi8(__m128i __A, __m128i __B)
{
  return (__m128i)__builtin_ia32_pcmpgtb128((__v16qi)__A, (__v16qi)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_cmpgt_epi16(__m128i __A, __m128i __B)
{
  return (__m128i)__builtin_ia32_pcmpgtw128((__v8hi)__A, (__v8hi)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_cmpgt_epi32(__m128i __A, __m128i __B)
{
  return (__m128i)__builtin_ia32_pcmpgtd128((__v4si)__A, (__v4si)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) int _mm_extract_epi16(const __m128i __A, const int __N)
{
  return (unsigned short int)__builtin_ia32_vec_ext_v8hi((__v8hi)__A, __N);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_insert_epi16(const __m128i __A, const int __D, const int __N)
{
  return (__m128i)__builtin_ia32_vec_set_v8hi((__v8hi)__A, __D, __N);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_max_epi16(__m128i __A, __m128i __B)
{
  return (__m128i)__builtin_ia32_pmaxsw128((__v8hi)__A, (__v8hi)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_max_epu8(__m128i __A, __m128i __B)
{
  return (__m128i)__builtin_ia32_pmaxub128((__v16qi)__A, (__v16qi)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_min_epi16(__m128i __A, __m128i __B)
{
  return (__m128i)__builtin_ia32_pminsw128((__v8hi)__A, (__v8hi)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_min_epu8(__m128i __A, __m128i __B)
{
  return (__m128i)__builtin_ia32_pminub128((__v16qi)__A, (__v16qi)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) int _mm_movemask_epi8(__m128i __A)
{
  return __builtin_ia32_pmovmskb128((__v16qi)__A);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_mulhi_epu16(__m128i __A, __m128i __B)
{
  return (__m128i)__builtin_ia32_pmulhuw128((__v8hi)__A, (__v8hi)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_shufflehi_epi16(__m128i __A, const int __mask)
{
  return (__m128i)__builtin_ia32_pshufhw((__v8hi)__A, __mask);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_shufflelo_epi16(__m128i __A, const int __mask)
{
  return (__m128i)__builtin_ia32_pshuflw((__v8hi)__A, __mask);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_shuffle_epi32(__m128i __A, const int __mask)
{
  return (__m128i)__builtin_ia32_pshufd((__v4si)__A, __mask);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) void _mm_maskmoveu_si128(__m128i __A, __m128i __B, char *__C)
{
  __builtin_ia32_maskmovdqu((__v16qi)__A, (__v16qi)__B, __C);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_avg_epu8(__m128i __A, __m128i __B)
{
  return (__m128i)__builtin_ia32_pavgb128((__v16qi)__A, (__v16qi)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_avg_epu16(__m128i __A, __m128i __B)
{
  return (__m128i)__builtin_ia32_pavgw128((__v8hi)__A, (__v8hi)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_sad_epu8(__m128i __A, __m128i __B)
{
  return (__m128i)__builtin_ia32_psadbw128((__v16qi)__A, (__v16qi)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) void _mm_stream_si32(int *__A, int __B)
{
  __builtin_ia32_movnti(__A, __B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) void _mm_stream_si64(long long int *__A, long long int __B)
{
  __builtin_ia32_movnti64(__A, __B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) void _mm_stream_si128(__m128i *__A, __m128i __B)
{
  __builtin_ia32_movntdq((__m128i *)__A, (__v2di)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) void _mm_stream_pd(double *__A, __m128d __B)
{
  __builtin_ia32_movntpd(__A, (__v2df)__B);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) void _mm_clflush(const void *__A)
{
  __builtin_ia32_clflush(__A);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) void _mm_lfence(void)
{
  __builtin_ia32_lfence();
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) void _mm_mfence(void)
{
  __builtin_ia32_mfence();
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_cvtsi32_si128(int __A)
{
  return _mm_set_epi32(0, 0, 0, __A);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_cvtsi64_si128(long long int __A)
{
  return _mm_set_epi64x(0, __A);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_cvtsi64x_si128(long long int __A)
{
  return _mm_set_epi64x(0, __A);
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128 _mm_castpd_ps(__m128d __A)
{
  return (__m128)__A;
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_castpd_si128(__m128d __A)
{
  return (__m128i)__A;
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128d _mm_castps_pd(__m128 __A)
{
  return (__m128d)__A;
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128i _mm_castps_si128(__m128 __A)
{
  return (__m128i)__A;
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128 _mm_castsi128_ps(__m128i __A)
{
  return (__m128)__A;
}
extern __inline __attribute__((__gnu_inline__)) __attribute__((__always_inline__)) __attribute__((__artificial__)) __m128d _mm_castsi128_pd(__m128i __A)
{
  return (__m128d)__A;
}
extern float sqrtf(float __x) __attribute__((__nothrow__)) __attribute__((__leaf__));
void kann_RMSprop(int n, float h0, const float *h, float decay, const float *g, float *t, float *r)
{
  __m128 vh;
  __m128 vd;
  __m128 vd1;
  __m128 vtiny;
  int i;
  __m128 vt;
  __m128 vr;
  __m128 vg;
  __m128 tmp;
  int n4 = n >> 2 << 2;
  vh = _mm_set1_ps(h0);
  vd = _mm_set1_ps(decay);
  vd1 = _mm_set1_ps(1.000000000000000000000000e+00f - decay);
  vtiny = _mm_set1_ps(9.999999974752427078783512e-07f);
  for (i = 0; i < n4; i += 4)
    {
      vt = _mm_loadu_ps(&t[i]);
      vr = _mm_loadu_ps(&r[i]);
      vg = _mm_loadu_ps(&g[i]);
      if (h)
        {
          vh = _mm_loadu_ps(&h[i]);
        }
      vr = _mm_add_ps(_mm_mul_ps(vd1, _mm_mul_ps(vg, vg)), _mm_mul_ps(vd, vr));
      _mm_storeu_ps(&r[i], vr);
      tmp = _mm_sub_ps(vt, _mm_mul_ps(_mm_mul_ps(vh, _mm_rsqrt_ps(_mm_add_ps(vtiny, vr))), vg));
      _mm_storeu_ps(&t[i], tmp);
    }
  for (; i < n;  ++i)
    {
      r[i] = (1.00000000000000000000000000000000000000000000000000000e+00 - decay) * g[i] * g[i] + decay * r[i];
      t[i] -= (h ? h[i] : h0) / sqrtf(9.999999974752427078783512e-07f + r[i]) * g[i];
    }
}
float kann_grad_clip(float thres, int n, float *g)
{
  int i;
  double s2 = 0.00000000000000000000000000000000000000000000000000000e+00;
  for (i = 0; i < n;  ++i)
    {
      s2 += g[i] * g[i];
    }
  s2 = sqrt(s2);
  if (s2 > thres)
    {
      for ((i = 0, s2 = 1.00000000000000000000000000000000000000000000000000000e+00 / s2); i < n;  ++i)
        {
          g[i] *= (float)s2;
        }
    }
  return (float)s2 / thres;
}
extern int fprintf(FILE *__restrict __stream, const char *__restrict __format, ...);
extern struct _IO_FILE *stderr;
extern int fputc(int __c, FILE *__stream);
int kann_train_fnn1(kann_t *ann, float lr, int mini_size, int max_epoch, int max_drop_streak, float frac_val, int n, float **_x, float **_y)
{
  int n_in;
  int n_out;
  int n_var;
  int n_const;
  float *r;
  int *shuf;
  float **x;
  float **y;
  int j;
  int n_val;
  int n_train;
  float *min_x;
  float *min_c;
  float *x1;
  float *y1;
  int i;
  int drop_streak = 0;
  int min_set = 0;
  float min_val_cost = 3.402823466385288598117042e+38f;
  n_in = kann_feed_dim(ann, 1, 0);
  n_out = kann_feed_dim(ann, 4, 0);
  if (n_in < 0 || n_out < 0)
    {
      return  -1;
    }
  n_var = kad_size_var((*ann).n, (*ann).v);
  n_const = kad_size_const((*ann).n, (*ann).v);
  r = (float *)calloc(n_var, sizeof(float));
  shuf = (int *)malloc(n * sizeof(int));
  x = (float **)malloc(n * sizeof(float *));
  y = (float **)malloc(n * sizeof(float *));
  kann_shuffle(n, shuf);
  for (j = 0; j < n;  ++j)
    {
      (x[j] = _x[shuf[j]], y[j] = _y[shuf[j]]);
    }
  n_val = (int)(n * frac_val);
  n_train = n - n_val;
  min_x = (float *)malloc(n_var * sizeof(float));
  min_c = (float *)malloc(n_const * sizeof(float));
  x1 = (float *)malloc(n_in * mini_size * sizeof(float));
  y1 = (float *)malloc(n_out * mini_size * sizeof(float));
  kann_feed_bind(ann, 1, 0, &x1);
  kann_feed_bind(ann, 4, 0, &y1);
  for (i = 0; i < max_epoch;  ++i)
    {
      int n_proc = 0;
      int n_train_err = 0;
      int n_val_err = 0;
      int n_train_base = 0;
      int n_val_base = 0;
      double train_cost = 0.00000000000000000000000000000000000000000000000000000e+00;
      double val_cost = 0.00000000000000000000000000000000000000000000000000000e+00;
      kann_shuffle(n_train, shuf);
      kann_switch(ann, 1);
      while (n_proc < n_train)
        {
          int b;
          int c;
          int ms = n_train - n_proc < mini_size ? n_train - n_proc : mini_size;
          for (b = 0; b < ms;  ++b)
            {
              memcpy(&x1[b * n_in], x[shuf[n_proc + b]], n_in * sizeof(float));
              memcpy(&y1[b * n_out], y[shuf[n_proc + b]], n_out * sizeof(float));
            }
          kad_sync_dim((*ann).n, (*ann).v, ms);
          c = kann_class_error(ann, &b);
          (n_train_err += c, n_train_base += b);
          kann_RMSprop(n_var, lr, 0, 8.999999761581420898437500e-01f, (*ann).g, (*ann).x, r);
          n_proc += ms;
        }
      train_cost /= n_train;
      kann_switch(ann, 0);
      n_proc = 0;
      while (n_proc < n_val)
        {
          int b;
          int c;
          int ms = n_val - n_proc < mini_size ? n_val - n_proc : mini_size;
          for (b = 0; b < ms;  ++b)
            {
              memcpy(&x1[b * n_in], x[n_train + n_proc + b], n_in * sizeof(float));
              memcpy(&y1[b * n_out], y[n_train + n_proc + b], n_out * sizeof(float));
            }
          kad_sync_dim((*ann).n, (*ann).v, ms);
          c = kann_class_error(ann, &b);
          (n_val_err += c, n_val_base += b);
          n_proc += ms;
        }
      if (n_val > 0)
        {
          val_cost /= n_val;
        }
      if (kann_verbose >= 3)
        {
          fprintf(stderr, "epoch: %d; training cost: %g", i + 1, train_cost);
          if (n_train_base)
            {
              fprintf(stderr, " (class error: %.2f%%)", 1.000000000000000000000000e+02f * n_train_err / n_train);
            }
          if (n_val > 0)
            {
              fprintf(stderr, "; validation cost: %g", val_cost);
              if (n_val_base)
                {
                  fprintf(stderr, " (class error: %.2f%%)", 1.000000000000000000000000e+02f * n_val_err / n_val);
                }
            }
          fputc('\n', stderr);
        }
      if (i >= max_drop_streak && n_val > 0)
        {
          if (val_cost < min_val_cost)
            {
              min_set = 1;
              memcpy(min_x, (*ann).x, n_var * sizeof(float));
              memcpy(min_c, (*ann).c, n_const * sizeof(float));
              drop_streak = 0;
              min_val_cost = (float)val_cost;
            }
          else
            {
              if ( ++drop_streak >= max_drop_streak)
                {
                  break;
                }
            }
        }
    }
  if (min_set)
    {
      memcpy((*ann).x, min_x, n_var * sizeof(float));
      memcpy((*ann).c, min_c, n_const * sizeof(float));
    }
  free(min_c);
  free(min_x);
  free(y1);
  free(x1);
  free(y);
  free(x);
  free(shuf);
  free(r);
  return i;
}
float kann_cost_fnn1(kann_t *ann, int n, float **x, float **y)
{
  int n_in;
  int n_out;
  float *x1;
  float *y1;
  int n_proc = 0;
  int mini_size = 64 < n ? 64 : n;
  double cost = 0.00000000000000000000000000000000000000000000000000000e+00;
  n_in = kann_feed_dim(ann, 1, 0);
  n_out = kann_feed_dim(ann, 4, 0);
  if ((n <= 0 || n_in < 0) || n_out < 0)
    {
      return 0.00000000000000000000000000000000000000000000000000000e+00;
    }
  x1 = (float *)malloc(n_in * mini_size * sizeof(float));
  y1 = (float *)malloc(n_out * mini_size * sizeof(float));
  kann_feed_bind(ann, 1, 0, &x1);
  kann_feed_bind(ann, 4, 0, &y1);
  kann_switch(ann, 0);
  while (n_proc < n)
    {
      int b;
      int ms = n - n_proc < mini_size ? n - n_proc : mini_size;
      for (b = 0; b < ms;  ++b)
        {
          memcpy(&x1[b * n_in], x[n_proc + b], n_in * sizeof(float));
          memcpy(&y1[b * n_out], y[n_proc + b], n_out * sizeof(float));
        }
      kad_sync_dim((*ann).n, (*ann).v, ms);
      n_proc += ms;
    }
  free(y1);
  free(x1);
  return (float)(cost / n);
}
const float *kann_apply1(kann_t *a, float *x)
{
  int i_out;
  i_out = kann_find(a, 2, 0);
  if (i_out < 0)
    {
      return 0;
    }
  kad_sync_dim((*a).n, (*a).v, 1);
  kann_feed_bind(a, 1, 0, &x);
  return (*(*a).v[i_out]).x;
}
static void smp_ol_kann_cost_0_unpacked(kann_t *a, int cost_label, int cal_grad, int i, int layers, int ulen, int *start, int *end, int parallel)
{
  {
    ompss_kann_cost_core(a, cost_label, cal_grad, i, layers, ulen, start, end, parallel);
  }
}
static void smp_ol_kann_cost_0(struct nanos_args_0_t *const args)
{
  {
    smp_ol_kann_cost_0_unpacked((*args).a, (*args).cost_label, (*args).cal_grad, (*args).i, (*args).layers, (*args).ulen, (*args).start, (*args).end, (*args).parallel);
  }
}
