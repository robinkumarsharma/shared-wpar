CC=			gcc
MCC=        mcc --ompss -k #--instrument #smpcc --ompss --instrument
CFLAGS=		-g -Wall -Wextra -Wc++-compat -O3 -lm
CPPFLAGS=	-DHAVE_PTHREAD -DHAVE_CBLAS 
INCLUDES=	-I.  -DMKL_ILP64 -m64 -I${MKLROOT}/include
#INCLUDES=	-I.
EXE=		
EXE_MCC= 	examples/d_textgen_parallel
LIBS=		-lz -L${MKLROOT}/lib/intel64 -Wl,--no-as-needed -lmkl_intel_ilp64 -lmkl_sequential -lmkl_core -lpthread -lm -ldl 

ifdef CBLAS
	CPPFLAGS+=-DHAVE_CBLAS
	INCLUDES+=-I$(CBLAS)/include
	LIBS=-fopenmp -pthread -L$(CBLAS)/lib -lopenblas -lz -lm
endif

.SUFFIXES:.c .o
.PHONY:all clean depend

.c.o:
		$(CC) -c $(CFLAGS) $(INCLUDES) $(CPPFLAGS) $< -o $@

all:kautodiff_parallel.o kann_parallel.o kann_extra/kann_data.o $(EXE) $(EXE_MCC)

kautodiff_parallel.o:kautodiff_parallel.c
		$(MCC) -c $(CFLAGS) $(CFLAGS_LIB) $(INCLUDES) $(CPPFLAGS) -o $@ $<

kann_parallel.o:kann_parallel.c
		$(MCC) -c $(CFLAGS) $(CFLAGS_LIB) $(INCLUDES) $(CPPFLAGS) -o $@ $<

kann_extra/kann_data.o:kann_extra/kann_data.c
		$(CC) -c $(CFLAGS) -DHAVE_ZLIB $< -o $@

examples/d_textgen_parallel:examples/d_textgen_parallel.o kautodiff_parallel.o kann_parallel.o kann_extra/kann_data.o
		$(MCC) $(CFLAGS) -o $@ $^ $(LIBS)

clean:
		rm -fr *.o */*.o a.out */a.out *.a *.dSYM */*.dSYM $(EXE) $(EXE_MCC)

depend:
		(LC_ALL=C; export LC_ALL; makedepend -Y -- $(CFLAGS) $(DFLAGS) -- *.c kann_extra/*.c examples/*.c)

# DO NOT DELETE

kann_parallel.o: kann_parallel.h kautodiff_parallel.h
kautodiff_parallel.o: kautodiff_parallel.h
kann_extra/kann_data.o: kann_extra/kseq.h kann_extra/kann_data.h
examples/d_textgen_parallel.o: kann_parallel.h kautodiff_parallel.h kann_extra/kann_data.h
